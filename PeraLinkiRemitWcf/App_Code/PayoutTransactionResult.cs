﻿ using System;
using System.Xml;
using System.IO;

/// <summary>
/// Represents an instance of a PayoutTransactionResult class
/// </summary> 
public class PayoutTransactionResult
{
    #region Constructors

    public PayoutTransactionResult()
    { }

    #endregion

    #region Fields

    private PayoutTransactionResultCode _resultCode = PayoutTransactionResultCode.UnrecognizedResponse;
    private string _messageToClient;
    private string _transactionNumber;
    private DateTime _payoutDate;

    #endregion

    #region Properties

    public PayoutTransactionResultCode ResultCode
    {
        get { return _resultCode; }
        set { _resultCode = value; }
    }

    public string MessageToClient
    {
        get { return _messageToClient; }
        set { _messageToClient = value; }
    }

    public string TransactionNumber
    {
        get { return _transactionNumber; }
        set { _transactionNumber = value; }
    }

    public DateTime PayoutDate
    {
        get { return _payoutDate; }
        set { _payoutDate = value; }
    }

    #endregion

    internal static PayoutTransactionResult GetPayoutResult(XmlNode xmlnode)
    {
        PayoutTransactionResult payoutResult = new PayoutTransactionResult();
        iRemitPayoutResult xmlResult = ReadPayoutXML(xmlnode);


        if (xmlResult.ResponseCode.Equals("0"))
        {
            payoutResult.ResultCode = PayoutTransactionResultCode.Successful;
            payoutResult.MessageToClient = "Payout transaction successful.";
            payoutResult.PayoutDate = DateTime.Now;
        }
        else
        {
            string message = "An error has occurred while paying out this transaction. Please contact ICT Support Desk.";

            switch (xmlResult.ResponseCode)
            {
                case "4":
                    message = xmlResult.ResponseText;
                    break;
                case "6":
                    message = xmlResult.ResponseText;
                    break;     
                default:
                    break;

            }
            string errorLogMessage = string.Format("RemittancePartnerPayout_GetResult\nStatus: {0}"
                                                    , xmlResult.ResponseCode);
            Utils.WriteToEventLog(errorLogMessage, System.Diagnostics.EventLogEntryType.Error);
            payoutResult._resultCode = PayoutTransactionResultCode.Unsuccessful;
            payoutResult._messageToClient = message;
        }

        payoutResult._messageToClient = string.Format("[{0}:{1}] {2}", RemittancePartnerConfiguration.ApplicationCode, xmlResult.ResponseCode, payoutResult._messageToClient);
        return payoutResult;
    }
    internal static iRemitPayoutResult ReadPayoutXML(XmlNode node)
    {
        iRemitPayoutResult iremitPayoutResult = new iRemitPayoutResult();


        string xmlresult = "<Result>" + node["Result"].InnerXml.ToString() + "</Result>";
        //string result = @"<?xml version=""1.0"" encoding=""utf-16""?>" + xmlresult;

        using (StringReader stringReader = new StringReader(xmlresult))
        using (XmlTextReader reader = new XmlTextReader(stringReader))
        {
            while (reader.Read())
            {
                if (reader.IsStartElement())
                    switch (reader.Name)
                    {
                        case "Status":
                            iremitPayoutResult.ResponseCode = reader.ReadString();
                            break;
                        case "Desc":
                            iremitPayoutResult.ResponseText = reader.ReadString();
                            break;            
                    }
            }
        }
        return iremitPayoutResult;
    }
}