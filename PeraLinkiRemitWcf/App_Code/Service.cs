﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Diagnostics;
using System.Web.Services.Protocols;
using System.Net;


// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service" in code, svc and config file together.
public class Service : IService
{
	public string GetData(int value)
	{
		return string.Format("You entered: {0}", value);
	}

	public CompositeType GetDataUsingDataContract(CompositeType composite)
	{
		if (composite == null)
		{
			throw new ArgumentNullException("composite");
		}
		if (composite.BoolValue)
		{
			composite.StringValue += "Suffix";
		}
		return composite;
	}
    public InquireTransactionResult ChangePin(string newPin)
    {
        return CreateNewPin(newPin);
    }


    public InquireTransactionResult PeraLinkPartnerLookup(InquireTransactionRequest inquireTransactionRequest)
    {

        try
        {
            InquireTransactionResult lookupTransactionResult = inquireTransactionRequest.InquireTransaction(inquireTransactionRequest.ReferenceNumber);
            return lookupTransactionResult;
        }
        catch (Exception error)
        {
            string concatenatedErrorMessage = error.InnerException != null ? string.Format("{0}\n{1}", error.Message, error.InnerException.Message) : error.Message;

            Utils.WriteToEventLog(string.Format("PeraLinkPartnerLookup\n{0}", concatenatedErrorMessage), EventLogEntryType.Error);

            InquireTransactionResult errorResponse = new InquireTransactionResult();
            errorResponse.ResultCode = LookupTransactionResultCode.ServerError;

            if (error is RemittanceException)
            {
                errorResponse.MessageToClient = error.Message;
            }
            else if (error is SoapException)
            {
                errorResponse.MessageToClient = "An error in the partner's web service has occurred while looking up the transaction.";
            }
            else if (error is WebException)
            {
                errorResponse.MessageToClient = "An error in the connection to the partner's web service has occurred while looking up the transaction. Please try again later.";
            }
            else
            {
                errorResponse.MessageToClient = "An error has occurred while retrieving the transaction details from the partner. Please contact ICT Support Desk.";
            }

            return errorResponse;
        }
    }
 public PayoutTransactionResult PeraLinkpartnerPayout(PayoutTransactionRequest payoutTransactionRequest)
    {
        try
        {
            PayoutTransactionResult payoutTransactionResult = payoutTransactionRequest.PayoutTransaction(payoutTransactionRequest);
            return payoutTransactionResult;
        }
        catch (Exception error)
        {
            string concatenatedErrorMessage = error.InnerException != null ? string.Format("{0}\n{1}", error.Message, error.InnerException.Message) : error.Message;

            Utils.WriteToEventLog(
                string.Format("PeraLinkpartnerPayout\n{0}", concatenatedErrorMessage),
                EventLogEntryType.Error);

            //Utils.WriteToEventLog(string.Format("{0}\n{1}", System.Reflection.MethodInfo.GetCurrentMethod().Name, concatenatedErrorMessage), EventLogEntryType.Error);

            PayoutTransactionResult errorResponse = new PayoutTransactionResult();
            errorResponse.ResultCode = PayoutTransactionResultCode.ServerError;

            if (error is RemittanceException)
            {
                errorResponse.MessageToClient = error.Message;
            }
            else if (error is SoapException)
            {
                errorResponse.MessageToClient = "An error in the partner's web service has occurred while paying out the transaction.";
            }
            else if (error is WebException)
            {
                errorResponse.MessageToClient = "An error in the connection to the partner's web service has occurred while paying out the transaction. Please try again later.";
            }
            else
            {
                errorResponse.MessageToClient = "An error has occurred while paying out the transaction. Please contact ICT Support Desk.";
            }

            return errorResponse;
        }
    
    }

 public InquireTagAsCompletedResult PeraLinkPartnerChecking(InquireTagAsCompletedRequest inquireTagAsCompletedRequest)
 {

     //try
     //{
     //    InquireTagAsCompletedResult inquireTagAsCompletedResult = inquireTagAsCompletedRequest.InquireTransaction(inquireTagAsCompletedRequest.ReferenceNumber);
     //    return inquireTagAsCompletedResult;
     //}
     //catch (Exception error)
     //{
     //    string concatenatedErrorMessage = error.InnerException != null ? string.Format("{0}\n{1}", error.Message, error.InnerException.Message) : error.Message;

     //    Utils.WriteToEventLog(string.Format("RemittancePartnerLookup_01\n{0}", concatenatedErrorMessage), EventLogEntryType.Error);

     //    InquireTagAsCompletedResult errorResponse = new InquireTagAsCompletedResult();
     //    errorResponse.ResultCode = LookupTransactionResultCode.ServerError;

     //    if (error is RemittanceException)
     //    {
     //        errorResponse.MessageToClient = error.Message;
     //    }
     //    else if (error is SoapException)
     //    {
     //        errorResponse.MessageToClient = "An error in the partner's web service has occurred while looking up the transaction.";
     //    }
     //    else if (error is WebException)
     //    {
     //        errorResponse.MessageToClient = "An error in the connection to the partner's web service has occurred while looking up the transaction. Please try again later.";
     //    }
     //    else
     //    {
     //        errorResponse.MessageToClient = "An error has occurred while retrieving the transaction details from the partner. Please contact ICT Support Desk.";
     //    }

     //    return errorResponse;
     //}
     return null;
 }

    #region Methods


    private InquireTransactionResult CreateNewPin(string newPin)
    {
        try
        {
            InquireTransactionRequest request = new InquireTransactionRequest();
            InquireTransactionResult result = request.ChangeHlhuillierPin(newPin);
            return result;
        }
        catch (Exception error)
        {
            Utils.WriteToEventLog(string.Format("CreateNewPin\n{0}", error.Message), EventLogEntryType.Error);
            InquireTransactionResult errorResult = new InquireTransactionResult();
            errorResult.ResultCode = LookupTransactionResultCode.Unsuccessful;
            errorResult.MessageToClient = "Server error";
            return errorResult;
        }

    }

    #endregion
}
