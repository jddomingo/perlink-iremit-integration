﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for RIALookupResult
/// </summary>
public class iRemitLookupResult
{
    #region Constructors
    public iRemitLookupResult()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion

    #region Fields
    private string _transferID;
    private string _orderFound;
    private string _pin;
    private string _orderNo;
    private string _seqIDra;
    private string _orderDate;
    private string _customerFirstName;
    private string _customerLastName;
    private string _customerAddress;
    private string _customerCity;
    private string _customerState;
    private string _customerCountry;
    private string _customerZipCode;
    private string _customerTelNo;

    private string _beneFirstName;
    private string _beneLastName;
    private string _beneAddress;
    private string _beneCity;
    private string _beneState;
    private string _beneCountry;
    private string _beneZipCode;
    private string _beneTelNo;
    private string _currency;
    private string _amount;
    private string _payoutDate;
    private string _responseCode;
    private string _responseText;
    #endregion

    #region Properties
    public string TransferID
    {
        get { return _transferID; }
        set { _transferID = value; }
    }
    public string OrderFound
    {
        get { return _orderFound; }
        set { _orderFound = value; }
    }
    public string Pin
    {
        get { return _pin; }
        set { _pin = value; }
    }
    public string OrderNo
    {
        get { return _orderNo; }
        set { _orderNo = value; }
    }
    public string SeqIDra
    {
        get { return _seqIDra; }
        set { _seqIDra = value; }
    }
    public string OrderDate
    {
        get { return _orderDate; }
        set { _orderDate = value; }
    }
    public string CustomerFirstName
    {
        get { return _customerFirstName; }
        set { _customerFirstName = value; }
    }
    public string CustomerLastName
    {
        get { return _customerLastName; }
        set { _customerLastName = value; }
    }
    public string CustomerAddress
    {
        get { return _customerAddress; }
        set { _customerAddress = value; }
    }
    public string CustomerCity
    {
        get { return _customerCity; }
        set { _customerCity = value; }
    }
    public string CustomerState
    {
        get { return _customerState; }
        set { _customerState = value; }
    }
    public string CustomerCountry
    {
        get { return _customerCountry; }
        set { _customerCountry = value; }
    }
    public string CustomerZipCode
    {
        get { return _customerZipCode; }
        set { _customerZipCode = value; }
    }
    public string CustomerTelNo
    {
        get { return _customerTelNo; }
        set { _customerTelNo = value; }
    }
    public string BeneFirstName
    {
        get { return _beneFirstName; }
        set { _beneFirstName = value; }
    }
    public string BeneLastName
    {
        get { return _beneLastName; }
        set { _beneLastName = value; }
    }
    public string BeneAddress
    {
        get { return _beneAddress; }
        set { _beneAddress = value; }
    }
    public string BeneCity
    {
        get { return _beneCity; }
        set { _beneCity = value; }
    }
    public string BeneState
    {
        get { return _beneState; }
        set { _beneState = value; }
    }
    public string BeneCountry
    {
        get { return _beneCountry; }
        set { _beneCountry = value; }
    }
    public string BeneZipCode
    {
        get { return _beneZipCode; }
        set { _beneZipCode = value; }
    }
    public string BeneTelNo
    {
        get { return _beneTelNo; }
        set { _beneTelNo = value; }
    }
    public string Currency
    {
        get { return _currency; }
        set { _currency = value; }
    }
    public string Amount
    {
        get { return _amount; }
        set { _amount = value; }
    }
    public string PayoutDate
    {
        get { return _payoutDate; }
        set { _payoutDate = value; }
    }
    public string ResponseCode
    {
        get { return _responseCode; }
        set { _responseCode = value; }
    }
    public string ResponseText
    {
        get { return _responseText; }
        set { _responseText = value; }
    }
    #endregion
}
