﻿using System;
using System.Xml;
using System.Data.SqlClient;
using System.Data;



using System.IO;

/// <summary>
/// Represents an instance of a LookupTransactionResult class
/// </summary>
public class InquireTransactionResult
{
    #region Constructors

    public InquireTransactionResult()
    { }

    #endregion

    #region Fields
    private LookupTransactionResultCode _resultCode = LookupTransactionResultCode.UnrecognizedResponse;
    private string _messageToClient;
    private string _transactionNumber;
    private TransactionStatus _transactionStatus = TransactionStatus.UnrecognizedStatus;

    private string _senderLastName;
    private string _senderFirstName;
    private string _senderFullName;
    private string _senderCountry;

    private string _beneficiaryLastName;
    private string _beneficiaryFirstName;
    private string _beneficiaryFullName;

    private string _payoutCurrency;
    private decimal _payoutAmount;
    private decimal _payoutAmountWithServiceCharge;

    private string _sessionID;
    private string _payoutCountry;
    private string _multiCurrencyPayoutCode;
    private DateTime _transactionDate;

    private bool _isCurrencySpecified;

    private string _beneficiaryPhoneNumber;

    private string _originCurrency;

    private decimal _currencyConversionRate;

    private string _partnerSessionID;

    private string _signature;

    private string _iRemitAdditionalID;



    #endregion

    #region Properties
    public LookupTransactionResultCode ResultCode
    {
        get { return _resultCode; }
        set { _resultCode = value; }
    }
    public string MessageToClient
    {
        get { return _messageToClient; }
        set { _messageToClient = value; }
    }

    public string TransactionNumber
    {
        get { return _transactionNumber; }
        set { _transactionNumber = value; }
    }
    public TransactionStatus TransactionStatus
    {
        get { return _transactionStatus; }
        set { _transactionStatus = value; }
    }
    public DateTime TransactionDate
    {
        get { return _transactionDate; }
        set { _transactionDate = value; }
    }
    public decimal PayoutAmount
    {
        get { return _payoutAmount; }
        set { _payoutAmount = value; }
    }
    public decimal PayoutAmountWithServiceCharge
    {
        get { return _payoutAmountWithServiceCharge; }
        set { _payoutAmountWithServiceCharge = value; }
    }
    public string PayoutCurrency
    {
        get { return _payoutCurrency; }
        set { _payoutCurrency = value; }
    }
    public string PayoutCountry
    {
        get { return _payoutCountry; }
        set { _payoutCountry = value; }
    }
    public string MultiCurrencyPayoutCode
    {
        get { return _multiCurrencyPayoutCode; }
        set { _multiCurrencyPayoutCode = value; }
    }
    public string SessionID
    {
        get { return _sessionID; }
        set { _sessionID = value; }
    }

    public string SenderLastName
    {
        get { return _senderLastName; }
        set { _senderLastName = value; }
    }
    public string SenderFirstName
    {
        get { return _senderFirstName; }
        set { _senderFirstName = value; }
    }
    public string SenderFullName
    {
        get { return _senderFullName; }
        set { _senderFullName = value; }
    }
    public string SenderCountry
    {
        get { return _senderCountry; }
        set { _senderCountry = value; }
    }

    public string BeneficiaryLastName
    {
        get { return _beneficiaryLastName; }
        set { _beneficiaryLastName = value; }
    }
    public string BeneficiaryFirstName
    {
        get { return _beneficiaryFirstName; }
        set { _beneficiaryFirstName = value; }
    }
    public string BeneficiaryFullName
    {
        get { return _beneficiaryFullName; }
        set { _beneficiaryFullName = value; }
    }
    public bool IsCurrencySpecified
    {
        get { return _isCurrencySpecified; }
        set { _isCurrencySpecified = value; }
    }

    public string BeneficiaryPhoneNumber
    {
        get { return _beneficiaryPhoneNumber; }
        set { _beneficiaryPhoneNumber = value; }
    }


    public string OriginCurrency
    {
        get { return _originCurrency; }
        set { _originCurrency = value; }
    }

    public decimal CurrencyConversionRate
    {
        get { return _currencyConversionRate; }
        set { _currencyConversionRate = value; }
    }


    public string PartnerSessionID
    {
        get { return _partnerSessionID; }
        set { _partnerSessionID = value; }
    }
    public string Signature
    {
        get { return _signature; }
        set { _signature = value; }
    }

    public string IRemitAdditionalID
    {
        get { return _iRemitAdditionalID; }
        set { _iRemitAdditionalID = value; }
    }
    #endregion

    private static decimal GetServiceCharge(decimal payoutAmount)
    {
        string sqlCommandText = "SELECT fld_ServiceChargeAmount FROM tbl_PartnerServiceCharge WHERE fld_PartnerCode = @parnertCode AND (fld_PrincipalFrom <= @principalAmount AND fld_PrincipalTo >= @principalAmount)";
        using (SqlConnection sqlConnection = new SqlConnection(RemittancePartnerConfiguration.ConnectionStringRemittanceDatabase))
        {
            sqlConnection.Open();
            using (SqlCommand sqlCommand = new SqlCommand())
            {
                sqlCommand.CommandType = CommandType.Text;
                sqlCommand.CommandText = sqlCommandText;
                sqlCommand.Connection = sqlConnection;

                sqlCommand.Parameters.AddWithValue("@parnertCode", RemittancePartnerConfiguration.ServiceCenterCode);
                sqlCommand.Parameters.AddWithValue("@principalAmount", payoutAmount);

                object result = sqlCommand.ExecuteScalar();
                return Convert.ToDecimal(result);
            }
        }
    }

    internal static InquireTransactionResult GetResult(XmlNode node, string cebuanaSession, string signature)
    {
        InquireTransactionResult lookupResult = new InquireTransactionResult();
        iRemitLookupResult xmlResult = ReadXml(node);

        //string resultSuccessful = string.Empty;

        if (xmlResult.ResponseCode.Equals("0"))
        {
            lookupResult.PayoutCurrency = xmlResult.Currency;

            string multiCurrencyPayoutCode = RemittancePartnerConfiguration.GetMultiCurrencyPayoutCode(lookupResult.PayoutCurrency);
            lookupResult.TransactionNumber = xmlResult.OrderNo;
            lookupResult.ResultCode = LookupTransactionResultCode.Successful;
            lookupResult.MessageToClient = "Lookup transaction successful.";
            lookupResult.TransactionStatus = TransactionStatus.ForPayout;
            lookupResult.TransactionDate = DateTime.Now;
            lookupResult.PayoutCountry = "PH";
            lookupResult.SenderCountry = "PH";
            lookupResult.MultiCurrencyPayoutCode = multiCurrencyPayoutCode;
            lookupResult.SenderFullName = xmlResult.CustomerLastName;
            lookupResult.BeneficiaryFullName = xmlResult.BeneLastName;
            lookupResult.PayoutAmount = Convert.ToDecimal(xmlResult.Amount);
            lookupResult.SessionID = cebuanaSession;
            lookupResult.PartnerSessionID = xmlResult.SeqIDra;
            lookupResult.Signature = signature;
            lookupResult.BeneficiaryPhoneNumber = xmlResult.BeneTelNo;
            lookupResult.CurrencyConversionRate = 1M;
            lookupResult.IRemitAdditionalID = RemittancePartnerConfiguration.IRemitAdditionalID;
            //7-3-2014 max payout limit
            if (lookupResult.PayoutCurrency.ToUpper().Equals("PHP"))
            {
                if (lookupResult.PayoutAmount > RemittancePartnerConfiguration.iREMITPHPMaxPayoutLimit)
                {
                    lookupResult.ResultCode = LookupTransactionResultCode.Unsuccessful;
                    lookupResult.MessageToClient = "Transaction amount exceeds transaction limit. Please contact CLSC.";
                }
            }
            if (lookupResult.PayoutCurrency.ToUpper().Equals("USD"))
            {
                if (lookupResult.PayoutAmount > RemittancePartnerConfiguration.iREMITUSDMaxPayoutLimit)
                {
                    lookupResult.ResultCode = LookupTransactionResultCode.Unsuccessful;
                    lookupResult.MessageToClient = "Transaction amount exceeds transaction limit. Please contact CLSC.";
                }
            }

            lookupResult.OriginCurrency = xmlResult.Currency;
            //lookupResult.CurrencyConversionRate = tempResult.CurrencyConversionRate;

            if (lookupResult.PayoutCurrency.ToUpper() != "PHP"
               && string.IsNullOrEmpty(multiCurrencyPayoutCode) == true
               )
            {
                lookupResult.ResultCode = LookupTransactionResultCode.Unsuccessful;
                lookupResult.MessageToClient = "This is a non-PHP transaction. Processing of this transaction is not yet allowed for this partner.";
            }

            #region parsing sender name
            try
            {
                lookupResult.SenderLastName = lookupResult.SenderFullName.Substring(0, lookupResult.SenderFullName.IndexOf(',')).Trim();
                lookupResult.SenderFirstName = lookupResult.SenderFullName.Substring((lookupResult.SenderFullName.IndexOf(',') + 2), (lookupResult.SenderFullName.Length - (lookupResult.SenderFullName.IndexOf(',') + 2))).Trim();
            }
            catch
            {
                lookupResult.SenderFirstName = string.Empty;
                lookupResult.SenderLastName = string.Empty;
            }
            #endregion

            #region parsing bene name
            try
            {

                lookupResult.BeneficiaryLastName = lookupResult.BeneficiaryFullName.Substring(0, lookupResult.BeneficiaryFullName.IndexOf(',')).Trim();
                lookupResult.BeneficiaryFirstName = lookupResult.BeneficiaryFullName.Substring((lookupResult.BeneficiaryFullName.IndexOf(',') + 2), (lookupResult.BeneficiaryFullName.Length - (lookupResult.BeneficiaryFullName.IndexOf(',') + 2))).Trim();
            }
            catch
            {
                lookupResult.BeneficiaryFirstName = string.Empty;
                lookupResult.BeneficiaryLastName = string.Empty;
            }
            #endregion

        }
        else
        {
            string message = "An error has occurred while looking up this transaction. Please contact ICT Support Desk.";
            switch (xmlResult.ResponseCode)
            {
                case "1":
                    message = "Transaction is already PAID";
                    break;
                case "2":
                    message = "Transaction does not exist";
                    break;
                case "3":
                    message = "Transaction is still in process";
                    break;
                case "4":
                    message = "Incorrect signature";
                    break;
                case "5":
                    message = "Cannot be processed due to errors";
                    break;
                default:
                    break;


            }

            string errorLogMessage = string.Format("RemittancePartnerLookup_GetResult\nStatus: {0}"
                                                    , xmlResult.ResponseCode);
            Utils.WriteToEventLog(errorLogMessage, System.Diagnostics.EventLogEntryType.Error);
            lookupResult.ResultCode = LookupTransactionResultCode.Unsuccessful;
            lookupResult.MessageToClient = message;
        }
        lookupResult.MessageToClient = string.Format("[{0}:{1}] {2}", RemittancePartnerConfiguration.ServiceCenterCode, xmlResult.ResponseCode.ToString(), lookupResult.MessageToClient);
        return lookupResult;
    }

    internal static InquireTransactionResult GetResult(XmlNode node, string newPin)
    {
        InquireTransactionResult lookupResult = new InquireTransactionResult();
        iRemitLookupResult xmlResult = ReadXML2(node);
        lookupResult.TransactionDate = DateTime.Now;
        lookupResult.TransactionNumber = newPin;


        if (xmlResult.ResponseCode.Equals("0"))
        {

            lookupResult.ResultCode = LookupTransactionResultCode.Successful;
            lookupResult.MessageToClient = "Successfully changed the Pin";

        }
        else
        {
            string message = "An error has occurred while looking up this transaction. Please contact ICT Support Desk.";
            switch (xmlResult.ResponseCode)
            {
                case "1":
                    message = "Incorrect Username or Password";
                    break;
                default:
                    break;
            }

            string errorLogMessage = string.Format("ChangePin_GetResult\nStatus: {0}"
                                                    , xmlResult.ResponseCode);
            Utils.WriteToEventLog(errorLogMessage, System.Diagnostics.EventLogEntryType.Error);
            lookupResult.ResultCode = LookupTransactionResultCode.Unsuccessful;
            lookupResult.MessageToClient = message;
        }
        lookupResult.MessageToClient = string.Format("[{0}:{1}] {2}", RemittancePartnerConfiguration.ServiceCenterCode, xmlResult.ResponseCode.ToString(), lookupResult.MessageToClient);
        return lookupResult;
    }
    internal static iRemitLookupResult ReadXML2(XmlNode node)
    {
        iRemitLookupResult iremitLookupResult = new iRemitLookupResult();

        string xmlresult = "<Result>" + node["Result"].InnerXml.ToString() + "</Result>";
        //string result = @"<?xml version=""1.0"" encoding=""utf-16""?>" + xmlresult;
        //iremitLookupResult.TransferID  = 
        using (StringReader stringReader = new StringReader(xmlresult))
        using (XmlTextReader reader = new XmlTextReader(stringReader))
        {
            while (reader.Read())
            {
                if (reader.IsStartElement())
                    switch (reader.Name)
                    {
                        case "Status":
                            iremitLookupResult.ResponseCode = reader.ReadString();
                            if (!iremitLookupResult.ResponseCode.Equals("0"))
                            {
                                return iremitLookupResult;
                            }
                            break;
                    }
            }
        }

        return iremitLookupResult;
    }

    internal static iRemitLookupResult ReadXml(XmlNode node)
    {
        iRemitLookupResult iremitLookupResult = new iRemitLookupResult();

        string xmlresult = "<Result>" + node["Result"].InnerXml.ToString() + "</Result>";
        //string result = @"<?xml version=""1.0"" encoding=""utf-16""?>" + xmlresult;

        using (StringReader stringReader = new StringReader(xmlresult))
        using (XmlTextReader reader = new XmlTextReader(stringReader))
        {
            while (reader.Read())
            {
                if (reader.IsStartElement())
                    switch (reader.Name)
                    {
                        case "Status":
                            iremitLookupResult.ResponseCode = reader.ReadString();
                            if (!iremitLookupResult.ResponseCode.Equals("0"))
                            {
                                return iremitLookupResult;
                            }
                            break;
                        case "Desc":
                            iremitLookupResult.ResponseText = reader.ReadString();
                            break;
                        case "Session":
                            iremitLookupResult.SeqIDra = reader.ReadString();
                            break;
                        case "RefNo":
                            iremitLookupResult.OrderNo = reader.ReadString();
                            break;
                        case "Currency":
                            iremitLookupResult.Currency = reader.ReadString();
                            break;
                        case "Amt":
                            iremitLookupResult.Amount = reader.ReadString();
                            break;
                        case "BeneName":
                            iremitLookupResult.BeneLastName = reader.ReadString();
                            break;
                        case "SenderName":
                            iremitLookupResult.CustomerLastName = reader.ReadString();
                            break;
                        case "Address":
                            iremitLookupResult.BeneAddress = reader.ReadString();
                            break;
                        case "TelNo":
                            iremitLookupResult.BeneTelNo = reader.ReadString();
                            break;
                    }
            }
        }



        //iremitLookupResult.ResponseCode =
        //iremitLookupResult.ResponseText = xmlElex["Desc"].InnerXml.ToString();
        //iremitLookupResult.TransferID = xmlElex["Session"].InnerXml.ToString();
        //iremitLookupResult.Amount = xmlElex["Amt"].InnerXml.ToString();

        //try
        //{
        //    iremitLookupResult.BeneLastName = xmlElex["BeneName"].InnerText.ToString().Substring(0, xmlElex["BeneName"].InnerText.ToString().IndexOf(',') + 1);
        //    iremitLookupResult.BeneFirstName = xmlElex["BeneName"].InnerText.ToString().Substring(xmlElex["BeneName"].InnerText.ToString().IndexOf(',') + 1, xmlElex["BeneName"].InnerText.ToString().Length - xmlElex["BeneName"].InnerText.ToString().IndexOf(',') + 1);
        //}
        //catch
        //{
        //}
        return iremitLookupResult;

    }
}

