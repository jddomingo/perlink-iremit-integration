﻿//#define TEST

using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Diagnostics;
using System.Configuration;


using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Xml;
/// <summary>
/// Represents an instance of a PayoutTransactionRequest class
/// </summary>
public class PayoutTransactionRequest
{
    #region Enums

    private enum TransactionStatuses
    {
        Outstanding = 19,
        PaidOut = 20,
        PayoutPending = 36
    }

    #endregion

    #region Constructors

    public PayoutTransactionRequest()
    {
    }

    #endregion

    #region Fields

    private string _transactionNumber;
    private decimal _payoutAmount;
    private decimal _payoutAmountWithServiceCharge;



    private string _sendingCurrency;
    private string _payoutCurrency;
    private decimal _currencyConversionRate;
    private string _payoutCountry;
    private string _senderLastName;
    private string _senderFirstName;
    private string _senderFullName;


    private string _receiverCustomerNumber;
    private string _receiverFullName;

    private string _receiverLastName;
    private string _receiverFirstName;
    private string _receiverIDCode;
    private string _receiverIDType;
    private string _receiverIDDetails;
    private DateTime _receiverIDIssuedDate = new DateTime(1900, 1, 1);
    private DateTime _receiverIDExpiryDate = new DateTime(1900, 1, 1);


    private string _receiverCity;
    private string _receiverCountry;

    private string _senderCountry;
    private string _senderState;
    private string _senderEmail;
    private string _senderMobileNumber;


    private string _sessionID;

    private string _beneficiaryPhoneNumber;
    private string _partnerSessionID;
    private string _signature;

    #endregion

    #region Properties
    public DateTime ReceiverIDIssuedDate
    {
        get { return _receiverIDIssuedDate; }
        set { _receiverIDIssuedDate = value; }
    }
    public DateTime ReceiverIDExpiryDate
    {
        get { return _receiverIDExpiryDate; }
        set { _receiverIDExpiryDate = value; }
    }



    public string TransactionNumber
    {
        get { return _transactionNumber; }
        set { _transactionNumber = value; }
    }

    public decimal PayoutAmount
    {
        get { return _payoutAmount; }
        set { _payoutAmount = value; }
    }
    public decimal PayoutAmountWithServiceCharge
    {
        get { return _payoutAmountWithServiceCharge; }
        set { _payoutAmountWithServiceCharge = value; }
    }

    public string SendingCurrency
    {
        get { return _sendingCurrency; }
        set { _sendingCurrency = value; }
    }

    public string PayoutCurrency
    {
        get { return _payoutCurrency; }
        set { _payoutCurrency = value; }
    }

    public decimal CurrencyConversionRate
    {
        get { return _currencyConversionRate; }
        set { _currencyConversionRate = value; }
    }

    public string PayoutCountry
    {
        get { return _payoutCountry; }
        set { _payoutCountry = value; }
    }
    public string SenderFullName
    {
        get { return _senderFullName; }
        set { _senderFullName = value; }
    }

    public string SenderLastName
    {
        get { return _senderLastName; }
        set { _senderLastName = value; }
    }

    public string SenderFirstName
    {
        get { return _senderFirstName; }
        set { _senderFirstName = value; }
    }

    public string ReceiverCustomerNumber
    {
        get { return _receiverCustomerNumber; }
        set { _receiverCustomerNumber = value; }
    }
    public string ReceiverFullName
    {
        get { return _receiverFullName; }
        set { _receiverFullName = value; }
    }

    public string ReceiverLastName
    {
        get { return _receiverLastName; }
        set { _receiverLastName = value; }
    }

    public string ReceiverFirstName
    {
        get { return _receiverFirstName; }
        set { _receiverFirstName = value; }
    }

    public string ReceiverIDCode
    {
        get { return _receiverIDCode; }
        set { _receiverIDCode = value; }
    }

    public string ReceiverIDType
    {
        get { return _receiverIDType; }
        set { _receiverIDType = value; }
    }

    public string ReceiverIDNumber
    {
        get { return _receiverIDDetails; }
        set { _receiverIDDetails = value; }
    }

    public string ReceiverCity
    {
        get { return _receiverCity; }
        set { _receiverCity = value; }
    }

    public string ReceiverCountry
    {
        get { return _receiverCountry; }
        set { _receiverCountry = value; }
    }

    public string SenderCountry
    {
        get { return _senderCountry; }
        set { _senderCountry = value; }
    }

    public string SenderState
    {
        get { return _senderState; }
        set { _senderState = value; }
    }

    public string SenderEmail
    {
        get { return _senderEmail; }
        set { _senderEmail = value; }
    }

    public string SenderMobileNumber
    {
        get { return _senderMobileNumber; }
        set { _senderMobileNumber = value; }
    }

    public string SessionID
    {
        get { return _sessionID; }
        set { _sessionID = value; }
    }

    public string BeneficiaryPhoneNumber
    {
        get { return _beneficiaryPhoneNumber; }
        set { _beneficiaryPhoneNumber = value; }
    }


    public string PartnerSessionID
    {
        get { return _partnerSessionID; }
        set { _partnerSessionID = value; }
    }
    public string Signature
    {
        get { return _signature; }
        set { _signature = value; }
    }


    #endregion

    #region Methods

    private string GetTransactionStatusDescription(TransactionStatuses transactionStatus)
    {
        switch (transactionStatus)
        {
            case TransactionStatuses.PaidOut:
                return "TIE UP PAID";
            case TransactionStatuses.PayoutPending:
                return "TIE UP PAYOUTPENDING";
            case TransactionStatuses.Outstanding:
                return "TIE UP OUTSTANDING";
            default:
                return "UNKNOWN STATUS";
        }
    }


    public PayoutTransactionResult PayoutTransaction(PayoutTransactionRequest request)
    {
        long transactionID = InsertTransactionToDatabase(TransactionStatuses.PayoutPending, SessionID, PartnerSessionID);
        PayoutTransactionResult finalPayoutTransactionResult = new PayoutTransactionResult();
        iRemitWcf.Service service = new iRemitWcf.Service();
        XmlNode xmlnode = service.PayOutTransaction(request.TransactionNumber, request.Signature, request.PartnerSessionID, request.SessionID);
        finalPayoutTransactionResult = PayoutTransactionResult.GetPayoutResult(xmlnode);

        if (finalPayoutTransactionResult.ResultCode.Equals(PayoutTransactionResultCode.Successful))
        {
            try
            {
                UpdateTransaction(transactionID, TransactionStatuses.PaidOut, SessionID, PartnerSessionID);
            }
            catch (Exception error)
            {
                Utils.WriteToEventLog(string.Format("UpdateTransaction\n{0}", error.Message), EventLogEntryType.Error);
                throw error;
            }
        }

        return finalPayoutTransactionResult;

    }


    private long InsertTransactionToDatabase(TransactionStatuses transactionStatus, string partnerInternalReferenceNumber, string partnerInternalReferenceNumber2)
    {
        long transactionID = long.MinValue;
        string operationIpAddress = string.Empty;
        OperationContext context = OperationContext.Current;
        MessageProperties messageProperties = context.IncomingMessageProperties;
        RemoteEndpointMessageProperty endpointProperty = messageProperties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
        if (endpointProperty != null)
        {
            operationIpAddress = endpointProperty.Address;
        }
        using (SqlConnection sqlConnection = new SqlConnection())
        {
            sqlConnection.ConnectionString = RemittancePartnerConfiguration.ConnectionStringRemittanceDatabase;
            SqlCommand sqlCommand = new SqlCommand(RemittancePartnerConfiguration.StoredProcedureInsertPayoutTransaction, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.AddWithValue("@PartnerCode", RemittancePartnerConfiguration.ApplicationCode);
            sqlCommand.Parameters.AddWithValue("@TransactionID", transactionID);
            sqlCommand.Parameters["@TransactionID"].Direction = ParameterDirection.Output;                   
            sqlCommand.Parameters.AddWithValue("@ControlNumber", _transactionNumber);
            sqlCommand.Parameters.AddWithValue("@TransactionStatusID", (int)transactionStatus);
            sqlCommand.Parameters.AddWithValue("@TransactionStatusDescription", GetTransactionStatusDescription(transactionStatus));
            sqlCommand.Parameters.AddWithValue("@PayoutAmount", _payoutAmount);
            sqlCommand.Parameters.AddWithValue("@SendingCurrency", _sendingCurrency);
            sqlCommand.Parameters.AddWithValue("@PayoutCurrency", _payoutCurrency);
            sqlCommand.Parameters.Add("@CurrencyConversionRate", SqlDbType.Decimal);
            sqlCommand.Parameters["@CurrencyConversionRate"].Precision = 18;
            sqlCommand.Parameters["@CurrencyConversionRate"].Scale = 2;
            sqlCommand.Parameters["@CurrencyConversionRate"].Value = _currencyConversionRate;
            sqlCommand.Parameters.AddWithValue("@PayoutCountry", _payoutCountry);

            sqlCommand.Parameters.AddWithValue("@SerderFullName", _senderFullName ?? string.Empty);
            sqlCommand.Parameters.AddWithValue("@SenderLastName", _senderLastName ?? string.Empty);
            sqlCommand.Parameters.AddWithValue("@SenderFirstName", _senderFirstName ?? string.Empty);

            sqlCommand.Parameters.AddWithValue("@BeneficiaryCustomerNumber", Convert.ToInt64(_receiverCustomerNumber));
            sqlCommand.Parameters.AddWithValue("@BeneficiaryFullName", _receiverFullName);
            sqlCommand.Parameters.AddWithValue("@BeneficiaryLastName", _receiverLastName);
            sqlCommand.Parameters.AddWithValue("@BeneficiaryFirstName", _receiverFirstName);
            sqlCommand.Parameters.AddWithValue("@BeneficiaryIDType", _receiverIDType);
            sqlCommand.Parameters.AddWithValue("@BeneficiaryIDDetails", _receiverIDDetails);

            sqlCommand.Parameters.AddWithValue("@ReceiverBranchUserID", RemittancePartnerConfiguration.ServiceCenterCode);
            sqlCommand.Parameters.AddWithValue("@PayoutBranchCode", RemittancePartnerConfiguration.ServiceCenterCode);


            sqlCommand.Parameters.AddWithValue("@AuditTracker",
                RemittanceAuditTrail.GetAuditTrailString(
                    RemittancePartnerConfiguration.ServiceCenterCode,
                       RemittancePartnerConfiguration.ServiceCenterCode,
                    "1",
                    "1",
                    operationIpAddress,
                      RemittancePartnerConfiguration.ServiceCenterCode)
                );

            sqlCommand.Parameters.AddWithValue("@PartnerInternalReferenceNumber", partnerInternalReferenceNumber);
            sqlCommand.Parameters.AddWithValue("@PartnerInternalReferenceNumber2", partnerInternalReferenceNumber2);

            sqlCommand.Parameters.AddWithValue("@SendingCountry", _senderCountry ?? string.Empty);
            sqlCommand.Parameters.AddWithValue("@SendingState", _senderState ?? string.Empty);
            sqlCommand.Parameters.AddWithValue("@SenderEmail", _senderEmail ?? string.Empty);
            sqlCommand.Parameters.AddWithValue("@SenderMobileNumber", _senderMobileNumber ?? string.Empty);
            sqlCommand.Parameters.AddWithValue("@beneficiaryNumber", BeneficiaryPhoneNumber ?? string.Empty);

            sqlConnection.Open();
            sqlCommand.ExecuteNonQuery();

            transactionID = Convert.ToInt64(sqlCommand.Parameters["@TransactionID"].Value);
            return transactionID;

        }
    }

    //private void UpdateTransaction(long transactionID, TransactionStatuses transactionStatus)
    //{
    //    UpdateTransaction(transactionID, transactionStatus, string.Empty, string.Empty);
    //}

    private void UpdateTransaction(long transactionID, TransactionStatuses transactionStatus, string partnerInternalReferenceNumber, string partnerInternalReferenceNumber2)
    {


        string operationIpAddress = string.Empty;
        OperationContext context = OperationContext.Current;
        MessageProperties messageProperties = context.IncomingMessageProperties;
        RemoteEndpointMessageProperty endpointProperty = messageProperties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
        if (endpointProperty != null)
        {
            operationIpAddress = endpointProperty.Address;
        }

        using (SqlConnection sqlConnection = new SqlConnection())
        {
            sqlConnection.ConnectionString = RemittancePartnerConfiguration.ConnectionStringRemittanceDatabase;
            SqlCommand sqlCommand = new SqlCommand(RemittancePartnerConfiguration.StoredProcedureUpdatePayoutTransaction, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.AddWithValue("@PartnerCode", RemittancePartnerConfiguration.ApplicationCode);
            sqlCommand.Parameters.AddWithValue("@TransactionID", transactionID);
            sqlCommand.Parameters.AddWithValue("@ControlNumber", _transactionNumber);
            sqlCommand.Parameters.AddWithValue("@TransactionStatusID", (int)transactionStatus);
            sqlCommand.Parameters.AddWithValue("@TransactionStatusDescription", GetTransactionStatusDescription(transactionStatus));
            sqlCommand.Parameters.AddWithValue("@PayoutAmount", _payoutAmount);
            sqlCommand.Parameters.AddWithValue("@SendingCurrency", _sendingCurrency);
            sqlCommand.Parameters.AddWithValue("@PayoutCurrency", _payoutCurrency);
            sqlCommand.Parameters.Add("@CurrencyConversionRate", SqlDbType.Decimal);
            sqlCommand.Parameters["@CurrencyConversionRate"].Precision = 18;
            sqlCommand.Parameters["@CurrencyConversionRate"].Scale = 2;
            sqlCommand.Parameters["@CurrencyConversionRate"].Value = _currencyConversionRate;
            sqlCommand.Parameters.AddWithValue("@PayoutCountry", _payoutCountry);

            sqlCommand.Parameters.AddWithValue("@SerderFullName", _senderFullName);
            sqlCommand.Parameters.AddWithValue("@SenderLastName", _senderLastName);
            sqlCommand.Parameters.AddWithValue("@SenderFirstName", _senderFirstName);

            sqlCommand.Parameters.AddWithValue("@BeneficiaryCustomerNumber", Convert.ToInt64(_receiverCustomerNumber));
            sqlCommand.Parameters.AddWithValue("@BeneficiaryFullName", _receiverFullName);
            sqlCommand.Parameters.AddWithValue("@BeneficiaryLastName", _receiverLastName);
            sqlCommand.Parameters.AddWithValue("@BeneficiaryFirstName", _receiverFirstName);
            sqlCommand.Parameters.AddWithValue("@BeneficiaryIDType", _receiverIDType);
            sqlCommand.Parameters.AddWithValue("@BeneficiaryIDDetails", _receiverIDDetails);

            sqlCommand.Parameters.AddWithValue("@ReceiverBranchUserID", RemittancePartnerConfiguration.ServiceCenterCode);
            sqlCommand.Parameters.AddWithValue("@PayoutBranchCode", RemittancePartnerConfiguration.ServiceCenterCode);

            sqlCommand.Parameters.AddWithValue("@AuditTracker",
                RemittanceAuditTrail.GetAuditTrailString(
                       RemittancePartnerConfiguration.ServiceCenterCode,
                       RemittancePartnerConfiguration.ServiceCenterCode,
                    "1",
                    "1",
                    operationIpAddress,
                       RemittancePartnerConfiguration.ServiceCenterCode)
                );

            sqlCommand.Parameters.AddWithValue("@PartnerInternalReferenceNumber", partnerInternalReferenceNumber);
            sqlCommand.Parameters.AddWithValue("@PartnerInternalReferenceNumber2", partnerInternalReferenceNumber2);

            sqlCommand.Parameters.AddWithValue("@SendingCountry", _senderCountry ?? string.Empty);
            sqlCommand.Parameters.AddWithValue("@SendingState", _senderState ?? string.Empty);
            sqlCommand.Parameters.AddWithValue("@SenderEmail", _senderEmail ?? string.Empty);
            sqlCommand.Parameters.AddWithValue("@SenderMobileNumber", _senderMobileNumber ?? string.Empty);

            sqlConnection.Open();
            sqlCommand.ExecuteNonQuery();
        }
    }

    #endregion
}
