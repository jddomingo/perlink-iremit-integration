﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for RIAPayoutResult
/// </summary>
public class iRemitPayoutResult
{
    #region Constructors
    public iRemitPayoutResult()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion

    #region Fields
    private string _transferID;
    private string _orderNo;
    private string _pin;
    private string _beneCurrency;
    private string _beneAmount;
    private string _responseDateTimeUTC;
    private string _responseCode;
    private string _responseText;

    #endregion

    #region Properties
    public string TransferID
    {
        get { return _transferID; }
        set { _transferID = value; }
    }
    public string OrderNo
    {
        get { return _orderNo; }
        set { _orderNo = value; }
    }
    public string Pin
    {
        get { return _pin; }
        set { _pin = value; }
    }
    public string BeneCurrency
    {
        get { return _beneCurrency; }
        set { _beneCurrency = value; }
    }

    public string BeneAmount
    {
        get { return _beneAmount; }
        set { _beneAmount = value; }
    }
    public string ResponseDateTimeUTC
    {
        get { return _responseDateTimeUTC; }
        set { _responseDateTimeUTC = value; }
    }

    public string ResponseCode
    {
        get { return _responseCode; }
        set { _responseCode = value; }
    }
    public string ResponseText
    {
        get { return _responseText; }
        set { _responseText = value; }
    }
    #endregion
}
