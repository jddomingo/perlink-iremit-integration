﻿using System;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using System.Security.Cryptography;

/// <summary>
/// Summary description for Utils
/// </summary>
public class Utils
{
    public static void WriteToEventLog(string logMessage, EventLogEntryType eventLogEntryType)
    {
        
        try
        {
            if (!EventLog.SourceExists(RemittancePartnerConfiguration.ApplicationName, "."))
            {
                EventSourceCreationData eventSourceCreationData = new EventSourceCreationData(RemittancePartnerConfiguration.ApplicationName, "Application");
                EventLog.CreateEventSource(eventSourceCreationData);
            }

            using (EventLog eventLog = new EventLog("Application", ".", RemittancePartnerConfiguration.ApplicationName))
            {
                EventLogEntryCollection evec = eventLog.Entries;
                eventLog.WriteEntry(logMessage, eventLogEntryType);
            }
        }
        catch (Exception ex)
        {
        }
    }

    public static string FilterValidCharacters(string textParam, string validCharactersParam)
    {
        StringBuilder returnValue = new StringBuilder();

        char[] arytextParam = textParam.ToCharArray();

        Array.ForEach<char>(arytextParam, delegate(char text)
            {
                if (validCharactersParam.Contains(text.ToString().ToUpper()))
                {
                    returnValue.Append(text.ToString());
                }
            });
        return returnValue.ToString();
    }

    public static long AgentRequestID(string branchCode)
    {
        long uniqueID = 0;
        System.Threading.Thread.Sleep(1000);
        string date = string.Format("{0}{1}{2}", DateTime.Now.Year.ToString().Substring(2,2),DateTime.Now.Month.ToString().PadLeft(2,'0'),DateTime.Now.Day.ToString().PadLeft(2,'0'));
        string time = string.Format("{0}{1}{2}", DateTime.Now.Hour.ToString().PadLeft(2, '0'), DateTime.Now.Minute.ToString().PadLeft(2, '0'), DateTime.Now.Second.ToString().PadLeft(2, '0'));
        long.TryParse(string.Format("{0}{1}{2}",date,branchCode, time), out uniqueID);

        return uniqueID;
    }


    public static string PasswordHashing(HashCodeType strHashCode, string strSignature)
    {
        string strEncryptedPass = string.Empty;
        switch (strHashCode)
        {
            case HashCodeType.encMD5:
                MD5CryptoServiceProvider MD5 = new MD5CryptoServiceProvider();
                byte[] bs_Signature = Encoding.UTF8.GetBytes(strSignature);
                bs_Signature = MD5.ComputeHash(bs_Signature);
                System.Text.StringBuilder strBuildSignature = new System.Text.StringBuilder();
                foreach (byte b in bs_Signature)
                {
                    if (b < 16)
                    {
                        strBuildSignature.Append("0" + b.ToString("x").ToLower());
                    }
                    else
                    {
                        strBuildSignature.Append(b.ToString("x").ToLower());
                    }

                }
                strEncryptedPass = strBuildSignature.ToString();
                break;
            default:
                break;
        }
        return strEncryptedPass;
    }
    public enum HashCodeType
    {
        encSHA1 = 0
        , encMD5 = 1
    }
   
}
