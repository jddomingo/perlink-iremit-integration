﻿using System;
using System.Net;
using System.Web.Services.Protocols;
using System.Xml;
using SoapHelpers;
using System.Configuration;
using iRemitWcf;
/// <summary>
/// Summary description for InquireTransactionRequest
/// </summary>
public class InquireTransactionRequest
{

		   #region Constructors

    public InquireTransactionRequest()
    {
    }

    #endregion

    #region Fields

    private string _ReferenceNumber ;
    private string _ServiceCenterCode , _Branch , _Signature;
    #endregion

    #region Properties
    public string ReferenceNumber
    {
        get { return _ReferenceNumber; }
        set { _ReferenceNumber = value; }
    }
    public string ServiceCenterCode
    {
        get { return _ServiceCenterCode; }
        set { _ServiceCenterCode = value; }
    }
    public string Branch
    {
        get { return _Branch; }
        set { _Branch = value; }
    }
        public string Signature
    {
        get { return _Signature; }
        set { _Signature = value; }
    }

    #endregion


     #region Methods

    public InquireTransactionResult InquireTransaction(string ReferenceNumber)
    {
        iRemitWcf.Service iremitWebService = new iRemitWcf.Service();

        string PartnerSession = Utils.AgentRequestID(RemittancePartnerConfiguration.ServiceCenterCode).ToString();
        string rawSignature = RemittancePartnerConfiguration.PinCode + ReferenceNumber + PartnerSession;
        string hashedSignature = Utils.PasswordHashing(Utils.HashCodeType.encMD5, rawSignature);

        try
        {
            XmlNode xmlNode = iremitWebService.InquireTransaction(ReferenceNumber, RemittancePartnerConfiguration.ServiceCenterCode, Branch, hashedSignature, PartnerSession);
            InquireTransactionResult lookuptransactionResult = new InquireTransactionResult();
            lookuptransactionResult = InquireTransactionResult.GetResult(xmlNode, PartnerSession, hashedSignature);
            return lookuptransactionResult;
        }
        catch (Exception error)
        {
            throw error;
        }
    }


    public InquireTransactionResult ChangeHlhuillierPin(string newPin)
    {
        string currentPinCode = ConfigurationManager.AppSettings["PinCode"].ToString();
        string serviceCenterCode = ConfigurationManager.AppSettings["ServiceCenterCode"].ToString();
        string hashedSignature = Utils.PasswordHashing(Utils.HashCodeType.encMD5, serviceCenterCode + currentPinCode);
        try
        {
            iRemitWcf.Service service = new iRemitWcf.Service();
            XmlNode node = service.ChangePin(serviceCenterCode, newPin, hashedSignature);
            InquireTransactionResult lookupResult = new InquireTransactionResult();
            lookupResult = InquireTransactionResult.GetResult(node,newPin);
            return lookupResult;
        }
        catch (Exception error)
        {
            throw error;
        }
    }

    #endregion
    }


