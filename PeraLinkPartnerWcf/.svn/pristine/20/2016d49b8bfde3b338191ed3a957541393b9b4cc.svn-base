using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class AddBeneficiaryRequest: BaseRequest
{
    #region Constructor
    public AddBeneficiaryRequest() { }
    #endregion

    #region Fields/Properties
    private string _firstName;
    [DataMember]
    public string FirstName
    {
        get { return _firstName; }
        set { _firstName = value; }
    }

    private string _middleName;
    [DataMember]
    public string MiddleName
    {
        get { return _middleName; }
        set { _middleName = value; }
    }

    private string _lastName;
    [DataMember]
    public string LastName
    {
        get { return _lastName; }
        set { _lastName = value; }
    }

    private Int64 _senderClientID;
    [DataMember]
    public Int64 SenderClientID
    {
        get { return _senderClientID; }
        set { _senderClientID = value; }
    }

    private DateTime? _birthDate;
    [DataMember]
    public DateTime? BirthDate
    {
        get { return _birthDate; }
        set { _birthDate = value; }
    }

    private int _cellphoneCountryID;
    [DataMember]
    public int CellphoneCountryID
    {
        get { return _cellphoneCountryID; }
        set { _cellphoneCountryID = value; }
    }

    private string _cellphoneNumber;
    [DataMember]
    public string CellphoneNumber
    {
        get { return _cellphoneNumber; }
        set { _cellphoneNumber = value; }
    }

    private int _telephoneCountryID;
    [DataMember]
    public int TelephoneCountryID
    {
        get { return _telephoneCountryID; }
        set { _telephoneCountryID = value; }
    }

    private string _telephoneAreaCode;
    [DataMember]
    public string TelephoneAreaCode
    {
        get { return _telephoneAreaCode; }
        set { _telephoneAreaCode = value; }
    }

    private string _telephoneNumber;
    [DataMember]
    public string TelephoneNumber
    {
        get { return _telephoneNumber; }
        set { _telephoneNumber = value; }
    }

    private int _countryAddressID;
    [DataMember]
    public int CountryAddressID
    {
        get { return _countryAddressID; }
        set { _countryAddressID = value; }
    }

    [DataMember]
    public int BirthCountryID { get; set; }

    private string _provinceAddress;
    [DataMember]
    public string ProvinceAddress
    {
        get { return _provinceAddress; }
        set { _provinceAddress = value; }
    }

    private string _address;
    [DataMember]
    public string Address
    {
        get { return _address; }
        set { _address = value; }
    }

    private string _userID;
    [DataMember]
    public string UserID
    {
        get { return _userID; }
        set { _userID = value; }
    }

	private string _zipCode;
	[DataMember]
	public string ZipCode
	{
		get { return _zipCode; }
		set { _zipCode = value; }
	}

	private string _occupation;
	[DataMember]
	public string Occupation
	{
		get { return _occupation; }
		set { _occupation = value; }
	}

	private string _sendingPartnerCode;
	[DataMember]
	public string SendingPartnerCode
	{
		get { return _sendingPartnerCode; }
		set { _sendingPartnerCode = value; }
	}

    private int? _stateIDAddress;
    [DataMember]
    public int? StateIDAddress
    {
        get { return _stateIDAddress; }
        set { _stateIDAddress = value; }
    }

	private string _tin;

	public string TIN
	{
		get { return _tin; }
		set { _tin = value; }
	}
    #endregion

    #region Internal Methods
    internal AddBeneficiaryResult Process()
    {
        AddBeneficiaryResult returnValue = new AddBeneficiaryResult();

        // Important: This function validates the request credential
        this.AuthenticateRequest();

        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
			#region Validate sending partner information
			if (String.IsNullOrWhiteSpace(this.SendingPartnerCode)){ }
			else
			{
				PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
				partner.PartnerCode = this.SendingPartnerCode;
				partner = serviceClient.FindPartnerCode(partner);

				if (partner.PartnerID == 0)
				{
					PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
					throw new RDFramework.ClientException(message.Content, message.MessageID);
				}
				else { }
			}
			#endregion

            PeraLinkCoreWcf.Beneficiary beneficiary = new PeraLinkCoreWcf.Beneficiary();
            beneficiary.CreatedBy = this.UserID;
            beneficiary.DateTimeCreated = DateTime.Now;
            beneficiary.FirstName = this.FirstName;
            beneficiary.LastName = this.LastName;
            beneficiary.MiddleName = this.MiddleName;
            beneficiary.BirthDate = Convert.ToDateTime("01/01/1900");

            beneficiary.SenderClient = new PeraLinkCoreWcf.Client();
            beneficiary.SenderClient.ClientID = this.SenderClientID;
            beneficiary.SenderClient = serviceClient.GetClient(beneficiary.SenderClient);

            if (beneficiary.SenderClient.ClientID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(11);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            { 
                // Proceed
            }

            //do nothing
            beneficiary = serviceClient.AddBeneficiary(beneficiary);
            returnValue.BeneficiaryID = beneficiary.BeneficiaryID;
        }

        returnValue.ResultStatus = ResultStatus.Successful;

        return returnValue;
    }
#endregion
}