using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ServiceCredential
/// </summary>
public class ServiceCredential
{
    #region Constructor
    public ServiceCredential()
    {
        //

        //
    }
    #endregion

    #region Fields/Properties
    private string _userID;

    public string UserID
    {
        get { return _userID; }
        set { _userID = value; }
    }
    private string _password;

    public string Password
    {
        get { return _password; }
        set { _password = value; }
    }
    private ExternalUserMgmtWS.AuthenticateUserResponse _authenticateUserResponse;
    #endregion

    #region Public Methods
    public void Authenticate()
    {
        SystemCache cache = new SystemCache();
        ServiceCredential cachedServiceCredential = cache.GetServiceCredential(this);

        if (cachedServiceCredential == null)
        {
            // Proceed to web service call
        }
        else
        {
            if (this.Password == cachedServiceCredential.Password)
            {
                _authenticateUserResponse = cachedServiceCredential._authenticateUserResponse;

                return;
            }
            else
            {
                // Proceed to web service call
            }
        }

        using (ExternalUserMgmtWSClient serviceClient = new ExternalUserMgmtWSClient())
        {
            try
            {
                _authenticateUserResponse = serviceClient.AuthenticateUser(this.UserID, this.Password);
                cache.InsertServiceCredential(this);
            }
            catch (RDFramework.ClientException clientEx)
            {
                switch (clientEx.MessageID)
                {
                    case ExternalUserMgmtWSClient.AuthenticateUserErrorID.FirstLogin:
                        {
                            InvokeFirstLogin();
                            _authenticateUserResponse = serviceClient.AuthenticateUser(this.UserID, this.Password);
                            cache.InsertServiceCredential(this);

                            return;
                        }
                    default:
                        {
                            throw;
                        }
                }
            }
        }
    }
    #endregion

    #region Private Methods
    private void InvokeFirstLogin()
    {
        using (ExternalUserMgmtWSClient serviceClient = new ExternalUserMgmtWSClient())
        {
            string originalPassword = this.Password;
            string temporaryPassword = string.Concat(this.Password, "0");
            AutoCommitFirstLogin(temporaryPassword);

            // Set Dummy Password History
            for (int index = 1; index <= 4; index++)
            {
                ServiceCredential temporaryCredential = new ServiceCredential();
                temporaryCredential.UserID = this.UserID;
                temporaryCredential.Password = temporaryPassword;
                temporaryPassword = string.Concat(this.Password, index.ToString());
                serviceClient.ChangePassword(temporaryCredential.UserID, temporaryCredential.Password, temporaryPassword);
            }

            // Return original password
            serviceClient.ChangePassword(this.UserID, temporaryPassword, this.Password);
        }
    }

    private void AutoCommitFirstLogin(string newPassword)
    {
        ExternalUserMgmtWS.ChangePasswordOnFirstLoginRequest changePasswordOnFirstLoginRequest = new ExternalUserMgmtWS.ChangePasswordOnFirstLoginRequest();
        changePasswordOnFirstLoginRequest.ApplicationCode = SystemSetting.ApplicationCodeForEums;
        changePasswordOnFirstLoginRequest.CurrentPassword = this.Password;
        changePasswordOnFirstLoginRequest.IPAddress = SystemUtility.GetClientIPAddress();
        changePasswordOnFirstLoginRequest.NewPassword = newPassword;

        List<ExternalUserMgmtWS.UserQuestion> questionList = new List<ExternalUserMgmtWS.UserQuestion>();

        ExternalUserMgmtWS.UserQuestion userQuestion1 = new ExternalUserMgmtWS.UserQuestion();
        userQuestion1.Answer = "Childhood hero?";
        userQuestion1.Question = "Childhood hero?";
        questionList.Add(userQuestion1);

        ExternalUserMgmtWS.UserQuestion userQuestion2 = new ExternalUserMgmtWS.UserQuestion();
        userQuestion2.Answer = "Dream job as a child?";
        userQuestion2.Question = "Dream job as a child?";
        questionList.Add(userQuestion2);

        ExternalUserMgmtWS.UserQuestion userQuestion3 = new ExternalUserMgmtWS.UserQuestion();
        userQuestion3.Answer = "Father's middle name?";
        userQuestion3.Question = "Father's middle name?";
        questionList.Add(userQuestion3);

        changePasswordOnFirstLoginRequest.QuestionList = questionList.ToArray();
        changePasswordOnFirstLoginRequest.UserID = this.UserID;

        using (ExternalUserMgmtWSClient serviceClient = new ExternalUserMgmtWSClient())
        {
            serviceClient.ChangePasswordOnFirstLogin(changePasswordOnFirstLoginRequest);
        }
    }
    #endregion
}