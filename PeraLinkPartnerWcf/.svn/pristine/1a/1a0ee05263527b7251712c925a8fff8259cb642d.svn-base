using System;
using System.Collections.Generic;
using System.Web;
using System.Runtime.Serialization;
using System.Linq;
using System.Threading.Tasks;

[DataContract]
public class GetEMoneyCollectionRequest : BaseRequest
{
	#region Constructor
	public GetEMoneyCollectionRequest()
	{
		//

		//
	}
	#endregion

	#region Fields/Properties

	private string _partnerCode;
	[DataMember]
	public string PartnerCode
	{
		get { return _partnerCode; }
		set { _partnerCode = value; }
	}

	private string _token;
	[DataMember]
	public string Token
	{
		get { return _token; }
		set { _token = value; }
	}

	private string _agentCode;
	[DataMember]
	public string AgentCode
	{
		get { return _agentCode; }
		set { _agentCode = value; }
	}

	private string _userID;
	[DataMember]
	public string UserID
	{
		get { return _userID; }
		set { _userID = value; }
	}
	#endregion

	#region Internal Methods
	internal GetEMoneyCollectionResult Process()
	{
		GetEMoneyCollectionResult returnValue = new GetEMoneyCollectionResult();

		// Important: This function validates the request credential
		this.AuthenticateRequest();

		using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
		{
			#region Validate Partner Record
			PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
			partner.PartnerCode = this.PartnerCode;
			partner = serviceClient.FindPartnerCode(partner);

			if (partner.PartnerID == 0)
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			else { }

			if (partner.Token == this.Token) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}

			if (partner.Activated) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}

			if (partner.SourceOfFundID == SourceOfFundID.EMoney) { /* Do nothing */ }
			else
			{
				throw SystemUtility.BuildClientError(1);
			}

			#endregion

			#region Validate Agent Record
			PeraLinkCoreWcf.Agent agent = new PeraLinkCoreWcf.Agent();
			agent.AgentCode = this.AgentCode;
			agent.Partner = partner;
			agent = serviceClient.FindAgentCodeOfPartner(agent);

			if (agent.AgentID == 0)
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			else
			{
				// Proceed
			}

			if (agent.Partner.PartnerID == partner.PartnerID)
			{ }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(43);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}

			if (agent.Activated) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(46);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}

			#endregion

			PeraLinkCoreWcf.GetEMoneyProfileRequest getEMoneyProfileRequest = new PeraLinkCoreWcf.GetEMoneyProfileRequest();
			getEMoneyProfileRequest.Partner = partner;
			PeraLinkCoreWcf.GetEMoneyProfileResult getEMoneyProfileResut = serviceClient.GetEMoneyProfile(getEMoneyProfileRequest);

			EMoneyWS.CustomerTransactionHistory[] rawHistory;

			using (EMoneyWSClient eMoneyWSClient = new EMoneyWSClient())
			{
				rawHistory = eMoneyWSClient.GetCustomerTransactionHistory2(getEMoneyProfileResut.EMoneyProfile.AccountNumber, DateTime.Today, DateTime.Today.AddDays(1));
			}


			EMoneyWS.CustomerTransactionHistory[] agentHistory = Array.FindAll(rawHistory,
				delegate(EMoneyWS.CustomerTransactionHistory item)
				{

                    string[] remarksList= new string[4];
                    if (string.IsNullOrWhiteSpace(item.TransTyepDesc) == false)
                    {
                        remarksList  = item.TransTyepDesc.Split('|');
                    }
                    else
                    { 
                        remarksList = new string[]{ string.Empty };
                    }

                    return remarksList[0] == this.AgentCode;
				}); 

            returnValue.EMoneyCollection = new EMoneyCollection();

            Parallel.ForEach(agentHistory, item =>
                {
                    EMoney eMoney = new EMoney();
                    eMoney.AccountNumber = item.AccountNo;
                    eMoney.CashInAmount = item.CashInAmount;
                    eMoney.CashOutAmount = item.CashOutAmount;
                    eMoney.ControlNumber = item.TransRefNo;
                    eMoney.PeraLinkAgentCode = this.AgentCode;
                    eMoney.DateCreated = item.DateCreated;
							   
                    string[] remarksList = item.TransTyepDesc.Split('|');

                    if (remarksList.Length > 0)
                    {
						eMoney.ControlNumber = remarksList[3];
						eMoney.Remarks = remarksList[2];
                        eMoney.Action = remarksList[1];
                    }
                    else
                    {
						eMoney.ControlNumber = item.TransRefNo;
                        eMoney.Remarks = item.TransTyepDesc;
                        eMoney.Action = "Unknown";
                    }

                    returnValue.EMoneyCollection.Add(eMoney);
                });
		}

		returnValue.ResultStatus = ResultStatus.Successful;

		return returnValue;
	}
	#endregion
}