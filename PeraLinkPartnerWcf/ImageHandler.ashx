﻿<%@ WebHandler Language="C#" Class="ImageHandler" %>

using System;
using System.Web;
using System.IO;
using System.Net;
using System.Drawing;

public class ImageHandler : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

	public void ProcessRequest(HttpContext context)
	{
		context.Response.Clear();
		try
		{
			if (context.Request.QueryString.Count != 0)
			{
				if (context.Request.Params["pcode"] != null)
				{
					string partnerCode = (context.Request.Params["pcode"]).ToString();
					if (string.IsNullOrWhiteSpace(partnerCode))
					{
						ReturnError(context);
					}
					else
					{
						if (IsAlphaNumeric(partnerCode))
						{
							string uriPath = SystemSetting.PeraLinkSiteImages + partnerCode + ".jpg";

							if (RemoteFileExists(uriPath))
							{
								using (var webClient = new System.Net.WebClient())
								{
									byte[] fileBytes = webClient.DownloadData(uriPath);
									if (fileBytes != null)
									{
										string result = System.Text.Encoding.UTF8.GetString(fileBytes);
										if (result.Contains("Page cannot be displayed"))
										{
											ReturnError(context);
										}
										else
										{
											context.Response.OutputStream.Write(fileBytes, 0, fileBytes.Length);
											context.Response.ContentType = "image/JPEG";
										}
									}
									else
									{
										ReturnError(context);
									}
								}
							}
							else
							{
								ReturnError(context);
							}
						}
						else
						{
							ReturnError(context);
						}
					}
				}
				else
				{
					ReturnError(context);
				}
			}
			else
			{
				ReturnError(context);
			}
		}
		catch (Exception error)
		{
			ReturnError(context);
		}
	}

	private bool RemoteFileExists(string url)
	{
		try
		{
			//Creating the HttpWebRequest
			HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
			//Setting the Request method HEAD, you can also use GET too.
			request.Method = "HEAD";
			//Getting the Web Response.
			HttpWebResponse response = request.GetResponse() as HttpWebResponse;
			//Returns TRUE if the Status code == 200
			return (response.StatusCode == HttpStatusCode.OK);
		}
		catch
		{
			//Any exception will returns false.
			return false;
		}
	}

	private void ReturnError(HttpContext context)
	{
		context.Response.Output.Write("Invalid request");
		context.Response.ContentType = "text/plain";
	}

	private bool IsAlphaNumeric(string text)
	{
		System.Text.RegularExpressions.Regex regEx = new System.Text.RegularExpressions.Regex(@"^[\w\d]+$");

		if (regEx.IsMatch(text))
		{
			return true;
		}

		return false;
	}

	public bool IsReusable
	{
		get
		{
			return false;
		}
	}
}