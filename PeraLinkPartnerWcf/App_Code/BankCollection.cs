using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BankCollection
/// </summary>
public class BankCollection: List<Bank>
{
	#region Constructor
	public BankCollection()
	{
		//

		//
	}
	#endregion

	#region Public Methods
	public void AddList(PeraLinkCoreWcf.Bank[] bankCollection)
	{
		Array.ForEach<PeraLinkCoreWcf.Bank>(bankCollection
			, delegate(PeraLinkCoreWcf.Bank eachBank)
			{
				Bank mappedBank = new Bank();
				mappedBank.Load(eachBank);
				this.Add(mappedBank);
			});
	}
	#endregion
}