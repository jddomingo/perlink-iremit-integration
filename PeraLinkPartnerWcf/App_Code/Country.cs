using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Country
/// </summary>
public class Country
{
    #region Constructor
    public Country()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private int _countryID;

    public int CountryID
    {
        get { return _countryID; }
        set { _countryID = value; }
    }

    private string _countryName;

    public string CountryName
    {
        get { return _countryName; }
        set { _countryName = value; }
    }

    private string _countryCodeAplha2;

    public string CountryCodeAplha2
    {
        get { return _countryCodeAplha2; }
        set { _countryCodeAplha2 = value; }
    }

    private string _countryCodeAlpha3;

    public string CountryCodeAlpha3
    {
        get { return _countryCodeAlpha3; }
        set { _countryCodeAlpha3 = value; }
    }

    private string _phoneCode;

    public string PhoneCode
    {
        get { return _phoneCode; }
        set { _phoneCode = value; }
    }
    #endregion

    #region Public Methods
    public void Load(PeraLinkCoreWcf.Country country)
    {
        if (country == null)
        { }
        else
        {
            this.CountryCodeAlpha3 = country.CountryCodeAlpha3;
            this.CountryCodeAplha2 = country.CountryCodeAplha2;
            this.CountryID = country.CountryID;
            this.CountryName = country.CountryName;
            this.PhoneCode = country.PhoneCode;
        }
    }
    #endregion
}