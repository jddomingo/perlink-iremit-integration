using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetClientSourceOfFundCollectionResult: ServiceResult
{
	#region Constructor
	public GetClientSourceOfFundCollectionResult()
	{
		//

		//
	}
	#endregion

	#region Fields/Properties
	private ClientSourceOfFundCollection _clientSourceOfFundCollection;
	[DataMember]
	public ClientSourceOfFundCollection ClientSourceOfFundCollection
	{
		get { return _clientSourceOfFundCollection; }
		set { _clientSourceOfFundCollection = value; }
	} 
	#endregion
}