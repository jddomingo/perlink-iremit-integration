using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetCountryCollectionResult: BaseResult
{
    #region Constructor
    public GetCountryCollectionResult()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private CountryCollection _countryCollection;
    [DataMember]
    public CountryCollection CountryCollection
    {
        get { return _countryCollection; }
        set { _countryCollection = value; }
    }
    #endregion
}