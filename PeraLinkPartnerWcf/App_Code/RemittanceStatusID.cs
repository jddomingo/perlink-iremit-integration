using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for RemittanceStatusID
/// </summary>
public class RemittanceStatusID
{
    #region Constructor
    public RemittanceStatusID()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    public const int Outstanding = 1;
    public const int Paid = 2;
    public const int Refunded = 3;
    public const int Cancelled = 4;
    public const int Amended = 5;
    public const int Cached = 6;
    #endregion
}