using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class VerifyRemitToAccountTransactionResult: ServiceResult
{
	#region Constructor
	public VerifyRemitToAccountTransactionResult()
	{ }
	#endregion

	#region Fields/Properties
	private RemitToAccountTransaction _remitToAccountTransaction;
	[DataMember]
	public RemitToAccountTransaction RemitToAccountTransaction
	{
		get { return _remitToAccountTransaction; }
		set { _remitToAccountTransaction = value; }
	}
	#endregion
}