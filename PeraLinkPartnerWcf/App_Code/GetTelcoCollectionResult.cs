using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for GetTelcoCollectionResult
/// </summary>
/// 
[DataContract]
public class GetTelcoCollectionResult : ServiceResult
{
    #region Constructor
    public GetTelcoCollectionResult()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion

    #region Fields/Properties
    private TelcoCollection _telcoCollection;
    [DataMember]
    public TelcoCollection TelcoCollection
    {
        get { return _telcoCollection; }
        set { _telcoCollection = value; }
    }
    #endregion
}