using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class DomesticRemittance
{
    #region Constructor
    public DomesticRemittance()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private Int64 _domesticRemittanceID;

    public Int64 DomesticRemittanceID
    {
        get { return _domesticRemittanceID; }
        set { _domesticRemittanceID = value; }
    }

    private string _controlNumber;

    public string ControlNumber
    {
        get { return _controlNumber; }
        set { _controlNumber = value; }
    }

    private Int64 _senderClientID;

    public Int64 SenderClientID
    {
        get { return _senderClientID; }
        set { _senderClientID = value; }
    }

    private string _senderClientNumber;

    public string SenderClientNumber
    {
        get { return _senderClientNumber; }
        set { _senderClientNumber = value; }
    }

    private string _senderFirstName;

    public string SenderFirstName
    {
        get { return _senderFirstName; }
        set { _senderFirstName = value; }
    }

    private string _senderMiddleName;

    public string SenderMiddleName
    {
        get { return _senderMiddleName; }
        set { _senderMiddleName = value; }
    }

    private string _senderLastName;

    public string SenderLastName
    {
        get { return _senderLastName; }
        set { _senderLastName = value; }
    }

    private Int64 _beneficiaryID;

    public Int64 BeneficiaryID
    {
        get { return _beneficiaryID; }
        set { _beneficiaryID = value; }
    }

    private string _beneficiaryFirstName;

    public string BeneficiaryFirstName
    {
        get { return _beneficiaryFirstName; }
        set { _beneficiaryFirstName = value; }
    }

    private string _beneficiaryMiddleName;

    public string BeneficiaryMiddleName
    {
        get { return _beneficiaryMiddleName; }
        set { _beneficiaryMiddleName = value; }
    }

    private string _beneficiaryLastName;

    public string BeneficiaryLastName
    {
        get { return _beneficiaryLastName; }
        set { _beneficiaryLastName = value; }
    }

    private decimal _principalAmount;

    public decimal PrincipalAmount
    {
        get { return _principalAmount; }
        set { _principalAmount = value; }
    }

    private decimal _serviceFee;

    public decimal ServiceFee
    {
        get { return _serviceFee; }
        set { _serviceFee = value; }
    }

    private int _remittanceStatusID;

    public int RemittanceStatusID
    {
        get { return _remittanceStatusID; }
        set { _remittanceStatusID = value; }
    }

    private string _remittanceStatusDescription;

    public string RemittanceStatusDescription
    {
        get { return _remittanceStatusDescription; }
        set { _remittanceStatusDescription = value; }
    }

    private int _sendCurrencyID;

    public int SendCurrencyID
    {
        get { return _sendCurrencyID; }
        set { _sendCurrencyID = value; }
    }

    private string _sendCurrencyCode;

    public string SendCurrencyCode
    {
        get { return _sendCurrencyCode; }
        set { _sendCurrencyCode = value; }
    }

    private DateTime _dateTimeSent;

    public DateTime DateTimeSent
    {
        get { return _dateTimeSent; }
        set { _dateTimeSent = value; }
    }

    private Int64 _sentByAgentID;

    public Int64 SentByAgentID
    {
        get { return _sentByAgentID; }
        set { _sentByAgentID = value; }
    }

    private string _sentByAgentCode;

    public string SentByAgentCode
    {
        get { return _sentByAgentCode; }
        set { _sentByAgentCode = value; }
    }

    private string _sentByUserID;

    public string SentByUserID
    {
        get { return _sentByUserID; }
        set { _sentByUserID = value; }
    }

    private DateTime? _dateTimeUpdated;

    public DateTime? DateTimeUpdated
    {
        get { return _dateTimeUpdated; }
        set { _dateTimeUpdated = value; }
    }

    private Int64 _updatedByAgentID;

    public Int64 UpdatedByAgentID
    {
        get { return _updatedByAgentID; }
        set { _updatedByAgentID = value; }
    }

    private string _updatedByAgentCode;

    public string UpdatedByAgentCode
    {
        get { return _updatedByAgentCode; }
        set { _updatedByAgentCode = value; }
    }

    private string _updatedByUserID;

    public string UpdatedByUserID
    {
        get { return _updatedByUserID; }
        set { _updatedByUserID = value; }
    }

    private Int64 _sentByPartnerID;

    public Int64 SentByPartnerID
    {
        get { return _sentByPartnerID; }
        set { _sentByPartnerID = value; }
    }

    private string _sentByPartnerCode;

    public string SentByPartnerCode
    {
        get { return _sentByPartnerCode; }
        set { _sentByPartnerCode = value; }
    }

    private string _sentByPartnerName;

    public string SentByPartnerName
    {
        get { return _sentByPartnerName; }
        set { _sentByPartnerName = value; }
    }

    private Int64 _updatedByPartnerID;

    public Int64 UpdatedByPartnerID
    {
        get { return _updatedByPartnerID; }
        set { _updatedByPartnerID = value; }
    }

    private string _updatedByPartnerCode;

    public string UpdatedByPartnerCode
    {
        get { return _updatedByPartnerCode; }
        set { _updatedByPartnerCode = value; }
    }

    private string _updatedByPartnerName;

    public string UpdatedByPartnerName
    {
        get { return _updatedByPartnerName; }
        set { _updatedByPartnerName = value; }
    }

	private Int64 _sentByTerminalID;

	public Int64 SentByTerminalID
	{
		get { return _sentByTerminalID; }
		set { _sentByTerminalID = value; }
	}

	private string _sentByTerminalCode;

	public string SentByTerminalCode
	{
		get { return _sentByTerminalCode; }
		set { _sentByTerminalCode = value; }
	}

	private Int64 _updatedByTerminalID;

	public Int64 UpdatedByTerminalID
	{
		get { return _updatedByTerminalID; }
		set { _updatedByTerminalID = value; }
	}

	private string _updatedByTerminalCode;

	public string UpdatedByTerminalCode
	{
		get { return _updatedByTerminalCode; }
		set { _updatedByTerminalCode = value; }
	}
    #endregion

    #region Public Methods
    public void Load(PeraLinkCoreWcf.DomesticRemittance domesticRemittance)
    {
        this.BeneficiaryFirstName = domesticRemittance.Beneficiary.FirstName;
        this.BeneficiaryID = domesticRemittance.Beneficiary.BeneficiaryID;
        this.BeneficiaryLastName = domesticRemittance.Beneficiary.LastName;
        this.BeneficiaryMiddleName = domesticRemittance.Beneficiary.MiddleName;
        this.ControlNumber = domesticRemittance.ControlNumber;
        this.DateTimeSent = domesticRemittance.DateTimeSent;
        this.DomesticRemittanceID = domesticRemittance.DomesticRemittanceID;
        this.PrincipalAmount = domesticRemittance.PrincipalAmount;
        this.RemittanceStatusDescription = domesticRemittance.RemittanceStatus.Description;
        this.RemittanceStatusID = domesticRemittance.RemittanceStatus.RemittanceStatusID;
        this.SendCurrencyCode = domesticRemittance.SendCurrency.Code;
        this.SendCurrencyID = domesticRemittance.SendCurrency.CurrencyID;
        this.SenderClientID = domesticRemittance.SenderClient.ClientID;
        this.SenderClientNumber = domesticRemittance.SenderClient.ClientNumber;
        this.SenderFirstName = domesticRemittance.SenderClient.FirstName;
        this.SenderLastName = domesticRemittance.SenderClient.LastName;
        this.SenderMiddleName = domesticRemittance.SenderClient.MiddleName;
        this.ServiceFee = domesticRemittance.ServiceFee;
        this.SentByAgentID = domesticRemittance.SentByAgent.AgentID;
        this.SentByAgentCode = domesticRemittance.SentByAgent.AgentCode;
        this.SentByPartnerID = domesticRemittance.SentByAgent.Partner.PartnerID;
        this.SentByPartnerCode = domesticRemittance.SentByAgent.Partner.PartnerCode;
        this.SentByPartnerName = domesticRemittance.SentByAgent.Partner.Name;
        this.SentByUserID = domesticRemittance.SentByUserID;
        this.DateTimeUpdated = domesticRemittance.DateTimeUpdated;
		this.SentByTerminalID = domesticRemittance.SentByTerminal.TerminalID;
		this.SentByTerminalCode = domesticRemittance.SentByTerminal.TerminalCode;

        if (domesticRemittance.UpdatedByAgent == null)
        { 
            // Do not map update details
        }
        else
        {
            this.UpdatedByAgentID = domesticRemittance.UpdatedByAgent.AgentID;
            this.UpdatedByAgentCode = domesticRemittance.UpdatedByAgent.AgentCode;
            this.UpdatedByUserID = domesticRemittance.UpdatedByUserID;
            this.UpdatedByPartnerID = domesticRemittance.UpdatedByAgent.Partner.PartnerID;
            this.UpdatedByPartnerCode = domesticRemittance.UpdatedByAgent.Partner.PartnerCode;
            this.UpdatedByPartnerName = domesticRemittance.UpdatedByAgent.Partner.Name;
			this.UpdatedByTerminalID = domesticRemittance.UpdatedByTerminal.TerminalID;
			this.UpdatedByTerminalCode = domesticRemittance.UpdatedByTerminal.TerminalCode;
        }
    }
    #endregion
}