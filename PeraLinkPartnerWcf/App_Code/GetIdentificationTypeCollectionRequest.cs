using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetIdentificationTypeCollectionRequest: BaseRequest
{
	#region Fields
	private string _pullTransactionPartnerCode;
	[DataMember]
	public string PullTransactionPartnerCode
	{
		get { return _pullTransactionPartnerCode; }
		set { _pullTransactionPartnerCode = value; }
	}
	#endregion
	
	#region Constructor
	public GetIdentificationTypeCollectionRequest()
	{
		//

		//
    }
    #endregion

    #region Internal Methods
    internal GetIdentificationTypeCollectionResult Process()
    {
        GetIdentificationTypeCollectionResult returnValue = new GetIdentificationTypeCollectionResult();

        // Important: This function validates the request credential
        this.AuthenticateRequest();

        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
			returnValue.IdentificationTypeCollection = new IdentificationTypeCollection();

			PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();

			if(String.IsNullOrWhiteSpace(this.PullTransactionPartnerCode))
			{/*do nothing*/}
			else
			{
				#region Validate Partner Record
				
				partner.PartnerCode = this.PullTransactionPartnerCode;
				partner = serviceClient.FindPartnerCode(partner);

				if (partner.PartnerID == 0)
				{
					PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
					throw new RDFramework.ClientException(message.Content, message.MessageID);
				}
				else
				{ /*do nothing*/}
				#endregion
			}
			
			
            returnValue.IdentificationTypeCollection.AddList(serviceClient.GetIdentificationTypeCollection(partner.PartnerID));
        }

        returnValue.ResultStatus = ResultStatus.Successful;

        return returnValue;
    }
    #endregion
}