using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Text;

[DataContract]
public class VerifyRemitToAccountTransactionRequest: BaseRequest
{
	#region Constructor
	public VerifyRemitToAccountTransactionRequest()
	{ }
	#endregion

	#region Fields/Properties
	private string _controlNumber;
	[DataMember]
	public string ControlNumber
	{
		get { return _controlNumber; }
		set { _controlNumber = value; }
	}

	private string _partnerCode;
	[DataMember]
	public string PartnerCode
	{
		get { return _partnerCode; }
		set { _partnerCode = value; }
	}

	private string _token;
	[DataMember]
	public string Token
	{
		get { return _token; }
		set { _token = value; }
	}

	private string _agentCode;
	[DataMember]
	public string AgentCode
	{
		get { return _agentCode; }
		set { _agentCode = value; }
	}

	private string _userID;
	[DataMember]
	public string UserID
	{
		get { return _userID; }
		set { _userID = value; }
	}
	#endregion

	#region Internal Method
	internal VerifyRemitToAccountTransactionResult Process()
	{
		VerifyRemitToAccountTransactionResult returnValue = new VerifyRemitToAccountTransactionResult();

		// Important: This function validates the request credential
		this.AuthenticateRequest();

		using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
		{
			#region Validate requesting partner information
			PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
			partner.PartnerCode = this.PartnerCode;
			partner = serviceClient.FindPartnerCode(partner);

			if (partner.PartnerID == 0)
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			else { }

			if (partner.Token == this.Token) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}

			if (partner.Activated) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			#endregion

			#region Validate requesting agent information
			PeraLinkCoreWcf.Agent agent = new PeraLinkCoreWcf.Agent();
			agent.AgentCode = this.AgentCode;
			agent.Partner = partner;
			agent = serviceClient.FindAgentCodeOfPartner(agent);

			if (agent.AgentID == 0)
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			else
			{
				// Proceed
			}

			if (agent.Partner.PartnerID == partner.PartnerID)
			{ }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(43);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}

			if (agent.Activated) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(46);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}

			#endregion

			#region Validate requesting partner's access to method (RemitToAccount)
			PeraLinkCoreWcf.PartnerServiceMapping[] partnerServiceMappingCollection = serviceClient.GetPartnerServiceMappingCollection(partner);

			PeraLinkCoreWcf.PartnerServiceMapping partnerServiceMapping = Array.Find<PeraLinkCoreWcf.PartnerServiceMapping>(partnerServiceMappingCollection
				, delegate(PeraLinkCoreWcf.PartnerServiceMapping eachPartnerServiceMapping)
				{
					return eachPartnerServiceMapping.ServiceType.ServiceTypeID == ServiceTypeID.RemitToAccount;
				});


			if (partnerServiceMapping == null)
			{
				PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(36);
				throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
			}
			else
			{
				// Proceed
			}
			#endregion

			#region Find Remit To Account Transaction
			PeraLinkCoreWcf.RemitToAccountTransaction remitToAccountTransaction = new PeraLinkCoreWcf.RemitToAccountTransaction();
			remitToAccountTransaction.ControlNumber = this.ControlNumber;
			remitToAccountTransaction = serviceClient.FindRemitToAccountTransaction(remitToAccountTransaction);

			if (remitToAccountTransaction.RemitToAccountTransactionID == 0)
			{
				PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(42);
				throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
			}
			else
			{
				// proceed
			}
			#endregion

			#region Validate Partner's Access to Transaction
			if (remitToAccountTransaction.SentByAgent.Partner.PartnerID == agent.Partner.PartnerID)
			{
				// proceed
			}
			else
			{
				PeraLinkCoreWcf.PartnerServicePair[] partnerServicePairCollection = serviceClient.GetPartnerServicePairCollection(partnerServiceMapping);

				PeraLinkCoreWcf.PartnerServicePair pairing = Array.Find<PeraLinkCoreWcf.PartnerServicePair>(partnerServicePairCollection
					, delegate(PeraLinkCoreWcf.PartnerServicePair eachPartnerServicePair)
					{
						if (eachPartnerServicePair.PairedPartner == null)
						{
							PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(40);
							throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
						}
						else
						{
							return eachPartnerServicePair.PairedPartner.PartnerID == remitToAccountTransaction.SentByAgent.Partner.PartnerID;
						}
					});

				if (pairing == null)
				{
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(38);
					throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
				}
				else
				{
					// Proceed
				}
				if (pairing.PairedPartner.Activated)
				{ }
				else
				{
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(39);
					throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
				}
			}
			#endregion

			#region Audit Trail
			PeraLinkCoreWcf.AuditTrail auditTrail = new PeraLinkCoreWcf.AuditTrail();
			auditTrail.CreatedByAgent = agent;
			auditTrail.CreatedByUserID = this.UserID;
			auditTrail.DateTimeCreated = DateTime.Now;
			auditTrail.IPAddress = SystemUtility.GetClientIPAddress();
			#endregion
            
            PeraLinkRemitToAccountWcf.RemitToAccount remitToAccountDetails = new PeraLinkRemitToAccountWcf.RemitToAccount();
            remitToAccountDetails.AccountNumber = remitToAccountTransaction.AccountNumber;
            remitToAccountDetails.BeneficiaryAddress1 = remitToAccountTransaction.BeneficiaryAddress;
            remitToAccountDetails.BeneficiaryFirstName = remitToAccountTransaction.BeneficiaryFirstName;
            remitToAccountDetails.BeneficiaryLastName = remitToAccountTransaction.BeneficiaryLastName;
            remitToAccountDetails.BeneficiaryMiddleName = remitToAccountTransaction.BeneficiaryMiddleName;
            remitToAccountDetails.OriginalAmount = remitToAccountTransaction.PrincipalAmount;
            remitToAccountDetails.OriginalCurrency = "PHP"; //TODO: Get currency from data, not hard coded
            remitToAccountDetails.Rate = 1;
            remitToAccountDetails.NetAmount = remitToAccountTransaction.PrincipalAmount * remitToAccountDetails.Rate;
            remitToAccountDetails.NetCurrency = "PHP";
            remitToAccountDetails.RemitterAddress1 = remitToAccountTransaction.SenderClient.Address;
            remitToAccountDetails.RemitterCountry = "PH"; //TODO: Get country code (iso alpha-2) from db
            remitToAccountDetails.RemitterFirstName = remitToAccountTransaction.SenderClient.FirstName;
            remitToAccountDetails.RemitterLastName = remitToAccountTransaction.SenderClient.LastName;
            remitToAccountDetails.RemitterMiddleName = remitToAccountTransaction.SenderClient.MiddleName;
            remitToAccountDetails.SourceOfFundsCode = String.Empty;
            remitToAccountDetails.ReferenceNumber = remitToAccountTransaction.ControlNumber;

            RemitToAccountTransaction partnerRemitToAccountTransaction = new RemitToAccountTransaction();
            partnerRemitToAccountTransaction.Load(remitToAccountTransaction);

			if (remitToAccountTransaction.RemitToAccountStatus.RemitToAccountStatusID != RemitToAccountStatusID.Successful
				&& remitToAccountTransaction.RemitToAccountStatus.RemitToAccountStatusID != RemitToAccountStatusID.SuccessfulWithIssue)
			{

				PeraLinkCoreWcf.UpdateRemitToAccountTransactionStatusRequest updateRemitToAccountTransactionStatusRequest = new PeraLinkCoreWcf.UpdateRemitToAccountTransactionStatusRequest();
				updateRemitToAccountTransactionStatusRequest.AuditTrail = auditTrail;
				updateRemitToAccountTransactionStatusRequest.RemitToAccountTransaction = remitToAccountTransaction;

				string bankPartnerCode = string.Empty;
				string bankAgentCode = string.Empty;

				string[] bankInfo = remitToAccountTransaction.AgentBank.AgentCode.Split('-');
				if (bankInfo != null && bankInfo.Length == 2)
				{
					bankPartnerCode = bankInfo[0];
					bankAgentCode = bankInfo[1];
				}
				else
				{
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(8);
					throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
				}


				if (bankPartnerCode.ToUpper() == SystemSetting.MetrobankPartnerCode)
				{
                    PeraLinkRemitToAccountWcf.MetrobankTransactionRequest metrobankTransactionRequest = new PeraLinkRemitToAccountWcf.MetrobankTransactionRequest();
                    metrobankTransactionRequest.RemitToAccount = remitToAccountDetails;
                    metrobankTransactionRequest.BeneficiaryMobileOrTelNumber = remitToAccountTransaction.BeneficiaryMobileNumber;

                    PeraLinkRemitToAccountWcf.MetrobankTransactionResult metrobankTransactionResult = new PeraLinkRemitToAccountWcf.MetrobankTransactionResult();
                    using (PeraLinkRemitToAccountWcfClient peraLinkRemitToAccount = new PeraLinkRemitToAccountWcfClient())
                    {
                        metrobankTransactionResult = peraLinkRemitToAccount.MetrobankTransaction(metrobankTransactionRequest);

                        updateRemitToAccountTransactionStatusRequest.RemitToAccountTransaction.PartnerReferenceNumber = String.Empty;
                        updateRemitToAccountTransactionStatusRequest.RemitToAccountTransaction.PartnerStatusCode = metrobankTransactionResult.ResponseCode;

                        switch (metrobankTransactionResult.ResultStatus)
                        {
                            case ResultStatus.Successful:
                                {
                                    if (metrobankTransactionResult.SuspectedTransaction)
                                    {
                                        if (metrobankTransactionResult.ResponseCode == "TMOUT")
                                        {
                                            updateRemitToAccountTransactionStatusRequest.RemitToAccountTransaction.RemitToAccountStatus.RemitToAccountStatusID = RemitToAccountStatusID.SuccessfulDueToTimeout;
                                        }
                                        SystemUtility.SendEmailAdvisory(partnerRemitToAccountTransaction, string.Format("{0}: {1}", metrobankTransactionResult.ResponseCode, metrobankTransactionResult.Message));
                                    }
                                    else
                                    {
                                        updateRemitToAccountTransactionStatusRequest.RemitToAccountTransaction.RemitToAccountStatus.RemitToAccountStatusID = RemitToAccountStatusID.Successful;
                                    }
                                    break;
                                }
                            case ResultStatus.Failed:
                            case ResultStatus.Error:
                                {
                                    updateRemitToAccountTransactionStatusRequest.RemitToAccountTransaction.RemitToAccountStatus.RemitToAccountStatusID = RemitToAccountStatusID.Failed;
                                    break;
                                }
                        }
                    }

				}
				else if (bankPartnerCode.ToUpper() == SystemSetting.SecurityBankPartnerCode)
				{
                    PeraLinkRemitToAccountWcf.SecurityBankTransactionRequest securityBankTransactionRequest = new PeraLinkRemitToAccountWcf.SecurityBankTransactionRequest();
                    securityBankTransactionRequest.BankCode = bankAgentCode;
                    securityBankTransactionRequest.BeneficiaryCity = remitToAccountTransaction.BeneficiaryAddress;
                    securityBankTransactionRequest.RemitterCity = remitToAccountTransaction.SenderClient.ProvinceAddress;
                    securityBankTransactionRequest.RemitToAccount = remitToAccountDetails;

                    PeraLinkRemitToAccountWcf.SecurityBankTransactionResult securityBankTransactionResult = new PeraLinkRemitToAccountWcf.SecurityBankTransactionResult();
                    using (PeraLinkRemitToAccountWcfClient peraLinkRemitToAccount = new PeraLinkRemitToAccountWcfClient())
                    {
                        securityBankTransactionResult = peraLinkRemitToAccount.SecurityBankTransaction(securityBankTransactionRequest);

                        updateRemitToAccountTransactionStatusRequest.RemitToAccountTransaction.PartnerReferenceNumber = securityBankTransactionResult.TransactionNumber;
                        updateRemitToAccountTransactionStatusRequest.RemitToAccountTransaction.PartnerStatusCode = securityBankTransactionResult.ResponseCode;

                        switch (securityBankTransactionResult.ResultStatus)
                        {
                            case ResultStatus.Successful:
                                {
                                    if (securityBankTransactionResult.SuspectedTransaction)
                                    {
                                        if (securityBankTransactionResult.ResponseCode == "TMOUT")
                                            updateRemitToAccountTransactionStatusRequest.RemitToAccountTransaction.RemitToAccountStatus.RemitToAccountStatusID = RemitToAccountStatusID.SuccessfulDueToTimeout;
                                        else
                                            updateRemitToAccountTransactionStatusRequest.RemitToAccountTransaction.RemitToAccountStatus.RemitToAccountStatusID = RemitToAccountStatusID.SuccessfulWithIssue;

                                        SystemUtility.SendEmailAdvisory(partnerRemitToAccountTransaction, string.Format("{0}: {1}", securityBankTransactionResult.ResponseCode, securityBankTransactionResult.Message));
                                    }
                                    else
                                    {
                                        updateRemitToAccountTransactionStatusRequest.RemitToAccountTransaction.RemitToAccountStatus.RemitToAccountStatusID = RemitToAccountStatusID.Successful;
                                    }
                                    break;
                                }
                            case ResultStatus.Failed:
                            case ResultStatus.Error:
                                {
                                    updateRemitToAccountTransactionStatusRequest.RemitToAccountTransaction.RemitToAccountStatus.RemitToAccountStatusID = RemitToAccountStatusID.Failed;
                                    break;
                                }
                        }
                    }
				}
				else
				{
					//Bank has no remit to account API set.
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(37);
					throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
				}
				
				PeraLinkCoreWcf.UpdateRemitToAccountTransactionStatusResult updateRemitToAccountTransactionStatusResult = serviceClient.UpdateRemitToAccountTransactionStatus(updateRemitToAccountTransactionStatusRequest);
				remitToAccountTransaction = serviceClient.FindRemitToAccountTransaction(remitToAccountTransaction);
			}
			returnValue.RemitToAccountTransaction = new RemitToAccountTransaction();
			returnValue.RemitToAccountTransaction.Load(remitToAccountTransaction);
			switch (returnValue.RemitToAccountTransaction.RemitToAccountStatusID)
			{
				case RemitToAccountStatusID.SuccessfulDueToTimeout:
				case RemitToAccountStatusID.SuccessfulWithIssue:
					returnValue.RemitToAccountTransaction.RemitToAccountStatusID = RemitToAccountStatusID.Successful;
					returnValue.RemitToAccountTransaction.RemitToAccountStatusDescription = "SUCCESSFUL";
					break;
				default:
					break;
			}
		}
		returnValue.ResultStatus = ResultStatus.Successful;
		return returnValue;
	
	}
	#endregion
}