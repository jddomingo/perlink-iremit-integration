using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Currency
/// </summary>
public class Currency
{
    #region Constructor
    public Currency()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private int _currencyID;

    public int CurrencyID
    {
        get { return _currencyID; }
        set { _currencyID = value; }
    }

    private string _code;

    public string Code
    {
        get { return _code; }
        set { _code = value; }
    }

    private string _description;

    public string Description
    {
        get { return _description; }
        set { _description = value; }
    }
    #endregion

    #region Public Methods
    public void Load(PeraLinkCoreWcf.Currency currency)
    {
        this.CurrencyID = currency.CurrencyID;
        this.Description = currency.Description;
        this.Code = currency.Code;
    }
    #endregion
}