using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CimbWcfClient : IDisposable
{
    #region Constructor
	public CimbWcfClient()
    {
        _serviceClient = new PeraLinkCimbWcf.ServiceClient();
    }
    #endregion

    #region Fields/Properties
    private PeraLinkCimbWcf.ServiceClient _serviceClient;
    #endregion

    #region Public Methods

    public PeraLinkCimbWcf.LookupTransactionResult LookupTransaction(PeraLinkCimbWcf.LookupTransactionRequest lookUpTransactionRequest)
    {

        PeraLinkCimbWcf.LookupTransactionResult lookupTransactionResult = _serviceClient.LookUpTransaction(lookUpTransactionRequest);

        switch (lookupTransactionResult.ResultCode)
        {
            case PeraLinkCimbWcf.LookupTransactionResultCode.Successful:
                {
                    return lookupTransactionResult;
                }
            case PeraLinkCimbWcf.LookupTransactionResultCode.PartnerError:
            case PeraLinkCimbWcf.LookupTransactionResultCode.ServerError:
            case PeraLinkCimbWcf.LookupTransactionResultCode.UnrecognizedResponse:
            case PeraLinkCimbWcf.LookupTransactionResultCode.Unsuccessful:
                {
                    throw new RDFramework.ClientException(lookupTransactionResult.MessageToClient, (int)lookupTransactionResult.ResultCode);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(lookupTransactionResult.ResultCode.ToString());
                }
        }
    }

    public PeraLinkCimbWcf.PayoutTransactionResult PayoutTransaction(PeraLinkCimbWcf.PayoutTransactionRequest payoutTransactionRequest)
    {
        PeraLinkCimbWcf.PayoutTransactionResult payoutTransactionResult = _serviceClient.PayoutTransaction(payoutTransactionRequest);

        switch (payoutTransactionResult.ResultCode)
        {
            case PeraLinkCimbWcf.PayoutTransactionResultCode.Successful:
                {
                    return payoutTransactionResult;
                }
            case PeraLinkCimbWcf.PayoutTransactionResultCode.PartnerError:
            case PeraLinkCimbWcf.PayoutTransactionResultCode.ServerError:
            case PeraLinkCimbWcf.PayoutTransactionResultCode.UnrecognizedResponse:
            case PeraLinkCimbWcf.PayoutTransactionResultCode.Unsuccessful:
                {
                    throw new RDFramework.ClientException(payoutTransactionResult.MessageToClient, (int)payoutTransactionResult.ResultCode);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(payoutTransactionResult.ResultCode.ToString());
                }
        }
    }

    public static PeraLinkCoreWcf.RemittanceStatus MapStatus(PeraLinkCimbWcf.TransactionStatus transactionStatus)
    {
        switch (transactionStatus)
        {
            case PeraLinkCimbWcf.TransactionStatus.ForPayout:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 1;
                    returnValue.Description = "Outstanding";

                    return returnValue;
                }
            case PeraLinkCimbWcf.TransactionStatus.PaidOut:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 2;
                    returnValue.Description = "Paid";

                    return returnValue;
                }
            case PeraLinkCimbWcf.TransactionStatus.Cancelled:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 4;
                    returnValue.Description = "Cancelled";

                    return returnValue;
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(transactionStatus.ToString());
                }
        }
    }
    #endregion

    void IDisposable.Dispose()
    {
        bool isClosed = false;

        try
        {
            if (this._serviceClient.State != System.ServiceModel.CommunicationState.Faulted)
            {
                this._serviceClient.Close();
                isClosed = true;
            }
            else
            {
                // Proceed with Abort in finally
            }
        }
        finally
        {
            if (!isClosed)
            {
                this._serviceClient.Abort();
            }
            else
            {
                // Do nothing since state is already closed
            }
        }
    }
}