#define INSURANCE_MICARE
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ServiceTypeID
/// </summary>
public class ServiceTypeID
{
    #region Constructor
    public ServiceTypeID()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    public const int SendRemittance = 1;
    public const int PayoutRemittance = 2;
    public const int CancelRemittance = 3;
    public const int RefundRemittance = 4;
    public const int AmendRemittance = 5;
	public const int RemitToAccount = 6;
    public const int ELoad = 8;
    public const int BillsPayment = 9;
#if INSURANCE_MICARE
    public const int Insurance = 12;
#else
#endif
    #endregion
}