using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class SendInternationalRemittanceResult: BaseResult
{
    #region Constructor
    public SendInternationalRemittanceResult()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private string _controlNumber;

    [DataMember]
    public string ControlNumber
    {
        get { return _controlNumber; }
        set { _controlNumber = value; }
    }
    private decimal _serviceFee;

    [DataMember]
    public decimal ServiceFee
    {
        get { return _serviceFee; }
        set { _serviceFee = value; }
    }
    #endregion
}