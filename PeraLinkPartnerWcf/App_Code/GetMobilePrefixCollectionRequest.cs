using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GetMobilePrefixCollectionRequest
/// </summary>
public class GetMobilePrefixCollectionRequest: BaseRequest
{
    #region Constructor
    public GetMobilePrefixCollectionRequest()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion

    #region Fields/Properties
    #endregion

    #region Internal Methods
    public GetMobilePrefixCollectionResult Process()
    {
        GetMobilePrefixCollectionResult returnValue = new GetMobilePrefixCollectionResult();
        PeraLinkELoadGatewayWcf.GetMobilePrefixCollectionResult getMobilePrefixCollectionResult = new PeraLinkELoadGatewayWcf.GetMobilePrefixCollectionResult();

        // Important: This function validates the request credential
        this.AuthenticateRequest();

        using (ELoadWSClient serviceClient = new ELoadWSClient())
        {
            PeraLinkELoadGatewayWcf.GetMobilePrefixCollectionRequest getMobilePrefixCollectionRequest = new PeraLinkELoadGatewayWcf.GetMobilePrefixCollectionRequest();
            getMobilePrefixCollectionResult = serviceClient.GetMobilePrefixCollection(getMobilePrefixCollectionRequest);


            returnValue.MobilePrefixCollection = new MobilePrefixCollection();
            returnValue.MobilePrefixCollection.AddList(getMobilePrefixCollectionResult.MobilePrefixCollection.ToArray());

            returnValue.ResultStatus = ResultStatus.Successful;

            return returnValue;

        }


    }
    #endregion
}