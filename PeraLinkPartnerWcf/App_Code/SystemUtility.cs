using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Newtonsoft.Json;
using System.Text;
using System.Net.Mail;
using System.Data;

/// <summary>
/// Summary description for SystemUtility
/// </summary>
public class SystemUtility
{
    #region Constructor
    public SystemUtility()
	{
		//

		//
    }
    #endregion

    #region Public Methods
    public static Exception BuildInternalError(int systemMessageID)
    {
        PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(1);
        return new Exception(message.Description);
    }

    public static RDFramework.ClientException BuildClientError(int systemMessageID)
    {
        PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(systemMessageID);
        return new RDFramework.ClientException(message.Description, message.SystemMessageID);
    }

    public static string GetClientIPAddress()
    {
        OperationContext context = OperationContext.Current;
        MessageProperties properties = context.IncomingMessageProperties;
        RemoteEndpointMessageProperty endpoint = properties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
        
        return endpoint.Address; 
    }

    public static Partner FindPartnerWithControlNumberFormat(string controlNumber)
    {
        ControlNumberFormat matchedFormat = SystemSetting.ControlNumberFormatCollection.Find(
            delegate(ControlNumberFormat eachControlNumberFormat)
            {
                return controlNumber.Length == eachControlNumberFormat.Length && controlNumber.ToUpper().StartsWith(eachControlNumberFormat.Prefix);
            });

        if (matchedFormat == null)
        {
            return null;
        }
        else
        {
            Partner returnValue = new Partner();
            returnValue.PartnerCode = matchedFormat.PartnerCode;

            return returnValue;
        }
    }
    public static Partner FindPartnerWithPartnerCodeFormat(string partnerCode)
    {
        Partner matchedFormat = SystemSetting.PartnerCollection.Find(
            delegate(Partner eachPartnerCodeFormat)
            {
                return partnerCode.ToUpper().StartsWith(eachPartnerCodeFormat.Prefix);
            });

        if (matchedFormat == null)
        {
            return null;
        }
        else
        {
            Partner returnValue = new Partner();
            returnValue.Prefix = matchedFormat.Prefix;
            return returnValue;
        }
    }

	public static string SuspectedRTATransMessage
	{
		get
		{
			return @"<span style='font-family:Calibri;font-size:16px;'>" +
				"Good Day!<br /><br />" +
				"Hi Support Desk,<br /><br />" +
				"Please refer to the table below for a reported RTA transaction with suspected status.<br />For your assistance please.<br /><br /><br /></span>";
		}
	}
	
	public static string SuspectedRTATransactionsBodyHeader
	{
		get
		{
			return "<table id=\"Table_01\" border=\"1\" cellpadding=\"5px\" cellspacing=\"0\" style=\"border:solid 2px gray; text-align:center \">" +
				   "<tr style=\"font-weight:bold;font-family:Calibri;font-size:16px;\">" +
					   "<td style=\"width: 160px;\" > Transaction Date </td> " +
					   "<td style=\"width: 150px;\"> Reference Number </td>" +
					   "<td style=\"width: 140px;\"> Account Number </td>" +
					   "<td style=\"width: 215px;\" > Sender Name </td> " +
					   "<td style=\"width: 220px;\" > Beneficiary Name </td> " +
					   "<td style=\"width: 175px;\" > Partner Status </td> " +
					   "<td style=\"width: 130px;\" > Amount </td> " +
					   "<td style=\"width: 220px;\" > PeraLink Partner </td> " +
					   "<td style=\"width: 220px;\" > PeraLink Agent </td> " +
				   "</tr>";
		}
	}

	public static string EmailBodyContent
	{
		get
		{
			return "<tr style=\"font-family:Calibri;font-size:16px;\">" +
					  "<td style=\"width: 160px;\" > {0} </td> " +
					   "<td style=\"width: 150px;\"> {1} </td>" +
					   "<td style=\"width: 140px;\"> {2} </td>" +
					   "<td style=\"width: 215px;\" > {3} </td> " +
					   "<td style=\"width: 220px;\" > {4} </td> " +
					   "<td style=\"width: 175px;\" > {5} </td> " +
					   "<td style=\"width: 130px;\" > {6} </td> " +
					   "<td style=\"width: 220px;\" > {7} </td> " +
					   "<td style=\"width: 220px;\" > {8} </td> " +
					"</tr>";

		}
	}
	
	public static string EmailBodyFooter
	{
		get
		{
			return @"</table><br />" +
				"<span style='font-family:Calibri;font-size:16px;'>" +
				"This is a system generated message. Please do not reply on this email.</span>";
		}
	}

    public static void SendEmailAdvisory(RemitToAccountTransaction remitToAccountTransaction, string responseMessage)
    {
        StringBuilder emailContent = new StringBuilder();

        emailContent.Append(SystemUtility.SuspectedRTATransMessage);
        emailContent.Append(SystemUtility.SuspectedRTATransactionsBodyHeader);

        string content = string.Format(SystemUtility.EmailBodyContent
                                        , remitToAccountTransaction.DateTimeSent
                                        , remitToAccountTransaction.ControlNumber
                                        , remitToAccountTransaction.AccountNumber
                                        , string.Format("{0} {1}", remitToAccountTransaction.SenderFirstName, remitToAccountTransaction.SenderLastName)
                                        , string.Format("{0} {1}", remitToAccountTransaction.BeneficiaryFirstName, remitToAccountTransaction.BeneficiaryLastName)
                                        , responseMessage
                                        , remitToAccountTransaction.PrincipalAmount
                                        , remitToAccountTransaction.SentByPartnerCode
                                        , remitToAccountTransaction.SentByAgentCode
                                        );

        emailContent.Append(content);

        emailContent.Append(SystemUtility.EmailBodyFooter);

        RDFramework.Email email = new RDFramework.Email(new SmtpClient());
        email.Body = emailContent.ToString();
        email.CCAddress = string.IsNullOrWhiteSpace(SystemSetting.EmailRecipientCC) ? null : SystemSetting.EmailRecipientCC.Split(',');
        email.IsHtml = true;
        email.Subject = string.Format(SystemSetting.EmailSubject, remitToAccountTransaction.AccountNumber);
        email.Priority = MailPriority.High;
        email.ToAddress = SystemSetting.EmailRecipientTO.Split(',');
        email.Send();
    }

    public static DataTable ConvertStringArrayToDataTable(string[][] stringArray)
    {
        DataTable Dtable = new DataTable();
        DataColumn Dcol = null;

        for (int xcnt = 0; xcnt < stringArray[0].Length; xcnt++)
        {
            Dcol = new DataColumn();
            Dcol.DataType = System.Type.GetType("System.String");
            Dcol.ColumnName = stringArray[0][xcnt].ToString();
            Dtable.Columns.Add(Dcol);
        }

        DataRow Drow = Dtable.NewRow();

        for (int xcnt = 0; xcnt < stringArray[0].Length; xcnt++)
        {
            if (stringArray[1][xcnt] != null)
            {
                Drow[stringArray[0][xcnt]] = stringArray[1][xcnt];
            }
            else
            {
                Drow[stringArray[0][xcnt]] = "";
            }
        }

        Dtable.Rows.Add(Drow);

        return Dtable;
    }

    public static string GenerateRemittanceAuditTrail()
    {
        DataColumn dcol = new DataColumn();
        DataTable dtable = new DataTable();
        DataSet dset = new DataSet();

        dset.DataSetName = "DTRACKER";
        dtable.TableName = "TRACKER";

        dcol.ColumnName = "BRID";
        dcol.DataType = System.Type.GetType("System.Int64");
        dtable.Columns.Add(dcol);

        dcol = new DataColumn();
        dcol.ColumnName = "UserID";
        dcol.DataType = System.Type.GetType("System.Int64");
        dtable.Columns.Add(dcol);

        dcol = new DataColumn();
        dcol.ColumnName = "RoleID";
        dcol.DataType = System.Type.GetType("System.Int64");
        dtable.Columns.Add(dcol);

        dcol = new DataColumn();
        dcol.ColumnName = "ModuleID";
        dcol.DataType = System.Type.GetType("System.Int64");
        dtable.Columns.Add(dcol);

        dcol = new DataColumn();
        dcol.ColumnName = "IPAdd";
        dcol.DataType = System.Type.GetType("System.String");
        dtable.Columns.Add(dcol);

        dcol = new DataColumn();
        dcol.ColumnName = "MACADD";
        dcol.DataType = System.Type.GetType("System.String");
        dtable.Columns.Add(dcol);

        DataRow drow = dtable.NewRow();

        drow[0] = 0;
        drow[1] = 0;
        drow[2] = 0;
        drow[3] = 0;
        drow[4] = SystemUtility.GetClientIPAddress();
        drow[5] = string.Empty;

        dtable.Rows.Add(drow);
        dset.Tables.Add(dtable);

        return dset.GetXml().ToString();
    }

    #endregion
}