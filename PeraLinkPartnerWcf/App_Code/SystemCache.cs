using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Caching;

/// <summary>
/// Summary description for SystemCache
/// </summary>
public class SystemCache
{
    #region Constructor
    public SystemCache()
	{
        _cacheItemPolicy = new CacheItemPolicy();
    }
    #endregion

    #region Fields/Properties
    private static ObjectCache _objectCache = MemoryCache.Default;
    private CacheItemPolicy _cacheItemPolicy;
    #endregion

    #region Public Methods
    public string GetPartnerKey(PeraLinkCoreWcf.Partner partner)
    {
        return string.Format("Partner-{0}", partner.PartnerCode);
    }

    public string GetServiceCredentialKey(ServiceCredential serviceCredential)
    {
        return string.Format("ServiceCredential-{0}", serviceCredential.UserID);
    }

    public void InsertPartner(PeraLinkCoreWcf.Partner partner)
    {
        string key = GetPartnerKey(partner);
        _cacheItemPolicy.RemovedCallback = new CacheEntryRemovedCallback(CachedItemRemovedCallback);
        _cacheItemPolicy.SlidingExpiration = new TimeSpan(0, 20, 0);
        _objectCache.Add(key, partner, _cacheItemPolicy);
    }

    public void InsertServiceCredential(ServiceCredential serviceCredential)
    {
        string key = GetServiceCredentialKey(serviceCredential);
        _cacheItemPolicy.RemovedCallback = new CacheEntryRemovedCallback(CachedItemRemovedCallback);
        _cacheItemPolicy.SlidingExpiration = new TimeSpan(0, 20, 0);
        _objectCache.Add(key, serviceCredential, _cacheItemPolicy);
    }

    public PeraLinkCoreWcf.Partner GetPartner(PeraLinkCoreWcf.Partner partner)
    {
        string key = GetPartnerKey(partner);

        if (_objectCache.Contains(key))
        {
            return _objectCache.Get(key) as PeraLinkCoreWcf.Partner;
        }
        else
        {
            return null;
        }
    }

    public ServiceCredential GetServiceCredential(ServiceCredential serviceCredential)
    {
        string key = GetServiceCredentialKey(serviceCredential);

        if (_objectCache.Contains(key))
        {
            return _objectCache.Get(key) as ServiceCredential;
        }
        else
        {
            return null;
        }
    }
    #endregion

    #region Private Methods
    private void CachedItemRemovedCallback(CacheEntryRemovedArguments cacheEntryRemovedArguments)
    {
       
        RDFramework.Utility.EventLog.SaveInformation(string.Format("Key: {0}\r\n{1}",cacheEntryRemovedArguments.CacheItem.Key, cacheEntryRemovedArguments.ToString()));
    }
    #endregion
}