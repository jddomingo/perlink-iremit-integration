using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PartnerTypeID
/// </summary>
public class PartnerTypeID
{
    #region Constructor
    public PartnerTypeID()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    public const int Domestic = 1;
    public const int International = 2;
    #endregion
}