using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class TrangloWcfClient: IDisposable
{
    #region Constructor
    public TrangloWcfClient()
	{
        _serviceClient = new PeraLinkTrangloWcf.ServiceClient();
    }
    #endregion

    #region Fields/Properties
    private PeraLinkTrangloWcf.ServiceClient _serviceClient;
    #endregion

    #region Public Methods
    public PeraLinkTrangloWcf.FindTransactionResult FindTransaction(PeraLinkTrangloWcf.FindTransactionRequest findTransactionRequest)
    {
        findTransactionRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkTrangloWcf.FindTransactionResult returnValue = _serviceClient.FindTransaction(findTransactionRequest);

        switch (returnValue.ResultStatus)
        {
            case ResultStatus.Successful:
                return returnValue;
            case ResultStatus.Failed:
            case ResultStatus.Error:
                throw new RDFramework.ClientException(returnValue.Message);
            default:
                    throw new ArgumentOutOfRangeException(returnValue.ResultStatus.ToString());
        }
    }

    public PeraLinkTrangloWcf.PayoutTransactionResult PayoutTransaction(PeraLinkTrangloWcf.PayoutTransactionRequest payoutTransactionRequest)
    {
        payoutTransactionRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkTrangloWcf.PayoutTransactionResult returnValue = _serviceClient.PayoutTransaction(payoutTransactionRequest);

        switch (returnValue.ResultStatus)
        {
            case ResultStatus.Successful:
                return returnValue;
            case ResultStatus.Failed:
            case ResultStatus.Error:
                throw new RDFramework.ClientException(returnValue.Message);
            default:
                throw new ArgumentOutOfRangeException(returnValue.ResultStatus.ToString());
        }
    }

    public static PeraLinkCoreWcf.RemittanceStatus MapStatus(PeraLinkTrangloWcf.TransactionStatus transactionStatus)
    {
        PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();

        switch (transactionStatus)
        {
            case PeraLinkTrangloWcf.TransactionStatus.ForPayout:
                    returnValue.RemittanceStatusID = 1;
                    returnValue.Description = "Outstanding";
                    break;
            case PeraLinkTrangloWcf.TransactionStatus.PaidOut:
            case PeraLinkTrangloWcf.TransactionStatus.ProcessedForPayout:
                    returnValue.RemittanceStatusID = 2;
                    returnValue.Description = "Paid";
                    break;
            default:
                    throw new ArgumentOutOfRangeException(transactionStatus.ToString());
        }

        return returnValue;
    }
    #endregion

    void IDisposable.Dispose()
    {
        bool isClosed = false;

        try
        {
            if (this._serviceClient.State != System.ServiceModel.CommunicationState.Faulted)
            {
                this._serviceClient.Close();
                isClosed = true;
            }
            else
            {
                // Proceed with Abort in finally
            }
        }
        finally
        {
            if (!isClosed)
            {
                this._serviceClient.Abort();
            }
            else
            {
                // Do nothing since state is already closed
            }
        }
    }
}