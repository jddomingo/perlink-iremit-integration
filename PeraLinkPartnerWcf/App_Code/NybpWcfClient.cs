using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class NybpWcfClient: IDisposable
{
    #region Constructor
    public NybpWcfClient()
	{
        _serviceClient = new NybpWcf.ServiceClient();
    }
    #endregion

    #region Fields/Properties
    private NybpWcf.ServiceClient _serviceClient;
    #endregion

    #region Public Methods

    public NybpWcf.LookupTransactionResult LookupTransaction(NybpWcf.LookupTransactionRequest lookUpTransactionRequest)
    {

        NybpWcf.LookupTransactionResult lookupTransactionResult = _serviceClient.LookUpTransaction(lookUpTransactionRequest);

        switch (lookupTransactionResult.ResultCode)
        {
            case NybpWcf.LookupTransactionResultCode.Successful:
                {
                    return lookupTransactionResult;
                }       
            case NybpWcf.LookupTransactionResultCode.PartnerError:
            case NybpWcf.LookupTransactionResultCode.ServerError:
            case NybpWcf.LookupTransactionResultCode.UnrecognizedResponse:
            case NybpWcf.LookupTransactionResultCode.Unsuccessful:
                {
                    throw new RDFramework.ClientException(lookupTransactionResult.MessageToClient, (int)lookupTransactionResult.ResultCode);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(lookupTransactionResult.ResultCode.ToString());
                }
        }
    }

    public NybpWcf.PayoutTransactionResult PayoutTransaction(NybpWcf.PayoutTransactionRequest payoutTransactionRequest)
    {
        NybpWcf.PayoutTransactionResult payoutTransactionResult = _serviceClient.PayoutTransaction(payoutTransactionRequest);

        switch (payoutTransactionResult.ResultCode)
        {
            case NybpWcf.PayoutTransactionResultCode.Successful:
                {
                    return payoutTransactionResult;
                }
            case NybpWcf.PayoutTransactionResultCode.PartnerError:
            case NybpWcf.PayoutTransactionResultCode.ServerError:
            case NybpWcf.PayoutTransactionResultCode.UnrecognizedResponse:
            case NybpWcf.PayoutTransactionResultCode.Unsuccessful:
                {
                    throw new RDFramework.ClientException(payoutTransactionResult.MessageToClient, (int)payoutTransactionResult.ResultCode);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(payoutTransactionResult.ResultCode.ToString());
                }
        }
    }

    public static PeraLinkCoreWcf.RemittanceStatus MapStatus(NybpWcf.TransactionStatus transactionStatus)
    {
        switch (transactionStatus)
        {
            case NybpWcf.TransactionStatus.ForPayout:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 1;
                    returnValue.Description = "Outstanding";

                    return returnValue;
                }
            case NybpWcf.TransactionStatus.PaidOut:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 2;
                    returnValue.Description = "Paid";

                    return returnValue;
                }
            case NybpWcf.TransactionStatus.Cancelled:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 4;
                    returnValue.Description = "Cancelled";

                    return returnValue;
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(transactionStatus.ToString());
                }
        }
    }
    #endregion

    void IDisposable.Dispose()
    {
        bool isClosed = false;

        try
        {
            if (this._serviceClient.State != System.ServiceModel.CommunicationState.Faulted)
            {
                this._serviceClient.Close();
                isClosed = true;
            }
            else
            {
                // Proceed with Abort in finally
            }
        }
        finally
        {
            if (!isClosed)
            {
                this._serviceClient.Abort();
            }
            else
            {
                // Do nothing since state is already closed
            }
        }
    }
}