using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for GetBankCollectionResult
/// </summary>
/// 
[DataContract]
public class GetBankCollectionResult : ServiceResult
{
	#region Constructor
	public GetBankCollectionResult()
	{
		//

		//
	}
	#endregion

	#region Fields/Properties
	private BankCollection _bankCollection;
	[DataMember]
	public BankCollection BankCollection
	{
		get { return _bankCollection; }
		set { _bankCollection = value; }
	}
	#endregion
}