﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for GetInsuranceTransactionHistoryResult
/// </summary>
public class GetInsuranceTransactionHistoryResult:ServiceResult
{
    #region Constructor
    public GetInsuranceTransactionHistoryResult()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion

    #region Fields/Properties
    private InsuranceCollection _isuranceCollection;
    [DataMember]
    public InsuranceCollection InsuranceCollection
    {
        get { return _isuranceCollection; }
        set { _isuranceCollection = value; }
    }

    private int _totalPageCount;
    [DataMember]
    public int TotalPageCount
    {
        get { return _totalPageCount; }
        set { _totalPageCount = value; }
    }
    #endregion

}
