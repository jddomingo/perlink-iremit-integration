using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class TransactionReport
{
    #region Constructor
    public TransactionReport()
	{ }
    #endregion

    #region Fields/Properties
    private string _controlNumber;

    public string ControlNumber
    {
        get { return _controlNumber; }
        set { _controlNumber = value; }
    }

    private string _sender;

    public string Sender
    {
        get { return _sender; }
        set { _sender = value; }
    }

    private string _receiver;

    public string Receiver
    {
        get { return _receiver; }
        set { _receiver = value; }
    }

    private decimal _principalAmount;

    public decimal PrincipalAmount
    {
        get { return _principalAmount; }
        set { _principalAmount = value; }
    }

    private decimal _serviceFees;

    public decimal ServiceFees
    {
        get { return _serviceFees; }
        set { _serviceFees = value; }
    }

    private decimal _total;

    public decimal Total
    {
        get { return _total; }
        set { _total = value; }
    }

    private string _remittanceStatusDescription;

    public string RemittanceStatusDescription
    {
        get { return _remittanceStatusDescription; }
        set { _remittanceStatusDescription = value; }
    }

    private DateTime _dateTimeUpdated;

    public DateTime DateTimeUpdated
    {
        get { return _dateTimeUpdated; }
        set { _dateTimeUpdated = value; }
    }

    private DateTime _dateTimeSent;

    public DateTime DateTimeSent
    {
        get { return _dateTimeSent; }
        set { _dateTimeSent = value; }
    }

    private DateTime _amendDateTimeCreated;

    public DateTime AmendDateTimeCreated
    {
        get { return _amendDateTimeCreated; }
        set { _amendDateTimeCreated = value; }
    }

    #endregion

    #region Public Methods
    public void Load(PeraLinkCoreWcf.TransactionReport transactionReport)
    {
        this.ControlNumber = transactionReport.ControlNumber;
        this.PrincipalAmount = transactionReport.PrincipalAmount;
        this.Receiver = transactionReport.Receiver;
        this.Sender = transactionReport.Sender;
        this.ServiceFees = transactionReport.ServiceFees;
        this.Total = transactionReport.Total;
        this.RemittanceStatusDescription = transactionReport.RemittanceStatusDescription;
        this.DateTimeUpdated = transactionReport.DateTimeUpdated;
        this.DateTimeSent = transactionReport.DateTimeSent;

        this.AmendDateTimeCreated = transactionReport.AmendDateTimeCreated;
    }
    #endregion
}