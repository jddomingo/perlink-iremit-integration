using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class FindBeneficiaryResult: BaseResult
{
    #region Constructor
    public FindBeneficiaryResult()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private BeneficiaryCollection _beneficiaryCollection;
    [DataMember]
    public BeneficiaryCollection BeneficiaryCollection
    {
        get { return _beneficiaryCollection; }
        set { _beneficiaryCollection = value; }
    }
    #endregion
}