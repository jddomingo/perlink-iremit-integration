﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for FindClientByCellphoneNumberResult
/// </summary>
public class FindClientByClientNumberResult:BaseResult
{
    #region Constructor
    public FindClientByClientNumberResult()
	{
	}
    #endregion

    #region Fields/Properties
    private ClientCollection _clientMobileCollection;
    [DataMember]
    public ClientCollection ClientCollection
    {
        get { return _clientMobileCollection; }
        set { _clientMobileCollection = value; }
    }
    #endregion
}