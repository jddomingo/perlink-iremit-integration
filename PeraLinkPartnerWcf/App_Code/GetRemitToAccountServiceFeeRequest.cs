using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;


public class GetRemitToAccountServiceFeeRequest: BaseRequest
{
	#region Constructor
	public GetRemitToAccountServiceFeeRequest()
	{ }
	#endregion

	#region Fields/Properties
	private string _partnerCode;
	[DataMember]
	public string PartnerCode
	{
		get { return _partnerCode; }
		set { _partnerCode = value; }
	}

	private string _token;
	[DataMember]
	public string Token
	{
		get { return _token; }
		set { _token = value; }
	}

	private string _agentCode;
	[DataMember]
	public string AgentCode
	{
		get { return _agentCode; }
		set { _agentCode = value; }
	}

	private decimal _principalAmount;
	[DataMember]
	public decimal PrincipalAmount
	{
		get { return _principalAmount; }
		set { _principalAmount = value; }
	}

	private Int64 _currencyID;
	[DataMember]
	public Int64 CurrencyID
	{
		get { return _currencyID; }
		set { _currencyID = value; }
	}
	#endregion

	#region Internal Methods
	internal GetRemitToAccountServiceFeeResult Process()
	{
		GetRemitToAccountServiceFeeResult returnValue = new GetRemitToAccountServiceFeeResult();

		// Important: This function validates the request credential
		this.AuthenticateRequest();

		using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
		{
			#region Validate Partner Record
			PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
			partner.PartnerCode = this.PartnerCode;
			partner = serviceClient.FindPartnerCode(partner);

			if (partner.PartnerID == 0)
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			else { }

			if (partner.Token == this.Token) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}

			if (partner.Activated) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			#endregion

			#region Validate Agent Record
			PeraLinkCoreWcf.Agent agent = new PeraLinkCoreWcf.Agent();
			agent.AgentCode = this.AgentCode;
			agent.Partner = partner;
			agent = serviceClient.FindAgentCodeOfPartner(agent);

			if (agent.AgentID == 0)
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			else
			{
				// Proceed
			}

			if (agent.Partner.PartnerID == partner.PartnerID)
			{ }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(43);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}

			if (agent.Activated) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(46);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			#endregion

			#region Validate Partner's Access To RTA Service
			PeraLinkCoreWcf.PartnerServiceMapping[] partnerServiceMappingCollection = serviceClient.GetPartnerServiceMappingCollection(partner);

			PeraLinkCoreWcf.PartnerServiceMapping partnerServiceMapping = Array.Find<PeraLinkCoreWcf.PartnerServiceMapping>(partnerServiceMappingCollection
				, delegate(PeraLinkCoreWcf.PartnerServiceMapping eachPartnerServiceMapping)
				{
					return eachPartnerServiceMapping.ServiceType.ServiceTypeID == ServiceTypeID.RemitToAccount;
				});


			if (partnerServiceMapping == null)
			{
				PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(36);
				throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
			}
			else
			{
				// Proceed
			}
			#endregion

			#region Validate Partner Currency
			PeraLinkCoreWcf.PartnerServiceCurrency[] partnerServiceCurrencyCollection = serviceClient.GetPartnerServiceCurrencyCollection(partnerServiceMapping);

			PeraLinkCoreWcf.PartnerServiceCurrency rtaCurrency = Array.Find<PeraLinkCoreWcf.PartnerServiceCurrency>(partnerServiceCurrencyCollection
				, delegate(PeraLinkCoreWcf.PartnerServiceCurrency eachPartnerServiceCurrency)
				{
					return eachPartnerServiceCurrency.Currency.CurrencyID == this.CurrencyID;
				});

			if (rtaCurrency == null)
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(4);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			else
			{
				// Proceed
			}
			#endregion

			#region Validate Partner Service Fee
			PeraLinkCoreWcf.PartnerServiceFee[] partnerServiceFeeCollection = serviceClient.GetPartnerServiceFeeCollection(rtaCurrency);

			if (partnerServiceFeeCollection.Length == 0)
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(5);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			else
			{
				// Proceed
			}

			decimal minSendAmount = SystemSetting.GetMinRemitToAccountAmount(rtaCurrency.Currency.Code);
			decimal maxSendAmount = SystemSetting.GetMaxRemitToAccountAmount(rtaCurrency.Currency.Code);

			if (minSendAmount <= this.PrincipalAmount && maxSendAmount >= this.PrincipalAmount)
			{
				// Proceed
			}
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(6);
				throw new RDFramework.ClientException(
					string.Format(message.Content, rtaCurrency.Currency.Code, minSendAmount, maxSendAmount)
					, message.MessageID);
			}

			PeraLinkCoreWcf.GetServiceFeeRequest getServiceFeeRequest = new PeraLinkCoreWcf.GetServiceFeeRequest();
			getServiceFeeRequest.PartnerServiceFee = new PeraLinkCoreWcf.PartnerServiceFee();
			getServiceFeeRequest.PartnerServiceFee.Amount = this.PrincipalAmount;
			getServiceFeeRequest.PartnerServiceFee.PartnerServiceCurrency = rtaCurrency;
			getServiceFeeRequest.PartnerServiceFee.PartnerRegionID = agent.PartnerRegionID;
			PeraLinkCoreWcf.PartnerServiceFee matchedServiceFee = serviceClient.GetServiceFee(getServiceFeeRequest);

			if (matchedServiceFee.PartnerServiceFeeID == 0)
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(7);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			else { }

			if (matchedServiceFee.DerivedFromSystem) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(28);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}

			if (matchedServiceFee.FeeType.FeeTypeID == FeeTypeID.Fixed)
			{
				returnValue.ServiceFee = matchedServiceFee.Fee;
			}
			else
			{
				returnValue.ServiceFee = this.PrincipalAmount * (matchedServiceFee.Fee / 100);
			}
			#endregion
		}
		returnValue.ResultStatus = ResultStatus.Successful;
		return returnValue;
	}
	#endregion
}