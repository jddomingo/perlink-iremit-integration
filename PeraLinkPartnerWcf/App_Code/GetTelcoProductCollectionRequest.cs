using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GetTelcoProductCollectionRequest
/// </summary>
public class GetTelcoProductCollectionRequest: BaseRequest
{
    #region Constructor
    public GetTelcoProductCollectionRequest()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion

    #region Fields/Properties
    private Telco _telco;

    public Telco Telco
    {
        get { return _telco; }
        set { _telco = value; }
    }
    #endregion


    #region Internal Methods
    public GetTelcoProductCollectionResult Process()
    {
        GetTelcoProductCollectionResult returnValue = new GetTelcoProductCollectionResult();
        PeraLinkELoadGatewayWcf.GetTelcoProductCollectionResult getTelcoProductCollectionResult = new PeraLinkELoadGatewayWcf.GetTelcoProductCollectionResult();

        // Important: This function validates the request credential
        this.AuthenticateRequest();

        using (ELoadWSClient serviceClient = new ELoadWSClient())
        {
            PeraLinkELoadGatewayWcf.GetTelcoProductCollectionRequest getTelcoProductCollectionRequest = new PeraLinkELoadGatewayWcf.GetTelcoProductCollectionRequest();
            getTelcoProductCollectionRequest.Telco = new PeraLinkELoadGatewayWcf.Telco();
            getTelcoProductCollectionRequest.Telco.IsActivated = this.Telco.IsActivated;
            getTelcoProductCollectionRequest.Telco.TelcoCode = this.Telco.TelcoCode;
            getTelcoProductCollectionRequest.Telco.TelcoName = this.Telco.TelcoName;
            getTelcoProductCollectionResult = serviceClient.GetTelcoProductCollection(getTelcoProductCollectionRequest);


            returnValue.TelcoProductCollection = new TelcoProductCollection();
            returnValue.TelcoProductCollection.AddList(getTelcoProductCollectionResult.TelcoProductCollection.ToArray());

            returnValue.ResultStatus = ResultStatus.Successful;

            return returnValue;

        }


    }
    #endregion
}