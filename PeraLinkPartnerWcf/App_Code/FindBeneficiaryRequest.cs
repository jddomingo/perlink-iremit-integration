using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
/// <summary>
/// Summary description for FindBeneficiaryFirstAndLastNameRequest
/// </summary>
[DataContract]
public class FindBeneficiaryRequest : BaseRequest
{
    #region Constructor
    public FindBeneficiaryRequest()
    {
        //

        //
    }
    #endregion

    #region Fields/Properties
    private string _firstName;
    [DataMember]
    public string FirstName
    {
        get { return _firstName; }
        set { _firstName = value; }
    }

    private string _lastName;
    [DataMember]
    public string LastName
    {
        get { return _lastName; }
        set { _lastName = value; }
    }

    private Int64 _senderClientID;
    [DataMember]
    public Int64 SenderClientID
    {
        get { return _senderClientID; }
        set { _senderClientID = value; }
    }

    private string _middleName;
    [DataMember]
    public string MiddleName
    {
        get { return _middleName; }
        set { _middleName = value; }
    }

    private DateTime? _birthDate;
    [DataMember]
    public DateTime? BirthDate
    {
        get { return _birthDate; }
        set { _birthDate = value; }
    }

    #endregion

    #region Internal Methods
    internal FindBeneficiaryResult Process()
    {
        FindBeneficiaryResult returnValue = new FindBeneficiaryResult();

        // Important: This function validates the request credential
        this.AuthenticateRequest();

        PeraLinkCoreWcf.Beneficiary beneficiary = new PeraLinkCoreWcf.Beneficiary();
        beneficiary.FirstName = this.FirstName;
        beneficiary.LastName = this.LastName;

        if (!String.IsNullOrEmpty(this.MiddleName))
        {
            beneficiary.MiddleName = this.MiddleName;
        }

        if (this.BirthDate != null && this.BirthDate != DateTime.MinValue)
        {
            beneficiary.BirthDate = this.BirthDate;
        }

        beneficiary.SenderClient = new PeraLinkCoreWcf.Client();
        beneficiary.SenderClient.ClientID = this.SenderClientID;

        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
            returnValue.BeneficiaryCollection = new BeneficiaryCollection();
            returnValue.BeneficiaryCollection.AddList(serviceClient.FindBeneficiary(beneficiary));
        }

        returnValue.ResultStatus = ResultStatus.Successful;

        return returnValue;
    }
    #endregion
}