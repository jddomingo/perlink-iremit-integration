using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ProcessLoadTransactionResult
/// </summary>

public class ProcessLoadTransactionResult: BaseResult
{
    #region Constructor
    public ProcessLoadTransactionResult()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion

    #region Fields/Properties
    private ELoadTransaction _eLoadTransaction;

    public ELoadTransaction ELoadTransaction
    {
        get { return _eLoadTransaction; }
        set { _eLoadTransaction = value; }
    }

    private ELoadStatus _eLoadStatus;

    public ELoadStatus ELoadStatus
    {
        get { return _eLoadStatus; }
        set { _eLoadStatus = value; }
    }

    private TelcoProduct _telcoProduct;

    public TelcoProduct TelcoProduct
    {
        get { return _telcoProduct; }
        set { _telcoProduct = value; }
    }

    private Telco _telco;

    public Telco Telco
    {
        get { return _telco; }
        set { _telco = value; }
    }


    private int _responseStatus;

    public int ResponseStatus
    {
        get { return _responseStatus; }
        set { _responseStatus = value; }
    }

    private int _responseCode;

    public int ResponseCode
    {
        get { return _responseCode; }
        set { _responseCode = value; }
    }

    private decimal _commissionFee;
    public decimal CommissionFee
    {
        get { return _commissionFee; }
        set { _commissionFee = value; }
    }
    #endregion
}