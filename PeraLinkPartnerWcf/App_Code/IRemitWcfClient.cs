﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;

/// <summary>
/// Summary description for IRemitWcfClient
/// </summary>
public class IRemitWcfClient : IDisposable
{
    #region Constructor
    public IRemitWcfClient()
	{
        _serviceClient = new PeraLinkIRemitWCF.ServiceClient();
    }
    #endregion
    #region Fields/Properties
    private PeraLinkIRemitWCF.ServiceClient _serviceClient;
      private string _ControlNumber;

      public string ControlNumber
      {
          get { return _ControlNumber; }
          set { _ControlNumber = value; }
      }

    #endregion

    #region public methods

    //public PeraLinkIRemitWCF.InquireTransactionResult InquireTransaction(PeraLinkIRemitWCF.InquireTransactionRequest inquireTransactionRequest)
    //{
    //    PeraLinkIRemitWCF.InquireTransactionResult returnValue = _serviceClient.InquireTransaction(inquireTransactionRequest);
    //    switch (returnValue.ResultStatus)
    //    {
    //        case ResultStatus.Successful:
    //            return returnValue;
    //        case ResultStatus.Failed:
    //        case ResultStatus.Error:
    //            throw new RDFramework.ClientException(returnValue.Message);
    //        default:
    //            throw new ArgumentOutOfRangeException(returnValue.ResultStatus.ToString());
    //    }
    //}

    //public PeraLinkIRemitWCF.PayoutTransactionResult PayoutTransaction(PeraLinkIRemitWCF.PayoutTransactionRequest payoutTransactionRequest)
    //{

    //    PeraLinkIRemitWCF.PayoutTransactionResult returnValue = _serviceClient.PeraLinkPartnerPayout(payoutTransactionRequest);

    //    switch (returnValue.ResultStatus)
    //    {
    //        case ResultStatus.Successful:
    //            return returnValue;
    //        case ResultStatus.Failed:
    //        case ResultStatus.Error:
    //            throw new RDFramework.ClientException(returnValue.Message);
    //        default:
    //            throw new ArgumentOutOfRangeException(returnValue.ResultStatus.ToString());
    //    }
    
    //}
      public static string PasswordHashing(HashCodeType strHashCode, string strSignature)
      {
          string strEncryptedPass = string.Empty;
          switch (strHashCode)
          {
              case HashCodeType.encMD5:
                  MD5CryptoServiceProvider MD5 = new MD5CryptoServiceProvider();
                  byte[] bs_Signature = Encoding.UTF8.GetBytes(strSignature);
                  bs_Signature = MD5.ComputeHash(bs_Signature);
                  System.Text.StringBuilder strBuildSignature = new System.Text.StringBuilder();
                  foreach (byte b in bs_Signature)
                  {
                      if (b < 16)
                      {
                          strBuildSignature.Append("0" + b.ToString("x").ToLower());
                      }
                      else
                      {
                          strBuildSignature.Append(b.ToString("x").ToLower());
                      }

                  }
                  strEncryptedPass = strBuildSignature.ToString();
                  break;
              default:
                  break;
          }
          return strEncryptedPass;
      }
      public enum HashCodeType
      {
          encSHA1 = 0
          , encMD5 = 1
      }
    

    public PeraLinkCoreWcf.InternationalRemittance InquireTransaction(string controlNumber)
    {
        PeraLinkCoreWcf.InternationalRemittance returnValue = new PeraLinkCoreWcf.InternationalRemittance();
        PeraLinkIRemitWCF.InquireTransactionRequest inquireTransactionRequest = new PeraLinkIRemitWCF.InquireTransactionRequest();
        
        inquireTransactionRequest.ReferenceNumber = controlNumber;
        inquireTransactionRequest.RequestToken = TokenAuth4.TokenAuth.Generate(SystemSetting.CryptoPrivateKey);
        //inquireTransactionRequest.Signature = hashedSignature;
        //inquireTransactionRequest.PSession = uniqueID.ToString();


        PeraLinkIRemitWCF.InquireTransactionResult inquireTransactionResult = _serviceClient.InquireTransaction(inquireTransactionRequest);

        switch(inquireTransactionResult.ResultStatus)
        {
                
            case ResultStatus.Successful:

                returnValue.Beneficiary = new PeraLinkCoreWcf.Beneficiary();
                returnValue.InternationalRemittanceID = 1;
                returnValue.Beneficiary.FirstName = inquireTransactionResult.BeneficiaryFirstName;
                returnValue.Beneficiary.LastName = inquireTransactionResult.BeneficiaryLastName;
                returnValue.Beneficiary.BirthDate = RDFramework.Utility.Validation.MinimumSqlDateTime;
                returnValue.DateTimeSent = DateTime.Now;
                returnValue.ControlNumber = controlNumber;
                returnValue.PrincipalAmount = inquireTransactionResult.PayoutAmount;
                returnValue.ServiceFee = inquireTransactionResult.PayoutAmountWithServiceCharge;
                returnValue.RemittanceStatus = MapStatus(inquireTransactionResult.TransactionStatus);
                returnValue.SendCurrency = new PeraLinkCoreWcf.Currency();
                returnValue.SendCurrency.CurrencyID = 6;
                returnValue.SendCurrency.Code = "PHP";
                returnValue.SenderClient = new PeraLinkCoreWcf.Client();
                returnValue.SenderClient.FirstName = inquireTransactionResult.SenderFirstName;
                returnValue.SenderClient.LastName = inquireTransactionResult.SenderLastName;
                returnValue.SenderClient.BirthDate = RDFramework.Utility.Validation.MinimumSqlDateTime;



                return returnValue;
            case ResultStatus.Error:
            case ResultStatus.Failed:
                throw new RDFramework.ClientException(inquireTransactionResult.Message, (int)inquireTransactionResult.MessageID);
            default:
                throw new ArgumentOutOfRangeException(inquireTransactionResult.Message);
        }
    }
    public PeraLinkIRemitWCF.PayoutTransactionResult PayoutTransaction(string controlNumber)
    {
        PeraLinkIRemitWCF.PayoutTransactionResult returnValue = new PeraLinkIRemitWCF.PayoutTransactionResult();
        PeraLinkIRemitWCF.PayoutTransactionRequest payoutTransactionRequest = new PeraLinkIRemitWCF.PayoutTransactionRequest();
        payoutTransactionRequest.TransactionNumber = controlNumber;
        //payoutTransactionRequest.Signature = Signature;
        //payoutTransactionRequest.PartnerSessionID = PartnerSessionID.ToString();
        payoutTransactionRequest.RequestToken = TokenAuth4.TokenAuth.Generate(SystemSetting.CryptoPrivateKey);

        returnValue = _serviceClient.PeraLinkPartnerPayout(payoutTransactionRequest);
        //returnValue.ResultStatus = ResultStatus.Successful;

        switch (returnValue.ResultStatus)
        {
            case ResultStatus.Successful:
                return returnValue;
            case ResultStatus.Error:
            case ResultStatus.Failed:
                RDFramework.Utility.EventLog.SaveError(string.Format("IREMIT: {0}|{1}", controlNumber, returnValue.ResultCode));
                throw new RDFramework.ClientException(returnValue.Message, (int)returnValue.MessageID);
            default:
                RDFramework.Utility.EventLog.SaveError(string.Format("IREMIT: {0}|{1}", controlNumber, returnValue.ResultCode));
                throw new ArgumentOutOfRangeException(returnValue.Message);
        }
    }
    public static PeraLinkCoreWcf.RemittanceStatus MapStatus(PeraLinkIRemitWCF.TransactionStatus iRemitStatus)
    {
        switch (iRemitStatus)
        {
            case PeraLinkIRemitWCF.TransactionStatus.UnrecognizedStatus:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 0;
                    returnValue.Description = "Unrecognized Status";

                    return returnValue;
                }
            case PeraLinkIRemitWCF.TransactionStatus.ForPayout:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 1;
                    returnValue.Description = "For Payout";

                    return returnValue;
                }
            case PeraLinkIRemitWCF.TransactionStatus.PaidOut:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 3;
                    returnValue.Description = "Paid Out";

                    return returnValue;
                }
            case PeraLinkIRemitWCF.TransactionStatus.Cancelled:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 4;
                    returnValue.Description = "Cancelled ";

                    return returnValue;
                }
            case PeraLinkIRemitWCF.TransactionStatus.ProcessedForPayout:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 5;
                    returnValue.Description = "ProcessedForPayout ";

                    return returnValue;
                }


            case PeraLinkIRemitWCF.TransactionStatus.Blocked:
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(97);
                    throw new RDFramework.ClientException(string.Format(message.Description, iRemitStatus.ToString()), message.SystemMessageID);
                }
            default:
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(86);
                    throw new RDFramework.ClientException(string.Format(message.Description, iRemitStatus.ToString()), message.SystemMessageID);
                }
        }
    }
    #endregion

    void IDisposable.Dispose()
    {
        bool isClosed = false;

        try
        {
            if (this._serviceClient.State != System.ServiceModel.CommunicationState.Faulted)
            {
                this._serviceClient.Close();
                isClosed = true;
            }
            else
            {
                // Proceed with Abort in finally
            }
        }
        finally
        {
            if (!isClosed)
            {
                this._serviceClient.Abort();
            }
            else
            {
                // Do nothing since state is already closed
            }
        }
    }
}