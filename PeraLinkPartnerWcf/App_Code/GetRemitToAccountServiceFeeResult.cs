using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;


public class GetRemitToAccountServiceFeeResult : ServiceResult
{
	#region Constructor
	public GetRemitToAccountServiceFeeResult()
	{ }
	#endregion

	#region Fields/Properties
	private decimal _serviceFee;
	[DataMember]
	public decimal ServiceFee
	{
		get { return _serviceFee; }
		set { _serviceFee = value; }
	}
	#endregion
}