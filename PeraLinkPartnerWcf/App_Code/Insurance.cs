﻿#define INSURANCE_MICARE
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
/// <summary>
/// Summary description for Insurance
/// </summary>
#if INSURANCE_MICARE
public class Insurance
{
#region Constructor
    public Insurance()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion

#region Fields/Properties
    private string _eCoc;
    [DataMember]
    public string ECoc
    {
        get { return _eCoc; }
        set { _eCoc = value; }
    }

    private string _clientMobileNumber;

    public string ClientMobileNumber
    {
        get { return _clientMobileNumber; }
        set { _clientMobileNumber = value; }
    }
    private Int64 _processedByTerminalID;
    [DataMember]
    public Int64 ProcessedByTerminalID
    {
        get { return _processedByTerminalID; }
        set { _processedByTerminalID = value; }
    }

    private Int64 _processedByAgentID;
    [DataMember]
    public Int64 ProcessedByAgentID
    {
        get { return _processedByAgentID; }
        set { _processedByAgentID = value; }
    }

    private string _processedByUserID;
    [DataMember]
    public string ProcessedByUserID
    {
        get { return _processedByUserID; }
        set { _processedByUserID = value; }
    }

    private string _ReferenceNumber;
    [DataMember]
    public string ReferenceNumber
    {
        get { return _ReferenceNumber; }
        set { _ReferenceNumber = value; }
    }

    private DateTime _DatetimeCreated;
    [DataMember]
    public DateTime DatetimeCreated
    {
        get { return _DatetimeCreated; }
        set { _DatetimeCreated = value; }
    }

    private string _processedByTerminalCode;
    [DataMember]
    public string ProcessedByTerminalCode
    {
        get { return _processedByTerminalCode; }
        set { _processedByTerminalCode = value; }
    }

    private string _processedByAgentCode;
    [DataMember]
    public string ProcessedByAgentCode
    {
        get { return _processedByAgentCode; }
        set { _processedByAgentCode = value; }
    }

    private decimal _premiumFee;
    [DataMember]
    public decimal PremiumFee
    {
        get { return _premiumFee; }
        set { _premiumFee = value; }
    }

    private string _clientFirstName;
    [DataMember]
    public string ClientFirstName
    {
        get { return _clientFirstName; }
        set { _clientFirstName = value; }
    }

    private string _clientLastName;
    [DataMember]
    public string ClientLastName
    {
        get { return _clientLastName; }
        set { _clientLastName = value; }
    }

    private string _clientMiddleName;
    [DataMember]
    public string ClientMiddleName
    {
        get { return _clientMiddleName; }
        set { _clientMiddleName = value; }
    }

    private decimal _adminFee;
    [DataMember]
    public decimal AdminFee
    {
        get { return _adminFee; }
        set { _adminFee = value; }
    }

    private int _status;
    [DataMember]
    public int Status
    {
        get { return _status; }
        set { _status = value; }
    }

    private string _statusDescription;
    [DataMember]
    public string StatusDescription
    {
        get { return _statusDescription; }
        set { _statusDescription = value; }
    }

    #endregion

#region Public Methods
    public void Load(PeraLinkCoreWcf.Insurance insurance)
    {
        this.DatetimeCreated = insurance.DateTimeCreated;
        this.ProcessedByAgentID = insurance.ProcessedByAgent.AgentID;
        this.ProcessedByTerminalID = insurance.ProcessedByTerminal.TerminalID;
        this.ProcessedByUserID = insurance.ProcessedByUserID;
        this.ProcessedByTerminalCode = insurance.ProcessedByTerminal.TerminalCode;
        this.ProcessedByAgentCode = insurance.ProcessedByAgent.AgentCode;
        this.ReferenceNumber = insurance.PeraLinkReferenceNumber;
        this.Status = insurance.Status.Status;
        this.StatusDescription = insurance.Status.Description;
        this.ClientMobileNumber = insurance.ClientMobileNumber;
        this.PremiumFee = insurance.PremiumFee;
        this.AdminFee = insurance.AdminFee;
        this.ClientLastName = insurance.LastName;
        this.ClientMiddleName = insurance.MiddleName;
        this.ClientFirstName = insurance.FirstName;
        this.ECoc = insurance.ECOC;
    }

    #endregion
}
#endif