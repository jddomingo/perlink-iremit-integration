using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for BaseResult
/// </summary>
[DataContract]
public class BaseResult
{
    #region Constructor
    public BaseResult()
    { }
    #endregion

    #region Fields/Properties
    private ResultStatus _resultStatus;

    [DataMember]
    public ResultStatus ResultStatus
    {
        get { return _resultStatus; }
        set { _resultStatus = value; }
    }

    private string _message;

    [DataMember]
    public string Message
    {
        get { return _message; }
        set { _message = value; }
    }

    private int _messageID;

    [DataMember]
    public int MessageID
    {
        get { return _messageID; }
        set { _messageID = value; }
    }

    private int _logID;

    [DataMember]
    public int LogID
    {
        get { return _logID; }
        set { _logID = value; }
    }
    #endregion
}