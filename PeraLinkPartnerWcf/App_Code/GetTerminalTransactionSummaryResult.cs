using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetTerminalTransactionSummaryResult: ServiceResult
{
	#region Constuctor
	public GetTerminalTransactionSummaryResult()
	{ }
	#endregion

	#region Fields/Properties
	private TerminalTransactionSummaryCollection _terminalTransactionSummaryCollection;
	[DataMember]
	public TerminalTransactionSummaryCollection TerminalTransactionSummaryCollection
	{
		get { return _terminalTransactionSummaryCollection; }
		set { _terminalTransactionSummaryCollection = value; }
	}

	private int _totalPageCount;
	[DataMember]
	public int TotalPageCount
	{
		get { return _totalPageCount; }
		set { _totalPageCount = value; }
	}
	#endregion
}