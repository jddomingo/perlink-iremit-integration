using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for GetEMoneyAccountDetailsResult
/// </summary>		
/// 

[DataContract]
public class GetEMoneyAccountDetailsResult: ServiceResult
{
	#region Constructor
	public GetEMoneyAccountDetailsResult()
	{ }
	#endregion

	#region Fields/Properties
	private string _accountName;
	public string AccountName
	{
		get { return _accountName; }
		set { _accountName = value; }
	}

	private decimal _totalCredit;
	public decimal TotalCredit
	{
		get { return _totalCredit; }
		set { _totalCredit = value; }
	}

	private decimal _totalDebit;
	public decimal TotalDebit
	{
		get { return _totalDebit; }
		set { _totalDebit = value; }
	}

	private decimal _currentBalance;
	public decimal CurrentBalance
	{
		get { return _currentBalance; }
		set { _currentBalance = value; }
	}

	private decimal _availableBalance;
	public decimal AvailableBalance
	{
		get { return _availableBalance; }
		set { _availableBalance = value; }
	}

    //private decimal _totalCommission;
    //public decimal TotalCommission
    //{
    //    get { return _totalCommission; }
    //    set { _totalCommission = value; }
    //}
    private decimal _totalCommission;

    public decimal TotalCommission
    {
        get { return _totalCommission; }
        set { _totalCommission = value; }
    }
	#endregion
}