using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetSummaryReportRequest: BaseRequest
{
	#region Constructor
	public GetSummaryReportRequest()
	{ }
	#endregion

	#region Fields/Properties
	private const int _pageSize = 10;

	private int _pageNumber;
	[DataMember]
	public int PageNumber
	{
		get { return _pageNumber; }
		set { _pageNumber = value; }
	}

	private string _partnerCode;
	[DataMember]
	public string PartnerCode
	{
		get { return _partnerCode; }
		set { _partnerCode = value; }
	}

	private string _token;
	[DataMember]
	public string Token
	{
		get { return _token; }
		set { _token = value; }
	}

	private string _agentCode;
	[DataMember]
	public string AgentCode
	{
		get { return _agentCode; }
		set { _agentCode = value; }
	}

	private DateTime _startDate;
	[DataMember]
	public DateTime StartDate
	{
		get { return _startDate; }
		set { _startDate = value; }
	}

	private DateTime _endDate;
	[DataMember]
	public DateTime EndDate
	{
		get { return _endDate; }
		set { _endDate = value; }
	}
	#endregion

	#region Internal Methods
	internal GetSummaryReportResult Process()
	{
		GetSummaryReportResult returnValue = new GetSummaryReportResult();

		// Important: This function validates the request credential
		this.AuthenticateRequest();

		using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
		{
			#region Validate Partner Record
			PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
			partner.PartnerCode = this.PartnerCode;
			partner = serviceClient.FindPartnerCode(partner);

			if (partner.PartnerID == 0)
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			else { }

			if (partner.Token == this.Token) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}

			if (partner.Activated) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			#endregion

			#region Validate Agent Record
			PeraLinkCoreWcf.Agent agent = new PeraLinkCoreWcf.Agent();
			if (this.AgentCode != null)
			{
				agent.AgentCode = this.AgentCode;
				agent.Partner = partner;
				agent = serviceClient.FindAgentCodeOfPartner(agent);

				if (agent.AgentID == 0)
				{
					PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
					throw new RDFramework.ClientException(message.Content, message.MessageID);
				}
				else
				{
					// Proceed
				}

				if (agent.Partner.PartnerID == partner.PartnerID)
				{ }
				else
				{
					PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(43);
					throw new RDFramework.ClientException(message.Content, message.MessageID);
				}

				if (agent.Activated) { }
				else
				{
					PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(46);
					throw new RDFramework.ClientException(message.Content, message.MessageID);
				}
			}
			else
			{
				agent.Partner = partner;
			}
			#endregion

			PeraLinkCoreWcf.GetSummaryReportRequest getSummaryReportRequest = new PeraLinkCoreWcf.GetSummaryReportRequest();
			getSummaryReportRequest.Agent = agent;
			getSummaryReportRequest.EndDate = this.EndDate;
			getSummaryReportRequest.EndRowNumber = (this.PageNumber * _pageSize);
			getSummaryReportRequest.StartDate = this.StartDate;
			getSummaryReportRequest.StartRowNumber = (getSummaryReportRequest.EndRowNumber - _pageSize) + 1;

			PeraLinkCoreWcf.GetSummaryReportResult getSummaryReportResult = serviceClient.GetSummaryReport(getSummaryReportRequest);
			returnValue.SendingSummaryReportCollection = new SummaryReportCollection();
			returnValue.SendingSummaryReportCollection.AddList(getSummaryReportResult.SendingSummaryReportCollection);

			returnValue.PayoutSummaryReportCollection = new SummaryReportCollection();
			returnValue.PayoutSummaryReportCollection.AddList(getSummaryReportResult.PayoutSummaryReportCollection);

            //returnValue.CancelSummaryReportCollection = new SummaryReportCollection();
            //returnValue.CancelSummaryReportCollection.AddList(getSummaryReportResult.CancelSummaryReportCollection);

            returnValue.RefundSummaryReportCollection = new SummaryReportCollection();
            returnValue.RefundSummaryReportCollection.AddList(getSummaryReportResult.RefundSummaryReportCollection);

			returnValue.TotalPageCount = ((getSummaryReportResult.TotalRecordCount % _pageSize) == 0) ? (getSummaryReportResult.TotalRecordCount / _pageSize) : (getSummaryReportResult.TotalRecordCount / _pageSize) + 1;
		}
		returnValue.ResultStatus = ResultStatus.Successful;
		return returnValue;
	}
	#endregion
}