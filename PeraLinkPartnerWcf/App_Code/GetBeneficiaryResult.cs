using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetBeneficiaryResult: BaseResult
{
    #region Constructor
    public GetBeneficiaryResult()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private Beneficiary _beneficiary;
    [DataMember]
    public Beneficiary Beneficiary
    {
        get { return _beneficiary; }
        set { _beneficiary = value; }
    }
    #endregion
}