using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CustomerPortalWSClient
/// </summary>
public class CustomerPortalWSClient: IDisposable
{
	#region Constructor
	public CustomerPortalWSClient()
	{
		_serviceSoapClient = new CustomerPortalWS.CustomerPortalWebServiceSoapClient();
	}
	#endregion

	#region Fields/Properties
	private CustomerPortalWS.CustomerPortalWebServiceSoapClient _serviceSoapClient;
	#endregion

	#region Public Methods
	public string GetEnrolledCustomer(string customerNumber)
	{
		string returnValue = _serviceSoapClient.GetEnrolledCustomer(customerNumber);

		if (returnValue == "Not Found" || returnValue == "Inactive")
		{
			return null;
		}
		else
		{
			return returnValue;
		}
	}
	#endregion

	void IDisposable.Dispose()
	{
		bool isClosed = false;

		try
		{
			if (this._serviceSoapClient.State != System.ServiceModel.CommunicationState.Faulted)
			{
				this._serviceSoapClient.Close();
				isClosed = true;
			}
			else
			{
				// Proceed with Abort in finally
			}
		}
		finally
		{
			if (!isClosed)
			{
				this._serviceSoapClient.Abort();
			}
			else
			{
				// Do nothing since state is already closed
			}
		}
	}
}