using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetSendCurrencyCollectionRequest: BaseRequest
{
    #region Constructor
    public GetSendCurrencyCollectionRequest()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private string _partnerCode;
    [DataMember]
    public string PartnerCode
    {
        get { return _partnerCode; }
        set { _partnerCode = value; }
    }

    private string _token;
    [DataMember]
    public string Token
    {
        get { return _token; }
        set { _token = value; }
    }

    private string _agentCode;
    [DataMember]
    public string AgentCode
    {
        get { return _agentCode; }
        set { _agentCode = value; }
    }
    #endregion

    #region Internal Methods
    internal GetSendCurrencyCollectionResult Process()
    {
        GetSendCurrencyCollectionResult returnValue = new GetSendCurrencyCollectionResult();

        // Important: This function validates the request credential
        this.AuthenticateRequest();

        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {

            #region Validate Partner Record
            PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
            partner.PartnerCode = this.PartnerCode;
            partner = serviceClient.FindPartnerCode(partner);

            if (partner.PartnerID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else { }

            if (partner.Token == this.Token) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (partner.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            #endregion

            #region Validate Agent Record
            PeraLinkCoreWcf.Agent agent = new PeraLinkCoreWcf.Agent();
            agent.AgentCode = this.AgentCode;
            agent.Partner = partner;
            agent = serviceClient.FindAgentCodeOfPartner(agent);

            if (agent.AgentID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }

            if (agent.Partner.PartnerID == partner.PartnerID)
            { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(43);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (agent.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(46);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            #endregion

            PeraLinkCoreWcf.PartnerServiceMapping[] partnerServiceMappingCollection = serviceClient.GetPartnerServiceMappingCollection(partner);

            PeraLinkCoreWcf.PartnerServiceMapping partnerServiceMapping = Array.Find<PeraLinkCoreWcf.PartnerServiceMapping>(partnerServiceMappingCollection
                , delegate(PeraLinkCoreWcf.PartnerServiceMapping eachPartnerServiceMapping)
                {
                    return eachPartnerServiceMapping.ServiceType.ServiceTypeID == ServiceTypeID.SendRemittance;
                });


            if (partnerServiceMapping == null)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(27);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }

            PeraLinkCoreWcf.PartnerServiceCurrency[] partnerServiceCurrencyCollection = serviceClient.GetPartnerServiceCurrencyCollection(partnerServiceMapping);

            if (partnerServiceCurrencyCollection.Length == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(27);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }

            returnValue.CurrencyCollection = new CurrencyCollection();

            Array.ForEach<PeraLinkCoreWcf.PartnerServiceCurrency>(partnerServiceCurrencyCollection
                , delegate(PeraLinkCoreWcf.PartnerServiceCurrency eachPartnerServiceCurrency)
                {
                    Currency sendCurrency = new Currency();
                    sendCurrency.Load(eachPartnerServiceCurrency.Currency);
                    returnValue.CurrencyCollection.Add(sendCurrency);
                });
        }

        returnValue.ResultStatus = ResultStatus.Successful;

        return returnValue;
    }
    #endregion
}