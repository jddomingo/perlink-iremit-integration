﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PeraLinkBillsPaymentWcfClient
/// </summary>
public class PeraLinkBillsPaymentWcfClient : IDisposable
{
    #region Constructor
    public PeraLinkBillsPaymentWcfClient() 
	{
        _serviceClient = new PeraLinkBillsPaymentWcf.ServiceClient();
	}
    #endregion

    #region Fields/Properties
    private PeraLinkBillsPaymentWcf.ServiceClient _serviceClient;
    #endregion

    #region Public Methods
    public PeraLinkBillsPaymentWcf.ProcessBillsPaymentResult ProcessBillsPayment(PeraLinkBillsPaymentWcf.ProcessBillsPaymentRequest processBillsPaymentRequest)
    {
        processBillsPaymentRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkBillsPaymentWcf.ProcessBillsPaymentResult returnValue = _serviceClient.ProcessBillsPayment(processBillsPaymentRequest);

        switch (returnValue.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return returnValue;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(returnValue.Message, returnValue.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(returnValue.ResultStatus.ToString());
                }
        }
    }
    #endregion

    void IDisposable.Dispose()
    {
        bool isClosed = false;

        try
        {
            if (this._serviceClient.State != System.ServiceModel.CommunicationState.Faulted)
            {
                this._serviceClient.Close();
                isClosed = true;
            }
            else
            {
                // Proceed with Abort in finally
            }
        }
        finally
        {
            if (!isClosed)
            {
                this._serviceClient.Abort();
            }
            else
            {
                // Do nothing since state is already closed
            }
        }
    }
}