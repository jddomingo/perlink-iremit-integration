using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for BaseRequest
/// </summary>
[DataContract]
public class BaseRequest
{
    #region Constructor
    public BaseRequest()
    {
        //

        //
    }
    #endregion

    #region Fields/Properties
    private ServiceCredential _serviceCredential;

    [DataMember]
    public ServiceCredential ServiceCredential
    {
        get { return _serviceCredential; }
        set { _serviceCredential = value; }
    }
    
    #endregion

    #region Public Methods
    public void AuthenticateRequest()
    {
        Rule rule = new Rule(this);
        rule.RequireServiceCredential();

        this.ServiceCredential.Authenticate();
    }
    #endregion

    #region SubClasses
    public class Rule
    {
        #region Constructor
        public Rule(BaseRequest baseRequest)
        {
            _baseRequest = baseRequest;
        }
        #endregion

        #region Fields/Properties
        private BaseRequest _baseRequest;
        #endregion

        #region Public Methods
        public void RequireServiceCredential()
        {
            if (_baseRequest.ServiceCredential == null)
            {
                throw new RDFramework.ClientException("ServiceCredential is required.");
            }
            else { }
        }
        #endregion
    }
    #endregion
}