using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ExternalUserMgmtWSClient
/// </summary>
public class ExternalUserMgmtWSClient: IDisposable
{
    #region Constructor
    public ExternalUserMgmtWSClient()
    {
        _serviceSoapClient = new ExternalUserMgmtWS.ServiceSoapClient();
    }
    #endregion

    #region Fields/Properties
    private ExternalUserMgmtWS.ServiceSoapClient _serviceSoapClient;
    #endregion

    #region Public Methods
    public string ChangePassword(string userID, string currentPassword, string newPassword)
    {
        ExternalUserMgmtWS.ChangePasswordRequest changePasswordRequest = new ExternalUserMgmtWS.ChangePasswordRequest();
        changePasswordRequest.CurrentPassword = currentPassword;
        changePasswordRequest.NewPassword = newPassword;
        changePasswordRequest.UserID = userID;
        changePasswordRequest.IPAddress = SystemUtility.GetClientIPAddress();
        changePasswordRequest.ApplicationCode = SystemSetting.ApplicationCodeForEums;

        ExternalUserMgmtWS.ChangePasswordResponse changePasswordResponse = _serviceSoapClient.ChangePassword(changePasswordRequest);

        switch (changePasswordResponse.ExecutionStatus)
        {
            case ExternalUserMgmtWS.StatusCode.Successful:
                {
                    return changePasswordResponse.SystemMessage;
                }
            case ExternalUserMgmtWS.StatusCode.Failed:
            case ExternalUserMgmtWS.StatusCode.Error:
                {
                    throw new RDFramework.ClientException(changePasswordResponse.SystemMessage, changePasswordResponse.ExecutionID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(changePasswordResponse.ExecutionStatus.ToString());
                }
        }
    }

    public string ChangePasswordOnFirstLogin(ExternalUserMgmtWS.ChangePasswordOnFirstLoginRequest changePasswordOnFirstLoginRequest)
    {
        ExternalUserMgmtWS.ChangePasswordOnFirstLoginResponse changePasswordOnFirstLoginResponse = _serviceSoapClient.ChangePasswordOnFirstLogin(changePasswordOnFirstLoginRequest);

        switch (changePasswordOnFirstLoginResponse.ExecutionStatus)
        {
            case ExternalUserMgmtWS.StatusCode.Successful:
                {
                    return changePasswordOnFirstLoginResponse.SystemMessage;
                }
            case ExternalUserMgmtWS.StatusCode.Failed:
            case ExternalUserMgmtWS.StatusCode.Error:
                {
                    throw new RDFramework.ClientException(changePasswordOnFirstLoginResponse.SystemMessage, changePasswordOnFirstLoginResponse.ExecutionID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(changePasswordOnFirstLoginResponse.ExecutionStatus.ToString());
                }
        }
    }

    public ExternalUserMgmtWS.AuthenticateUserResponse AuthenticateUser(string userID, string password)
    {
        ExternalUserMgmtWS.AuthenticateUserRequest authenticateUseRequest = new ExternalUserMgmtWS.AuthenticateUserRequest();
        authenticateUseRequest.ApplicationCode = SystemSetting.ApplicationCodeForEums;
        authenticateUseRequest.Password = password;
        authenticateUseRequest.UserID = userID;
        ExternalUserMgmtWS.AuthenticateUserResponse authenticateUserResponse = _serviceSoapClient.AuthenticateUser(authenticateUseRequest);

        switch (authenticateUserResponse.ExecutionStatus)
        {
            case ExternalUserMgmtWS.StatusCode.Successful:
                {
                    return authenticateUserResponse;
                }
            case ExternalUserMgmtWS.StatusCode.Failed:
            case ExternalUserMgmtWS.StatusCode.Error:
                {
                    throw new RDFramework.ClientException(authenticateUserResponse.SystemMessage, authenticateUserResponse.ExecutionID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(authenticateUserResponse.ExecutionStatus.ToString());
                }
        }
    }
    #endregion

    #region SubClasses
    public class AuthenticateUserErrorID
    {
        #region Constructor
        public AuthenticateUserErrorID() { }
        #endregion

        #region Fields/Properties
        public const int IsDefaultPassword = -105;
        public const int ExpiredPassword = -107;
        public const int FirstLogin = -108;
        public const int PasswordToExpire = -109;
        #endregion
    }
    #endregion

    void IDisposable.Dispose()
    {
        bool isClosed = false;

        try
        {
            if (this._serviceSoapClient.State != System.ServiceModel.CommunicationState.Faulted)
            {
                this._serviceSoapClient.Close();
                isClosed = true;
            }
            else
            { 
                // Proceed with Abort in finally
            }
        }
        finally
        {
            if (!isClosed)
            {
                this._serviceSoapClient.Abort();
            }
            else
            { 
                // Do nothing since state is already closed
            }
        }
    }
}