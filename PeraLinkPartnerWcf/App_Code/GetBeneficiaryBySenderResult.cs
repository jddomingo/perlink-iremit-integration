using System;
using System.Collections.Generic;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetBeneficiaryBySenderResult: BaseResult
{
    #region Constructor
    public GetBeneficiaryBySenderResult()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private BeneficiaryCollection _beneficiaryCollection;

    [DataMember]
    public BeneficiaryCollection BeneficiaryCollection
    {
        get { return _beneficiaryCollection; }
        set { _beneficiaryCollection = value; }
    }
    #endregion
}