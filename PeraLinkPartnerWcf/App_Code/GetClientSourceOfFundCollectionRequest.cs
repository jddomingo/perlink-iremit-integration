using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

/// <summary>
/// Summary description for GetClientSourceOfFundCollectionRequest
/// </summary>
public class GetClientSourceOfFundCollectionRequest: BaseRequest
{
	#region Constructor
	public GetClientSourceOfFundCollectionRequest()
	{
		//

		//
	}
	#endregion

	#region Fields/Properties
	
	#endregion

	#region Internal Methods
	internal GetClientSourceOfFundCollectionResult Process()
	{
		GetClientSourceOfFundCollectionResult returnValue = new GetClientSourceOfFundCollectionResult();

		// Important: This function validates the request credential
		this.AuthenticateRequest();

		using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
		{
			PeraLinkCoreWcf.GetClientSourceOfFundCollectionRequest getClientSourceOfFundRequest = new PeraLinkCoreWcf.GetClientSourceOfFundCollectionRequest();
			PeraLinkCoreWcf.GetClientSourceOfFundCollectionResult getClientSourceOfFundResult = new PeraLinkCoreWcf.GetClientSourceOfFundCollectionResult();
			getClientSourceOfFundResult = serviceClient.GetClientSourceOfFundCollection(getClientSourceOfFundRequest);
			returnValue.ClientSourceOfFundCollection = new ClientSourceOfFundCollection();
			returnValue.ClientSourceOfFundCollection.AddList(getClientSourceOfFundResult.ClientSourceOfFundCollection);
		}

		returnValue.ResultStatus = ResultStatus.Successful;

		return returnValue;
	}
	#endregion
}