﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for InsuranceCollection
/// </summary>
public class InsuranceCollection : List<Insurance>
{
    #region Constructor
    public InsuranceCollection()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion

    #region Public Methods
    public void AddList(PeraLinkCoreWcf.Insurance[] insuranceTransactionCollection)
    {
        Array.ForEach<PeraLinkCoreWcf.Insurance>(insuranceTransactionCollection
            , delegate(PeraLinkCoreWcf.Insurance eachInsuranceTransaction)
            {
                Insurance mappedInsuranceTransaction = new Insurance();
                mappedInsuranceTransaction.Load(eachInsuranceTransaction);
                this.Add(mappedInsuranceTransaction);
            });
    }
    #endregion


}
