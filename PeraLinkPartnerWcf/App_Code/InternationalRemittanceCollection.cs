using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class InternationalRemittanceCollection: List<InternationalRemittance>
{
    #region Constructor
    public InternationalRemittanceCollection()
	{
		//

		//
    }
    #endregion

    #region Public Methods
    public void AddList(PeraLinkCoreWcf.InternationalRemittance[] internationalRemittanceCollection)
    {
        Array.ForEach<PeraLinkCoreWcf.InternationalRemittance>(internationalRemittanceCollection
            , delegate(PeraLinkCoreWcf.InternationalRemittance eachInternationalRemittance)
            {
                InternationalRemittance mappedRemittance = new InternationalRemittance();
                mappedRemittance.Load(eachInternationalRemittance);
                this.Add(mappedRemittance);
            });
    }
    #endregion
}