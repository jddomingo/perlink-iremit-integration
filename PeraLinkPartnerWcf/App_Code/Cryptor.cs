using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

/// <summary>
/// Summary description for Cryptor
/// </summary>
public class Cryptor
{
    public Cryptor()
    { }

    public static string ByteToString(byte[] buff)
    {
        string sbinary = "";

        for (int i = 0; i < buff.Length; i++)
        {
            sbinary += buff[i].ToString("x2"); // hex format
        }
        return (sbinary);
    }

    public static string GenerateHmacSha256(string key, string message)
    {
        System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
        byte[] keyByte = encoding.GetBytes(key);

        HMACMD5 hmacmd5 = new HMACMD5(keyByte);
        HMACSHA1 hmacsha1 = new HMACSHA1(keyByte);
        HMACSHA256 hmacsha256 = new HMACSHA256(keyByte);

        byte[] messageBytes = encoding.GetBytes(message);

        byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);

        return ByteToString(hashmessage);
    }

    public static bool TestHmacSha256(string key, string hashValue, string clearText)
    {
        return GenerateHmacSha256(key, clearText).Equals(hashValue, StringComparison.OrdinalIgnoreCase);
    }

    public static string GeneratePassKey()
    {
        return Cryptor.GenerateHmacSha256(ConfigurationManager.AppSettings["CryptoPrivateKey"].ToString(), DateTime.UtcNow.ToString("MM/dd/yy HH:mm"));
    }
}