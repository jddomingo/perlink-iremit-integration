using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetInternationalPartnerCollectionRequest : BaseRequest
{
	#region Constructor
    public GetInternationalPartnerCollectionRequest()
	{
		//

		//
	}
	#endregion

	#region Fields/Properties
	private string _partnerCode;
	[DataMember]
	public string PartnerCode
	{
		get { return _partnerCode; }
		set { _partnerCode = value; }
	}

	private string _token;
	[DataMember]
	public string Token
	{
		get { return _token; }
		set { _token = value; }
	}


	#endregion

	#region Internal Methods
	internal GetInternationalPartnerCollectionResult Process()
	{
        GetInternationalPartnerCollectionResult returnValue = new GetInternationalPartnerCollectionResult();
		this.AuthenticateRequest();

		using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
		{
			#region Validate Partner Record
			PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
			partner.PartnerCode = this.PartnerCode;
			partner = serviceClient.FindPartnerCode(partner);

			if (partner.PartnerID == 0)
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			else { }

			if (partner.Token == this.Token) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}

			if (partner.Activated) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			#endregion

			returnValue.PartnerCollection = new PartnerCollection();

            // Get the PartnerServiceMapping for the remittance payout service of the current partner
			PeraLinkCoreWcf.PartnerServiceMapping mapping = new PeraLinkCoreWcf.PartnerServiceMapping();
            mapping.Partner = partner;
			mapping.ServiceType = new PeraLinkCoreWcf.ServiceType();
            mapping.ServiceType.ServiceTypeID = ServiceTypeID.PayoutRemittance;

            PeraLinkCoreWcf.GetPartnerServiceMappingRequest getPartnerServiceMappingRequest = new PeraLinkCoreWcf.GetPartnerServiceMappingRequest();
            getPartnerServiceMappingRequest.PartnerServiceMapping = mapping;
            mapping = serviceClient.GetPartnerServiceMapping(getPartnerServiceMappingRequest).PartnerServiceMapping;

            // Get the paired partners
			PeraLinkCoreWcf.PartnerServicePair[] partnerServicePairCollection = serviceClient.GetPartnerServicePairCollection(mapping);
            Array.ForEach(partnerServicePairCollection, 
                delegate(PeraLinkCoreWcf.PartnerServicePair item) 
                {
                    if (item.PairedPartner.PartnerType.PartnerTypeID == PartnerTypeID.International)
                    {
                        Partner convertedPartner = new Partner();
                        convertedPartner.Load(item.PairedPartner);
                        returnValue.PartnerCollection.Add(convertedPartner);
                    }
                    else { /* Do nothing */ }
                });

            returnValue.ResultStatus = ResultStatus.Successful;

			return returnValue;
		}
	}
	#endregion
}