﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TokenAuth4;

public class LbcWcfClient
{
    #region Constructor
    public LbcWcfClient()
	{
        _serviceClient = new PeraLinkLbcWcf.ServiceClient();
    }
    #endregion

    #region Fields/Properties
    private PeraLinkLbcWcf.ServiceClient _serviceClient;

    private string _key;

    public string Key
    {
        get { return _key; }
        set { _key = value; }
    }
    #endregion

    #region Public Methods
    public PeraLinkCoreWcf.DomesticRemittance FindRemittance(string controlNumber)
    {
        PeraLinkCoreWcf.DomesticRemittance returnValue = new PeraLinkCoreWcf.DomesticRemittance();

        PeraLinkLbcWcf.FindRemittanceRequest findRemittanceRequest = new PeraLinkLbcWcf.FindRemittanceRequest();
        findRemittanceRequest.ControlNumber = controlNumber;
        findRemittanceRequest.RequestToken = TokenAuth.Generate(SystemSetting.CryptoPrivateKey);

        PeraLinkLbcWcf.FindRemittanceResult findRemittanceResult = _serviceClient.FindRemittance(findRemittanceRequest);

        switch (findRemittanceResult.ResultStatus)
        {
            case ResultStatus.Successful:
                returnValue.Beneficiary = new PeraLinkCoreWcf.Beneficiary();
                returnValue.Beneficiary.FirstName = findRemittanceResult.BeneficiaryFirstName;
                returnValue.Beneficiary.LastName = findRemittanceResult.BeneficiaryLastName;
                returnValue.Beneficiary.BirthDate = RDFramework.Utility.Validation.MinimumSqlDateTime;
                returnValue.DateTimeSent = DateTime.Now;
                returnValue.ControlNumber = controlNumber;
                returnValue.PrincipalAmount = findRemittanceResult.Amount;
                returnValue.ServiceFee = findRemittanceResult.ServiceCharge;
                returnValue.RemittanceStatus = MapStatus(findRemittanceResult.TransactionStatus.Value);
                returnValue.SendCurrency = new PeraLinkCoreWcf.Currency();
                returnValue.SendCurrency.CurrencyID = 6;
                returnValue.SendCurrency.Code = "PHP";
                returnValue.SenderClient = new PeraLinkCoreWcf.Client();
                returnValue.SenderClient.FirstName = findRemittanceResult.SenderFirstName;
                returnValue.SenderClient.LastName = findRemittanceResult.SenderLastName;
                returnValue.SenderClient.BirthDate = RDFramework.Utility.Validation.MinimumSqlDateTime;
                returnValue.SentByUserID =findRemittanceResult.SendingAgent;
                returnValue.SentByAgent = new PeraLinkCoreWcf.Agent();
                returnValue.SentByAgent.AgentCode = findRemittanceResult.SendingAgent;
                this.Key = findRemittanceResult.LookupKey;
                return returnValue;
            case ResultStatus.Error:
            case ResultStatus.Failed:
                throw new RDFramework.ClientException(findRemittanceResult.Message, (int)findRemittanceResult.MessageID);
            default:
                throw new ArgumentOutOfRangeException(findRemittanceResult.Message);
        }
    }

    public PeraLinkLbcWcf.PayoutRemittanceResult PayoutRemittance(string controlNumber, int payoutID, string idNumber)
    {
        PeraLinkLbcWcf.PayoutRemittanceResult returnValue = new PeraLinkLbcWcf.PayoutRemittanceResult();
        PeraLinkLbcWcf.PayoutRemittanceRequest payoutRemittanceRequest = new PeraLinkLbcWcf.PayoutRemittanceRequest();
        payoutRemittanceRequest.ControlNumber = controlNumber;
        payoutRemittanceRequest.IdNumber = idNumber;
        payoutRemittanceRequest.PayoutId = payoutID;
        payoutRemittanceRequest.RequestToken = TokenAuth.Generate(SystemSetting.CryptoPrivateKey);

        returnValue = _serviceClient.PayoutRemittance(payoutRemittanceRequest);


        switch (returnValue.ResultStatus)
        {
            case ResultStatus.Successful:
                return returnValue;
            case ResultStatus.Error:
            case ResultStatus.Failed:
                RDFramework.Utility.EventLog.SaveError(string.Format("LBC: {0}|{1}", controlNumber, returnValue.Key));
                throw new RDFramework.ClientException(returnValue.Message, (int)returnValue.MessageID);
            default:
                RDFramework.Utility.EventLog.SaveError(string.Format("LBC: {0}|{1}", controlNumber, returnValue.Key));
                throw new ArgumentOutOfRangeException(returnValue.Message);
        }
    }


    public static PeraLinkCoreWcf.RemittanceStatus MapStatus(PeraLinkLbcWcf.TransactionStatus lbcStatus)
    {
        switch (lbcStatus)
        {
            case PeraLinkLbcWcf.TransactionStatus.Outstanding:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 1;
                    returnValue.Description = "Outstanding";

                    return returnValue;
                }
            case PeraLinkLbcWcf.TransactionStatus.Paid:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 2;
                    returnValue.Description = "Paid";

                    return returnValue;
                }
            case PeraLinkLbcWcf.TransactionStatus.Locked:
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(97);
                    throw new RDFramework.ClientException(string.Format(message.Description, lbcStatus.ToString()), message.SystemMessageID);
                }
            default:
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(86);
                    throw new RDFramework.ClientException(string.Format(message.Description, lbcStatus.ToString()), message.SystemMessageID);
                }
        }
    }
    #endregion
}