using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PlacidWcfClient
/// </summary>
public class PlacidWcfClient: IDisposable
{
    #region Constructor
    public PlacidWcfClient()
	{
        _serviceClient = new PeraLinkPlacidSpotCashWcf.ServiceClient();
    }
    #endregion

    #region Fields/Properties
    private PeraLinkPlacidSpotCashWcf.ServiceClient _serviceClient;
    #endregion

    #region Public Methods

    public PeraLinkPlacidSpotCashWcf.LookupTransactionResult LookupTransaction(PeraLinkPlacidSpotCashWcf.LookupTransactionRequest lookUpTransactionRequest)
    {

        PeraLinkPlacidSpotCashWcf.LookupTransactionResult lookupTransactionResult = _serviceClient.LookUpTransaction(lookUpTransactionRequest);

        switch (lookupTransactionResult.ResultCode)
        {
            case PeraLinkPlacidSpotCashWcf.LookupTransactionResultCode.Successful:
                {
                    return lookupTransactionResult;
                }
            case PeraLinkPlacidSpotCashWcf.LookupTransactionResultCode.PartnerError:
            case PeraLinkPlacidSpotCashWcf.LookupTransactionResultCode.ServerError:
            case PeraLinkPlacidSpotCashWcf.LookupTransactionResultCode.UnrecognizedResponse:
            case PeraLinkPlacidSpotCashWcf.LookupTransactionResultCode.Unsuccessful:
                {
                    throw new RDFramework.ClientException(lookupTransactionResult.StatusMessage, (int)lookupTransactionResult.ResultCode);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(lookupTransactionResult.ResultCode.ToString());
                }
        }
    }

    public PeraLinkPlacidSpotCashWcf.PayoutTransactionResult PayoutTransaction(PeraLinkPlacidSpotCashWcf.PayoutTransactionRequest payoutTransactionRequest)
    {
        PeraLinkPlacidSpotCashWcf.PayoutTransactionResult payoutTransactionResult = _serviceClient.PayoutTransaction(payoutTransactionRequest);

        switch (payoutTransactionResult.ResultCode)
        {
            case PeraLinkPlacidSpotCashWcf.PayoutTransactionResultCode.Successful:
                {
                    return payoutTransactionResult;
                }
            case PeraLinkPlacidSpotCashWcf.PayoutTransactionResultCode.PartnerError:
            case PeraLinkPlacidSpotCashWcf.PayoutTransactionResultCode.ServerError:
            case PeraLinkPlacidSpotCashWcf.PayoutTransactionResultCode.UnrecognizedResponse:
            case PeraLinkPlacidSpotCashWcf.PayoutTransactionResultCode.Unsuccessful:
                {
                    throw new RDFramework.ClientException(payoutTransactionResult.StatusMessage, (int)payoutTransactionResult.ResultCode);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(payoutTransactionResult.ResultCode.ToString());
                }
        }
    }

    public static PeraLinkCoreWcf.RemittanceStatus MapStatus(PeraLinkPlacidSpotCashWcf.TransactionStatus transactionStatus)
    {
        switch (transactionStatus)
        {
            case PeraLinkPlacidSpotCashWcf.TransactionStatus.ForPayout:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 1;
                    returnValue.Description = "Outstanding";

                    return returnValue;
                }
            case PeraLinkPlacidSpotCashWcf.TransactionStatus.PaidOut:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 2;
                    returnValue.Description = "Paid";

                    return returnValue;
                }
            case PeraLinkPlacidSpotCashWcf.TransactionStatus.Cancelled:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 4;
                    returnValue.Description = "Cancelled";

                    return returnValue;
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(transactionStatus.ToString());
                }
        }
    }
    #endregion

    void IDisposable.Dispose()
    {
        bool isClosed = false;

        try
        {
            if (this._serviceClient.State != System.ServiceModel.CommunicationState.Faulted)
            {
                this._serviceClient.Close();
                isClosed = true;
            }
            else
            {
                // Proceed with Abort in finally
            }
        }
        finally
        {
            if (!isClosed)
            {
                this._serviceClient.Abort();
            }
            else
            {
                // Do nothing since state is already closed
            }
        }
    }
}