using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PeraLinkRemitToAccountWcfClient
/// </summary>
public class PeraLinkRemitToAccountWcfClient : IDisposable
{
	#region Constructor
	public PeraLinkRemitToAccountWcfClient()
	{
		_serviceClient = new PeraLinkRemitToAccountWcf.ServiceClient();
	}
	#endregion

	#region Fields/Properties
	private PeraLinkRemitToAccountWcf.ServiceClient _serviceClient;
	#endregion

	#region Public Methods
	public PeraLinkRemitToAccountWcf.SecurityBankTransactionResult SecurityBankTransaction(PeraLinkRemitToAccountWcf.SecurityBankTransactionRequest securityBankTransactionRequest)
	{
		PeraLinkRemitToAccountWcf.SecurityBankTransactionResult returnValue = _serviceClient.SecurityBankTransaction(securityBankTransactionRequest);
		switch (returnValue.ResultStatus)
		{
			case ResultStatus.Successful:
				{
					return returnValue;
				}
			case ResultStatus.Failed:
			case ResultStatus.Error:
				{
					throw new RDFramework.ClientException(returnValue.Message, returnValue.MessageID, returnValue.LogID);
				}
			default:
				{
					throw new ArgumentOutOfRangeException(returnValue.ResultStatus.ToString());
				}
		}
	}


    public PeraLinkRemitToAccountWcf.MetrobankTransactionResult MetrobankTransaction(PeraLinkRemitToAccountWcf.MetrobankTransactionRequest metrobankTransactionRequest)
    {
        PeraLinkRemitToAccountWcf.MetrobankTransactionResult returnValue = _serviceClient.MetrobankTransaction(metrobankTransactionRequest);
        switch (returnValue.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return returnValue;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(returnValue.Message, returnValue.MessageID, returnValue.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(returnValue.ResultStatus.ToString());
                }
        }
    }
	#endregion

	void IDisposable.Dispose()
	{
		bool isClosed = false;

		try
		{
			if (this._serviceClient.State != System.ServiceModel.CommunicationState.Faulted)
			{
				this._serviceClient.Close();
				isClosed = true;
			}
			else
			{
				// Proceed with Abort in finally
			}
		}
		finally
		{
			if (!isClosed)
			{
				this._serviceClient.Abort();
			}
			else
			{
				// Do nothing since state is already closed
			}
		}
	}
}