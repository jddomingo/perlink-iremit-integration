using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetSummaryReportResult : ServiceResult
{
	#region Constructor
	public GetSummaryReportResult()
	{ }
	#endregion

	#region Fields/Properties
	private SummaryReportCollection _sendingSummaryReportCollection;
	[DataMember]
	public SummaryReportCollection SendingSummaryReportCollection
	{
		get { return _sendingSummaryReportCollection; }
		set { _sendingSummaryReportCollection = value; }
	}

	private SummaryReportCollection _payoutSummaryReportCollection;
	[DataMember]
	public SummaryReportCollection PayoutSummaryReportCollection
	{
		get { return _payoutSummaryReportCollection; }
		set { _payoutSummaryReportCollection = value; }
	}

    //private SummaryReportCollection _cancelSummaryReportCollection;
    //[DataMember]
    //public SummaryReportCollection CancelSummaryReportCollection
    //{
    //    get { return _cancelSummaryReportCollection; }
    //    set { _cancelSummaryReportCollection = value; }
    //}

    private SummaryReportCollection _refundSummaryReportCollection;
    [DataMember]
    public SummaryReportCollection RefundSummaryReportCollection
    {
        get { return _refundSummaryReportCollection; }
        set { _refundSummaryReportCollection = value; }
    }

	private int _totalPageCount;
	[DataMember]
	public int TotalPageCount
	{
		get { return _totalPageCount; }
		set { _totalPageCount = value; }
	}
	#endregion
}