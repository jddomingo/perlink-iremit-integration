﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Threading.Tasks;

/// <summary>
/// Summary description for GetInsuranceTransactionHistoryRequest
/// </summary>
public class GetInsuranceTransactionHistoryRequest:BaseRequest
{
    #region Constructor
    public GetInsuranceTransactionHistoryRequest()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion

    #region Fields/Properties
    private const int _pageSize = 10;

    private int _pageNumber;
    [DataMember]
    public int PageNumber
    {
        get { return _pageNumber; }
        set { _pageNumber = value; }
    }

    private string _partnerCode;
    [DataMember]
    public string PartnerCode
    {
        get { return _partnerCode; }
        set { _partnerCode = value; }
    }

    private string _token;
    [DataMember]
    public string Token
    {
        get { return _token; }
        set { _token = value; }
    }

    private string _agentCode;
    [DataMember]
    public string AgentCode
    {
        get { return _agentCode; }
        set { _agentCode = value; }
    }

    private string _userID;
    [DataMember]
    public string UserID
    {
        get { return _userID; }
        set { _userID = value; }
    }

    private string _terminalCode;
    [DataMember]
    public string TerminalCode
    {
        get { return _terminalCode; }
        set { _terminalCode = value; }
    }

    private DateTime? _startDate;
    [DataMember]
    public DateTime? StartDate
    {
        get { return _startDate; }
        set { _startDate = value; }
    }

    private DateTime? _endDate;
    [DataMember]
    public DateTime? EndDate
    {
        get { return _endDate; }
        set { _endDate = value; }
    }
    #endregion

    #region Internal Method
    internal GetInsuranceTransactionHistoryResult Process()
    {
        GetInsuranceTransactionHistoryResult returnValue = new GetInsuranceTransactionHistoryResult();

        this.AuthenticateRequest();

        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
            #region Validate Partner Record
            PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
            partner.PartnerCode = this.PartnerCode;
            partner = serviceClient.FindPartnerCode(partner);

            if (partner.PartnerID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else { }

            if (partner.Token == this.Token) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (partner.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            #endregion

            #region Validate Agent Record
            PeraLinkCoreWcf.Agent agent = new PeraLinkCoreWcf.Agent();
            if (this.AgentCode != null)
            {
                agent.AgentCode = this.AgentCode;
                agent.Partner = partner;
                agent = serviceClient.FindAgentCodeOfPartner(agent);

                if (agent.AgentID == 0)
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
                else
                {
                    // Proceed
                }

                if (agent.Partner.PartnerID == partner.PartnerID)
                { }
                else
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(43);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }

                if (agent.Activated) { }
                else
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(46);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
            }
            else
            {
                agent.Partner = partner;
            }
            #endregion

            #region Validate Terminal Record
            PeraLinkCoreWcf.Terminal terminal = new PeraLinkCoreWcf.Terminal();

            if (string.IsNullOrWhiteSpace(this.TerminalCode))
            {
                if (serviceClient.GetAgentTerminalCollection(agent).Length > 0)
                {
                    terminal.TerminalCode = this.TerminalCode;
                    terminal.Agent = agent;
                    terminal.Agent.Partner = partner;
                    terminal = serviceClient.FindTerminal(terminal);

                    if (terminal.TerminalID == 0)
                    {
                        PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(104);
                        throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                    }
                    else
                    {
                        // Proceed
                    }

                    if (terminal.Agent.AgentID == agent.AgentID)
                    { }
                    else
                    {
                        PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(105);
                        throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                    }

                    if (terminal.Activated) { }
                    else
                    {
                        PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(106);
                        throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                    }
                }
                else
                {
                    //do nothing
                }
            }
            #endregion

            PeraLinkCoreWcf.GetInsuranceTransactionHistoryRequest request = new PeraLinkCoreWcf.GetInsuranceTransactionHistoryRequest();

            #region Validate Dates
            if (this.StartDate.HasValue && this.EndDate == null)
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(111);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }

            if (this.StartDate == null && this.EndDate.HasValue)
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(110);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }

            if (this.StartDate.HasValue && this.EndDate.HasValue)
            {
                if (this.StartDate.Value > this.EndDate.Value)
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(111);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }

                if (this.StartDate.Value.Date < DateTime.Today.Date.AddDays(-30))
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(110);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }

                if (this.EndDate.Value.Date > DateTime.Today.Date)
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(111);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }

                request.StartDate = this.StartDate.Value;
                request.EndDate = this.EndDate.Value.AddDays(1);
            }

            if (this.StartDate == null && this.EndDate == null)
            {
                request.StartDate = DateTime.Today;
                request.EndDate = DateTime.Today.AddDays(1);
            }
            #endregion

            request.Agent = agent;
            request.UserID = this.UserID;
            request.Terminal = terminal;
            request.EndRowNumber = (this.PageNumber * _pageSize);
            request.StartRowNumber = (request.EndRowNumber - _pageSize) + 1;

            PeraLinkCoreWcf.GetInsuranceTransactionHistoryResult result = serviceClient.GetInsuranceTransactionHistory(request);
            returnValue.InsuranceCollection = new InsuranceCollection();
            returnValue.InsuranceCollection.AddList(result.InsuranceCollection);
            returnValue.TotalPageCount = ((result.TotalRecordCount % _pageSize) == 0) ? (result.TotalRecordCount / _pageSize) : (result.TotalRecordCount / _pageSize) + 1;
        }


        return returnValue;
    }
    #endregion
}