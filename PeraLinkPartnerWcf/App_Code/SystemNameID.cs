using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SystemNameID
/// </summary>
public class SystemNameID
{
    #region Constructor
    public SystemNameID()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    public const int PeraLinkCoreWcf = 1;
    public const int PeraLinkPartnerWcf = 2;
    public const int PeraLinkAdminWcf = 3;
    #endregion
}