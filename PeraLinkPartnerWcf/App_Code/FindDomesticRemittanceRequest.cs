#define LIBREPADALA_PROMPT
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class FindDomesticRemittanceRequest : BaseRequest
{
    #region Constructor
    public FindDomesticRemittanceRequest()
    {
        //

        //
    }
    #endregion

    #region Fields/Properties
    private string _controlNumber;
    [DataMember]
    public string ControlNumber
    {
        get { return _controlNumber; }
        set { _controlNumber = value; }
    }

    private string _partnerCode;
    [DataMember]
    public string PartnerCode
    {
        get { return _partnerCode; }
        set { _partnerCode = value; }
    }

    private string _token;
    [DataMember]
    public string Token
    {
        get { return _token; }
        set { _token = value; }
    }

    private string _agentCode;
    [DataMember]
    public string AgentCode
    {
        get { return _agentCode; }
        set { _agentCode = value; }
    }

    private string _pullTransactionPartnerCode;
    [DataMember]
    public string PullTransactionPartnerCode
    {
        get { return _pullTransactionPartnerCode; }
        set { _pullTransactionPartnerCode = value; }
    }

    private string _userID;
    [DataMember]
    public string UserID
    {
        get { return _userID; }
        set { _userID = value; }
    }

    private string _externalPartnerBeneficiaryNumber;

    internal string ExternalPartnerBeneficiaryNumber
    {
        get { return _externalPartnerBeneficiaryNumber; }
    }

    private string _externalPartnerClientNumber;

    internal string ExternalPartnerClientNumber
    {
        get { return _externalPartnerClientNumber; }
    }

    private Int64 _externalPartnerRemittanceID;

    internal Int64 ExternalPartnerRemittanceID
    {
        get { return _externalPartnerRemittanceID; }
    }

    private string _terminalCode;
    [DataMember]
    public string TerminalCode
    {
        get { return _terminalCode; }
        set { _terminalCode = value; }
    }
    #endregion

    #region Internal Methods
    internal FindDomesticRemittanceResult Process()
    {
        FindDomesticRemittanceResult returnValue = new FindDomesticRemittanceResult();

        // Important: This function validates the request credential
        this.AuthenticateRequest();

        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
            #region Validate Partner Record
            PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
            partner.PartnerCode = this.PartnerCode;
            partner = serviceClient.FindPartnerCode(partner);

            if (partner.PartnerID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else { }

            if (partner.Token == this.Token) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (partner.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            #endregion

            #region Validate Agent Record
            PeraLinkCoreWcf.Agent agent = new PeraLinkCoreWcf.Agent();
            agent.AgentCode = this.AgentCode;
            agent.Partner = partner;
            agent = serviceClient.FindAgentCodeOfPartner(agent);

            if (agent.AgentID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }

            if (agent.Partner.PartnerID == partner.PartnerID)
            { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(43);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (agent.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(46);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            #endregion

            #region Validate Terminal Record
            PeraLinkCoreWcf.Terminal terminal = new PeraLinkCoreWcf.Terminal();

            if (serviceClient.GetAgentTerminalCollection(agent).Length > 0)
            {
                terminal.TerminalCode = this.TerminalCode;
                terminal.Agent = agent;
                terminal.Agent.Partner = partner;
                terminal = serviceClient.FindTerminal(terminal);

                if (terminal.TerminalID == 0)
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(104);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
                else
                {
                    // Proceed
                }

                if (terminal.Agent.AgentID == agent.AgentID)
                { }
                else
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(105);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }

                if (terminal.Activated) { }
                else
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(106);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
            }
            else
            {
                //do nothing
            }
            #endregion

            #region Validate Partner's Access To Find Domestic Remittance Service
            PeraLinkCoreWcf.PartnerServiceMapping[] partnerServiceMappingCollection = serviceClient.GetPartnerServiceMappingCollection(partner);

            PeraLinkCoreWcf.PartnerServiceMapping partnerServiceMapping = Array.Find<PeraLinkCoreWcf.PartnerServiceMapping>(partnerServiceMappingCollection
                , delegate(PeraLinkCoreWcf.PartnerServiceMapping eachPartnerServiceMapping)
                {
                    return eachPartnerServiceMapping.ServiceType.ServiceTypeID == ServiceTypeID.PayoutRemittance
                        || eachPartnerServiceMapping.ServiceType.ServiceTypeID == ServiceTypeID.AmendRemittance
                        || eachPartnerServiceMapping.ServiceType.ServiceTypeID == ServiceTypeID.RefundRemittance
                        || eachPartnerServiceMapping.ServiceType.ServiceTypeID == ServiceTypeID.CancelRemittance;
                });

            if (partnerServiceMapping == null)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(23);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }
            #endregion

            #region Log Audit Trail
            PeraLinkCoreWcf.AuditTrail auditTrail = new PeraLinkCoreWcf.AuditTrail();
            auditTrail.CreatedByAgent = agent;
            this.UserID = string.IsNullOrWhiteSpace(this.UserID) ? "SYSTEM" : this.UserID;
            auditTrail.CreatedByUserID = this.UserID;
            auditTrail.DateTimeCreated = DateTime.Now;
            auditTrail.IPAddress = SystemUtility.GetClientIPAddress();
            auditTrail.AuditAction = new PeraLinkCoreWcf.AuditAction();
            auditTrail.AuditAction.AuditActionID = AuditActionID.FindDomesticRemittanceTransaction;
            auditTrail.AuditDetails = string.Format("Control Number: {0}", this.ControlNumber);
            serviceClient.AddAuditTrail(auditTrail);
            #endregion

            if (string.IsNullOrWhiteSpace(this.PullTransactionPartnerCode))
            {
                #region Find Domestic Remittance
                PeraLinkCoreWcf.DomesticRemittance domesticRemittance = new PeraLinkCoreWcf.DomesticRemittance();
                domesticRemittance.ControlNumber = this.ControlNumber;

                Partner matchedPartner = SystemUtility.FindPartnerWithControlNumberFormat(domesticRemittance.ControlNumber);

                if (matchedPartner == null)
                {
                    domesticRemittance = serviceClient.FindDomesticRemittance(domesticRemittance);

                    if (domesticRemittance.DomesticRemittanceID == 0)
                    {
                        PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(14);
                        throw new RDFramework.ClientException(message.Content, message.MessageID);
                    }
                    else
                    {
                        PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(125);
                        returnValue.Message = message.Description;
                        returnValue.MessageID = message.SystemMessageID;
                    }

                    #region Validate Partner's access To Transaction
                    if (domesticRemittance.SentByAgent.Partner.PartnerID == agent.Partner.PartnerID)
                    {
                        // Proceed
                    }
                    else
                    {
                        PeraLinkCoreWcf.PartnerServicePair[] partnerServicePairCollection = serviceClient.GetPartnerServicePairCollection(partnerServiceMapping);

                        PeraLinkCoreWcf.PartnerServicePair pairing = Array.Find<PeraLinkCoreWcf.PartnerServicePair>(partnerServicePairCollection
                            , delegate(PeraLinkCoreWcf.PartnerServicePair eachPartnerServicePair)
                            {
                                if (eachPartnerServicePair.PairedPartner == null)
                                {
                                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(2);
                                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                                }
                                else
                                {
                                    return eachPartnerServicePair.PairedPartner.PartnerID == domesticRemittance.SentByAgent.Partner.PartnerID;
                                }
                            });

                        if (pairing == null)
                        {
                            PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(127);
                            throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                        }
                        else
                        {
                            // Proceed
                        }
                        if (pairing.PairedPartner.Activated)
                        { }
                        else
                        {
                            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
                            throw new RDFramework.ClientException(message.Content, message.MessageID);
                        }
                    }
                    #endregion
                }
                else
                {
                    PeraLinkCoreWcf.Partner transactionPartner = new PeraLinkCoreWcf.Partner();
                    transactionPartner.PartnerCode = matchedPartner.PartnerCode;
                    transactionPartner = serviceClient.FindPartnerCode(transactionPartner);

                    PeraLinkCoreWcf.PartnerServicePair[] partnerServicePairCollection = serviceClient.GetPartnerServicePairCollection(partnerServiceMapping);

                    PeraLinkCoreWcf.PartnerServicePair pairing = Array.Find<PeraLinkCoreWcf.PartnerServicePair>(partnerServicePairCollection
                        , delegate(PeraLinkCoreWcf.PartnerServicePair eachPartnerServicePair)
                        {
                            if (eachPartnerServicePair.PairedPartner == null)
                            {
                                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(2);
                                throw new RDFramework.ClientException(message.Content, message.MessageID);
                            }
                            else
                            {
                                return eachPartnerServicePair.PairedPartner.PartnerID == transactionPartner.PartnerID;
                            }
                        });

                    if (pairing == null)
                    {
                        PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(14);
                        throw new RDFramework.ClientException(message.Content, message.MessageID);
                    }
                    else
                    {
                        // Proceed
                    }

                    if (pairing.PairedPartner.Activated) { }
                    else
                    {
                        PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
                        throw new RDFramework.ClientException(message.Content, message.MessageID);
                    }

                    if (matchedPartner.PartnerCode == SystemSetting.CebuanaLhuillierPartnerCode)
                    {
                        #region Cebuana Lhuillier
                        using (CebuanaLhuillierWSClient cebuanaLhuillierWSClient = new CebuanaLhuillierWSClient())
                        {
                            domesticRemittance = cebuanaLhuillierWSClient.FindDomesticRemittance(domesticRemittance.ControlNumber);

                            if (domesticRemittance == null)
                            {
                                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(14);
                                throw new RDFramework.ClientException(message.Content, message.MessageID);
                            }
                            else
                            {
#if LIBREPADALA_PROMPT
                                #region Validate Service Fee
                                if (domesticRemittance.ServiceFee > 0)
                                {
                                    // proceed
                                }
                                else
                                {
                                    if (domesticRemittance.RemittanceStatus.RemittanceStatusID == RemittanceStatusID.Paid
                                        || domesticRemittance.RemittanceStatus.RemittanceStatusID == RemittanceStatusID.Refunded
                                        || domesticRemittance.RemittanceStatus.RemittanceStatusID == RemittanceStatusID.Cancelled)
                                    {
                                        // proceed
                                    }
                                    else
                                    {
                                        PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(127);
                                        throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                                    }                                    
                                }
                                #endregion
#else
#endif

                                // Important: Keep the original sender and beneficiary number from the external partner database
                                _externalPartnerBeneficiaryNumber = domesticRemittance.Beneficiary.Client.ClientNumber;
                                _externalPartnerClientNumber = domesticRemittance.SenderClient.ClientNumber;
                                _externalPartnerRemittanceID = domesticRemittance.DomesticRemittanceID;

                                domesticRemittance.SendCurrency = serviceClient.FindCurrency(domesticRemittance.SendCurrency);

#region Register sender in PeraLink
                                PeraLinkCoreWcf.Client senderCopy = new PeraLinkCoreWcf.Client();
                                senderCopy.FirstName = domesticRemittance.SenderClient.FirstName;
                                senderCopy.LastName = domesticRemittance.SenderClient.LastName;
                                senderCopy.MiddleName = domesticRemittance.SenderClient.MiddleName;
                                senderCopy.BirthDate = domesticRemittance.SenderClient.BirthDate;

                                PeraLinkCoreWcf.Client[] registeredSenderCollection = serviceClient.FindClient(senderCopy, partner.PartnerID);

                                if (registeredSenderCollection.Length > 0)
                                {
                                    domesticRemittance.SenderClient = registeredSenderCollection[0];
                                }
                                else
                                {
                                    domesticRemittance.SenderClient.CreatedByAgent = agent;
                                    domesticRemittance.SenderClient.CreatedByUserID = this.UserID;
                                    domesticRemittance.SenderClient.DateTimeCreated = DateTime.Now;
                                    domesticRemittance.SenderClient.CreatedByUserID = this.UserID;
                                    domesticRemittance.SenderClient = serviceClient.AddClient(domesticRemittance.SenderClient);
                                }
#endregion

#region Register beneficiary in PeraLink
                                domesticRemittance.Beneficiary.SenderClient = domesticRemittance.SenderClient;
                                PeraLinkCoreWcf.Beneficiary beneficiaryCopy = new PeraLinkCoreWcf.Beneficiary();
                                beneficiaryCopy.FirstName = domesticRemittance.Beneficiary.FirstName;
                                beneficiaryCopy.LastName = domesticRemittance.Beneficiary.LastName;
                                beneficiaryCopy.MiddleName = domesticRemittance.Beneficiary.MiddleName;
                                beneficiaryCopy.BirthDate = domesticRemittance.Beneficiary.BirthDate;
                                beneficiaryCopy.SenderClient = domesticRemittance.SenderClient;

                                PeraLinkCoreWcf.Beneficiary[] registeredBeneficiaryCollection = serviceClient.FindBeneficiary(beneficiaryCopy);

                                if (registeredBeneficiaryCollection.Length > 0)
                                {
                                    domesticRemittance.Beneficiary = registeredBeneficiaryCollection[0];
                                }
                                else
                                {
                                    domesticRemittance.Beneficiary.CreatedBy = this.UserID;
                                    domesticRemittance.Beneficiary.DateTimeCreated = DateTime.Now;
                                    domesticRemittance.Beneficiary = serviceClient.AddBeneficiary(domesticRemittance.Beneficiary);
                                }

#endregion
                                domesticRemittance.SentByAgent.Partner = new PeraLinkCoreWcf.Partner();
                                domesticRemittance.SentByAgent.Partner.PartnerID = transactionPartner.PartnerID;

                                domesticRemittance.SentByAgent = serviceClient.FindAgentCode(domesticRemittance.SentByAgent);

                                domesticRemittance.SentByAgent.Partner.PartnerCode = transactionPartner.PartnerCode;

                                if (domesticRemittance.SentByAgent.AgentID == 0)
                                {
                                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(38);
                                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                                }
                                else { }
                            }
                        }
#endregion
                    }
                    else if (matchedPartner.PartnerCode == SystemSetting.OkRemitPartnerCode)
                    {
#region OK Remit
                        OkRemitWcfClient okRemitWcfClient = new OkRemitWcfClient();
                        domesticRemittance = okRemitWcfClient.FindRemittance(this.ControlNumber);

                        if (domesticRemittance == null)
                        {
                            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(14);
                            throw new RDFramework.ClientException(message.Content, message.MessageID);
                        }
                        else
                        {
                            if (domesticRemittance.RemittanceStatus.RemittanceStatusID == RemittanceStatusID.Paid)
                            {
                                domesticRemittance = serviceClient.FindDomesticRemittance(domesticRemittance);
                            }
                            else
                            {

#region Register sender in PeraLink
                                PeraLinkCoreWcf.Client senderCopy = new PeraLinkCoreWcf.Client();
                                senderCopy.FirstName = domesticRemittance.SenderClient.FirstName;
                                senderCopy.LastName = domesticRemittance.SenderClient.LastName;
                                senderCopy.MiddleName = domesticRemittance.SenderClient.MiddleName;
                                senderCopy.BirthDate = domesticRemittance.SenderClient.BirthDate;

                                PeraLinkCoreWcf.Client[] registeredSenderCollection = serviceClient.FindClient(senderCopy, partner.PartnerID);

                                if (registeredSenderCollection.Length > 0)
                                {
                                    domesticRemittance.SenderClient = registeredSenderCollection[0];
                                }
                                else
                                {
                                    domesticRemittance.SenderClient.CreatedByAgent = agent;
                                    domesticRemittance.SenderClient.CreatedByUserID = this.UserID;
                                    domesticRemittance.SenderClient.DateTimeCreated = DateTime.Now;
                                    domesticRemittance.SenderClient.CreatedByUserID = this.UserID;
                                    domesticRemittance.SenderClient = serviceClient.AddClient(domesticRemittance.SenderClient);
                                }
#endregion

#region Register beneficiary in PeraLink
                                domesticRemittance.Beneficiary.SenderClient = domesticRemittance.SenderClient;
                                PeraLinkCoreWcf.Beneficiary beneficiaryCopy = new PeraLinkCoreWcf.Beneficiary();
                                beneficiaryCopy.FirstName = domesticRemittance.Beneficiary.FirstName;
                                beneficiaryCopy.LastName = domesticRemittance.Beneficiary.LastName;
                                beneficiaryCopy.MiddleName = domesticRemittance.Beneficiary.MiddleName;
                                beneficiaryCopy.BirthDate = domesticRemittance.Beneficiary.BirthDate;
                                beneficiaryCopy.SenderClient = domesticRemittance.SenderClient;

                                PeraLinkCoreWcf.Beneficiary[] registeredBeneficiaryCollection = serviceClient.FindBeneficiary(beneficiaryCopy);

                                if (registeredBeneficiaryCollection.Length > 0)
                                {
                                    domesticRemittance.Beneficiary = registeredBeneficiaryCollection[0];
                                }
                                else
                                {
                                    domesticRemittance.Beneficiary.CreatedBy = this.UserID;
                                    domesticRemittance.Beneficiary.DateTimeCreated = DateTime.Now;
                                    domesticRemittance.Beneficiary = serviceClient.AddBeneficiary(domesticRemittance.Beneficiary);
                                }
#endregion

                            }

                            domesticRemittance.DomesticRemittanceID = 1;
                            domesticRemittance.SendCurrency = serviceClient.FindCurrency(domesticRemittance.SendCurrency);

                            domesticRemittance.SentByAgent = new PeraLinkCoreWcf.Agent();
                            domesticRemittance.SentByAgent.Partner = transactionPartner;

                            PeraLinkCoreWcf.Agent[] agentCollection = serviceClient.GetPartnerAgentCollection(transactionPartner);
                            if (agentCollection.Length == null)
                            {
                                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                                throw new RDFramework.ClientException(message.Content, message.MessageID);

                            }
                            else if (agentCollection.Length < 1)
                            {
                                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                                throw new RDFramework.ClientException(message.Content, message.MessageID);
                            }
                            else
                            {
                                domesticRemittance.SentByAgent.AgentCode = agentCollection[0].AgentCode;
                                domesticRemittance.SentByAgent = serviceClient.FindAgentCode(domesticRemittance.SentByAgent);

                                domesticRemittance.SentByAgent.Partner = transactionPartner;
                            }
                        }

#endregion
                    }

                    else if (matchedPartner.PartnerCode == SystemSetting.LbcPartnerCode)
                    {
#region LBC
                        LbcWcfClient lbcWcfClient = new LbcWcfClient();
                        domesticRemittance = lbcWcfClient.FindRemittance(this.ControlNumber);

                        if (domesticRemittance == null)
                        {
                            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(14);
                            throw new RDFramework.ClientException(message.Content, message.MessageID);
                        }
                        else
                        {
                            if (domesticRemittance.RemittanceStatus.RemittanceStatusID == RemittanceStatusID.Paid)
                            {
                                domesticRemittance = serviceClient.FindDomesticRemittance(domesticRemittance);
                            }
                            else
                            {

#region Register sender in PeraLink
                                PeraLinkCoreWcf.Client senderCopy = new PeraLinkCoreWcf.Client();
                                senderCopy.FirstName = domesticRemittance.SenderClient.FirstName;
                                senderCopy.LastName = domesticRemittance.SenderClient.LastName;
                                senderCopy.MiddleName = domesticRemittance.SenderClient.MiddleName;
                                senderCopy.BirthDate = domesticRemittance.SenderClient.BirthDate;

                                PeraLinkCoreWcf.Client[] registeredSenderCollection = serviceClient.FindClient(senderCopy, partner.PartnerID);

                                if (registeredSenderCollection.Length > 0)
                                {
                                    domesticRemittance.SenderClient = registeredSenderCollection[0];
                                }
                                else
                                {
                                    domesticRemittance.SenderClient.CreatedByAgent = agent;
                                    domesticRemittance.SenderClient.CreatedByUserID = this.UserID;
                                    domesticRemittance.SenderClient.DateTimeCreated = DateTime.Now;
                                    domesticRemittance.SenderClient.CreatedByUserID = this.UserID;
                                    domesticRemittance.SenderClient = serviceClient.AddClient(domesticRemittance.SenderClient);
                                }
#endregion

#region Register beneficiary in PeraLink
                                domesticRemittance.Beneficiary.SenderClient = domesticRemittance.SenderClient;
                                PeraLinkCoreWcf.Beneficiary beneficiaryCopy = new PeraLinkCoreWcf.Beneficiary();
                                beneficiaryCopy.FirstName = domesticRemittance.Beneficiary.FirstName;
                                beneficiaryCopy.LastName = domesticRemittance.Beneficiary.LastName;
                                beneficiaryCopy.MiddleName = domesticRemittance.Beneficiary.MiddleName;
                                beneficiaryCopy.BirthDate = domesticRemittance.Beneficiary.BirthDate;
                                beneficiaryCopy.SenderClient = domesticRemittance.SenderClient;

                                PeraLinkCoreWcf.Beneficiary[] registeredBeneficiaryCollection = serviceClient.FindBeneficiary(beneficiaryCopy);

                                if (registeredBeneficiaryCollection.Length > 0)
                                {
                                    domesticRemittance.Beneficiary = registeredBeneficiaryCollection[0];
                                }
                                else
                                {
                                    domesticRemittance.Beneficiary.CreatedBy = this.UserID;
                                    domesticRemittance.Beneficiary.DateTimeCreated = DateTime.Now;
                                    domesticRemittance.Beneficiary = serviceClient.AddBeneficiary(domesticRemittance.Beneficiary);
                                }
#endregion

                            }

                            domesticRemittance.DomesticRemittanceID = 1;
                            domesticRemittance.SendCurrency = serviceClient.FindCurrency(domesticRemittance.SendCurrency);

                            domesticRemittance.SentByAgent.Partner = transactionPartner;
                            domesticRemittance.SentByAgent = serviceClient.FindAgentCode(domesticRemittance.SentByAgent);

                            if (domesticRemittance.SentByAgent.AgentID == 0)
                            {
                                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(140);
                                throw new RDFramework.ClientException(string.Format(message.Description, domesticRemittance.SentByAgent.AgentCode, transactionPartner.Name), message.SystemMessageID);
                            }

                            domesticRemittance.SentByAgent.Partner = transactionPartner;

                            GetSendServiceFeeRequest feeRequest = new GetSendServiceFeeRequest();
                            feeRequest.AgentCode = domesticRemittance.SentByAgent.AgentCode;
                            feeRequest.PartnerCode = domesticRemittance.SentByAgent.Partner.PartnerCode;
                            feeRequest.Token = domesticRemittance.SentByAgent.Partner.Token;
                            feeRequest.CurrencyID = domesticRemittance.SendCurrency.CurrencyID;
                            feeRequest.ServiceCredential = this.ServiceCredential;
                            feeRequest.PrincipalAmount = domesticRemittance.PrincipalAmount;

                            GetSendServiceFeeResult feeResult = new GetSendServiceFeeResult();

                            try
                            {
                                feeResult = feeRequest.Process();
                                domesticRemittance.ServiceFee = feeResult.ServiceFee;
                            }
                            catch (RDFramework.ClientException clientEx)
                            {
                                if (clientEx.MessageID == SystemResource.GetMessageWithListID(28).MessageID)
                                {
                                    domesticRemittance.ServiceFee = domesticRemittance.ServiceFee;
                                }
                                else
                                {
                                    throw clientEx;
                                }
                            }

                        }

#endregion
                    }
                    else if (matchedPartner.PartnerCode == SystemSetting.UsscPartnerCode)
                    {
#region USSC
                        UsscWcfClient usscWcfClient = new UsscWcfClient();
                        string refNum = string.Format("{0}CEB{1}", DateTime.Now.ToString("yyyyMMdd"), DateTime.Now.Ticks.ToString().PadLeft(20, '0'));
                        
                        domesticRemittance = usscWcfClient.FindRemittance(this.ControlNumber, this.AgentCode, refNum);

                        if (domesticRemittance == null)
                        {
                            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(14);
                            throw new RDFramework.ClientException(message.Content, message.MessageID);
                        }
                        else
                        {
                            if (domesticRemittance.RemittanceStatus.RemittanceStatusID == RemittanceStatusID.Paid)
                            {
                                domesticRemittance = serviceClient.FindDomesticRemittance(domesticRemittance);
                            }
                            else
                            {

#region Register sender in PeraLink
                                PeraLinkCoreWcf.Client senderCopy = new PeraLinkCoreWcf.Client();
                                senderCopy.FirstName = domesticRemittance.SenderClient.FirstName;
                                senderCopy.LastName = domesticRemittance.SenderClient.LastName;
                                senderCopy.MiddleName = domesticRemittance.SenderClient.MiddleName;
                                senderCopy.BirthDate = domesticRemittance.SenderClient.BirthDate;

                                PeraLinkCoreWcf.Client[] registeredSenderCollection = serviceClient.FindClient(senderCopy, partner.PartnerID);

                                if (registeredSenderCollection.Length > 0)
                                {
                                    domesticRemittance.SenderClient = registeredSenderCollection[0];
                                }
                                else
                                {
                                    domesticRemittance.SenderClient.CreatedByAgent = agent;
                                    domesticRemittance.SenderClient.CreatedByUserID = this.UserID;
                                    domesticRemittance.SenderClient.DateTimeCreated = DateTime.Now;
                                    domesticRemittance.SenderClient.CreatedByUserID = this.UserID;
                                    domesticRemittance.SenderClient = serviceClient.AddClient(domesticRemittance.SenderClient);
                                }
#endregion

#region Register beneficiary in PeraLink
                                domesticRemittance.Beneficiary.SenderClient = domesticRemittance.SenderClient;
                                PeraLinkCoreWcf.Beneficiary beneficiaryCopy = new PeraLinkCoreWcf.Beneficiary();
                                beneficiaryCopy.FirstName = domesticRemittance.Beneficiary.FirstName;
                                beneficiaryCopy.LastName = domesticRemittance.Beneficiary.LastName;
                                beneficiaryCopy.MiddleName = domesticRemittance.Beneficiary.MiddleName;
                                beneficiaryCopy.BirthDate = domesticRemittance.Beneficiary.BirthDate;
                                beneficiaryCopy.SenderClient = domesticRemittance.SenderClient;

                                PeraLinkCoreWcf.Beneficiary[] registeredBeneficiaryCollection = serviceClient.FindBeneficiary(beneficiaryCopy);

                                if (registeredBeneficiaryCollection.Length > 0)
                                {
                                    domesticRemittance.Beneficiary = registeredBeneficiaryCollection[0];
                                }
                                else
                                {
                                    domesticRemittance.Beneficiary.CreatedBy = this.UserID;
                                    domesticRemittance.Beneficiary.DateTimeCreated = DateTime.Now;
                                    domesticRemittance.Beneficiary = serviceClient.AddBeneficiary(domesticRemittance.Beneficiary);
                                }
#endregion

                            }

                            domesticRemittance.DomesticRemittanceID = 1;
                            domesticRemittance.SendCurrency = serviceClient.FindCurrency(domesticRemittance.SendCurrency);

                            domesticRemittance.SentByAgent.Partner = transactionPartner;
                            domesticRemittance.SentByAgent = serviceClient.FindAgentCode(domesticRemittance.SentByAgent);

                            if (domesticRemittance.SentByAgent.AgentID == 0)
                            {
                                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(140);
                                throw new RDFramework.ClientException(string.Format(message.Description, domesticRemittance.SentByAgent.AgentCode, transactionPartner.Name), message.SystemMessageID);
                            }

                            domesticRemittance.SentByAgent.Partner = transactionPartner;
                        }

#endregion
                    }
                    else
                    {
                        PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(20);
                        throw new RDFramework.ClientException(message.Content, message.MessageID);
                    }
                }
#endregion

#region Map Transaction Details
                returnValue.BeneficiaryFirstName = domesticRemittance.Beneficiary.FirstName;
                returnValue.BeneficiaryID = domesticRemittance.Beneficiary.BeneficiaryID;
                returnValue.BeneficiaryLastName = domesticRemittance.Beneficiary.LastName;
                returnValue.BeneficiaryMiddleName = domesticRemittance.Beneficiary.MiddleName;
                returnValue.BeneficiaryBirthDate = domesticRemittance.Beneficiary.BirthDate;
                returnValue.ControlNumber = domesticRemittance.ControlNumber;
                returnValue.DateTimeSent = domesticRemittance.DateTimeSent;
                returnValue.DomesticRemittanceID = domesticRemittance.DomesticRemittanceID;
                returnValue.PrincipalAmount = domesticRemittance.PrincipalAmount;
                returnValue.RemittanceStatusDescription = domesticRemittance.RemittanceStatus.Description;
                returnValue.RemittanceStatusID = domesticRemittance.RemittanceStatus.RemittanceStatusID;
                returnValue.SendCurrencyCode = domesticRemittance.SendCurrency.Code;
                returnValue.SendCurrencyID = domesticRemittance.SendCurrency.CurrencyID;
                returnValue.SenderClientID = domesticRemittance.SenderClient.ClientID;
                returnValue.SenderClientNumber = domesticRemittance.SenderClient.ClientNumber;
                returnValue.SenderFirstName = domesticRemittance.SenderClient.FirstName;
                returnValue.SenderLastName = domesticRemittance.SenderClient.LastName;
                returnValue.SenderMiddleName = domesticRemittance.SenderClient.MiddleName;
                returnValue.SenderBirthDate = domesticRemittance.SenderClient.BirthDate;
                returnValue.ServiceFee = domesticRemittance.ServiceFee;
                returnValue.SentByPartnerCode = domesticRemittance.SentByAgent.Partner.PartnerCode;
                returnValue.SentByPartnerID = domesticRemittance.SentByAgent.Partner.PartnerID;
                returnValue.SentByPartnerName = domesticRemittance.SentByAgent.Partner.Name;
                returnValue.SentByAgentCode = domesticRemittance.SentByAgent.AgentCode;
                returnValue.SentByAgentID = domesticRemittance.SentByAgent.AgentID;
                returnValue.SentByUserID = domesticRemittance.SentByUserID;

                if (domesticRemittance.SentByTerminal != null)
                {
                    returnValue.SentByTerminalID = domesticRemittance.SentByTerminal.TerminalID;
                    returnValue.SentByTerminalCode = domesticRemittance.SentByTerminal.TerminalCode;
                }

                if (domesticRemittance.RemittanceStatus.RemittanceStatusID == RemittanceStatusID.Paid)
                {
                    if (domesticRemittance.DateTimeUpdated.HasValue)
                        returnValue.PayoutDateTime = domesticRemittance.DateTimeUpdated.Value;

                    if (domesticRemittance.UpdatedByAgent != null
                        && domesticRemittance.UpdatedByAgent.Partner != null)
                    {
                        returnValue.PayoutAgent = domesticRemittance.UpdatedByAgent.AgentCode;
                        returnValue.PayoutPartnerCode = domesticRemittance.UpdatedByAgent.Partner.PartnerCode;
                    }

                    if (domesticRemittance.UpdatedByTerminal != null)
                    {
                        returnValue.PayoutTerminalCode = domesticRemittance.UpdatedByTerminal.TerminalCode;
                    }
                }
#endregion
            }
            else
            {
#region Validate Pull Partner

                PeraLinkCoreWcf.Partner pullPartner = new PeraLinkCoreWcf.Partner();
                pullPartner.PartnerCode = this.PullTransactionPartnerCode;
                pullPartner = serviceClient.FindPartnerCode(partner);

                if (pullPartner.PartnerID == 0)
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
                else
                {
                    // Proceed
                }

                if (pullPartner.Activated)
                { }
                else
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
#endregion

#region Validate Partner's access To Transaction
                PeraLinkCoreWcf.PartnerServicePair[] partnerServicePairCollection = serviceClient.GetPartnerServicePairCollection(partnerServiceMapping);

                PeraLinkCoreWcf.PartnerServicePair pairing = Array.Find<PeraLinkCoreWcf.PartnerServicePair>(partnerServicePairCollection
                    , delegate(PeraLinkCoreWcf.PartnerServicePair eachPartnerServicePair)
                    {
                        if (eachPartnerServicePair.PairedPartner == null)
                        {
                            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(2);
                            throw new RDFramework.ClientException(message.Content, message.MessageID);
                        }
                        else
                        {
                            return eachPartnerServicePair.PairedPartner.PartnerID == pullPartner.PartnerID;
                        }
                    });

                if (pairing == null)
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(14);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
                else
                {
                    // Proceed
                }
#endregion

#region Direct transaction search
                PeraLinkCoreWcf.Message searchMessage = SystemResource.GetMessageWithListID(20);
                throw new RDFramework.ClientException(searchMessage.Content, searchMessage.MessageID);
#endregion
            }

            if (returnValue.RemittanceStatusID == RemittanceStatusID.Paid
                && returnValue.PayoutPartnerCode == this.PartnerCode)
            {
                //do nothing
            }
            else
            {
                returnValue.PayoutPartnerCode = string.Empty;
                returnValue.PayoutAgent = string.Empty;
            }
        }

        returnValue.ResultStatus = ResultStatus.Successful;

        return returnValue;
    }
#endregion
}