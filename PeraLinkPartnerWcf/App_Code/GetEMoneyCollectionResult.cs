using System;
using System.Collections.Generic;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetEMoneyCollectionResult: ServiceResult
{
    #region Constructor
    public GetEMoneyCollectionResult()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private EMoneyCollection _eMoneyCollection;
    [DataMember]
    public EMoneyCollection EMoneyCollection
    {
        get { return _eMoneyCollection; }
        set { _eMoneyCollection = value; }
    }

    private decimal _totalBalance;
    [DataMember]
    public decimal TotalBalance
    {
        get { return _totalBalance; }
        set { _totalBalance = value; }
    }

    private decimal _totalCashIn;
    [DataMember]
    public decimal TotalCashIn
    {
        get { return _totalCashIn; }
        set { _totalCashIn = value; }
    }

    private decimal _totalCashOut;
    [DataMember]
    public decimal TotalCashOut
    {
        get { return _totalCashOut; }
        set { _totalCashOut = value; }
    }
    #endregion
}