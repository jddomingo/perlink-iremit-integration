using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ClientCollection
/// </summary>
public class ClientCollection: List<Client>
{
    #region Constructor
    public ClientCollection()
	{
		//

		//
    }
    #endregion

    #region Public Methods
    public void AddList(PeraLinkCoreWcf.Client[] clientCollection)
    {
        Array.ForEach<PeraLinkCoreWcf.Client>(clientCollection
            , delegate(PeraLinkCoreWcf.Client eachClient)
            {
                Client mappedClient = new Client();
                mappedClient.Load(eachClient);
                this.Add(mappedClient);
            });
    }

    #endregion
}