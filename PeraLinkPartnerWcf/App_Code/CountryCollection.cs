using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CountryCollection
/// </summary>
public class CountryCollection: List<Country>
{
    #region Constructor
    public CountryCollection()
	{
		//

		//
    }
    #endregion

    #region Public Methods
    public void AddList(PeraLinkCoreWcf.Country[] countryCollection)
    {
        Array.ForEach<PeraLinkCoreWcf.Country>(countryCollection
            , delegate(PeraLinkCoreWcf.Country eachClient)
            {
                Country mappedCountry = new Country();
                mappedCountry.Load(eachClient);
                this.Add(mappedCountry);
            });
    }
    #endregion
}