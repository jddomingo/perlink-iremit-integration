using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for XoomWSClient
/// </summary>
public class XoomWSClient : IDisposable
{
    #region Constructor
    public XoomWSClient()
    {
        _serviceClient = new PeraLinkXoomWS.RemittancePartnerWebServiceSoapClient();
    }
    #endregion

    #region Fields/Properties
    private PeraLinkXoomWS.RemittancePartnerWebServiceSoapClient _serviceClient;
    #endregion

    #region Public Methods
    public PeraLinkXoomWS.LookupTransactionResult LookupTransaction(PeraLinkXoomWS.LookupTransactionRequest lookUpTransactionRequest)
    {
        lookUpTransactionRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkXoomWS.LookupTransactionResult lookupTransactionResult = _serviceClient.RemittancePartnerLookup(lookUpTransactionRequest);

        switch (lookupTransactionResult.ResultCode)
        {
            case PeraLinkXoomWS.LookupTransactionResultCode.Successful:
                {
                    return lookupTransactionResult;
                }
            case PeraLinkXoomWS.LookupTransactionResultCode.PartnerError:
            case PeraLinkXoomWS.LookupTransactionResultCode.ServerError:
            case PeraLinkXoomWS.LookupTransactionResultCode.UnrecognizedResponse:
            case PeraLinkXoomWS.LookupTransactionResultCode.Unsuccessful:
                {
                    throw new RDFramework.ClientException(lookupTransactionResult.MessageToClient, (int)lookupTransactionResult.ResultCode);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(lookupTransactionResult.ResultCode.ToString());
                }
        }
    }

    public PeraLinkXoomWS.PayoutTransactionResult PayoutTransaction(PeraLinkXoomWS.PayoutTransactionRequest payoutTransactionRequest)
    {
        payoutTransactionRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkXoomWS.PayoutTransactionResult payoutTransactionResult = _serviceClient.RemittancePartnerPayout(payoutTransactionRequest);

        switch (payoutTransactionResult.ResultCode)
        {
            case PeraLinkXoomWS.PayoutTransactionResultCode.Successful:
                {
                    return payoutTransactionResult;
                }
            case PeraLinkXoomWS.PayoutTransactionResultCode.PartnerError:
            case PeraLinkXoomWS.PayoutTransactionResultCode.ServerError:
            case PeraLinkXoomWS.PayoutTransactionResultCode.UnrecognizedResponse:
            case PeraLinkXoomWS.PayoutTransactionResultCode.Unsuccessful:
                {
                    throw new RDFramework.ClientException(payoutTransactionResult.MessageToClient, (int)payoutTransactionResult.ResultCode);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(payoutTransactionResult.ResultCode.ToString());
                }
        }
    }


    public static PeraLinkCoreWcf.RemittanceStatus MapStatus(PeraLinkXoomWS.TransactionStatus transactionStatus)
    {
        switch (transactionStatus)
        {
            case PeraLinkXoomWS.TransactionStatus.ForPayout:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 1;
                    returnValue.Description = "Outstanding";

                    return returnValue;
                }
            case PeraLinkXoomWS.TransactionStatus.PaidOut:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 2;
                    returnValue.Description = "Paid";

                    return returnValue;
                }
            case PeraLinkXoomWS.TransactionStatus.Cancelled:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 4;
                    returnValue.Description = "Cancelled";

                    return returnValue;
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(transactionStatus.ToString());
                }
        }
    }
    #endregion

    void IDisposable.Dispose()
    {
        bool isClosed = false;

        try
        {
            if (this._serviceClient.State != System.ServiceModel.CommunicationState.Faulted)
            {
                this._serviceClient.Close();
                isClosed = true;
            }
            else
            {
                // Proceed with Abort in finally
            }
        }
        finally
        {
            if (!isClosed)
            {
                this._serviceClient.Abort();
            }
            else
            {
                // Do nothing since state is already closed
            }
        }
    }
}