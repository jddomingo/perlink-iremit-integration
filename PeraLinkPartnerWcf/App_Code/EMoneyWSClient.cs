#define CONVERTION_TO_BUSINESS_ACCOUNT
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EMoneyWSClient
/// </summary>
public class EMoneyWSClient : IDisposable
{
	#region Constructor
	public EMoneyWSClient()
	{
		_serviceSoapClient = new EMoneyWS.PeraCardServiceSoapClient();
	}
	#endregion

	#region Fields/Properties
	EMoneyWS.PeraCardServiceSoapClient _serviceSoapClient;
	#endregion

	#region Public Methods
    public EMoneyWS.CustomerTransactionHistory[] GetCustomerTransactionHistory2(string accountNumber, DateTime startDate, DateTime endDate)
    {
        return _serviceSoapClient.GetCustomerTransactionHistory2(accountNumber, startDate, endDate);
    }

    public EMoneyWS.CustomerTransactionHistorySummary GetCustomerTransactionHistorySummary2(string accountNumber, DateTime startDate, DateTime endDate)
    {
        return _serviceSoapClient.GetCustomerTransactionHistorySummary2(accountNumber, startDate, endDate);
    }

	public EMoneyWS.UpdateEMoneyResult UpdateEMoney(List<EMoneyWS.UpdateEMoneyRequest> requests)
	{
		EMoneyWS.UpdateEMoneyRequest[] requestArray = requests.ToArray();
		EMoneyWS.UpdateEMoneyResult result = _serviceSoapClient.ProcessEMoneyRequests(requestArray);
		return result;
	}

	public EMoneyWS.AccountInfo RetrieveAccountDetails(string accountNumber)
	{
		EMoneyWS.AccountInfo accountDetails = _serviceSoapClient.RetrieveAccountDetails(accountNumber);
		return accountDetails;
	}
#if CONVERTION_TO_BUSINESS_ACCOUNT
    public EMoneyWS.EMoneyInquiryDetails EMoneyInquiry(string accountNumber)
    {
        EMoneyWS.EMoneyInquiryDetails eMoneyInquiryDetails = _serviceSoapClient.EMoneyInquiry(accountNumber);
        return eMoneyInquiryDetails;
    }
#else
#endif
	#endregion

	void IDisposable.Dispose()
	{
		bool isClosed = false;

		try
		{
			if (this._serviceSoapClient.State != System.ServiceModel.CommunicationState.Faulted)
			{
				this._serviceSoapClient.Close();
				isClosed = true;
			}
			else
			{
				// Proceed with Abort in finally
			}
		}
		finally
		{
			if (!isClosed)
			{
				this._serviceSoapClient.Abort();
			}
			else
			{
				// Do nothing since state is already closed
			}
		}
	}
}