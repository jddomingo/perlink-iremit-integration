using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ELoadTransactionCollection
/// </summary>
public class ELoadTransactionCollection : List<ELoadTransaction>
{
    #region Constructor
    public ELoadTransactionCollection()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion

    #region Public Methods
    public void AddList(PeraLinkCoreWcf.ELoadTransaction[] eLoadTransactionCollection)
    {
        Array.ForEach<PeraLinkCoreWcf.ELoadTransaction>(eLoadTransactionCollection
            , delegate(PeraLinkCoreWcf.ELoadTransaction eachELoadTransaction)
            {
                ELoadTransaction mappedELoadTransaction = new ELoadTransaction();
                mappedELoadTransaction.Load(eachELoadTransaction);
                this.Add(mappedELoadTransaction);
            });
    }
    #endregion
}