#define INSURANCE_MICARE
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service" in code, svc and config file together.
public class Service : IService
{
	private T NullParameterError<T>() where T : IServiceResult, new()
	{
		T returnValue = new T();

		returnValue.Message = "Method parameter must not be null.";
		returnValue.ResultStatus = ResultStatus.Failed;

		return returnValue;
	}

	private T ExecuteMethod<T>(Func<T> action) where T : IServiceResult, new()
	
    {
		ServiceResult serviceResult;
		T returnValue = new T();

		try
		{
			return action();
		}
		catch (RDFramework.ClientException clientEx)
		{
			serviceResult = new ServiceResult(clientEx);
		}
		catch (Exception ex)
		{
			serviceResult = new ServiceResult(ex);
		}

		returnValue.Message = serviceResult.Message;
		returnValue.MessageID = serviceResult.MessageID;
		returnValue.LogID = serviceResult.LogID;
		returnValue.ResultStatus = serviceResult.ResultStatus;

		return returnValue;
	}

    public GetEMoneyCollectionResult GetEMoneyCollection(GetEMoneyCollectionRequest getEMoneyCollectionRequest)
    {
        return getEMoneyCollectionRequest != null ? ExecuteMethod(getEMoneyCollectionRequest.Process) : NullParameterError<GetEMoneyCollectionResult>();
    }

	public GetEMoneyAccountDetailsResult GetEMoneyAccountDetail(GetEMoneyAccountDetailsRequest getEMoneyAccountDetailsRequest)
	{
		return getEMoneyAccountDetailsRequest != null ? ExecuteMethod(getEMoneyAccountDetailsRequest.Process) : NullParameterError<GetEMoneyAccountDetailsResult>();
	}

    public ValidateBeneficiaryDetailsResult ValidateBeneficiaryDetails(ValidateBeneficiaryDetailsRequest validateBeneficiaryRequest)
    {
        ValidateBeneficiaryDetailsResult returnValue = null;

        try
        {
            return validateBeneficiaryRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new ValidateBeneficiaryDetailsResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new ValidateBeneficiaryDetailsResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

	public GetClientSourceOfFundCollectionResult GetClientSourceOfFundCollection(GetClientSourceOfFundCollectionRequest getClientSourceOfFundRequest)
	{
		return getClientSourceOfFundRequest != null ? ExecuteMethod(getClientSourceOfFundRequest.Process) : NullParameterError<GetClientSourceOfFundCollectionResult>();
	}

	public RemitToAccountResult RemitToAccount(RemitToAccountRequest remitToAccountRequest)
	{
		return remitToAccountRequest != null ? ExecuteMethod(remitToAccountRequest.Process) : NullParameterError<RemitToAccountResult>();
	}

	public GetBankCollectionResult GetBankCollection(GetBankCollectionRequest getBankCollectionRequest)
	{
		return getBankCollectionRequest != null ? ExecuteMethod(getBankCollectionRequest.Process) : NullParameterError<GetBankCollectionResult>();
	}

	public GetRemitToAccountServiceFeeResult GetRemitToAccountServiceFee(GetRemitToAccountServiceFeeRequest getRemitToAccountServiceFeeRequest)
	{
		return getRemitToAccountServiceFeeRequest != null ? ExecuteMethod(getRemitToAccountServiceFeeRequest.Process) : NullParameterError<GetRemitToAccountServiceFeeResult>();
	}

	public GetRemitToAccountTransactionHistoryResult GetRemitToAccountTransactionHistory(GetRemitToAccountTransactionHistoryRequest getRemitToAccountTransactionHistoryRequest)
	{
		return getRemitToAccountTransactionHistoryRequest != null ? ExecuteMethod(getRemitToAccountTransactionHistoryRequest.Process) : NullParameterError<GetRemitToAccountTransactionHistoryResult>();
	}

	public VerifyRemitToAccountTransactionResult VerifyRemitToAccountTransaction(VerifyRemitToAccountTransactionRequest verifyRemitToAccountTransactionRequest)
	{
		return verifyRemitToAccountTransactionRequest != null ? ExecuteMethod(verifyRemitToAccountTransactionRequest.Process) : NullParameterError<VerifyRemitToAccountTransactionResult>();
	}

	public GetTerminalTransactionSummaryResult GetTerminalTransactionSummary(GetTerminalTransactionSummaryRequest getTerminalTransactionSummaryRequest)
	{
		return getTerminalTransactionSummaryRequest != null ? ExecuteMethod(getTerminalTransactionSummaryRequest.Process) : NullParameterError<GetTerminalTransactionSummaryResult>();
	}

	public GetTerminalReadingReportResult GetTerminalReadingReport(GetTerminalReadingReportRequest getTerminalReadingRequest)
	{
		return getTerminalReadingRequest != null ? ExecuteMethod(getTerminalReadingRequest.Process) : NullParameterError<GetTerminalReadingReportResult>();
	}

	public GetSummaryReportResult GetSummaryReport(GetSummaryReportRequest getSummartReportRequest)
	{
		return getSummartReportRequest != null ? ExecuteMethod(getSummartReportRequest.Process) : NullParameterError<GetSummaryReportResult>();
	}

    public GetLastDomesticRemittanceControlNumberResult GetLastDomesticRemittanceControlNumber(GetLastDomesticRemittanceControlNumberRequest getLastDomesticRemittanceControlNumberRequest)
    {
        return getLastDomesticRemittanceControlNumberRequest != null ? ExecuteMethod(getLastDomesticRemittanceControlNumberRequest.Process) : NullParameterError<GetLastDomesticRemittanceControlNumberResult>();
    }

    public GetLastProcessedTransactionResult GetLastProcessedTransaction(GetLastProcessedTransactionRequest getLastProcessedTransactionRequest)
    {
        return getLastProcessedTransactionRequest != null ? ExecuteMethod(getLastProcessedTransactionRequest.Process) : NullParameterError<GetLastProcessedTransactionResult>();
    }

    public GetSendingTransactionReportResult GetSendingTransactionReport(GetSendingTransactionReportRequest getSendingTransactionReportRequest)
    {
        return getSendingTransactionReportRequest != null ? ExecuteMethod(getSendingTransactionReportRequest.Process) : NullParameterError<GetSendingTransactionReportResult>();
    }

    public GetPayoutTransactionReportResult GetPayoutTransactionReport(GetPayoutTransactionReportRequest getPayoutTransactionReportRequest)
    {
        return getPayoutTransactionReportRequest != null ? ExecuteMethod(getPayoutTransactionReportRequest.Process) : NullParameterError<GetPayoutTransactionReportResult>();
    }

    public GetCancelTransactionReportResult GetCancelTransactionReport(GetCancelTransactionReportRequest getCancelTransactionReportRequest)
    {
        return getCancelTransactionReportRequest != null ? ExecuteMethod(getCancelTransactionReportRequest.Process) : NullParameterError<GetCancelTransactionReportResult>();
    }
    public GetRefundTransactionReportResult GetRefundTransactionReport(GetRefundTransactionReportRequest getRefundTransactionReportRequest)
    {
        return getRefundTransactionReportRequest != null ? ExecuteMethod(getRefundTransactionReportRequest.Process) : NullParameterError<GetRefundTransactionReportResult>();
    }

    public GetAmendTransactionReportResult GetAmendTransactionReport(GetAmendTransactionReportRequest getAmendTransactionReportRequest)
    {
        return getAmendTransactionReportRequest != null ? ExecuteMethod(getAmendTransactionReportRequest.Process) : NullParameterError<GetAmendTransactionReportResult>();
    }

    public GetELoadTransactionHistoryResult GetELoadTransactionHistory(GetELoadTransactionHistoryRequest getELoadTransactionHistoryRequest)
    {
        return getELoadTransactionHistoryRequest != null ? ExecuteMethod(getELoadTransactionHistoryRequest.Process) : NullParameterError<GetELoadTransactionHistoryResult>();
    }

    public PayBillResult PayBill(PayBillRequest payBillRequest)
    {
        return payBillRequest != null ? ExecuteMethod(payBillRequest.Process) : NullParameterError<PayBillResult>();
    }

    public UpdateBeneficiaryResult UpdateBeneficiary(UpdateBeneficiaryRequest updateBeneficiaryRequestRequest)
    {
        UpdateBeneficiaryResult returnValue = null;

        try
        {
            return updateBeneficiaryRequestRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new UpdateBeneficiaryResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new UpdateBeneficiaryResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public GetAmendmentFeeResult GetAmendmentFee(GetAmendmentFeeRequest getAmendmentFeeRequest)
    {
        GetAmendmentFeeResult returnValue = null;

        try
        {
            return getAmendmentFeeRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new GetAmendmentFeeResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new GetAmendmentFeeResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public GetBeneficiaryResult GetBeneficiary(GetBeneficiaryRequest getBeneficiaryRequest)
    {
        GetBeneficiaryResult returnValue = null;

        try
        {
            return getBeneficiaryRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new GetBeneficiaryResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new GetBeneficiaryResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public GetClientResult GetClient(GetClientRequest getClientRequest)
    {
        GetClientResult returnValue = null;

        try
        {
            return getClientRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new GetClientResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new GetClientResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public UpdateClientResult UpdateClient(UpdateClientRequest updateClientRequest)
    {
        UpdateClientResult returnValue = null;

        try
        {
            return updateClientRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new UpdateClientResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new UpdateClientResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public GetCountryCollectionResult GetCountryCollection(GetCountryCollectionRequest getCountryCollectionRequest)
    {
        GetCountryCollectionResult returnValue = null;

        try
        {
            return getCountryCollectionRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new GetCountryCollectionResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new GetCountryCollectionResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public GetStateCollectionByCountryResult GetStateCollectionByCountry(GetStateCollectionByCountryRequest getStateCollectionByCountryrRequest)
    {
        GetStateCollectionByCountryResult returnValue = null;

        try
        {
            return getStateCollectionByCountryrRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new GetStateCollectionByCountryResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new GetStateCollectionByCountryResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }


    public GetAgentCollectionResult GetAgentCollection(GetAgentCollectionRequest getAgentCollectionRequest)
    {
        GetAgentCollectionResult returnValue = null;

        try
        {
            return getAgentCollectionRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new GetAgentCollectionResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new GetAgentCollectionResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public GetInternationalPartnerCollectionResult GetInternationalPartnerCollection(GetInternationalPartnerCollectionRequest getInternationalPartnerCollectionRequest)
	{
        GetInternationalPartnerCollectionResult returnValue = null;

		try
		{
			return getInternationalPartnerCollectionRequest.Process();
		}
		catch (RDFramework.ClientException clientEx)
		{
            returnValue = new GetInternationalPartnerCollectionResult();

			returnValue.LogID = clientEx.LogID;
			returnValue.Message = clientEx.Message;
			returnValue.MessageID = clientEx.MessageID;
			returnValue.ResultStatus = ResultStatus.Failed;

			return returnValue;
		}
		catch (Exception ex)
		{
            returnValue = new GetInternationalPartnerCollectionResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
			returnValue.Message = SystemSetting.ServerErrorMessage;
			returnValue.ResultStatus = ResultStatus.Error;

			return returnValue;
		}
	}

	public GetAgentResult GetAgent(GetAgentRequest getAgentRequest)
	{
		GetAgentResult returnValue = null;

		try
		{
			return getAgentRequest.Process();
		}
		catch (RDFramework.ClientException clientEx)
		{
			returnValue = new GetAgentResult();

			returnValue.LogID = clientEx.LogID;
			returnValue.Message = clientEx.Message;
			returnValue.MessageID = clientEx.MessageID;
			returnValue.ResultStatus = ResultStatus.Failed;

			return returnValue;
		}
		catch (Exception ex)
		{
			returnValue = new GetAgentResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
			returnValue.Message = SystemSetting.ServerErrorMessage;
			returnValue.ResultStatus = ResultStatus.Error;

			return returnValue;
		}
	}

    public GetDomesticPullPartnerResult GetDomesticPullPartner(GetDomesticPullPartnerRequest getDomesticPullPartnerRequest)
    {
        GetDomesticPullPartnerResult returnValue = null;

        try
        {
            return getDomesticPullPartnerRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new GetDomesticPullPartnerResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new GetDomesticPullPartnerResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public GetDomesticRemittanceHistoryResult GetDomesticRemittanceHistory(GetDomesticRemittanceHistoryRequest getDomesticRemittanceHistoryRequest)
    {
        GetDomesticRemittanceHistoryResult returnValue = null;

        try
        {
            return getDomesticRemittanceHistoryRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new GetDomesticRemittanceHistoryResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new GetDomesticRemittanceHistoryResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public GetInternationalRemittanceHistoryResult GetInternationalRemittanceHistory(GetInternationalRemittanceHistoryRequest getInternationalRemittanceHistoryRequest)
    {
        GetInternationalRemittanceHistoryResult returnValue = null;

        try
        {
            return getInternationalRemittanceHistoryRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new GetInternationalRemittanceHistoryResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new GetInternationalRemittanceHistoryResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public GetSendServiceFeeResult GetSendServiceFee(GetSendServiceFeeRequest getSendServiceFeeRequest)
    {
        GetSendServiceFeeResult returnValue = null;

        try
        {
            return getSendServiceFeeRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new GetSendServiceFeeResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new GetSendServiceFeeResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public GetSendCurrencyCollectionResult GetSendCurrencyCollection(GetSendCurrencyCollectionRequest getSendCurrencyCollectionRequest)
    {
        GetSendCurrencyCollectionResult returnValue = null;

        try
        {
            return getSendCurrencyCollectionRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new GetSendCurrencyCollectionResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new GetSendCurrencyCollectionResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public FindClientResult FindClient(FindClientRequest findClientRequest)
    {
        FindClientResult returnValue = null;

        try
        {
            return findClientRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new FindClientResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new FindClientResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public RefundDomesticRemittanceResult RefundDomesticRemittance(RefundDomesticRemittanceRequest refundDomesticRemittanceRequest)
    {
        RefundDomesticRemittanceResult returnValue = null;

        try
        {
            return refundDomesticRemittanceRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new RefundDomesticRemittanceResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new RefundDomesticRemittanceResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public CancelDomesticRemittanceResult CancelDomesticRemittance(CancelDomesticRemittanceRequest cancelDomesticRemittanceRequest)
    {
        CancelDomesticRemittanceResult returnValue = null;

        try
        {
            return cancelDomesticRemittanceRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new CancelDomesticRemittanceResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new CancelDomesticRemittanceResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public AmendDomesticRemittanceResult AmendDomesticRemittance(AmendDomesticRemittanceRequest amendDomesticRemittanceRequest)
    {
        AmendDomesticRemittanceResult returnValue = null;

        try
        {
            return amendDomesticRemittanceRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new AmendDomesticRemittanceResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new AmendDomesticRemittanceResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public GetBeneficiaryBySenderResult GetBeneficiaryBySender(GetBeneficiaryBySenderRequest getBeneficiaryBySenderRequest)
    {
        GetBeneficiaryBySenderResult returnValue = null;

        try
        {
            return getBeneficiaryBySenderRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new GetBeneficiaryBySenderResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new GetBeneficiaryBySenderResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public FindBeneficiaryResult FindBeneficiary(FindBeneficiaryRequest findBeneficiaryRequest)
    {
        FindBeneficiaryResult returnValue = null;

        try
        {
            return findBeneficiaryRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new FindBeneficiaryResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new FindBeneficiaryResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public GetIdentificationTypeCollectionResult GetIdentificationTypeCollection(GetIdentificationTypeCollectionRequest getIdentificationTypeCollectionRequest)
    {
        GetIdentificationTypeCollectionResult returnValue = null;

        try
        {
            return getIdentificationTypeCollectionRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new GetIdentificationTypeCollectionResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new GetIdentificationTypeCollectionResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public FindDomesticRemittanceResult FindDomesticRemittance(FindDomesticRemittanceRequest findDomesticRemittanceRequest)
    {
        FindDomesticRemittanceResult returnValue = null;

        try
        {
            return findDomesticRemittanceRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        
        {
            returnValue = new FindDomesticRemittanceResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new FindDomesticRemittanceResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public FindInternationalRemittanceResult FindInternationalRemittance(FindInternationalRemittanceRequest findInternationalRemittanceRequest)
    {
        FindInternationalRemittanceResult returnValue = null;

        try
        {
            return findInternationalRemittanceRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new FindInternationalRemittanceResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new FindInternationalRemittanceResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public PayoutDomesticRemittanceResult PayoutDomesticRemittance(PayoutDomesticRemittanceRequest payoutDomesticRemittanceRequest)
    {
        PayoutDomesticRemittanceResult returnValue = null;

        try
        {
            return payoutDomesticRemittanceRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new PayoutDomesticRemittanceResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new PayoutDomesticRemittanceResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public PayoutInternationalRemittanceResult PayoutInternationalRemittance(PayoutInternationalRemittanceRequest payoutInternationalRemittanceRequest)
    {
        PayoutInternationalRemittanceResult returnValue = null;

        try
        {
            return payoutInternationalRemittanceRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new PayoutInternationalRemittanceResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new PayoutInternationalRemittanceResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public AddClientResult AddClient(AddClientRequest addClientRequest)
    {
        AddClientResult returnValue = null;

        try
        {
            return addClientRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new AddClientResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new AddClientResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }

    }
    public SendDomesticRemittanceResult SendDomesticRemittance(SendDomesticRemittanceRequest sendDomesticRemittanceRequest)
    {
        SendDomesticRemittanceResult returnValue = null;

        try
        {
            return sendDomesticRemittanceRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new SendDomesticRemittanceResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new SendDomesticRemittanceResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public AddBeneficiaryResult AddBeneficiary(AddBeneficiaryRequest addBeneficiaryRequest)
    {
        AddBeneficiaryResult returnValue = null;

        try
        {
            return addBeneficiaryRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new AddBeneficiaryResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new AddBeneficiaryResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public GetTelcoCollectionResult GetTelcoCollection(GetTelcoCollectionRequest getTelcoCollectionRequest)
    {
        return getTelcoCollectionRequest != null ? ExecuteMethod(getTelcoCollectionRequest.Process) : NullParameterError<GetTelcoCollectionResult>();
    }

    public GetTelcoProductCollectionResult GetTelcoProductCollection(GetTelcoProductCollectionRequest getTelcoProductCollectionRequest)
    {
        GetTelcoProductCollectionResult returnValue = null;

        try
        {
            return getTelcoProductCollectionRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new GetTelcoProductCollectionResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new GetTelcoProductCollectionResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }



    public GetMobilePrefixCollectionResult GetMobilePrefixCollection(GetMobilePrefixCollectionRequest getMobilePrefixCollectionRequest)
    {
        return getMobilePrefixCollectionRequest != null ? ExecuteMethod(getMobilePrefixCollectionRequest.Process) : NullParameterError<GetMobilePrefixCollectionResult>();
    }

    public ProcessLoadTransactionResult ProcessLoadTransaction(ProcessLoadTransactionRequest processLoadTransactionRequest)
    {
        ProcessLoadTransactionResult returnValue = null;

        try
        {
            return processLoadTransactionRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new ProcessLoadTransactionResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new ProcessLoadTransactionResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public CheckELoadTransactionResult CheckELoadTransaction(CheckELoadTransactionRequest checkELoadTransactionRequest)
    {
        CheckELoadTransactionResult returnValue = null;

        try
        {
            return checkELoadTransactionRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new CheckELoadTransactionResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new CheckELoadTransactionResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }


    public FindClientByCellphoneNumberResult FindClientByCellphoneNumber(FindClientByCellphoneNumberRequest findClientByCellphoneNumberRequest)
    {
        FindClientByCellphoneNumberResult returnValue = null;

        try
        {
            return findClientByCellphoneNumberRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new FindClientByCellphoneNumberResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new FindClientByCellphoneNumberResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public UpdateBeneficiaryMobileResult UpdateBeneficiaryMobile(UpdateBeneficiaryMobileRequest updateBeneficiaryMobileRequest)
    {
        UpdateBeneficiaryMobileResult returnValue = null;

        try
        {
            return updateBeneficiaryMobileRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new UpdateBeneficiaryMobileResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new UpdateBeneficiaryMobileResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public PayoutDomesticRemittanceMobileResult PayoutDomesticRemittanceMobile(PayoutDomesticRemittanceMobileRequest payoutDomesticRemittanceMobileRequest)
    {
        PayoutDomesticRemittanceMobileResult returnValue = null;

        try
        {
            return payoutDomesticRemittanceMobileRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new PayoutDomesticRemittanceMobileResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new PayoutDomesticRemittanceMobileResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public FindClientByNameResult FindClientByName(FindClientByNameRequest findClientByNameRequest)
    {
        FindClientByNameResult returnValue = null;

        try
        {
            return findClientByNameRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new FindClientByNameResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new FindClientByNameResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public FindClientByClientNumberResult FindClientByClientNumber(FindClientByClientNumberRequest findClientByNameRequest)
    {
        FindClientByClientNumberResult returnValue = null;

        try
        {
            return findClientByNameRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new FindClientByClientNumberResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new FindClientByClientNumberResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public GetCommissionFeeResult GetCommissionFee(GetCommissionFeeRequest getCommissionFeeRequest)
    {
        GetCommissionFeeResult returnValue = null;

        try
        {
            return getCommissionFeeRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new GetCommissionFeeResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new GetCommissionFeeResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public GetBillsPaymentServiceFeeResult GetBillsPaymentServiceFee(GetBillsPaymentServiceFeeRequest getBillsPaymentFeeRequest)
    {
        GetBillsPaymentServiceFeeResult returnValue = null;

        try
        {
            return getBillsPaymentFeeRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new GetBillsPaymentServiceFeeResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new GetBillsPaymentServiceFeeResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public GetBillsPaymentTransactionHistoryResult GetBillsPaymentTransactionHistory(GetBillsPaymentTransactionHistoryRequest getBillsPaymentTransactionHistoryRequest)
    {
        GetBillsPaymentTransactionHistoryResult returnValue = null;

        try
        {
            return getBillsPaymentTransactionHistoryRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new GetBillsPaymentTransactionHistoryResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new GetBillsPaymentTransactionHistoryResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

#if INSURANCE_MICARE
    public ProcessInsuranceResult ProcessInsurance(ProcessInsuranceRequest processInsuranceRequest)
    {
        ProcessInsuranceResult returnValue = null;

        try
        {
            return processInsuranceRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new ProcessInsuranceResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new ProcessInsuranceResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }

    public GetInsuranceTransactionHistoryResult GetInsuranceTransactionHistory(GetInsuranceTransactionHistoryRequest getInsuranceTransactionHistoryRequest)
    {
        GetInsuranceTransactionHistoryResult returnValue = null;

        try
        {
            return getInsuranceTransactionHistoryRequest.Process();
        }
        catch (RDFramework.ClientException clientEx)
        {
            returnValue = new GetInsuranceTransactionHistoryResult();

            returnValue.LogID = clientEx.LogID;
            returnValue.Message = clientEx.Message;
            returnValue.MessageID = clientEx.MessageID;
            returnValue.ResultStatus = ResultStatus.Failed;

            return returnValue;
        }
        catch (Exception ex)
        {
            returnValue = new GetInsuranceTransactionHistoryResult();

            returnValue.LogID = RDFramework.Utility.EventLog.SaveError(ex.ToString());
            returnValue.Message = SystemSetting.ServerErrorMessage;
            returnValue.ResultStatus = ResultStatus.Error;

            return returnValue;
        }
    }
#else
#endif
}