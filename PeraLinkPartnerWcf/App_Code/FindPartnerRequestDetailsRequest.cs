using PeraLinkCoreWcf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for FindPartnerRequestDetailRequest
/// </summary>
[DataContract]
public class FindPartnerRequestDetailsRequest : BaseRequest
{
    #region Constructors
    public FindPartnerRequestDetailsRequest()
	{
    }
    #endregion
    
    #region Fields
    private string _partnerCode;
    private Int64 _agentID;
    private string _controlNumber;
    #endregion

    #region Properties
    [DataMember]
    public string PartnerCode
    {
        get { return _partnerCode; }
        set { _partnerCode = value; }
    }

    [DataMember]
    public Int64 AgentID
    {
        get { return _agentID; }
        set { _agentID = value; }
    }

    [DataMember]
    public string ControlNumber
    {
        get { return _controlNumber; }
        set { _controlNumber = value; }
    }
    #endregion

    #region Internal Methods
    internal FindPartnerRequestDetailsResult Process()
    {
        FindPartnerRequestDetailsResult returnValue = new FindPartnerRequestDetailsResult();

        // Important: This function validates the request credential
        this.AuthenticateRequest();

        PeraLinkCoreWcf.PartnerRequestDetails partnerRequestDetails = new PeraLinkCoreWcf.PartnerRequestDetails();
        partnerRequestDetails.PartnerCode = this.PartnerCode;
        partnerRequestDetails.AgentID = this.AgentID;
        partnerRequestDetails.ControlNumber = this.ControlNumber;

        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
            partnerRequestDetails = serviceClient.FindPartnerRequestDetails(partnerRequestDetails);
        }

        returnValue.ResultStatus = ResultStatus.Successful;

        return returnValue;
    }
    #endregion
}