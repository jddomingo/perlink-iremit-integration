using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AuditActionID
/// </summary>
public class AuditActionID
{
    #region Constructor
    public AuditActionID()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    public const int SendDomesticRemittance = 1;
    public const int PayoutDomesticRemittance = 2;
	public const int FindDomesticRemittanceTransaction = 25;
	public const int FindInternationalRemittanceTransaction = 26;
    #endregion
}