﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for GetBillsPaymentServiceFeeResult
/// </summary>
public class GetBillsPaymentServiceFeeResult:BaseResult
{
    #region Constructor
    public GetBillsPaymentServiceFeeResult()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion

    #region Fields/Properties
    private decimal _serviceFee;
    [DataMember]
    public decimal ServiceFee
    {
        get { return _serviceFee; }
        set { _serviceFee = value; }
    }

    #endregion
}
