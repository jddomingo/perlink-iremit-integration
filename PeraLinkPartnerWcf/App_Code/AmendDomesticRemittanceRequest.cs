using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using AppCryptor;

[DataContract]
public class AmendDomesticRemittanceRequest : BaseRequest
{
	#region Constructor
	public AmendDomesticRemittanceRequest()
	{
		//

		//
	}
	#endregion

	#region Fields/Properties

	private string _controlNumber;
	[DataMember]
	public string ControlNumber
	{
		get { return _controlNumber; }
		set { _controlNumber = value; }
	}

	private Int64 _newBeneficiaryID;
	[DataMember]
	public Int64 NewBeneficiaryID
	{
		get { return _newBeneficiaryID; }
		set { _newBeneficiaryID = value; }
	}

	private string _partnerCode;
	[DataMember]
	public string PartnerCode
	{
		get { return _partnerCode; }
		set { _partnerCode = value; }
	}

	private string _token;
	[DataMember]
	public string Token
	{
		get { return _token; }
		set { _token = value; }
	}

	private string _agentCode;
	[DataMember]
	public string AgentCode
	{
		get { return _agentCode; }
		set { _agentCode = value; }
	}

	private string _userID;
	[DataMember]
	public string UserID
	{
		get { return _userID; }
		set { _userID = value; }
	}

	private string _pin;
	[DataMember]
	public string Pin
	{
		get { return _pin; }
		set { _pin = value; }
	}

	private decimal _amendmentFee;
	[DataMember]
	public decimal AmendmentFee
	{
		get { return _amendmentFee; }
		set { _amendmentFee = value; }
	}

	private string _terminalCode;
	[DataMember]
	public string TerminalCode
	{
		get { return _terminalCode; }
		set { _terminalCode = value; }
	}
	#endregion

	#region Internal Methods
	internal AmendDomesticRemittanceResult Process()
	{
		AmendDomesticRemittanceResult returnValue = new AmendDomesticRemittanceResult();
		bool isEMoney = false;
        decimal commissionFee = 0;
		// Important: This function validates the request credential
		this.AuthenticateRequest();

		using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
		{
			#region Validate Partner Record
			PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
			partner.PartnerCode = this.PartnerCode;
			partner = serviceClient.FindPartnerCode(partner);

			if (partner.PartnerID == 0)
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			else { }

			if (partner.Token == this.Token) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}

			if (partner.Activated) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}

			if (partner.SourceOfFundID == SystemSetting.EMoneySourceOfFundID)
			{
				isEMoney = true;
			}
			#endregion

			#region Validate Agent Record
			PeraLinkCoreWcf.Agent agent = new PeraLinkCoreWcf.Agent();
			agent.AgentCode = this.AgentCode;
			agent.Partner = partner;
			agent = serviceClient.FindAgentCodeOfPartner(agent);

			if (agent.AgentID == 0)
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			else
			{
				// Proceed
			}

			if (agent.Partner.PartnerID == partner.PartnerID)
			{ }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(43);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}

			if (agent.Activated) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(46);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			#endregion

			#region Validate Terminal Record
			PeraLinkCoreWcf.Terminal terminal = new PeraLinkCoreWcf.Terminal();
			int terminalCount = serviceClient.GetAgentTerminalCollection(agent).Length;

			if (terminalCount > 0)
			{
				terminal.TerminalCode = this.TerminalCode;
				terminal.Agent = agent;
				terminal.Agent.Partner = partner;
				terminal = serviceClient.FindTerminal(terminal);

				if (terminal.TerminalID == 0)
				{
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(104);
					throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
				}
				else
				{
					// Proceed
				}

				if (terminal.Agent.AgentID == agent.AgentID)
				{ }
				else
				{
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(105);
					throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
				}

				if (terminal.Activated) { }
				else
				{
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(106);
					throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
				}
			}
			else
			{
				//do nothing
			}
			#endregion

			#region Validate Partner's Access To Amend Domestic Remittance Service
			PeraLinkCoreWcf.PartnerServiceMapping[] partnerServiceMappingCollection = serviceClient.GetPartnerServiceMappingCollection(partner);

			PeraLinkCoreWcf.PartnerServiceMapping partnerServiceMapping = Array.Find<PeraLinkCoreWcf.PartnerServiceMapping>(partnerServiceMappingCollection
				, delegate(PeraLinkCoreWcf.PartnerServiceMapping eachPartnerServiceMapping)
				{
					return eachPartnerServiceMapping.ServiceType.ServiceTypeID == ServiceTypeID.AmendRemittance;
				});


			if (partnerServiceMapping == null)
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(10);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			else
			{
				// Proceed
			}
			#endregion

			#region Find Domestic Remittance
			FindDomesticRemittanceRequest findDomesticRemittanceRequest = new FindDomesticRemittanceRequest();
			findDomesticRemittanceRequest.AgentCode = this.AgentCode;
			findDomesticRemittanceRequest.ControlNumber = this.ControlNumber;
			findDomesticRemittanceRequest.PartnerCode = this.PartnerCode;
			findDomesticRemittanceRequest.TerminalCode = this.TerminalCode;
			findDomesticRemittanceRequest.ServiceCredential = this.ServiceCredential;
			findDomesticRemittanceRequest.Token = this.Token;
			findDomesticRemittanceRequest.UserID = this.UserID;
			FindDomesticRemittanceResult findDomesticRemittanceResult = findDomesticRemittanceRequest.Process();

			PeraLinkCoreWcf.DomesticRemittance domesticRemittance = new PeraLinkCoreWcf.DomesticRemittance();
			domesticRemittance.ControlNumber = findDomesticRemittanceResult.ControlNumber;
			domesticRemittance.DomesticRemittanceID = findDomesticRemittanceResult.DomesticRemittanceID;
			domesticRemittance.SentByAgent = new PeraLinkCoreWcf.Agent();
			domesticRemittance.SentByAgent.AgentID = findDomesticRemittanceResult.SentByAgentID;
			domesticRemittance.SentByAgent.AgentCode = findDomesticRemittanceResult.SentByAgentCode;
			domesticRemittance.SentByAgent.Partner = new PeraLinkCoreWcf.Partner();
			domesticRemittance.SentByAgent.Partner.PartnerID = findDomesticRemittanceResult.SentByPartnerID;
			domesticRemittance.SentByAgent.Partner.PartnerCode = findDomesticRemittanceResult.SentByPartnerCode;
			domesticRemittance.SendCurrency = new PeraLinkCoreWcf.Currency();
			domesticRemittance.SendCurrency.CurrencyID = findDomesticRemittanceResult.SendCurrencyID;
			domesticRemittance.SendCurrency.Code = findDomesticRemittanceResult.SendCurrencyCode;
			domesticRemittance.Beneficiary = new PeraLinkCoreWcf.Beneficiary();
			domesticRemittance.Beneficiary.BeneficiaryID = findDomesticRemittanceResult.BeneficiaryID;
			domesticRemittance.PrincipalAmount = findDomesticRemittanceResult.PrincipalAmount;
			domesticRemittance.ServiceFee = findDomesticRemittanceResult.ServiceFee;
			domesticRemittance.DateTimeSent = findDomesticRemittanceResult.DateTimeSent;
			domesticRemittance.SenderClient = new PeraLinkCoreWcf.Client();
			domesticRemittance.SenderClient.ClientID = findDomesticRemittanceResult.SenderClientID;
			domesticRemittance.SentByUserID = findDomesticRemittanceResult.SentByUserID;

			if (domesticRemittance.SentByAgent.AgentID == agent.AgentID)
			{
				// Proceed
			}
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(13);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}

			if (terminalCount > 0)
			{
				if (domesticRemittance.SentByTerminal != null)
				{
					if (domesticRemittance.SentByTerminal.TerminalID == terminal.TerminalID)
					{
						//Proceed
					}
					else
					{
						PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(107);
						throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
					}
				}
				else
				{
					//Allow
				}
			}
			else
			{
				//Allow
			}
			#endregion

			#region Calculate Amendment Fee
			GetAmendmentFeeRequest feeRequest = new GetAmendmentFeeRequest();
			feeRequest.AgentCode = this.AgentCode;
			feeRequest.PartnerCode = this.PartnerCode;
			feeRequest.Token = this.Token;
			feeRequest.CurrencyID = domesticRemittance.SendCurrency.CurrencyID;
			feeRequest.ServiceCredential = this.ServiceCredential;
			feeRequest.PrincipalAmount = domesticRemittance.PrincipalAmount;

			GetAmendmentFeeResult feeResult = new GetAmendmentFeeResult();

			try
			{
				feeResult = feeRequest.Process();
				domesticRemittance.ServiceFee = feeResult.ServiceFee;
			}
			catch (RDFramework.ClientException clientEx)
			{
				if (clientEx.MessageID == SystemResource.GetMessageWithListID(28).MessageID)
				{
					domesticRemittance.ServiceFee = this.AmendmentFee;
				}
				else
				{
					throw;
				}
			}
			#endregion

			domesticRemittance.Beneficiary = new PeraLinkCoreWcf.Beneficiary();
			domesticRemittance.Beneficiary.BeneficiaryID = this.NewBeneficiaryID;
			domesticRemittance.UpdatedByAgent = agent;
			domesticRemittance.UpdatedByUserID = this.UserID;
			domesticRemittance.DateTimeUpdated = DateTime.Now;
			domesticRemittance.UpdatedByTerminal = terminal;

			serviceClient.AmendDomesticRemittance(domesticRemittance);

            

            #region E-Money
            if (isEMoney)
            {		//domesticRemittance.ServiceFee 
                if (domesticRemittance.ServiceFee > 0)
                {
                    #region Get E-Money Profile
                    PeraLinkCoreWcf.EMoneyProfile eMoneyProfile = new PeraLinkCoreWcf.EMoneyProfile();

                    PeraLinkCoreWcf.GetEMoneyProfileRequest profileRequest = new PeraLinkCoreWcf.GetEMoneyProfileRequest();
                    profileRequest.Partner = partner;

                    eMoneyProfile = serviceClient.GetEMoneyProfile(profileRequest).EMoneyProfile;

                    if (string.IsNullOrWhiteSpace(eMoneyProfile.AccountNumber))
                    {
                        PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(2);
                        throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                    }

                    if (string.IsNullOrWhiteSpace(eMoneyProfile.ClientNumber))
                    {
                        PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(3);
                        throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                    }
                    #endregion

                    #region Validate PIN
                    //do nothing
                    #endregion

                    #region Commission Fee
                    GetCommissionFeeRequest getCommissionFeeRequest = new GetCommissionFeeRequest();
                    getCommissionFeeRequest.PartnerCode = domesticRemittance.SentByAgent.Partner.PartnerCode;
                    getCommissionFeeRequest.CurrencyID = domesticRemittance.SendCurrency.CurrencyID;
                    getCommissionFeeRequest.ServiceTypeID = ServiceType.AmendRemittance;
                    getCommissionFeeRequest.PrincipalAmount = domesticRemittance.PrincipalAmount;
                    getCommissionFeeRequest.ServiceFee = domesticRemittance.ServiceFee;


                    GetCommissionFeeResult getCommissionFeeResult = new GetCommissionFeeResult();

                    getCommissionFeeResult = getCommissionFeeRequest.Process();
                    commissionFee = getCommissionFeeResult.CommissionFee;
                    #endregion

                    #region Debit
                    EMoneyWSClient eMoneyServiceClient = new EMoneyWSClient();
                    EMoneyWS.UpdateEMoneyRequest request = new EMoneyWS.UpdateEMoneyRequest();

                    List<EMoneyWS.UpdateEMoneyRequest> requestList = new List<EMoneyWS.UpdateEMoneyRequest>();

                    EMoney eMoneyRequest = new EMoney();
                    eMoneyRequest.AccountNumber = eMoneyProfile.AccountNumber; //EMONEY ACCOUNT FROM PARTNER's SETTINGS
                    eMoneyRequest.ControlNumber = domesticRemittance.ControlNumber;
                    eMoneyRequest.Remarks = "Amend";
                    eMoneyRequest.PeraLinkAgentCode = this.AgentCode;

                    eMoneyRequest.CashInAmount = 0;
                    eMoneyRequest.CashOutAmount = domesticRemittance.ServiceFee;
                    requestList.Add(eMoneyRequest.ServiceCharge());
                    eMoneyRequest.CashInAmount = commissionFee;
                    eMoneyRequest.CashOutAmount = 0;
                    requestList.Add(eMoneyRequest.CommissionFee());
                    EMoneyWS.UpdateEMoneyResult result = eMoneyServiceClient.UpdateEMoney(requestList);

                    switch (result.ResultCode)
                    {
                        case EMoneyWS.ResultCodes.Successful:
                            // do nothing
                            break;
                        case EMoneyWS.ResultCodes.Failed:
                        case EMoneyWS.ResultCodes.Error:
                            throw new RDFramework.ClientException(result.ResultMessage);
                    }
                    #endregion

                    #region Insert Total Commission
                    AddCommissionTransactionRequest addCommissionTransactionRequest = new AddCommissionTransactionRequest();
                    addCommissionTransactionRequest.ISControlNo = domesticRemittance.ControlNumber;
                    addCommissionTransactionRequest.PartnerID = partner.PartnerID;

                    AddCommissionTransactionResult addCommissionTransactionResult = new AddCommissionTransactionResult();
                    addCommissionTransactionResult = addCommissionTransactionRequest.Process();
                    #endregion
                }
                else
                {
                    //do nothing
                }
            }
            #endregion

            
		}

		returnValue.ResultStatus = ResultStatus.Successful;

		return returnValue;
	}
	#endregion
}