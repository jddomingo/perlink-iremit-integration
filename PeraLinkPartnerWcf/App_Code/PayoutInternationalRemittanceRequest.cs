#define INTERNATIONAL_COMMISSION_FEE

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.Web.Services.Protocols;
using AppCryptor;

[DataContract]
public class PayoutInternationalRemittanceRequest : BaseRequest
{
    #region Constructor
    public PayoutInternationalRemittanceRequest()
    {
        //

        //
    }
    #endregion

    #region Fields/Properties
    private string _controlNumber;
    [DataMember]
    public string ControlNumber
    {
        get { return _controlNumber; }
        set { _controlNumber = value; }
    }

    private Int64 _beneficiaryID;
    [DataMember]
    public Int64 BeneficiaryID
    {
        get { return _beneficiaryID; }
        set { _beneficiaryID = value; }
    }

    private decimal _transactionAmount;
    [DataMember]
    public decimal TransactionAmount
    {
        get { return _transactionAmount; }
        set { _transactionAmount = value; }
    }

    private int _identificationTypeID;
    [DataMember]
    public int IdentificationTypeID
    {
        get { return _identificationTypeID; }
        set { _identificationTypeID = value; }
    }

    private string _identificationNumber;
    [DataMember]
    public string IdentificationNumber
    {
        get { return _identificationNumber; }
        set { _identificationNumber = value; }
    }

    private int _passportIDIssuedCountry;
    [DataMember]
    public int PassportIDIssuedCountry
    {
        get { return _passportIDIssuedCountry; }
        set { _passportIDIssuedCountry = value; }
    }

    private int _passportIDIssuedState;
    [DataMember]
    public int PassportIDIssuedState
    {
        get { return _passportIDIssuedState; }
        set { _passportIDIssuedState = value; }
    }

    private string _partnerCode;
    [DataMember]
    public string PartnerCode
    {
        get { return _partnerCode; }
        set { _partnerCode = value; }
    }

    private string _token;
    [DataMember]
    public string Token
    {
        get { return _token; }
        set { _token = value; }
    }

    private string _agentCode;
    [DataMember]
    public string AgentCode
    {
        get { return _agentCode; }
        set { _agentCode = value; }
    }

    private string _userID;
    [DataMember]
    public string UserID
    {
        get { return _userID; }
        set { _userID = value; }
    }

    private string _pullTransactionPartnerCode;
    [DataMember]
    public string PullTransactionPartnerCode
    {
        get { return _pullTransactionPartnerCode; }
        set { _pullTransactionPartnerCode = value; }
    }

    private string _idIssuingCity;
    [DataMember]
    public string IdIssuingCity
    {
        get { return _idIssuingCity; }
        set { _idIssuingCity = value; }
    }

    private DateTime? _idIssuingDate;
    [DataMember]
    public DateTime? IdIssuingDate
    {
        get { return _idIssuingDate; }
        set { _idIssuingDate = value; }
    }

    private string _idIssuingCountry;
    [DataMember]
    public string IdIssuingCountry
    {
        get { return _idIssuingCountry; }
        set { _idIssuingCountry = value; }
    }

    private string _idIssuingState;
    [DataMember]
    public string IdIssuingState
    {
        get { return _idIssuingState; }
        set { _idIssuingState = value; }
    }

    private string _pin;
    [DataMember]
    public string Pin
    {
        get { return _pin; }
        set { _pin = value; }
    }

    private string _terminalCode;
    [DataMember]
    public string TerminalCode
    {
        get { return _terminalCode; }
        set { _terminalCode = value; }
    }

    #endregion

    #region Internal Methods
    internal PayoutInternationalRemittanceResult Process()
    {
        PayoutInternationalRemittanceResult returnValue = new PayoutInternationalRemittanceResult();
        PartnerType partnerType = PartnerType.Pull;
        long partnerID = 0;
        decimal commissionFee = 0;
#if INTERNATIONAL_COMMISSION_FEE
        PeraLinkCoreWcf.PartnerServicePair pairing = new PeraLinkCoreWcf.PartnerServicePair();
#else
#endif

        // Important: This function validates the request credential
        this.AuthenticateRequest();

        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
            PeraLinkCoreWcf.InternationalRemittance internationalRemittance = new PeraLinkCoreWcf.InternationalRemittance();

            #region Validate requesting partner information
            PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
            partner.PartnerCode = this.PartnerCode;
            partner = serviceClient.FindPartnerCode(partner);

            if (partner.PartnerID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else { }

            if (partner.Token == this.Token) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (partner.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            #endregion

            #region Validate requesting agent information
            PeraLinkCoreWcf.Agent agent = new PeraLinkCoreWcf.Agent();
            agent.AgentCode = this.AgentCode;
            agent.Partner = partner;
            agent = serviceClient.FindAgentCodeOfPartner(agent);

            if (agent.AgentID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }

            if (agent.Partner.PartnerID == partner.PartnerID)
            { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(43);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (agent.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(46);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            #endregion

            #region Validate Terminal Record
            PeraLinkCoreWcf.Terminal terminal = new PeraLinkCoreWcf.Terminal();

            if (serviceClient.GetAgentTerminalCollection(agent).Length > 0)
            {
                terminal.TerminalCode = this.TerminalCode;
                terminal.Agent = agent;
                terminal.Agent.Partner = partner;
                terminal = serviceClient.FindTerminal(terminal);

                if (terminal.TerminalID == 0)
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(104);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
                else
                {
                    // Proceed
                }

                if (terminal.Agent.AgentID == agent.AgentID)
                { }
                else
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(105);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }

                if (terminal.Activated) { }
                else
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(106);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
            }
            else
            {
                //do nothing
            }
            #endregion

            #region Validate requesting partner's access to method (PayoutInternationalRemittance)
            PeraLinkCoreWcf.PartnerServiceMapping[] partnerServiceMappingCollection = serviceClient.GetPartnerServiceMappingCollection(partner);

            PeraLinkCoreWcf.PartnerServiceMapping partnerServiceMapping = Array.Find<PeraLinkCoreWcf.PartnerServiceMapping>(partnerServiceMappingCollection
                , delegate(PeraLinkCoreWcf.PartnerServiceMapping eachPartnerServiceMapping)
                {
                    return eachPartnerServiceMapping.ServiceType.ServiceTypeID == ServiceTypeID.PayoutRemittance;
                });


            if (partnerServiceMapping == null)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(47);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }

            #endregion

            #region Validate Payout Details
            if (RDFramework.Utility.Validation.IsEmptyString(this.IdentificationNumber))
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(98);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
            else { }

            if (this.IdentificationTypeID <= 0)
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(99);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
            else { }

            if (this.IdentificationNumber.Length > 50)
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(100);
                throw new RDFramework.ClientException(string.Format(message.Description, "50"), message.SystemMessageID);
            }
            #endregion

            PeraLinkCoreWcf.EMoneyProfile eMoneyProfile = new PeraLinkCoreWcf.EMoneyProfile();
            if (partner.SourceOfFundID == SourceOfFundID.EMoney)
            {
                #region Get E-Money Profile
                PeraLinkCoreWcf.GetEMoneyProfileRequest request = new PeraLinkCoreWcf.GetEMoneyProfileRequest();
                request.Partner = partner;

                eMoneyProfile = serviceClient.GetEMoneyProfile(request).EMoneyProfile;

                if (string.IsNullOrWhiteSpace(eMoneyProfile.AccountNumber))
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(2);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }

                if (string.IsNullOrWhiteSpace(eMoneyProfile.ClientNumber))
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(3);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }

                #endregion

                #region Validate PIN
                //do nothing
                #endregion

                #region Validate Balance
                {

                }
                #endregion
            }

            #region Find transaction
            FindInternationalRemittanceRequest findInternationalRemittanceRequest = new FindInternationalRemittanceRequest();
            findInternationalRemittanceRequest.AgentCode = this.AgentCode;
            findInternationalRemittanceRequest.ControlNumber = this.ControlNumber;
            findInternationalRemittanceRequest.BeneficiaryID = this.BeneficiaryID;
            findInternationalRemittanceRequest.PartnerCode = this.PartnerCode;
            findInternationalRemittanceRequest.PullTransactionPartnerCode = this.PullTransactionPartnerCode;
            findInternationalRemittanceRequest.ServiceCredential = this.ServiceCredential;
            findInternationalRemittanceRequest.Token = this.Token;
            findInternationalRemittanceRequest.UserID = this.UserID;
            FindInternationalRemittanceResult findInternationalRemittanceResult = findInternationalRemittanceRequest.Process();

            if (this.PullTransactionPartnerCode == SystemSetting.MoneyGramPartnerCode
                && !findInternationalRemittanceResult.OkForAgent)
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(85);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }

            internationalRemittance = new PeraLinkCoreWcf.InternationalRemittance();
            internationalRemittance.ControlNumber = findInternationalRemittanceResult.ControlNumber;
            internationalRemittance.InternationalRemittanceID = findInternationalRemittanceResult.InternationalRemittanceID;
            internationalRemittance.SentByAgent = new PeraLinkCoreWcf.Agent();
            internationalRemittance.SentByAgent.AgentID = findInternationalRemittanceResult.SentByAgentID;
            internationalRemittance.SentByAgent.AgentCode = findInternationalRemittanceResult.SentByAgentCode;
            internationalRemittance.SentByAgent.Partner = new PeraLinkCoreWcf.Partner();
            internationalRemittance.SentByAgent.Partner.PartnerID = findInternationalRemittanceResult.SentByPartnerID;
            internationalRemittance.SentByAgent.Partner.PartnerCode = findInternationalRemittanceResult.SentByPartnerCode;

            internationalRemittance.SendCurrency = new PeraLinkCoreWcf.Currency();
            internationalRemittance.SendCurrency.CurrencyID = findInternationalRemittanceResult.SendCurrencyID;
            internationalRemittance.SendCurrency.Code = findInternationalRemittanceResult.SendCurrencyCode;
            internationalRemittance.Beneficiary = new PeraLinkCoreWcf.Beneficiary();
            internationalRemittance.Beneficiary.BeneficiaryID = findInternationalRemittanceResult.BeneficiaryID;
            internationalRemittance.PrincipalAmount = findInternationalRemittanceResult.PrincipalAmount;
            internationalRemittance.ServiceFee = findInternationalRemittanceResult.ServiceFee;
            internationalRemittance.DateTimeSent = findInternationalRemittanceResult.DateTimeSent;
            internationalRemittance.SenderClient = new PeraLinkCoreWcf.Client();
            internationalRemittance.SenderClient.ClientID = findInternationalRemittanceResult.SenderClientID;
            internationalRemittance.SentByUserID = findInternationalRemittanceResult.SentByUserID;
            #endregion

            #region Validate requesting partner's access to transaction
            if (internationalRemittance.SentByAgent.Partner.PartnerID == agent.Partner.PartnerID)
            {
                if (internationalRemittance.SentByUserID == this.UserID)
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(44);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
                else { }

                if (internationalRemittance.SentByAgent.AgentID == agent.AgentID)
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(16);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
                else
                {
                    // Proceed
                }
            }
            else
            {
                PeraLinkCoreWcf.PartnerServicePair[] partnerServicePairCollection = serviceClient.GetPartnerServicePairCollection(partnerServiceMapping);
#if INTERNATIONAL_COMMISSION_FEE
                pairing = Array.Find<PeraLinkCoreWcf.PartnerServicePair>(partnerServicePairCollection
#else
                   PeraLinkCoreWcf.PartnerServicePair pairing = Array.Find<PeraLinkCoreWcf.PartnerServicePair>(partnerServicePairCollection
#endif
, delegate(PeraLinkCoreWcf.PartnerServicePair eachPartnerServicePair)
                    {
                        if (eachPartnerServicePair.PairedPartner == null)
                        {
                            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(2);
                            throw new RDFramework.ClientException(message.Content, message.MessageID);
                        }
                        else
                        {
                            return eachPartnerServicePair.PairedPartner.PartnerID == internationalRemittance.SentByAgent.Partner.PartnerID;
                        }
                    });

                if (pairing == null)
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(14);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
                else
                {
                    // Proceed
                    partnerID = pairing.PairedPartner.PartnerID;
                }
            }
            #endregion

            #region Validate transaction status
            if (findInternationalRemittanceResult.RemittanceStatusID == RemittanceStatusID.Outstanding
                || findInternationalRemittanceResult.RemittanceStatusID == RemittanceStatusID.Amended)
            {
                //Proceed
            }
            else
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(82);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
            #endregion

            #region ValidateCapping
            if (agent.CapAmount > 0)
            {
                PeraLinkCoreWcf.GetAgentTotalTransactionAmountRequest getAgentTotalTransactionAmountRequest = new PeraLinkCoreWcf.GetAgentTotalTransactionAmountRequest();
                getAgentTotalTransactionAmountRequest.Agent = agent;
                getAgentTotalTransactionAmountRequest.StartDate = DateTime.Today;
                getAgentTotalTransactionAmountRequest.EndDate = DateTime.Today.AddDays(1);

                PeraLinkCoreWcf.GetAgentTotalTransactionAmountResult getAgentTotalTransactionAmountResult = new PeraLinkCoreWcf.GetAgentTotalTransactionAmountResult();
                getAgentTotalTransactionAmountResult = serviceClient.GetAgentTotalTransactionAmount(getAgentTotalTransactionAmountRequest);

                decimal totalAmount = internationalRemittance.PrincipalAmount + getAgentTotalTransactionAmountResult.TotalTransactionAmount;

                if (agent.CapAmount < totalAmount)
                {
                    decimal remainingAmount = agent.CapAmount - getAgentTotalTransactionAmountResult.TotalTransactionAmount;
                    remainingAmount = (remainingAmount < 0) ? 0 : remainingAmount;
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(102);
                    throw new RDFramework.ClientException(string.Format(message.Description, remainingAmount), message.SystemMessageID);
                }
            }
            #endregion

            #region Validate Transaction Currency
            PeraLinkCoreWcf.PartnerServiceCurrency[] partnerServiceCurrencyCollection = serviceClient.GetPartnerServiceCurrencyCollection(partnerServiceMapping);

            PeraLinkCoreWcf.PartnerServiceCurrency transactionCurrency = Array.Find<PeraLinkCoreWcf.PartnerServiceCurrency>(partnerServiceCurrencyCollection
                , delegate(PeraLinkCoreWcf.PartnerServiceCurrency eachPartnerServiceCurrency)
                {
                    return eachPartnerServiceCurrency.Currency.CurrencyID == internationalRemittance.SendCurrency.CurrencyID;
                });

            if (transactionCurrency == null)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(26);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }
            #endregion

            #region Validate Transaction Amount
            decimal minGeneralSendAmount = SystemSetting.GetMinSendAmount(transactionCurrency.Currency.Code);
            decimal maxGeneralSendAmount = SystemSetting.GetMaxSendAmount(transactionCurrency.Currency.Code);

            if (minGeneralSendAmount <= internationalRemittance.PrincipalAmount && maxGeneralSendAmount >= internationalRemittance.PrincipalAmount)
            {
                // Proceed
            }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(6);
                throw new RDFramework.ClientException(
                    string.Format(message.Content, transactionCurrency.Currency.Code, minGeneralSendAmount, maxGeneralSendAmount)
                    , message.MessageID);
            }

            #endregion

            #region Payout
            PeraLinkCoreWcf.PayoutInternationalRemittanceRequest payoutInternationalRemittanceRequest = new PeraLinkCoreWcf.PayoutInternationalRemittanceRequest();
            payoutInternationalRemittanceRequest.InternationalBeneficiaryIdentification = new PeraLinkCoreWcf.InternationalBeneficiaryIdentification();
            payoutInternationalRemittanceRequest.InternationalBeneficiaryIdentification.IdentificationNumber = this.IdentificationNumber;
            payoutInternationalRemittanceRequest.InternationalBeneficiaryIdentification.IdentificationType = new PeraLinkCoreWcf.IdentificationType();
            payoutInternationalRemittanceRequest.InternationalBeneficiaryIdentification.IdentificationType.IdentificationTypeID = this.IdentificationTypeID;
            payoutInternationalRemittanceRequest.InternationalBeneficiaryIdentification.PassportIDIssuedCountry = this.PassportIDIssuedCountry;
            payoutInternationalRemittanceRequest.InternationalBeneficiaryIdentification.PassportIDIssuedState = this.PassportIDIssuedState;
            payoutInternationalRemittanceRequest.InternationalBeneficiaryIdentification.CreatedBy = this.UserID;
            payoutInternationalRemittanceRequest.InternationalBeneficiaryIdentification.DateTimeCreated = DateTime.Now;

            internationalRemittance.InternationalPayoutDetail = new PeraLinkCoreWcf.InternationalPayoutDetail();
            internationalRemittance.InternationalPayoutDetail.PayoutAmount = internationalRemittance.PrincipalAmount;
            internationalRemittance.InternationalPayoutDetail.PayoutCurrency = internationalRemittance.SendCurrency;

            internationalRemittance.UpdatedByAgent = agent;
            internationalRemittance.UpdatedByUserID = this.UserID;
            internationalRemittance.UpdatedByTerminal = terminal;

            internationalRemittance.DateTimeUpdated = DateTime.Now;
            payoutInternationalRemittanceRequest.InternationalRemittance = internationalRemittance;

            PeraLinkCoreWcf.AuditTrail auditTrail = new PeraLinkCoreWcf.AuditTrail();
            auditTrail.CreatedByAgent = agent;
            auditTrail.CreatedByUserID = this.UserID;
            auditTrail.DateTimeCreated = DateTime.Now;
            auditTrail.IPAddress = SystemUtility.GetClientIPAddress();
            payoutInternationalRemittanceRequest.AuditTrail = auditTrail;

            #region Partner Type
            switch (this.PullTransactionPartnerCode)
            {
                case "ABS2":
                case "IRT":
                case "FRX-PS":
                case "WRT-PS":
                case "ACE-PS":
                case "OWC-PS":
                case "EGS-PS":
                case "LMI-PS":
                case "TMS-PS":
                case "LNR-PS":
                case "SWF-PS":
                case "BKK-PS":
                case "KEB-PS":
                case "AFX-PS":
                case "GSC-PS":
                case "BMX-PS":
                case "GMT-PS":
                case "PMT-PS":
                case "TMF-PS":
                case "IDX-PS":
                case "MFS-PS":
                case "EMQ-PS":
                case "CMT-PS":
                case "MPL-PS":
                case "LLU-PS":
                    partnerType = PartnerType.Push;
                    break;
                default:
                    break;
            }
            #endregion

            #region For non-pull partner
            if (string.IsNullOrWhiteSpace(this.PullTransactionPartnerCode))
            {
                Partner matchedPartner = SystemUtility.FindPartnerWithControlNumberFormat(internationalRemittance.ControlNumber);

                if (matchedPartner == null)
                {
                    // Proceed PeraLink payout
                }
                else
                {
                    // Partner has no payout API set
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(39);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
            }
            else if (partnerType == PartnerType.Push)
            {

                #region Create a local copy of remittance
                PeraLinkCoreWcf.CacheInternationalRemittanceRequest cacheInternationalRemittanceRequest = new PeraLinkCoreWcf.CacheInternationalRemittanceRequest();
                cacheInternationalRemittanceRequest.InternationalRemittance = internationalRemittance;
                cacheInternationalRemittanceRequest.UseAssignedControlNumber = true;
                internationalRemittance = serviceClient.CacheInternationalRemittance(cacheInternationalRemittanceRequest);
                #endregion

                #region Cebuana Lhuillier Push Remiitance
                using (PeraLinkPushRemittanceWSClient peraLinkPushRemittanceWSClient = new PeraLinkPushRemittanceWSClient())
                {
                    PeraLinkPushRemittanceWS.PushRemittanceData pushRemittancePayoutRequest = new PeraLinkPushRemittanceWS.PushRemittanceData();
                    pushRemittancePayoutRequest.RefNumber = this.ControlNumber;
                    pushRemittancePayoutRequest.TransactionID = findInternationalRemittanceResult.InternationalRemittanceID.ToString();
                    pushRemittancePayoutRequest.SenderFullName = string.Format("{0}, {1}", findInternationalRemittanceResult.SenderLastName, findInternationalRemittanceResult.SenderFirstName);//pushRemittancePayoutData.Rows[0]["fldSFullname"].ToString();
                    pushRemittancePayoutRequest.BeneFName = findInternationalRemittanceResult.BeneficiaryFirstName;
                    pushRemittancePayoutRequest.BeneLName = findInternationalRemittanceResult.BeneficiaryLastName;
                    pushRemittancePayoutRequest.BeneFullName = string.Format("{0}, {1}", findInternationalRemittanceResult.BeneficiaryLastName, findInternationalRemittanceResult.BeneficiaryFirstName);//pushRemittancePayoutData.Rows[0]["fldBFullname"].ToString();
                    pushRemittancePayoutRequest.SendingCurrency = findInternationalRemittanceResult.SendCurrencyCode;
                    pushRemittancePayoutRequest.ReceivedCurrency = "PHP";
                    pushRemittancePayoutRequest.ReceivedAmount = findInternationalRemittanceResult.PrincipalAmount.ToString();
                    pushRemittancePayoutRequest.SendingBranch = findInternationalRemittanceResult.SentByPartnerCode;
                    pushRemittancePayoutRequest.TransStatus = findInternationalRemittanceResult.RemittanceStatusID.ToString();
                    pushRemittancePayoutRequest.TransDate = findInternationalRemittanceResult.DateTimeSent;
                    pushRemittancePayoutRequest.ConvertionRate = string.Empty;
                    pushRemittancePayoutRequest.Discount = "0";
                    pushRemittancePayoutRequest.OtherCharges = "0";
                    pushRemittancePayoutRequest.AuditTrail = SystemUtility.GenerateRemittanceAuditTrail();
                    pushRemittancePayoutRequest.PayoutBranch = this.AgentCode;
                    PeraLinkCoreWcf.IdentificationType[] identificationTypeCollection = serviceClient.GetIdentificationTypeCollection();
                    PeraLinkCoreWcf.IdentificationType submittedID = Array.Find<PeraLinkCoreWcf.IdentificationType>(identificationTypeCollection
                        , delegate(PeraLinkCoreWcf.IdentificationType eachIdentificationType)
                        {
                            return eachIdentificationType.IdentificationTypeID == this.IdentificationTypeID;
                        });
                    pushRemittancePayoutRequest.ID1Prensented = submittedID.Description;
                    pushRemittancePayoutRequest.ID1Details = this.IdentificationNumber;
                    pushRemittancePayoutRequest.ID2Prensented = string.Empty;
                    pushRemittancePayoutRequest.ID2Details = string.Empty;
                    pushRemittancePayoutRequest.BUID = findInternationalRemittanceResult.Beneficiary.BeneficiaryID.ToString(); //No Customer Number for Beneficiary in PeraLink
                    pushRemittancePayoutRequest.ReceiverID = this.UserID;

                    PeraLinkPushRemittanceWS.TransResult pushRemittancePayoutResult = new PeraLinkPushRemittanceWS.TransResult();
                    peraLinkPushRemittanceWSClient.NewPayoutTransaction(pushRemittancePayoutRequest);
                }
                #endregion
            }
            #endregion

            #region For pull partner
            else
            {
                #region Create a local copy of remittance
                PeraLinkCoreWcf.CacheInternationalRemittanceRequest cacheInternationalRemittanceRequest = new PeraLinkCoreWcf.CacheInternationalRemittanceRequest();
                cacheInternationalRemittanceRequest.InternationalRemittance = internationalRemittance;
                cacheInternationalRemittanceRequest.UseAssignedControlNumber = true;
                internationalRemittance = serviceClient.CacheInternationalRemittance(cacheInternationalRemittanceRequest);
                #endregion

                #region NYBP
                if (this.PullTransactionPartnerCode == SystemSetting.NYBPPartnerCode)
                {
                    NybpWcf.PayoutTransactionRequest payoutTransactionRequest = new NybpWcf.PayoutTransactionRequest();
                    payoutTransactionRequest.AgentID = agent.AgentID;
                    payoutTransactionRequest.SessionID = findInternationalRemittanceResult.OtherDetails;
                    payoutTransactionRequest.TransactionNumber = findInternationalRemittanceResult.ControlNumber;
                    payoutTransactionRequest.UserID = this.UserID;
                    decimal minSendAmount = SystemSetting.GetMinSendAmount(transactionCurrency.Currency.Code);
                    decimal maxSendAmountNYBPRetailAgents = SystemSetting.MaxSendAmountNYBPRetailAgents(transactionCurrency.Currency.Code);

                    Partner matchedPartnerCode = SystemUtility.FindPartnerWithPartnerCodeFormat(this.PartnerCode);

                    if (matchedPartnerCode == null)
                    {
                        using (NybpWcfClient nybpWcfClient = new NybpWcfClient())
                        {
                            nybpWcfClient.PayoutTransaction(payoutTransactionRequest);
                        }
                    }
                    else
                    {
                        if (minSendAmount <= internationalRemittance.PrincipalAmount && maxSendAmountNYBPRetailAgents >= internationalRemittance.PrincipalAmount)
                        {
                            using (NybpWcfClient nybpWcfClient = new NybpWcfClient())
                            {
                                nybpWcfClient.PayoutTransaction(payoutTransactionRequest);
                            }
                        }
                        else
                        {
                            PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(126);
                            throw new RDFramework.ClientException(string.Format(message.Description, maxSendAmountNYBPRetailAgents), message.SystemMessageID);
                        }
                    }

                }
                #endregion

                #region Placid SpotCash
                else if (this.PullTransactionPartnerCode == SystemSetting.PlacidSpotCashPartnerCode)
                {
                    PeraLinkPlacidSpotCashWcf.PayoutTransactionRequest payoutTransactionRequest = new PeraLinkPlacidSpotCashWcf.PayoutTransactionRequest();
                    string[] otherDetails = findInternationalRemittanceResult.OtherDetails.Split('|');
                    payoutTransactionRequest.ReceiverIDType = this.IdentificationTypeID.ToString();
                    payoutTransactionRequest.ReceiverIDNumber = this.IdentificationNumber;
                    payoutTransactionRequest.AccessCode = otherDetails[0];
                    payoutTransactionRequest.ExtraValue = otherDetails[1];
                    payoutTransactionRequest.Token = otherDetails[2];
                    payoutTransactionRequest.TransactionNumber = findInternationalRemittanceResult.ControlNumber;
                    using (PlacidWcfClient placidWcfClient = new PlacidWcfClient())
                    {
                        placidWcfClient.PayoutTransaction(payoutTransactionRequest);
                    }
                }
                #endregion

                #region Transfer Money Link
                else if (this.PullTransactionPartnerCode == SystemSetting.TransferMoneyLinkPartnerCode)
                {

                    PeraLinkTmlWcf.PayoutTransactionRequest payoutTransactionRequest = new PeraLinkTmlWcf.PayoutTransactionRequest();

                    using (PeraLinkCoreWcfClient coreWcfClient = new PeraLinkCoreWcfClient())
                    {
                        PeraLinkCoreWcf.PartnerRequestDetails partnerRequestDetails = new PeraLinkCoreWcf.PartnerRequestDetails();
                        partnerRequestDetails.AgentID = agent.AgentID;
                        partnerRequestDetails.PartnerCode = this._partnerCode;
                        partnerRequestDetails.ControlNumber = this._controlNumber;
                        partnerRequestDetails = coreWcfClient.FindPartnerRequestDetails(partnerRequestDetails);

                        payoutTransactionRequest.AgentSessionID = agent.AgentCode;
                        payoutTransactionRequest.ReferenceNumber = this._controlNumber;

                        PeraLinkCoreWcf.Country payoutCountry = new PeraLinkCoreWcf.Country();
                        payoutCountry.CountryID = this.PassportIDIssuedCountry;
                        payoutCountry = serviceClient.FindCountry(payoutCountry);

                        payoutTransactionRequest.ReceiverCountry = payoutCountry.CountryName;

                        PeraLinkCoreWcf.IdentificationType identificationType = new PeraLinkCoreWcf.IdentificationType();
                        identificationType.IdentificationTypeID = this.IdentificationTypeID;
                        identificationType = serviceClient.GetIdentificationType(identificationType);

                        payoutTransactionRequest.ReceiverIDType = identificationType.Description.ToString();
                        payoutTransactionRequest.ReceiverIDNumber = this.IdentificationNumber;
                        payoutTransactionRequest.ReceiverMobileNumber = (findInternationalRemittanceResult.Beneficiary.CellphoneNumber == null ? "" : findInternationalRemittanceResult.Beneficiary.CellphoneNumber);
                    }

                    using (TmlWcfClient tmlWcfClient = new TmlWcfClient())
                    {
                        // Transaction Lookup
                        PeraLinkTmlWcf.LookupTransactionRequest lookupTransactionRequest = new PeraLinkTmlWcf.LookupTransactionRequest();
                        lookupTransactionRequest.AgentSessionID = this._agentCode;
                        lookupTransactionRequest.ReferenceNumber = this._controlNumber;
                        PeraLinkTmlWcf.LookupTransactionResult lookupTransactionResult = tmlWcfClient.LookupTransaction(lookupTransactionRequest);

                        payoutTransactionRequest.PayTokenID = lookupTransactionResult.TokenID;
                        tmlWcfClient.PayoutTransaction(payoutTransactionRequest);

                        PeraLinkTmlWcf.UnlockTransactionRequest unlockTransactionRequest = new PeraLinkTmlWcf.UnlockTransactionRequest();
                        unlockTransactionRequest.AgentSessionID = agent.AgentCode;
                        unlockTransactionRequest.ReferenceNumber = this._controlNumber;
                        unlockTransactionRequest.PayTokenID = lookupTransactionResult.TokenID;//tokenID;

                        try
                        {
                            tmlWcfClient.UnlockTransaction(unlockTransactionRequest);
                        }
                        catch (Exception ex)
                        {
                            // Do nothing
                        }

                    }
                }
                #endregion

                #region CIMB
                else if (this.PullTransactionPartnerCode == SystemSetting.CimbPartnerCode)
                {
                    PeraLinkCimbWcf.PayoutTransactionRequest payoutTransactionRequest = new PeraLinkCimbWcf.PayoutTransactionRequest();
                    payoutTransactionRequest.Transaction = new PeraLinkCimbWcf.Transaction();
                    payoutTransactionRequest.Transaction.TransactionNumber = findInternationalRemittanceResult.ControlNumber;
                    payoutTransactionRequest.Transaction.SessionID = findInternationalRemittanceResult.OtherDetails.Split('|')[0];
                    payoutTransactionRequest.Transaction.ReceiverFullName = String.Format("{0} {1}", findInternationalRemittanceResult.BeneficiaryFirstName, findInternationalRemittanceResult.BeneficiaryLastName);
                    payoutTransactionRequest.Transaction.PayoutCurrency = findInternationalRemittanceResult.OtherDetails.Split('|')[1];
                    payoutTransactionRequest.Transaction.ReceiverCountry = findInternationalRemittanceResult.OtherDetails.Split('|')[2];
                    payoutTransactionRequest.Transaction.SenderFullName = String.Format("{0} {1}", findInternationalRemittanceResult.SenderFirstName, findInternationalRemittanceResult.SenderLastName);
                    payoutTransactionRequest.Transaction.SenderIDType = findInternationalRemittanceResult.OtherDetails.Split('|')[3];
                    payoutTransactionRequest.Transaction.SenderIDNumber = findInternationalRemittanceResult.OtherDetails.Split('|')[4];
                    payoutTransactionRequest.Transaction.SenderCountry = findInternationalRemittanceResult.OtherDetails.Split('|')[5];
                    payoutTransactionRequest.Transaction.ReceiverBirthDate = findInternationalRemittanceResult.BeneficiaryBirthDate;
                    payoutTransactionRequest.Transaction.ReceiverAddress = findInternationalRemittanceResult.OtherDetails.Split('|')[6];
                    payoutTransactionRequest.Transaction.ReceiverMobileNumber = findInternationalRemittanceResult.OtherDetails.Split('|')[7];
                    payoutTransactionRequest.Transaction.ReceiverPhoneNumber = findInternationalRemittanceResult.OtherDetails.Split('|')[8];
                    payoutTransactionRequest.Transaction.ReceiverOccupation = findInternationalRemittanceResult.OtherDetails.Split('|')[9];
                    payoutTransactionRequest.Transaction.ReceiverIdentificationTypeID = this.IdentificationTypeID;
                    payoutTransactionRequest.Transaction.ReceiverIDDetails = findInternationalRemittanceResult.OtherDetails.Split('|')[10];
                    payoutTransactionRequest.Transaction.ReceiverIDExpiryDate = (string.IsNullOrEmpty(findInternationalRemittanceResult.OtherDetails.Split('|')[11]) ? RDFramework.Utility.Validation.MinimumSqlDateTime : Convert.ToDateTime(findInternationalRemittanceResult.OtherDetails.Split('|')[11]));

                    using (CimbWcfClient cimbWcfClient = new CimbWcfClient())
                    {
                        cimbWcfClient.PayoutTransaction(payoutTransactionRequest);
                    }
                }
                #endregion

                #region MoneyGram
                else if (this.PullTransactionPartnerCode == SystemSetting.MoneyGramPartnerCode)
                {
                    DateTime timeStamp = DateTime.Now;

                    PeraLinkCoreWcf.Country payoutCountry = new PeraLinkCoreWcf.Country();
                    payoutCountry.CountryID = partner.Country.CountryID;
                    payoutCountry = serviceClient.FindCountry(payoutCountry);

                    PeraLinkCoreWcf.Beneficiary beneficiary = new PeraLinkCoreWcf.Beneficiary();
                    beneficiary.BeneficiaryID = this.BeneficiaryID;
                    beneficiary = serviceClient.GetBeneficiary(beneficiary);

                    PeraLinkCoreWcf.AgentAccessDetail agentAccessDetail = new PeraLinkCoreWcf.AgentAccessDetail();
                    agentAccessDetail.AgentID = agent.AgentID;
                    agentAccessDetail.PartnerID = partnerID;
                    PeraLinkCoreWcf.GetAgentAccessDetailResult getAgentAccessDetailResult = serviceClient.GetAgentAccessDetail(agentAccessDetail);

                    MoneyGramAgentDetail moneyGramAgentDetail = JsonConvert.DeserializeObject<MoneyGramAgentDetail>(getAgentAccessDetailResult.AgentAccessDetails.AccessDetail);
                    MoneyGramWcf.PayoutTransactionRequest payoutTransactionRequest = new MoneyGramWcf.PayoutTransactionRequest();
                    payoutTransactionRequest.AgentID = moneyGramAgentDetail.AgentID;
                    payoutTransactionRequest.AgentSequence = moneyGramAgentDetail.AgentSequence;
                    payoutTransactionRequest.ApiVersion = moneyGramAgentDetail.ApiVersion;
                    payoutTransactionRequest.ClientSoftwareVersion = moneyGramAgentDetail.ClientSoftwareVersion;
                    payoutTransactionRequest.TimeStamp = timeStamp;
                    payoutTransactionRequest.Token = moneyGramAgentDetail.Token;
                    payoutTransactionRequest.PeraLinkAgentCode = agent.AgentCode;
                    payoutTransactionRequest.PeraLinkUserID = this.UserID;
                    payoutTransactionRequest.ReferenceNumber = this.ControlNumber;

                    //TRANSACTION DETAILS
                    payoutTransactionRequest.AgentCheckAmount = internationalRemittance.PrincipalAmount;
                    payoutTransactionRequest.ReceiveCurrency = internationalRemittance.SendCurrency.Code;
                    payoutTransactionRequest.ReceiverDateOfBirth = beneficiary.BirthDate.Value;
                    payoutTransactionRequest.ReceiverCountry = beneficiary.CountryAddress.CountryCodeAlpha3;
                    payoutTransactionRequest.ReceiverPhotoIdCountry = payoutTransactionRequest.ReceiverCountry;
                    payoutTransactionRequest.DeliveryOption = findInternationalRemittanceResult.DeliveryOption;

                    if (String.IsNullOrWhiteSpace(beneficiary.Address))
                    {
                        PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(49);
                        throw new RDFramework.ClientException(message.Content, message.MessageID);
                    }
                    else
                    {
                        payoutTransactionRequest.ReceiverAddress = beneficiary.Address;
                        payoutTransactionRequest.ReceiverCity = beneficiary.ProvinceAddress;
                    }

                    if (String.IsNullOrWhiteSpace(beneficiary.ZipCode))
                    {
                        PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(50);
                        throw new RDFramework.ClientException(message.Content, message.MessageID);
                    }
                    else
                    {
                        payoutTransactionRequest.ReceiverZipCode = beneficiary.ZipCode;
                    }

                    if (String.IsNullOrWhiteSpace(beneficiary.CountryAddress.CountryCodeAlpha3))
                    {
                        PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(83);
                        throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                    }
                    else
                    {
                        payoutTransactionRequest.ReceiverCountryOfBirth = beneficiary.BirthCountry.CountryCodeAlpha3;
                    }

                    if (String.IsNullOrWhiteSpace(beneficiary.BirthCountry.CountryCodeAlpha3))
                    {
                        PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(84);
                        throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                    }
                    else
                    {
                        payoutTransactionRequest.ReceiverCountryOfBirth = beneficiary.BirthCountry.CountryCodeAlpha3;
                    }

                    if (!String.IsNullOrWhiteSpace(beneficiary.TelephoneNumber))
                    {
                        payoutTransactionRequest.ReceiverPhone = string.Format("{0}{1}{2}",
                                                                                beneficiary.TelephoneCountry.PhoneCode,
                                                                                beneficiary.TelephoneAreaCode,
                                                                                beneficiary.TelephoneNumber);
                    }
                    else if (!String.IsNullOrWhiteSpace(beneficiary.CellphoneNumber))
                    {
                        payoutTransactionRequest.ReceiverPhone = string.Format("{0}{1}",
                                                                                beneficiary.CellphoneCountry.PhoneCode,
                                                                                beneficiary.CellphoneNumber);
                    }
                    else
                    {
                        PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(36);
                        throw new RDFramework.ClientException(message.Content, message.MessageID);
                    }

                    if (String.IsNullOrWhiteSpace(beneficiary.Occupation))
                    {
                        PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(51);
                        throw new RDFramework.ClientException(message.Content, message.MessageID);
                    }
                    else
                    {
                        payoutTransactionRequest.ReceiverOccupation = beneficiary.Occupation;
                    }


                    payoutTransactionRequest.ReceiverPhotoIdNumber = this.IdentificationNumber;
                    if (this.IdentificationNumber.Length > 14)
                    {
                        payoutTransactionRequest.ReceiverLegalIdNumber = this.IdentificationNumber.Substring(0, 14).ToString().Trim();
                    }
                    else
                    {
                        payoutTransactionRequest.ReceiverLegalIdNumber = this.IdentificationNumber;
                    }

                    switch (this.IdentificationTypeID)
                    {
                        case 1:
                        case 2: //Barangay Clearance
                        case 3:
                        case 4:
                        case 5:
                        case 7:
                        case 8:
                        case 9:
                        case 10:
                        case 11:
                        case 12:
                        case 13:
                        case 14:
                        case 15:
                        case 17:
                        case 18:
                        case 19:
                        case 20:
                        case 21:
                        case 22:
                            payoutTransactionRequest.ReceiverPhotoIdType = MoneyGramWcf.photoIdType.GOV;

                            break;
                        case 6:
                            payoutTransactionRequest.ReceiverPhotoIdType = MoneyGramWcf.photoIdType.DRV;

                            break;
                        case 16:

                            payoutTransactionRequest.ReceiverPhotoIdType = MoneyGramWcf.photoIdType.PAS;
                            payoutTransactionRequest.ReceiverPassportIssueCity = this.IdIssuingCity;
                            payoutTransactionRequest.ReceiverPassportIssueDate = this.IdIssuingDate.Value;
                            break;
                    }

                    PeraLinkCoreWcf.Country photoIDCountry = new PeraLinkCoreWcf.Country();
                    photoIDCountry.CountryID = this.PassportIDIssuedCountry;
                    photoIDCountry = serviceClient.FindCountry(photoIDCountry);
                    payoutTransactionRequest.ReceiverPhotoIdCountry = photoIDCountry.CountryCodeAlpha3;

                    PeraLinkCoreWcf.State photoIDState = new PeraLinkCoreWcf.State();
                    if (!string.IsNullOrWhiteSpace(this.PassportIDIssuedState.ToString()))
                    {
                        photoIDState.StateID = this.PassportIDIssuedState;
                        photoIDState = serviceClient.FindState(photoIDState);
                        payoutTransactionRequest.ReceiverPhotoIdState = photoIDState.StateCode;
                    }

                    if (beneficiary.StateIDAddress != null)
                    {
                        PeraLinkCoreWcf.State stateAddress = new PeraLinkCoreWcf.State();
                        stateAddress.StateID = beneficiary.StateIDAddress.Value;
                        stateAddress = serviceClient.FindState(stateAddress);
                        payoutTransactionRequest.ReceiverState = stateAddress.StateCode;
                    }

                    using (MoneyGramWcfClient moneyGramWcfClient = new MoneyGramWcfClient())
                    {
                        try
                        {
                            MoneyGramWcf.PayoutTransactionResult payoutTransactionResult = moneyGramWcfClient.PayoutTransaction(payoutTransactionRequest);
                            returnValue.AuthorizationCode = payoutTransactionResult.AuthorizationCode;
                        }
                        catch (SoapException error)
                        {
                            throw error;
                        }
                    }


                }
                #endregion

                #region Xoom
                else if (this.PullTransactionPartnerCode == SystemSetting.XoomPartnerCode)
                {
                    PeraLinkXoomWS.PayoutTransactionRequest payoutTransactionRequest = new PeraLinkXoomWS.PayoutTransactionRequest();
                    //branchInformation
                    PeraLinkXoomWS.CebuanaBranchInformation cebuanaBranchInformation = new PeraLinkXoomWS.CebuanaBranchInformation();
                    cebuanaBranchInformation.BranchCode = this.AgentCode;
                    cebuanaBranchInformation.BranchUserID = this.UserID;
                    cebuanaBranchInformation.ClientApplicationVersion = string.Empty;
                    payoutTransactionRequest.CebuanaBranchInformation = cebuanaBranchInformation;
                    //transactionIdentifier
                    payoutTransactionRequest.TransactionNumber = findInternationalRemittanceResult.ControlNumber;
                    //disbursementInformation
                    payoutTransactionRequest.PayoutAmount = findInternationalRemittanceResult.PrincipalAmount;
                    payoutTransactionRequest.PayoutCurrency = findInternationalRemittanceResult.SendCurrencyCode;
                    payoutTransactionRequest.SendingCurrency = findInternationalRemittanceResult.SendCurrencyCode;
                    //payoutTransactionRequest.CurrencyConversionRate = 1M;
                    payoutTransactionRequest.PayoutCountry = findInternationalRemittanceResult.PayoutCountry;
                    payoutTransactionRequest.SenderLastName = findInternationalRemittanceResult.SenderLastName;
                    payoutTransactionRequest.SenderFirstName = findInternationalRemittanceResult.SenderFirstName;
                    payoutTransactionRequest.ReceiverCustomerNumber = findInternationalRemittanceResult.BeneficiaryID.ToString();
                    payoutTransactionRequest.ReceiverLastName = findInternationalRemittanceResult.BeneficiaryLastName;
                    payoutTransactionRequest.ReceiverFirstName = findInternationalRemittanceResult.BeneficiaryFirstName;
                    payoutTransactionRequest.ReceiverIDCode = this.IdentificationTypeID.ToString();
                    //payoutTransactionRequest.ReceiverIDType = _cebuanaCustomerIDs[cboCustomerIDSubmitted.SelectedIndex].IDDescription;
                    payoutTransactionRequest.ReceiverIDNumber = this.IdentificationNumber;
                    payoutTransactionRequest.ReceiverCity = findInternationalRemittanceResult.Beneficiary.ProvinceAddress;
                    payoutTransactionRequest.ReceiverCountry = findInternationalRemittanceResult.Beneficiary.CountryAddress.CountryCodeAplha2;

                    using (XoomWSClient xoomWSClient = new XoomWSClient())
                    {
                        xoomWSClient.PayoutTransaction(payoutTransactionRequest);
                    }
                }
                #endregion

                #region Tranglo
                else if (this.PullTransactionPartnerCode == SystemSetting.TrangloPartnerCode)
                {
                    PeraLinkTrangloWcf.PayoutTransactionRequest payoutTransactionRequest = new PeraLinkTrangloWcf.PayoutTransactionRequest();
                    payoutTransactionRequest.ControlNumber = findInternationalRemittanceResult.ControlNumber;
                    using (TrangloWcfClient trangloWcfClient = new TrangloWcfClient())
                    {
                        trangloWcfClient.PayoutTransaction(payoutTransactionRequest);
                    }
                }
                #endregion
                #region iRemit
                else if (this.PullTransactionPartnerCode == SystemSetting.IRemitPartnerCode)
                {
                    PeraLinkIRemitWCF.PayoutTransactionRequest payoutTransactionRequest = new PeraLinkIRemitWCF.PayoutTransactionRequest();
                    payoutTransactionRequest.TransactionNumber = findInternationalRemittanceResult.ControlNumber;
                    using (IRemitWcfClient iRemitWcfClient = new IRemitWcfClient())
                    {
                        iRemitWcfClient.PayoutTransaction(payoutTransactionRequest.TransactionNumber);
                    }
                }
                #endregion

                else if (this.PullTransactionPartnerCode.Length == 5
                    && this.PullTransactionPartnerCode.StartsWith("P"))
                {
                    PeraLinkRASInternationalWcf.PayoutTieUpRemittanceRequest payoutTieUpRemittanceRequest = new PeraLinkRASInternationalWcf.PayoutTieUpRemittanceRequest();
                    PeraLinkRASInternationalWcf.PayoutTieUpRemittanceResult payoutTieUpRemittanceResult = new PeraLinkRASInternationalWcf.PayoutTieUpRemittanceResult();

                    payoutTieUpRemittanceRequest.RemittanceTransaction = new PeraLinkRASInternationalWcf.RemittanceTransaction();
                    payoutTieUpRemittanceRequest.RemittanceTransaction.ControlNumber = this.ControlNumber;
                    payoutTieUpRemittanceRequest.RemittanceTransaction.PayoutDetails = new PeraLinkRASInternationalWcf.PayoutDetails();
                    payoutTieUpRemittanceRequest.RemittanceTransaction.PayoutDetails.Id1Details = this.IdentificationNumber;

                    PeraLinkCoreWcf.IdentificationType[] identificationTypeCollection = serviceClient.GetIdentificationTypeCollection();
                    PeraLinkCoreWcf.IdentificationType submittedID = Array.Find<PeraLinkCoreWcf.IdentificationType>(identificationTypeCollection
                        , delegate(PeraLinkCoreWcf.IdentificationType eachIdentificationType)
                        {
                            return eachIdentificationType.IdentificationTypeID == this.IdentificationTypeID;
                        });
                    payoutTieUpRemittanceRequest.RemittanceTransaction.PayoutDetails.Id1Type = submittedID.Description;

                    payoutTieUpRemittanceRequest.RemittanceTransaction.PayoutDetails.PaidByUser = this.UserID;
                    payoutTieUpRemittanceRequest.RemittanceTransaction.PayoutDetails.PayoutBranch = SystemSetting.PeraLinkBDSBranchCode;
                    payoutTieUpRemittanceRequest.RemittanceTransaction.PayoutDetails.PayoutDateTime = DateTime.Now;
                    payoutTieUpRemittanceRequest.RemittanceTransaction.SendingPartner = this.PullTransactionPartnerCode;

                    using (PeraLinkRASInternationalWcfClient peraLinkRASInternationalWcfClient = new PeraLinkRASInternationalWcfClient())
                    {
                        peraLinkRASInternationalWcfClient.PayoutTieUpTransaction(payoutTieUpRemittanceRequest);
                    }
                }

                #region Unknown partner
                else
                {
                    // Partner has no payout API set
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(39);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
                #endregion

            }
            #endregion

            payoutInternationalRemittanceRequest.InternationalRemittance = internationalRemittance;
            serviceClient.PayoutInternationalRemittance(payoutInternationalRemittanceRequest);
            #endregion

            if (partner.SourceOfFundID == SourceOfFundID.EMoney)
            {
#if INTERNATIONAL_COMMISSION_FEE
                #region Commission Fee
                PeraLinkCoreWcf.GetPartnerPairingCommissionFeeRequest feeRequest = new PeraLinkCoreWcf.GetPartnerPairingCommissionFeeRequest();
                feeRequest.PartnerPairingCommissionFee = new PeraLinkCoreWcf.PartnerPairingCommissionFee();
                feeRequest.PartnerPairingCommissionFee.Amount = internationalRemittance.PrincipalAmount;
                feeRequest.PartnerPairingCommissionFee.PartnerServicePair = pairing;

                PeraLinkCoreWcf.GetPartnerPairingCommissionFeeResult feeResult = new PeraLinkCoreWcf.GetPartnerPairingCommissionFeeResult();
                feeResult = serviceClient.GetPartnerPairingCommissionFee(feeRequest);
                
                if (feeResult.PartnerPairingCommissionFee.PartnerPairingCommissionFeeID == 0)
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(137);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
                else
                {

                    if (feeResult.PartnerPairingCommissionFee.FeeType.FeeTypeID == FeeTypeID.Fixed)
                    {
                        returnValue.CommissionFee = feeResult.PartnerPairingCommissionFee.Fee;
                    }
                    else
                    {
                        //Proceed. 
                    }
                }
                #endregion
#else
                #region Commission Fee
                GetCommissionFeeRequest getCommissionFeeRequest = new GetCommissionFeeRequest();
                getCommissionFeeRequest.PartnerCode = this.PartnerCode;
                getCommissionFeeRequest.CurrencyID = internationalRemittance.SendCurrency.CurrencyID;
                getCommissionFeeRequest.ServiceTypeID = ServiceType.PayoutRemittance;
                getCommissionFeeRequest.PrincipalAmount = internationalRemittance.InternationalPayoutDetail.PayoutAmount;
                getCommissionFeeRequest.ServiceFee = internationalRemittance.ServiceFee;


                GetCommissionFeeResult getCommissionFeeResult = new GetCommissionFeeResult();

                getCommissionFeeResult = getCommissionFeeRequest.Process();
                commissionFee = getCommissionFeeResult.CommissionFee;
                #endregion
#endif

                #region Credit to E-Money
                EMoneyWSClient eMoneyServiceClient = new EMoneyWSClient();

                List<EMoneyWS.UpdateEMoneyRequest> requestList = new List<EMoneyWS.UpdateEMoneyRequest>();

                EMoney eMoneyRequest = new EMoney();
                eMoneyRequest.AccountNumber = eMoneyProfile.AccountNumber; //EMONEY ACCOUNT FROM PARTNER's SETTINGS  
                eMoneyRequest.ControlNumber = internationalRemittance.ControlNumber;
                eMoneyRequest.Remarks = "International Payout";
                eMoneyRequest.PeraLinkAgentCode = this.AgentCode;

                eMoneyRequest.CashInAmount = internationalRemittance.PrincipalAmount;
                eMoneyRequest.CashOutAmount = 0;
                requestList.Add(eMoneyRequest.Principal());
#if INTERNATIONAL_COMMISSION_FEE
                eMoneyRequest.CashInAmount = returnValue.CommissionFee;
#else
                eMoneyRequest.CashInAmount = commissionFee;
#endif
                eMoneyRequest.CashOutAmount = 0;
                requestList.Add(eMoneyRequest.CommissionFee());

                EMoneyWS.UpdateEMoneyResult result = eMoneyServiceClient.UpdateEMoney(requestList);

                switch (result.ResultCode)
                {
                    case EMoneyWS.ResultCodes.Successful:
                        // do nothing  
                        break;
                    case EMoneyWS.ResultCodes.Failed:
                    case EMoneyWS.ResultCodes.Error:
                        throw new RDFramework.ClientException(result.ResultMessage);
                }
                #endregion

                #region Insert Total Commission
                AddCommissionTransactionRequest addCommissionTransactionRequest = new AddCommissionTransactionRequest();
                addCommissionTransactionRequest.ISControlNo = internationalRemittance.ControlNumber;
                addCommissionTransactionRequest.PartnerID = partner.PartnerID;

                AddCommissionTransactionResult addCommissionTransactionResult = new AddCommissionTransactionResult();
                addCommissionTransactionResult = addCommissionTransactionRequest.Process();
                #endregion
            }

            returnValue.ResultStatus = ResultStatus.Successful;

            return returnValue;
        }
    }
    #endregion
}