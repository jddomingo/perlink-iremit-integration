#define CHANGE_SERVICE_TYPE_TO_SENDREMITTANCE
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using AppCryptor;

[DataContract]
public class CancelDomesticRemittanceRequest : BaseRequest
{
	#region Constructor
	public CancelDomesticRemittanceRequest()
	{
		//

		//
	}
	#endregion

	#region Fields/Properties
	private string _controlNumber;
	[DataMember]
	public string ControlNumber
	{
		get { return _controlNumber; }
		set { _controlNumber = value; }
	}

	private string _partnerCode;
	[DataMember]
	public string PartnerCode
	{
		get { return _partnerCode; }
		set { _partnerCode = value; }
	}

	private string _token;
	[DataMember]
	public string Token
	{
		get { return _token; }
		set { _token = value; }
	}

	private string _agentCode;
	[DataMember]
	public string AgentCode
	{
		get { return _agentCode; }
		set { _agentCode = value; }
	}

	private string _userID;
	[DataMember]
	public string UserID
	{
		get { return _userID; }
		set { _userID = value; }
	}

	private string _pin;
	[DataMember]
	public string Pin
	{
		get { return _pin; }
		set { _pin = value; }
	}

	private string _terminalCode;
	[DataMember]
	public string TerminalCode
	{
		get { return _terminalCode; }
		set { _terminalCode = value; }
	}
	#endregion

	#region Internal Methods
	internal CancelDomesticRemittanceResult Process()
	{
		CancelDomesticRemittanceResult returnValue = new CancelDomesticRemittanceResult();
		bool isEmoney = false;
        decimal commissionFee = 0;
		// Important: This function validates the request credential
		this.AuthenticateRequest();

		using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
		{
			#region Validate Partner Record
			PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
			partner.PartnerCode = this.PartnerCode;
			partner = serviceClient.FindPartnerCode(partner);

			if (partner.PartnerID == 0)
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			else { }

			if (partner.Token == this.Token) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}

			if (partner.Activated) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}

			if (partner.SourceOfFundID == SystemSetting.EMoneySourceOfFundID)
			{
				isEmoney = true;
			}
			#endregion

			#region Validate Agent Record
			PeraLinkCoreWcf.Agent agent = new PeraLinkCoreWcf.Agent();
			agent.AgentCode = this.AgentCode;
			agent.Partner = partner;
			agent = serviceClient.FindAgentCodeOfPartner(agent);

			if (agent.AgentID == 0)
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			else
			{
				// Proceed
			}

			if (agent.Partner.PartnerID == partner.PartnerID)
			{ }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(43);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}

			if (agent.Activated) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(46);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			#endregion

			#region Validate Terminal Record
			PeraLinkCoreWcf.Terminal terminal = new PeraLinkCoreWcf.Terminal();
			int terminalCount = serviceClient.GetAgentTerminalCollection(agent).Length;

			if (terminalCount > 0)
			{
				terminal.TerminalCode = this.TerminalCode;
				terminal.Agent = agent;
				terminal.Agent.Partner = partner;
				terminal = serviceClient.FindTerminal(terminal);

				if (terminal.TerminalID == 0)
				{
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(104);
					throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
				}
				else
				{
					// Proceed
				}

				if (terminal.Agent.AgentID == agent.AgentID)
				{ }
				else
				{
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(105);
					throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
				}

				if (terminal.Activated) { }
				else
				{
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(106);
					throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
				}
			}
			else
			{
				//do nothing
			}
			#endregion

			#region Validate Partner's Access To Cancel Domestic Remittance Service
			PeraLinkCoreWcf.PartnerServiceMapping[] partnerServiceMappingCollection = serviceClient.GetPartnerServiceMappingCollection(partner);

			PeraLinkCoreWcf.PartnerServiceMapping partnerServiceMapping = Array.Find<PeraLinkCoreWcf.PartnerServiceMapping>(partnerServiceMappingCollection
				, delegate(PeraLinkCoreWcf.PartnerServiceMapping eachPartnerServiceMapping)
				{
					return eachPartnerServiceMapping.ServiceType.ServiceTypeID == ServiceTypeID.CancelRemittance;
				});


			if (partnerServiceMapping == null)
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(17);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			else
			{
				// Proceed
			}
			#endregion

			#region Get E-Money Profile
			PeraLinkCoreWcf.EMoneyProfile eMoneyProfile = new PeraLinkCoreWcf.EMoneyProfile();
			if (isEmoney)
			{
				PeraLinkCoreWcf.GetEMoneyProfileRequest request = new PeraLinkCoreWcf.GetEMoneyProfileRequest();
				request.Partner = partner;

				eMoneyProfile = serviceClient.GetEMoneyProfile(request).EMoneyProfile;

				if (string.IsNullOrWhiteSpace(eMoneyProfile.AccountNumber))
				{
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(2);
					throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
				}

				if (string.IsNullOrWhiteSpace(eMoneyProfile.ClientNumber))
				{
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(3);
					throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
				}
			}
			#endregion

			#region Validate PIN for E-Money
        //do nothing
			#endregion

			#region Find Domestic Remittance
			FindDomesticRemittanceRequest findDomesticRemittanceRequest = new FindDomesticRemittanceRequest();
			findDomesticRemittanceRequest.AgentCode = this.AgentCode;
			findDomesticRemittanceRequest.ControlNumber = this.ControlNumber;
			findDomesticRemittanceRequest.PartnerCode = this.PartnerCode;
			findDomesticRemittanceRequest.TerminalCode = this.TerminalCode;
			findDomesticRemittanceRequest.ServiceCredential = this.ServiceCredential;
			findDomesticRemittanceRequest.Token = this.Token;
			findDomesticRemittanceRequest.UserID = this.UserID;
			FindDomesticRemittanceResult findDomesticRemittanceResult = findDomesticRemittanceRequest.Process();

			PeraLinkCoreWcf.DomesticRemittance domesticRemittance = new PeraLinkCoreWcf.DomesticRemittance();
			domesticRemittance.ControlNumber = findDomesticRemittanceResult.ControlNumber;
			domesticRemittance.DomesticRemittanceID = findDomesticRemittanceResult.DomesticRemittanceID;
			domesticRemittance.SentByAgent = new PeraLinkCoreWcf.Agent();
			domesticRemittance.SentByAgent.AgentID = findDomesticRemittanceResult.SentByAgentID;
			domesticRemittance.SentByAgent.AgentCode = findDomesticRemittanceResult.SentByAgentCode;
			domesticRemittance.SentByAgent.Partner = new PeraLinkCoreWcf.Partner();
			domesticRemittance.SentByAgent.Partner.PartnerID = findDomesticRemittanceResult.SentByPartnerID;
			domesticRemittance.SentByAgent.Partner.PartnerCode = findDomesticRemittanceResult.SentByPartnerCode;
			domesticRemittance.SendCurrency = new PeraLinkCoreWcf.Currency();
			domesticRemittance.SendCurrency.CurrencyID = findDomesticRemittanceResult.SendCurrencyID;
			domesticRemittance.SendCurrency.Code = findDomesticRemittanceResult.SendCurrencyCode;
			domesticRemittance.Beneficiary = new PeraLinkCoreWcf.Beneficiary();
			domesticRemittance.Beneficiary.BeneficiaryID = findDomesticRemittanceResult.BeneficiaryID;
			domesticRemittance.PrincipalAmount = findDomesticRemittanceResult.PrincipalAmount;
			domesticRemittance.ServiceFee = findDomesticRemittanceResult.ServiceFee;
			domesticRemittance.DateTimeSent = findDomesticRemittanceResult.DateTimeSent;
			domesticRemittance.SenderClient = new PeraLinkCoreWcf.Client();
			domesticRemittance.SenderClient.ClientID = findDomesticRemittanceResult.SenderClientID;
			domesticRemittance.SentByUserID = findDomesticRemittanceResult.SentByUserID;

			if (domesticRemittance.SentByAgent.AgentID == agent.AgentID)
			{
				// Proceed
			}
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(19);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}

			if (terminalCount > 0)
			{
				if (domesticRemittance.SentByTerminal != null)
				{
					if (domesticRemittance.SentByTerminal.TerminalID == terminal.TerminalID)
					{
						//Proceed
					}
					else
					{
						PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(108);
						throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
					}
				}
				else
				{
					//Allow
				}
			}
			else
			{
				//Allow
			}
			#endregion

			domesticRemittance.UpdatedByAgent = agent;
			domesticRemittance.UpdatedByUserID = this.UserID;
			domesticRemittance.UpdatedByTerminal = terminal;
			domesticRemittance.DateTimeUpdated = DateTime.Now;

			domesticRemittance.DomesticCancelDetail = new PeraLinkCoreWcf.DomesticCancelDetail();
			domesticRemittance.DomesticCancelDetail.Reason = "User Initiated";
			domesticRemittance.DomesticCancelDetail.DomesticRemittanceID = domesticRemittance.DomesticRemittanceID;
			domesticRemittance.DomesticCancelDetail.CreatedBy = this.UserID;
			domesticRemittance.DomesticCancelDetail.DateTimeCreated = domesticRemittance.DateTimeUpdated;

			serviceClient.CancelDomesticRemittance(domesticRemittance);

			
			if (isEmoney)
			{
                #region Commission Fee
                GetCommissionFeeRequest getCommissionFeeRequest = new GetCommissionFeeRequest();
                getCommissionFeeRequest.PartnerCode = domesticRemittance.SentByAgent.Partner.PartnerCode;
                getCommissionFeeRequest.CurrencyID = domesticRemittance.SendCurrency.CurrencyID;
#if CHANGE_SERVICE_TYPE_TO_SENDREMITTANCE
                getCommissionFeeRequest.ServiceTypeID = ServiceType.SendRemittance;
#else
                getCommissionFeeRequest.ServiceTypeID = ServiceType.CancelRemittance;
#endif
                getCommissionFeeRequest.PrincipalAmount = domesticRemittance.PrincipalAmount;
                getCommissionFeeRequest.ServiceFee = domesticRemittance.ServiceFee;


                GetCommissionFeeResult getCommissionFeeResult = new GetCommissionFeeResult();

                getCommissionFeeResult = getCommissionFeeRequest.Process();
                commissionFee = getCommissionFeeResult.CommissionFee;
                #endregion

                #region Credit E-Money
                EMoneyWSClient eMoneyServiceClient = new EMoneyWSClient();
				EMoneyWS.UpdateEMoneyRequest request = new EMoneyWS.UpdateEMoneyRequest();

				List<EMoneyWS.UpdateEMoneyRequest> requestList = new List<EMoneyWS.UpdateEMoneyRequest>();

				EMoney eMoneyRequest = new EMoney();
				eMoneyRequest.AccountNumber = eMoneyProfile.AccountNumber; //EMONEY ACCOUNT FROM PARTNER's SETTINGS
				eMoneyRequest.ControlNumber = domesticRemittance.ControlNumber;
				eMoneyRequest.Remarks = "Cancel";
				eMoneyRequest.PeraLinkAgentCode = this.AgentCode;

				eMoneyRequest.CashInAmount = domesticRemittance.PrincipalAmount;
				eMoneyRequest.CashOutAmount = 0;
				requestList.Add(eMoneyRequest.Principal());

				eMoneyRequest.CashInAmount = domesticRemittance.ServiceFee;
				eMoneyRequest.CashOutAmount = 0;
				requestList.Add(eMoneyRequest.ServiceCharge());

                eMoneyRequest.CashInAmount = 0;
                eMoneyRequest.CashOutAmount = commissionFee;
                requestList.Add(eMoneyRequest.CommissionFee());

				EMoneyWS.UpdateEMoneyResult result = eMoneyServiceClient.UpdateEMoney(requestList);

				switch (result.ResultCode)
				{
					case EMoneyWS.ResultCodes.Successful:
						// do nothing
						break;
					case EMoneyWS.ResultCodes.Failed:
					case EMoneyWS.ResultCodes.Error:
						throw new RDFramework.ClientException(result.ResultMessage);
                }
                #endregion

                #region Insert Total Commission
                AddCommissionTransactionRequest addCommissionTransactionRequest = new AddCommissionTransactionRequest();
            addCommissionTransactionRequest.ISControlNo = domesticRemittance.ControlNumber;
            addCommissionTransactionRequest.PartnerID = partner.PartnerID;

            AddCommissionTransactionResult addCommissionTransactionResult = new AddCommissionTransactionResult();
            addCommissionTransactionResult = addCommissionTransactionRequest.Process();
            #endregion
            }
                

            
		}

		returnValue.ResultStatus = ResultStatus.Successful;

		return returnValue;
	}
	#endregion
}