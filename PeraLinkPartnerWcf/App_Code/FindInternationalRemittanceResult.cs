using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class FindInternationalRemittanceResult: BaseResult
{
    #region Constructor
    public FindInternationalRemittanceResult()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private Int64 _internationalRemittanceID;
    [DataMember]
    public Int64 InternationalRemittanceID
    {
        get { return _internationalRemittanceID; }
        set { _internationalRemittanceID = value; }
    }

    private string _controlNumber;
    [DataMember]
    public string ControlNumber
    {
        get { return _controlNumber; }
        set { _controlNumber = value; }
    }

    private Int64 _senderClientID;
    [DataMember]
    public Int64 SenderClientID
    {
        get { return _senderClientID; }
        set { _senderClientID = value; }
    }

    private string _senderClientNumber;
    [DataMember]
    public string SenderClientNumber
    {
        get { return _senderClientNumber; }
        set { _senderClientNumber = value; }
    }

    private string _senderFirstName;
    [DataMember]
    public string SenderFirstName
    {
        get { return _senderFirstName; }
        set { _senderFirstName = value; }
    }

    private string _senderMiddleName;
    [DataMember]
    public string SenderMiddleName
    {
        get { return _senderMiddleName; }
        set { _senderMiddleName = value; }
    }

    private string _senderLastName;
    [DataMember]
    public string SenderLastName
    {
        get { return _senderLastName; }
        set { _senderLastName = value; }
    }

    private DateTime? _senderBirthDate;
    [DataMember]
    public DateTime? SenderBirthDate
    {
        get { return _senderBirthDate; }
        set { _senderBirthDate = value; }
    }

	private string _senderAddress;
	[DataMember]
	public string SenderAddress
	{
		get { return _senderAddress; }
		set { _senderAddress = value; }
	}

    private Int64 _beneficiaryID;
    [DataMember]
    public Int64 BeneficiaryID
    {
        get { return _beneficiaryID; }
        set { _beneficiaryID = value; }
    }

	private string _beneficiaryFirstName;
    [DataMember]
    public string BeneficiaryFirstName
    {
        get { return _beneficiaryFirstName; }
        set { _beneficiaryFirstName = value; }
    }

    private string _beneficiaryMiddleName;
    [DataMember]
    public string BeneficiaryMiddleName
    {
        get { return _beneficiaryMiddleName; }
        set { _beneficiaryMiddleName = value; }
    }

    private DateTime? _beneficiaryBirthDate;
    [DataMember]
    public DateTime? BeneficiaryBirthDate
    {
        get { return _beneficiaryBirthDate; }
        set { _beneficiaryBirthDate = value; }
    }

    private string _beneficiaryLastName;
    [DataMember]
    public string BeneficiaryLastName
    {
        get { return _beneficiaryLastName; }
        set { _beneficiaryLastName = value; }
    }

    private decimal _principalAmount;
    [DataMember]
    public decimal PrincipalAmount
    {
        get { return _principalAmount; }
        set { _principalAmount = value; }
    }

    private decimal _serviceFee;
    [DataMember]
    public decimal ServiceFee
    {
        get { return _serviceFee; }
        set { _serviceFee = value; }
    }

    private int _remittanceStatusID;
    [DataMember]
    public int RemittanceStatusID
    {
        get { return _remittanceStatusID; }
        set { _remittanceStatusID = value; }
    }

    private string _remittanceStatusDescription;
    [DataMember]
    public string RemittanceStatusDescription
    {
        get { return _remittanceStatusDescription; }
        set { _remittanceStatusDescription = value; }
    }

    private int _sendCurrencyID;
    [DataMember]
    public int SendCurrencyID
    {
        get { return _sendCurrencyID; }
        set { _sendCurrencyID = value; }
    }

    private string _sendCurrencyCode;
    [DataMember]
    public string SendCurrencyCode
    {
        get { return _sendCurrencyCode; }
        set { _sendCurrencyCode = value; }
    }

    private DateTime _dateTimeSent;
    [DataMember]
    public DateTime DateTimeSent
    {
        get { return _dateTimeSent; }
        set { _dateTimeSent = value; }
    }

    private Int64 _sentByAgentID;
    [DataMember]
    public Int64 SentByAgentID
    {
        get { return _sentByAgentID; }
        set { _sentByAgentID = value; }
    }

    private string _sentByAgentCode;
    [DataMember]
    public string SentByAgentCode
    {
        get { return _sentByAgentCode; }
        set { _sentByAgentCode = value; }
    }

    private Int64 _sentByPartnerID;
    [DataMember]
    public Int64 SentByPartnerID
    {
        get { return _sentByPartnerID; }
        set { _sentByPartnerID = value; }
    }

    private string _sentByPartnerCode;
    [DataMember]
    public string SentByPartnerCode
    {
        get { return _sentByPartnerCode; }
        set { _sentByPartnerCode = value; }
    }

    private string _sentByPartnerName;
    [DataMember]
    public string SentByPartnerName
    {
        get { return _sentByPartnerName; }
        set { _sentByPartnerName = value; }
    }

    private string _sentByUserID;
    [DataMember]
    public string SentByUserID
    {
        get { return _sentByUserID; }
        set { _sentByUserID = value; }
    }

    private string _otherDetails;
    [DataMember]
    public string OtherDetails
    {
        get { return _otherDetails; }
        set { _otherDetails = value; }
    }

	private Beneficiary _beneficiary;
	[DataMember]
	public Beneficiary Beneficiary
	{
		get { return _beneficiary; }
		set { _beneficiary = value; }
	}

	private string _payoutCountry;
	[DataMember]
	public string PayoutCountry
	{
		get { return _payoutCountry; }
		set { _payoutCountry = value; }
	}

	private string _originatingCountry;
	[DataMember]
	public string OriginatingCountry
	{
		get { return _originatingCountry; }
		set { _originatingCountry = value; }
	}

    private string _authorizationCode;
    [DataMember]
    public string AuthorizationCode
    {
        get { return _authorizationCode; }
        set { _authorizationCode = value; }
    }

    private string _deliveryOption;
    [DataMember]
    public string DeliveryOption
    {
        get { return _deliveryOption; }
        set { _deliveryOption = value; }
    }

	private bool _okForAgent;
	[DataMember]
	internal bool OkForAgent
	{
		get { return _okForAgent; }
		set { _okForAgent = value; }
	}

	private string _originalSendCurrency;
	[DataMember]
	public string OriginalSendCurrency
	{
		get { return _originalSendCurrency; }
		set { _originalSendCurrency = value; }
	}

	private decimal _originalSendAmount;
	[DataMember]
	public decimal OriginalSendAmount
	{
		get { return _originalSendAmount; }
		set { _originalSendAmount = value; }
	}

	private decimal _exchangeRate;
	[DataMember]
	public decimal ExchangeRate
	{
		get { return _exchangeRate; }
		set { _exchangeRate = value; }
	}

    #endregion
}