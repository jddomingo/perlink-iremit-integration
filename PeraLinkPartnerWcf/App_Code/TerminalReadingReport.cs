using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class TerminalReadingReport
{
	#region Constructor
	public TerminalReadingReport()
	{ }
	#endregion

	#region Fields/Properties
	private Int64 _terminalID;

	public Int64 TerminalID
	{
		get { return _terminalID; }
		set { _terminalID = value; }
	}

	private DateTime _transactionDate;

	public DateTime TransactionDate
	{
		get { return _transactionDate; }
		set { _transactionDate = value; }
	}

	private int _transactionCount;

	public int TransactionCount
	{
		get { return _transactionCount; }
		set { _transactionCount = value; }
	}

	private decimal _oldGrandTotal;

	public decimal OldGrandTotal
	{
		get { return _oldGrandTotal; }
		set { _oldGrandTotal = value; }
	}

	private decimal _principalAmount;

	public decimal PrincipalAmount
	{
		get { return _principalAmount; }
		set { _principalAmount = value; }
	}

	private decimal _serviceFee;

	public decimal ServiceFee
	{
		get { return _serviceFee; }
		set { _serviceFee = value; }
	}

	private decimal _totalTransactionAmount;

	public decimal TotalTransactionAmount
	{
		get { return _totalTransactionAmount; }
		set { _totalTransactionAmount = value; }
	}

	private decimal _newGrandTotal;

	public decimal NewGrandTotal
	{
		get { return _newGrandTotal; }
		set { _newGrandTotal = value; }
	}

	private int _voidTransaction;

	public int VoidTransaction
	{
		get { return _voidTransaction; }
		set { _voidTransaction = value; }
	}
	#endregion

	#region Public Methods
	public void Load(PeraLinkCoreWcf.TerminalReadingReport terminalReadingReport)
	{
		this.TerminalID = terminalReadingReport.Terminal.TerminalID;
		this.TransactionDate = terminalReadingReport.TransactionDate;
		this.TransactionCount = terminalReadingReport.TransactionCount;
		this.PrincipalAmount = terminalReadingReport.PrincipalAmount;
		this.ServiceFee = terminalReadingReport.ServiceFee;
		this.TotalTransactionAmount = terminalReadingReport.TotalTransactionAmount;
		this.NewGrandTotal = terminalReadingReport.NewGrandTotal;
		this.VoidTransaction = terminalReadingReport.VoidTransaction;
	}
	#endregion
}