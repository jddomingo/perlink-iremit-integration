using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class PeraLinkRASInternationalWcfClient: IDisposable
{
	#region Constructor
	public PeraLinkRASInternationalWcfClient()
	{
		_serviceClient = new PeraLinkRASInternationalWcf.ServiceClient();
	}
	#endregion

	#region Fields/Properties
	private PeraLinkRASInternationalWcf.ServiceClient _serviceClient;
	#endregion

	#region Public Methods
	public PeraLinkRASInternationalWcf.LookUpTieUpTransactionResult LookUpTieUpTransaction(PeraLinkRASInternationalWcf.LookUpTieUpTransactionRequest lookUpTieUpTransactionRequest)
	{
		PeraLinkRASInternationalWcf.LookUpTieUpTransactionResult returnValue = _serviceClient.LookUpTieUpTransaction(lookUpTieUpTransactionRequest);

		switch (returnValue.ResultStatus)
		{
			case ResultStatus.Successful:
				return returnValue;
			case ResultStatus.Failed:
			case ResultStatus.Error:
				throw new RDFramework.ClientException(returnValue.Message);
			default:
				throw new ArgumentOutOfRangeException(returnValue.ResultStatus.ToString());
		}
	}

	public PeraLinkRASInternationalWcf.PayoutTieUpRemittanceResult PayoutTieUpTransaction(PeraLinkRASInternationalWcf.PayoutTieUpRemittanceRequest payoutTieUpTransactionRequest)
	{
		PeraLinkRASInternationalWcf.PayoutTieUpRemittanceResult returnValue = _serviceClient.PayoutTieUpRemittance(payoutTieUpTransactionRequest);

		switch (returnValue.ResultStatus)
		{
			case ResultStatus.Successful:
				return returnValue;
			case ResultStatus.Failed:
			case ResultStatus.Error:
				throw new RDFramework.ClientException(returnValue.Message);
			default:
				throw new ArgumentOutOfRangeException(returnValue.ResultStatus.ToString());
		}
	}

	public static PeraLinkCoreWcf.RemittanceStatus MapStatus(PeraLinkRASInternationalWcf.TransactionStatus transactionStatus)
	{
		PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
		switch (transactionStatus)
		{
			case PeraLinkRASInternationalWcf.TransactionStatus.AMEND:
			case PeraLinkRASInternationalWcf.TransactionStatus.OUTSTANDING:
				{
					returnValue.RemittanceStatusID = 1;
					returnValue.Description = "Outstanding";
					return returnValue;
				}
			case PeraLinkRASInternationalWcf.TransactionStatus.PAID:
				{
					returnValue.RemittanceStatusID = 2;
					returnValue.Description = "Paid";
					return returnValue;
				}
			case PeraLinkRASInternationalWcf.TransactionStatus.CANCEL:
				{
					returnValue.RemittanceStatusID = 4;
					returnValue.Description = "Cancelled";
					return returnValue;
				}
			default:
				{
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(86);
					throw new RDFramework.ClientException(string.Format(message.Description, transactionStatus.ToString()), message.SystemMessageID);
				}
		}
	}
	#endregion
	void IDisposable.Dispose()
	{
		bool isClosed = false;

		try
		{
			if (this._serviceClient.State != System.ServiceModel.CommunicationState.Faulted)
			{
				this._serviceClient.Close();
				isClosed = true;
			}
			else
			{
				// Proceed with Abort in finally
			}
		}
		finally
		{
			if (!isClosed)
			{
				this._serviceClient.Abort();
			}
			else
			{
				// Do nothing since state is already closed
			}
		}
	}
}