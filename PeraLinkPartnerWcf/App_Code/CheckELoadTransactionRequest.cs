using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for CheckELoadTransactionRequest
/// </summary>
public class CheckELoadTransactionRequest : BaseRequest
{
    #region Constructor
    public CheckELoadTransactionRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion

    #region Fields/Properties
    private string _referenceNumber;

    public string ReferenceNumber
    {
        get { return _referenceNumber; }
        set { _referenceNumber = value; }
    }

    private string _partnerCode;
    [DataMember]
    public string PartnerCode
    {
        get { return _partnerCode; }
        set { _partnerCode = value; }
    }

    private string _token;
    [DataMember]
    public string Token
    {
        get { return _token; }
        set { _token = value; }
    }

    private string _agentCode;
    [DataMember]
    public string AgentCode
    {
        get { return _agentCode; }
        set { _agentCode = value; }
    }

    private string _terminalCode;
    [DataMember]
    public string TerminalCode
    {
        get { return _terminalCode; }
        set { _terminalCode = value; }
    }

    private string _userID;
    [DataMember]
    public string UserID
    {
        get { return _userID; }
        set { _userID = value; }
    }

    private bool _allowSearchAll;
    [DataMember]
    public bool AllowSearchAll
    {
        get { return _allowSearchAll; }
        set { _allowSearchAll = value; }
    }

    #endregion

    #region Internal Methods
    public CheckELoadTransactionResult Process()
    {
        CheckELoadTransactionResult returnValue = new CheckELoadTransactionResult();
        PeraLinkELoadGatewayWcf.CheckELoadTransactionResult checkELoadTransactionResult = new PeraLinkELoadGatewayWcf.CheckELoadTransactionResult();
        bool isEmoney = false;
        // Important: This function validates the request credential
        this.AuthenticateRequest();

        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
            #region Validate Partner Record
            PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
            partner.PartnerCode = this.PartnerCode;
            partner = serviceClient.FindPartnerCode(partner);

            if (partner.PartnerID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else { }

            if (partner.Token == this.Token) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (partner.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (partner.SourceOfFundID == SystemSetting.EMoneySourceOfFundID)
            {
                isEmoney = true;
            }
            #endregion

            #region Validate Agent Record
            PeraLinkCoreWcf.Agent agent = new PeraLinkCoreWcf.Agent();
            agent.AgentCode = this.AgentCode;
            agent.Partner = partner;
            agent = serviceClient.FindAgentCodeOfPartner(agent);

            if (agent.AgentID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }

            if (agent.Partner.PartnerID == partner.PartnerID)
            { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(43);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (agent.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(46);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            #endregion

            #region Validate Terminal Record
            PeraLinkCoreWcf.Terminal terminal = new PeraLinkCoreWcf.Terminal();

            if (serviceClient.GetAgentTerminalCollection(agent).Length > 0)
            {
                terminal.TerminalCode = this.TerminalCode;
                terminal.Agent = agent;
                terminal.Agent.Partner = partner;
                terminal = serviceClient.FindTerminal(terminal);

                if (terminal.TerminalID == 0)
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(104);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
                else
                {
                    // Proceed
                }

                if (terminal.Agent.AgentID == agent.AgentID)
                { }
                else
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(105);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }

                if (terminal.Activated) { }
                else
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(106);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
            }
            else
            {
                //do nothing
            }
            #endregion

            #region Validate Partner's Access To ELoad Service
            PeraLinkCoreWcf.PartnerServiceMapping[] partnerServiceMappingCollection = serviceClient.GetPartnerServiceMappingCollection(partner);

            PeraLinkCoreWcf.PartnerServiceMapping partnerServiceMapping = Array.Find<PeraLinkCoreWcf.PartnerServiceMapping>(partnerServiceMappingCollection
                , delegate(PeraLinkCoreWcf.PartnerServiceMapping eachPartnerServiceMapping)
                {
                    return eachPartnerServiceMapping.ServiceType.ServiceTypeID == ServiceTypeID.ELoad;
                });


            if (partnerServiceMapping == null)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(1);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }

            if (partner.PartnerType.PartnerTypeID == PartnerTypeID.Domestic)
            {
                // Proceed
            }
            else
            {
                PeraLinkCoreWcf.PartnerServicePair[] partnerServicePairCollection = serviceClient.GetPartnerServicePairCollection(partnerServiceMapping);

                PeraLinkCoreWcf.PartnerServicePair domesticPartner = Array.Find<PeraLinkCoreWcf.PartnerServicePair>(partnerServicePairCollection
                    , delegate(PeraLinkCoreWcf.PartnerServicePair eachPartnerServicePair)
                    {
                        if (eachPartnerServicePair.PairedPartner == null)
                        {
                            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(2);
                            throw new RDFramework.ClientException(message.Content, message.MessageID);
                        }
                        else
                        {
                            return eachPartnerServicePair.PairedPartner.PartnerType.PartnerTypeID == PartnerTypeID.Domestic;
                        }
                    });

                if (domesticPartner == null)
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(3);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
                else
                {
                    // Proceed
                }
            }
            #endregion

            PeraLinkCoreWcf.AuditTrail auditTrail = new PeraLinkCoreWcf.AuditTrail();

            ELoadWSClient eLoadServiceClient = new ELoadWSClient();
            PeraLinkELoadGatewayWcf.GetELoadTransactionResult getELoadTransactionResult = new PeraLinkELoadGatewayWcf.GetELoadTransactionResult();
            PeraLinkELoadGatewayWcf.GetELoadTransactionRequest getELoadTransactionRequest = new PeraLinkELoadGatewayWcf.GetELoadTransactionRequest();
            getELoadTransactionRequest.ReferenceNumber = this.ReferenceNumber;
            getELoadTransactionResult = eLoadServiceClient.GetELoadTransaction(getELoadTransactionRequest);
            auditTrail.ValueBeforeUpdate = RDFramework.Utility.Conversion.SerializeToXml<PeraLinkELoadGatewayWcf.ELoadTransaction>(getELoadTransactionResult.ELoadTransaction);

            try
            {
                PeraLinkELoadGatewayWcf.CheckELoadTransactionRequest checkELoadTransactionRequest = new PeraLinkELoadGatewayWcf.CheckELoadTransactionRequest();
                checkELoadTransactionRequest.ReferenceNumber = this.ReferenceNumber;
                checkELoadTransactionResult = eLoadServiceClient.CheckEloadTransaction(checkELoadTransactionRequest);

                //filter by partner
                PeraLinkCoreWcf.Terminal checkELoadTransactionResultTerminal = new PeraLinkCoreWcf.Terminal();
                PeraLinkCoreWcf.Agent checkELoadTransactionResultAgent = new PeraLinkCoreWcf.Agent();
                PeraLinkCoreWcf.Partner checkELoadTransactionResultPartner = new PeraLinkCoreWcf.Partner();
                checkELoadTransactionResultPartner = checkELoadTransactionResultAgent.Partner;
                checkELoadTransactionResultAgent.AgentID = checkELoadTransactionResult.ELoadTransaction.LoadedByAgentID;
                checkELoadTransactionResultAgent = serviceClient.GetAgent(checkELoadTransactionResultAgent);
                checkELoadTransactionResultTerminal.TerminalID = checkELoadTransactionResult.ELoadTransaction.LoadedByTerminalID;
                if (checkELoadTransactionResult.ELoadTransaction.LoadedByTerminalID != 0)
                {
                    checkELoadTransactionResultTerminal = serviceClient.GetTerminal(checkELoadTransactionResultTerminal);
                }
                checkELoadTransactionResultPartner = checkELoadTransactionResultAgent.Partner;


                if (checkELoadTransactionResultPartner.PartnerID == partner.PartnerID && (checkELoadTransactionResultAgent.AgentCode == agent.AgentCode || this.AllowSearchAll == true))
                {
                    returnValue.ELoadTransaction = new ELoadTransaction();
                    returnValue.ELoadTransaction.Load(checkELoadTransactionResult.ELoadTransaction);
                    returnValue.ELoadStatus = new ELoadStatus();
                    returnValue.ELoadStatus.Load(checkELoadTransactionResult.ELoadTransaction.ELoadStatus);
                    returnValue.Telco = new Telco();
                    returnValue.Telco.Load(checkELoadTransactionResult.ELoadTransaction.TelcoProduct.Telco);
                    returnValue.TelcoProduct = new TelcoProduct();
                    returnValue.TelcoProduct.Load(checkELoadTransactionResult.ELoadTransaction.TelcoProduct);
                    returnValue.Agent = new Agent();
                    returnValue.Agent.Load(checkELoadTransactionResultAgent);
                    if (checkELoadTransactionResult.ELoadTransaction.LoadedByTerminalID != 0)
                    {
                        returnValue.Terminal = new Terminal();
                        returnValue.Terminal.Load(checkELoadTransactionResultTerminal);
                    }
                    if (checkELoadTransactionResult.ForReversal && isEmoney)
                    {
                        #region E-Money
                        PeraLinkCoreWcf.EMoneyProfile eMoneyProfile = new PeraLinkCoreWcf.EMoneyProfile();
                        if (partner.SourceOfFundID == SourceOfFundID.EMoney)
                        {

                            #region Get E-Money Profile
                            PeraLinkCoreWcf.GetEMoneyProfileRequest request = new PeraLinkCoreWcf.GetEMoneyProfileRequest();
                            request.Partner = partner;

                            eMoneyProfile = serviceClient.GetEMoneyProfile(request).EMoneyProfile;

                            if (string.IsNullOrWhiteSpace(eMoneyProfile.AccountNumber))
                            {
                                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(2);
                                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                            }

                            if (string.IsNullOrWhiteSpace(eMoneyProfile.ClientNumber))
                            {
                                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(3);
                                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                            }
                            #endregion
                        }
                        #endregion

                        EMoneyWSClient eMoneyServiceClient = new EMoneyWSClient();
                        List<EMoneyWS.UpdateEMoneyRequest> requestList = new List<EMoneyWS.UpdateEMoneyRequest>();

                        EMoney eMoneyLoadRequest = new EMoney();
                        eMoneyLoadRequest.AccountNumber = eMoneyProfile.AccountNumber; //EMONEY ACCOUNT FROM PARTNER's SETTINGS
                        eMoneyLoadRequest.ControlNumber = returnValue.ELoadTransaction.ReferenceNumber;
                        eMoneyLoadRequest.Remarks = "ELoadReversal";
                        eMoneyLoadRequest.PeraLinkAgentCode = this.AgentCode;

                        eMoneyLoadRequest.CashInAmount = returnValue.ELoadTransaction.Amount;
                        eMoneyLoadRequest.CashOutAmount = 0;
                        requestList.Add(eMoneyLoadRequest.Principal());

                        EMoneyWS.UpdateEMoneyResult result = eMoneyServiceClient.UpdateEMoney(requestList);

                        switch (result.ResultCode)
                        {
                            case EMoneyWS.ResultCodes.Successful:
                                // do nothing
                                break;
                            case EMoneyWS.ResultCodes.Failed:
                            case EMoneyWS.ResultCodes.Error:
                                throw new RDFramework.ClientException(result.ResultMessage);
                        }
                    }

                    #region Audit Trail
                    auditTrail.CreatedByAgent = agent;
                    auditTrail.CreatedByUserID = this.UserID;
                    auditTrail.DateTimeCreated = DateTime.Now;
                    auditTrail.IPAddress = SystemUtility.GetClientIPAddress();

                    PeraLinkCoreWcf.CheckELoadTransactionResult LoadTransactionResult = new PeraLinkCoreWcf.CheckELoadTransactionResult();
                    PeraLinkCoreWcf.CheckELoadTransactionRequest LoadTransactionRequest = new PeraLinkCoreWcf.CheckELoadTransactionRequest();
                    LoadTransactionRequest.ELoadTransaction = new PeraLinkCoreWcf.ELoadTransaction();
                    LoadTransactionRequest.ELoadTransaction.ReferenceNumber = this.ReferenceNumber;
                    LoadTransactionRequest.ELoadTransaction.Amount = checkELoadTransactionResult.ELoadTransaction.Amount;
                    LoadTransactionRequest.ELoadTransaction.BranchCode = SystemSetting.EMoneyBranchCode;
                    LoadTransactionRequest.ELoadTransaction.Destination = checkELoadTransactionResult.ELoadTransaction.Destination;
                    LoadTransactionRequest.ELoadTransaction.LoadedByAgentID = agent.AgentID;
                    LoadTransactionRequest.ELoadTransaction.LoadedByUserID = this.UserID;
                    LoadTransactionRequest.ELoadTransaction.LoadedByTerminalID = terminal.TerminalID;
                    LoadTransactionRequest.ELoadTransaction.SessionID = checkELoadTransactionResult.ELoadTransaction.SessionID;

                    LoadTransactionRequest.ELoadTransaction.ELoadStatus = new PeraLinkCoreWcf.ELoadStatus();
                    LoadTransactionRequest.ELoadTransaction.ELoadStatus.InternalStatus = checkELoadTransactionResult.ELoadTransaction.ELoadStatus.InternalStatus;
                    LoadTransactionRequest.ELoadTransaction.ELoadStatus.InternalStatusCode = checkELoadTransactionResult.ELoadTransaction.ELoadStatus.InternalStatusCode;
                    LoadTransactionRequest.ELoadTransaction.ELoadStatus.PartnerResponseCode = checkELoadTransactionResult.ELoadTransaction.ELoadStatus.PartnerResponseCode;
                    LoadTransactionRequest.ELoadTransaction.ELoadStatus.PartnerStatus = checkELoadTransactionResult.ELoadTransaction.ELoadStatus.PartnerStatus;
                    LoadTransactionRequest.ELoadTransaction.ELoadStatus.PartnerStatusCode = checkELoadTransactionResult.ELoadTransaction.ELoadStatus.PartnerStatusCode;

                    LoadTransactionRequest.ELoadTransaction.TelcoProduct = new PeraLinkCoreWcf.TelcoProduct();
                    LoadTransactionRequest.ELoadTransaction.TelcoProduct.MaxAmount = checkELoadTransactionResult.ELoadTransaction.TelcoProduct.MaxAmount;
                    LoadTransactionRequest.ELoadTransaction.TelcoProduct.MinAmount = checkELoadTransactionResult.ELoadTransaction.TelcoProduct.MinAmount;
                    LoadTransactionRequest.ELoadTransaction.TelcoProduct.ProductCode = checkELoadTransactionResult.ELoadTransaction.TelcoProduct.ProductCode;
                    LoadTransactionRequest.ELoadTransaction.TelcoProduct.ProductDescription = checkELoadTransactionResult.ELoadTransaction.TelcoProduct.ProductDescription;
                    LoadTransactionRequest.ELoadTransaction.TelcoProduct.ReqAmount = checkELoadTransactionResult.ELoadTransaction.TelcoProduct.ReqAmount;

                    LoadTransactionRequest.ELoadTransaction.TelcoProduct.Telco = new PeraLinkCoreWcf.Telco();
                    LoadTransactionRequest.ELoadTransaction.TelcoProduct.Telco.IsActivated = checkELoadTransactionResult.ELoadTransaction.TelcoProduct.Telco.IsActivated;
                    LoadTransactionRequest.ELoadTransaction.TelcoProduct.Telco.TelcoCode = checkELoadTransactionResult.ELoadTransaction.TelcoProduct.Telco.TelcoCode;
                    LoadTransactionRequest.ELoadTransaction.TelcoProduct.Telco.TelcoName = checkELoadTransactionResult.ELoadTransaction.TelcoProduct.Telco.TelcoName;
                    LoadTransactionRequest.AuditTrail = auditTrail;

                    LoadTransactionResult = serviceClient.CheckELoadTransaction(LoadTransactionRequest);
                    #endregion
                }
                else
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(114);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Invalid reference number."))
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(114);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
                else
                {
                    throw new RDFramework.ClientException(ex.Message);
                }
            }
        }

        returnValue.ResultStatus = ResultStatus.Successful;

        return returnValue;
    }
    #endregion
}