using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class FindClientByNameResult: BaseResult
{
    #region Constructor
    public FindClientByNameResult()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private ClientCollection _clientCollection;
    [DataMember]
    public ClientCollection ClientCollection
    {
        get { return _clientCollection; }
        set { _clientCollection = value; }
    }

    #endregion
}