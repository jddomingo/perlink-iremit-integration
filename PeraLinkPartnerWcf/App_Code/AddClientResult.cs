using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class AddClientResult: BaseResult
{
    #region Constructor
    public AddClientResult() { }
    #endregion

    #region Fields/Properties
    private Int64 _clientID;

    [DataMember]
    public Int64 ClientID
    {
        get { return _clientID; }
        set { _clientID = value; }
    }
    private string _clientNumber;

    [DataMember]
    public string ClientNumber
    {
        get { return _clientNumber; }
        set { _clientNumber = value; }
    }
    #endregion
}