#define CONVERTION_TO_BUSINESS_ACCOUNT
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using AppCryptor;

/// <summary>
/// Summary description for SendDomesticRemittanceRequest
/// </summary>
[DataContract]
public class SendDomesticRemittanceRequest : BaseRequest
{
	#region Constructor
	public SendDomesticRemittanceRequest()
	{
		//

		//
	}
	#endregion

	#region Fields/Properties
	private Int64 _beneficiaryID;
	[DataMember]
	public Int64 BeneficiaryID
	{
		get { return _beneficiaryID; }
		set { _beneficiaryID = value; }
	}

	private int _sendCurrencyID;
	[DataMember]
	public int SendCurrencyID
	{
		get { return _sendCurrencyID; }
		set { _sendCurrencyID = value; }
	}

	private decimal _principalAmount;
	[DataMember]
	public decimal PrincipalAmount
	{
		get { return _principalAmount; }
		set { _principalAmount = value; }
	}

	private decimal _serviceFee;
	[DataMember]
	public decimal ServiceFee
	{
		get { return _serviceFee; }
		set { _serviceFee = value; }
	}

	private string _partnerCode;
	[DataMember]
	public string PartnerCode
	{
		get { return _partnerCode; }
		set { _partnerCode = value; }
	}

	private string _token;
	[DataMember]
	public string Token
	{
		get { return _token; }
		set { _token = value; }
	}

	private string _agentCode;
	[DataMember]
	public string AgentCode
	{
		get { return _agentCode; }
		set { _agentCode = value; }
	}

	private string _userID;
	[DataMember]
	public string UserID
	{
		get { return _userID; }
		set { _userID = value; }
	}

	private string _pin;
	[DataMember]
	public string Pin
	{
		get { return _pin; }
		set { _pin = value; }
	}

	private string _terminalCode;
	[DataMember]
	public string TerminalCode
	{
		get { return _terminalCode; }
		set { _terminalCode = value; }
	}

	#endregion

	#region Internal Methods
	internal SendDomesticRemittanceResult Process()
	{
		SendDomesticRemittanceResult returnValue = new SendDomesticRemittanceResult();
		bool isEmoney = false;
        decimal commissionFee = 0;
		// Important: This function validates the request credential
		this.AuthenticateRequest();

		// Retrieve agent information
		using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
		{
			#region Validate Partner Record
			PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
			partner.PartnerCode = this.PartnerCode;
			partner = serviceClient.FindPartnerCode(partner);

			if (partner.PartnerID == 0)
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			else { }

			if (partner.Token == this.Token) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}

			if (partner.Activated) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}

			if (partner.SourceOfFundID == SystemSetting.EMoneySourceOfFundID)
			{
				isEmoney = true;
			}
			#endregion

			#region Validate Agent Record
			PeraLinkCoreWcf.Agent agent = new PeraLinkCoreWcf.Agent();
			agent.AgentCode = this.AgentCode;
			agent.Partner = partner;
			agent = serviceClient.FindAgentCodeOfPartner(agent);

			if (agent.AgentID == 0)
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			else
			{
				// Proceed
			}

			if (agent.Partner.PartnerID == partner.PartnerID)
			{ }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(43);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}

			if (agent.Activated) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(46);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			#endregion

			#region Validate Terminal Record
			PeraLinkCoreWcf.Terminal terminal = new PeraLinkCoreWcf.Terminal();
			
			if (serviceClient.GetAgentTerminalCollection(agent).Length > 0)
			{
				terminal.TerminalCode = this.TerminalCode;
				terminal.Agent = agent;
				terminal.Agent.Partner = partner;
				terminal = serviceClient.FindTerminal(terminal);

				if (terminal.TerminalID == 0)
				{
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(104);
					throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
				}
				else
				{
					// Proceed
				}

				if (terminal.Agent.AgentID == agent.AgentID)
				{ }
				else
				{
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(105);
					throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
				}

				if (terminal.Activated) { }
				else
				{
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(106);
					throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
				}
			}
			else
			{
				//do nothing
			}
			#endregion

			#region Validate Partner's Access To Send Domestic Remittance Service
			PeraLinkCoreWcf.PartnerServiceMapping[] partnerServiceMappingCollection = serviceClient.GetPartnerServiceMappingCollection(partner);

			PeraLinkCoreWcf.PartnerServiceMapping partnerServiceMapping = Array.Find<PeraLinkCoreWcf.PartnerServiceMapping>(partnerServiceMappingCollection
				, delegate(PeraLinkCoreWcf.PartnerServiceMapping eachPartnerServiceMapping)
				{
					return eachPartnerServiceMapping.ServiceType.ServiceTypeID == ServiceTypeID.SendRemittance;
				});


			if (partnerServiceMapping == null)
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(1);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			else
			{
				// Proceed
			}

			if (partner.PartnerType.PartnerTypeID == PartnerTypeID.Domestic)
			{
				// Proceed
			}
			else
			{
				PeraLinkCoreWcf.PartnerServicePair[] partnerServicePairCollection = serviceClient.GetPartnerServicePairCollection(partnerServiceMapping);

				PeraLinkCoreWcf.PartnerServicePair domesticPartner = Array.Find<PeraLinkCoreWcf.PartnerServicePair>(partnerServicePairCollection
					, delegate(PeraLinkCoreWcf.PartnerServicePair eachPartnerServicePair)
					{
						if (eachPartnerServicePair.PairedPartner == null)
						{
							PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(2);
							throw new RDFramework.ClientException(message.Content, message.MessageID);
						}
						else
						{
							return eachPartnerServicePair.PairedPartner.PartnerType.PartnerTypeID == PartnerTypeID.Domestic;
						}
					});

				if (domesticPartner == null)
				{
					PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(3);
					throw new RDFramework.ClientException(message.Content, message.MessageID);
				}
				else
				{
					// Proceed
				}
			}
			#endregion

			#region Validate Capping
			if (partner.SendCapAmount > 0)
			{
				PeraLinkCoreWcf.GetPartnerTotalSendAmountRequest getPartnerTotalSendAmountRequest = new PeraLinkCoreWcf.GetPartnerTotalSendAmountRequest();
				getPartnerTotalSendAmountRequest.Partner = partner;
				getPartnerTotalSendAmountRequest.StartDate = DateTime.Today;
				getPartnerTotalSendAmountRequest.EndDate = DateTime.Today.AddDays(1);

				PeraLinkCoreWcf.GetPartnerTotalSendAmountResult getPartnerTotalSendAmountResult = new PeraLinkCoreWcf.GetPartnerTotalSendAmountResult();
				getPartnerTotalSendAmountResult = serviceClient.GetPartnerTotalSendAmount(getPartnerTotalSendAmountRequest);

				decimal totalAmount = this.PrincipalAmount + getPartnerTotalSendAmountResult.SendTotalAmount;

				if (partner.SendCapAmount < totalAmount)
				{
					decimal remainingAmount = partner.SendCapAmount - getPartnerTotalSendAmountResult.SendTotalAmount;
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(5);
					throw new RDFramework.ClientException(string.Format(message.Description, remainingAmount), message.SystemMessageID);
				}
				else
				{
					/* Do nothing*/
				}
			}

			if (agent.CapAmount > 0)
			{
				PeraLinkCoreWcf.GetAgentTotalTransactionAmountRequest getAgentTotalTransactionAmountRequest = new PeraLinkCoreWcf.GetAgentTotalTransactionAmountRequest();
				getAgentTotalTransactionAmountRequest.Agent = agent;
				getAgentTotalTransactionAmountRequest.StartDate = DateTime.Today;
				getAgentTotalTransactionAmountRequest.EndDate = DateTime.Today.AddDays(1);

				PeraLinkCoreWcf.GetAgentTotalTransactionAmountResult getAgentTotalTransactionAmountResult = new PeraLinkCoreWcf.GetAgentTotalTransactionAmountResult();
				getAgentTotalTransactionAmountResult = serviceClient.GetAgentTotalTransactionAmount(getAgentTotalTransactionAmountRequest);

				decimal totalAmount = this.PrincipalAmount + getAgentTotalTransactionAmountResult.TotalTransactionAmount;

				if (agent.CapAmount < totalAmount)
				{
					decimal remainingAmount = agent.CapAmount - getAgentTotalTransactionAmountResult.TotalTransactionAmount;
					remainingAmount = (remainingAmount < 0) ? 0 : remainingAmount;
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(102);
					throw new RDFramework.ClientException(string.Format(message.Description, remainingAmount), message.SystemMessageID);
				}
			}
			#endregion


			PeraLinkCoreWcf.EMoneyProfile eMoneyProfile = new PeraLinkCoreWcf.EMoneyProfile();
			if (partner.SourceOfFundID == SourceOfFundID.EMoney)
			{
				#region Get E-Money Profile
				PeraLinkCoreWcf.GetEMoneyProfileRequest request = new PeraLinkCoreWcf.GetEMoneyProfileRequest();
				request.Partner = partner;

				eMoneyProfile = serviceClient.GetEMoneyProfile(request).EMoneyProfile;

				if (string.IsNullOrWhiteSpace(eMoneyProfile.AccountNumber))
				{
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(2);
					throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
				}

				if (string.IsNullOrWhiteSpace(eMoneyProfile.ClientNumber))
				{
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(3);
					throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
				}

				#endregion

				#region Validate PIN
        //do nothing

				#endregion

				#region Validate Balance
				EMoneyWSClient eMoneyServiceClient = new EMoneyWSClient();

#if CONVERTION_TO_BUSINESS_ACCOUNT
                decimal balance = eMoneyServiceClient.EMoneyInquiry(eMoneyProfile.AccountNumber).AvailableBalance;
#else
                decimal balance = eMoneyServiceClient.RetrieveAccountDetails(eMoneyProfile.ClientNumber).DcAvailableBalance;
#endif

				if ((balance - this.PrincipalAmount) < 0)
				{
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(6);
					throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
				}
				#endregion
			}


			#region Validate Partner Service Fee
			PeraLinkCoreWcf.DomesticRemittance domesticRemittance = new PeraLinkCoreWcf.DomesticRemittance();

			GetSendServiceFeeRequest feeRequest = new GetSendServiceFeeRequest();
			feeRequest.AgentCode = this.AgentCode;
			feeRequest.PartnerCode = this.PartnerCode;
			feeRequest.Token = this.Token;
			feeRequest.CurrencyID = this.SendCurrencyID;
			feeRequest.ServiceCredential = this.ServiceCredential;
			feeRequest.PrincipalAmount = this.PrincipalAmount;

			GetSendServiceFeeResult feeResult = new GetSendServiceFeeResult();

			try
			{
				feeResult = feeRequest.Process();
				domesticRemittance.ServiceFee = feeResult.ServiceFee;
			}
			catch (RDFramework.ClientException clientEx)
			{
				if (clientEx.MessageID == SystemResource.GetMessageWithListID(28).MessageID)
				{
					domesticRemittance.ServiceFee = this.ServiceFee;
				}
				else
				{
					throw;
				}
			}
			#endregion

			PeraLinkCoreWcf.Beneficiary beneficiary = new PeraLinkCoreWcf.Beneficiary();
			beneficiary.BeneficiaryID = this.BeneficiaryID;
			beneficiary = serviceClient.GetBeneficiary(beneficiary);
			domesticRemittance.Beneficiary = beneficiary;

			domesticRemittance.DateTimeSent = DateTime.Now;
			domesticRemittance.PrincipalAmount = this.PrincipalAmount;
			domesticRemittance.SendCurrency = new PeraLinkCoreWcf.Currency();
			domesticRemittance.SendCurrency.CurrencyID = this.SendCurrencyID;

			domesticRemittance.SenderClient = serviceClient.FindClientByBeneficiaryID(beneficiary);

			if (domesticRemittance.SenderClient.ClientID == 0)
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(12);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			else
			{
				// Proceed
			}



			agent.Partner = partner;
			domesticRemittance.SentByAgent = agent;
			domesticRemittance.SentByUserID = this.UserID;
			domesticRemittance.SentByTerminal = terminal;

			PeraLinkCoreWcf.SendDomesticRemittanceRequest sendDomesticRemittanceRequest = new PeraLinkCoreWcf.SendDomesticRemittanceRequest();
			sendDomesticRemittanceRequest.DomesticRemittance = domesticRemittance;
			sendDomesticRemittanceRequest.UseAssignedControlNumber = false;

			returnValue.ControlNumber = serviceClient.SendDomesticRemittance(sendDomesticRemittanceRequest).ControlNumber;


            
            if (isEmoney)
			{
                #region Commission Fee
                GetCommissionFeeRequest getCommissionFeeRequest = new GetCommissionFeeRequest();
                getCommissionFeeRequest.PartnerCode = this.PartnerCode;
                getCommissionFeeRequest.CurrencyID = this.SendCurrencyID;
                getCommissionFeeRequest.ServiceTypeID = ServiceType.SendRemittance;
                getCommissionFeeRequest.PrincipalAmount = this.PrincipalAmount;
                getCommissionFeeRequest.ServiceFee = this.ServiceFee;


                GetCommissionFeeResult getCommissionFeeResult = new GetCommissionFeeResult();

                getCommissionFeeResult = getCommissionFeeRequest.Process();
                commissionFee = getCommissionFeeResult.CommissionFee;
                #endregion

                #region Debit E-Money
                EMoneyWSClient eMoneyServiceClient = new EMoneyWSClient();
				EMoneyWS.UpdateEMoneyRequest request = new EMoneyWS.UpdateEMoneyRequest();

				List<EMoneyWS.UpdateEMoneyRequest> requestList = new List<EMoneyWS.UpdateEMoneyRequest>();

				EMoney eMoneyRequest = new EMoney();
				eMoneyRequest.AccountNumber = eMoneyProfile.AccountNumber; //EMONEY ACCOUNT FROM PARTNER's SETTINGS
				eMoneyRequest.ControlNumber = returnValue.ControlNumber;
				eMoneyRequest.Remarks = "Send";
				eMoneyRequest.PeraLinkAgentCode = this.AgentCode;

				eMoneyRequest.CashInAmount = 0;
				eMoneyRequest.CashOutAmount = this.PrincipalAmount;
				requestList.Add(eMoneyRequest.Principal());

				eMoneyRequest.CashInAmount = 0;
				eMoneyRequest.CashOutAmount = domesticRemittance.ServiceFee;
				requestList.Add(eMoneyRequest.ServiceCharge());
                eMoneyRequest.CashInAmount = commissionFee;
                eMoneyRequest.CashOutAmount = 0;
                requestList.Add(eMoneyRequest.CommissionFee());
				EMoneyWS.UpdateEMoneyResult result = eMoneyServiceClient.UpdateEMoney(requestList);

				switch (result.ResultCode)
				{
					case EMoneyWS.ResultCodes.Successful:
						// do nothing
						break;
					case EMoneyWS.ResultCodes.Failed:
					case EMoneyWS.ResultCodes.Error:
						{
							FindDomesticRemittanceRequest findDomesticRemittanceRequest = new FindDomesticRemittanceRequest();
							findDomesticRemittanceRequest.AgentCode = this.AgentCode;
							findDomesticRemittanceRequest.ControlNumber = returnValue.ControlNumber;
							findDomesticRemittanceRequest.PartnerCode = this.PartnerCode;
							findDomesticRemittanceRequest.ServiceCredential = this.ServiceCredential;
							findDomesticRemittanceRequest.Token = this.Token;
							findDomesticRemittanceRequest.UserID = this.UserID;
							FindDomesticRemittanceResult findDomesticRemittanceResult = findDomesticRemittanceRequest.Process();

							PeraLinkCoreWcf.DomesticRemittance domesticRemittanceToCancel = new PeraLinkCoreWcf.DomesticRemittance();
							domesticRemittanceToCancel.ControlNumber = findDomesticRemittanceResult.ControlNumber;
							domesticRemittanceToCancel.DomesticRemittanceID = findDomesticRemittanceResult.DomesticRemittanceID;
							domesticRemittanceToCancel.UpdatedByAgent = new PeraLinkCoreWcf.Agent();
							domesticRemittanceToCancel.UpdatedByAgent.AgentID = findDomesticRemittanceResult.SentByAgentID;
							domesticRemittanceToCancel.UpdatedByAgent.AgentCode = findDomesticRemittanceResult.SentByAgentCode;
							domesticRemittanceToCancel.UpdatedByUserID = "SYSTEM";
							domesticRemittanceToCancel.DateTimeUpdated = DateTime.Now;

							domesticRemittanceToCancel.Deleted = true;

							domesticRemittanceToCancel.DomesticCancelDetail = new PeraLinkCoreWcf.DomesticCancelDetail();
							domesticRemittanceToCancel.DomesticCancelDetail.CreatedBy = domesticRemittanceToCancel.UpdatedByUserID;
							domesticRemittanceToCancel.DomesticCancelDetail.DateTimeCreated = domesticRemittanceToCancel.DateTimeUpdated;
							domesticRemittanceToCancel.DomesticCancelDetail.DomesticRemittanceID = domesticRemittanceToCancel.DomesticRemittanceID;
							domesticRemittanceToCancel.DomesticCancelDetail.Reason = string.Format("E-Money: {0}", result.ResultMessageCode);

							serviceClient.CancelAndDeleteDomesticRemittance(domesticRemittanceToCancel);

							throw new RDFramework.ClientException(result.ResultMessage);
						}
                }
                #endregion

                #region Insert Total Commission
                AddCommissionTransactionRequest addCommissionTransactionRequest = new AddCommissionTransactionRequest();
                addCommissionTransactionRequest.ISControlNo = returnValue.ControlNumber;
                addCommissionTransactionRequest.PartnerID = partner.PartnerID;

                AddCommissionTransactionResult addCommissionTransactionResult = new AddCommissionTransactionResult();
                addCommissionTransactionResult = addCommissionTransactionRequest.Process();
                #endregion

            }
            
        }

		returnValue.ResultStatus = ResultStatus.Successful;

		return returnValue;
	}
	#endregion
}