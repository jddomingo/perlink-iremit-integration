using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetStateCollectionByCountryRequest: BaseRequest
{
    #region Constructor
    public GetStateCollectionByCountryRequest()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion

    #region Fields/Properties
    private int _stateCountryID;
    [DataMember]
    public int StateCountryID
    {
        get { return _stateCountryID; }
        set { _stateCountryID = value; }
    }
    #endregion

    #region Internal Methods
    internal GetStateCollectionByCountryResult Process()
    {
        GetStateCollectionByCountryResult returnValue = new GetStateCollectionByCountryResult();

        // Important: This function validates the request credential
        this.AuthenticateRequest();

        PeraLinkCoreWcf.State stateCountry = new PeraLinkCoreWcf.State();
        stateCountry.CountryID = this.StateCountryID;

        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
            returnValue.StateCollectionByCountry = new StateCollectionByCountry();
            returnValue.StateCollectionByCountry.AddList(serviceClient.GetStateCollectionByCountry(stateCountry));
        }

        returnValue.ResultStatus = ResultStatus.Successful;

        return returnValue;
    }
    #endregion
}