#define CONVERTION_TO_BUSINESS_ACCOUNT
#define REPLACE_COMMISSION_FEE_BY_PAIRING_COMMISSION_FEE

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

/// <summary>
/// Summary description for PayBillRequest
/// </summary>
public class PayBillRequest : BaseRequest
{
    #region Constructor
    public PayBillRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion

    #region Fields/Properties
    private string _requestParam;
    [DataMember]
    public string RequestParam
    {
        get { return _requestParam; }
        set { _requestParam = value; }
    }

    private string _partnerCode;
    [DataMember]
    public string PartnerCode
    {
        get { return _partnerCode; }
        set { _partnerCode = value; }
    }

    private string _token;
    [DataMember]
    public string Token
    {
        get { return _token; }
        set { _token = value; }
    }

    private string _agentCode;
    [DataMember]
    public string AgentCode
    {
        get { return _agentCode; }
        set { _agentCode = value; }
    }

    private string _terminalCode;
    [DataMember]
    public string TerminalCode
    {
        get { return _terminalCode; }
        set { _terminalCode = value; }
    }

    private string _userID;
    [DataMember]
    public string UserID
    {
        get { return _userID; }
        set { _userID = value; }
    }

    private string _clientMobileNumber;
    [DataMember]
    public string ClientMobileNumber
    {
        get { return _clientMobileNumber; }
        set { _clientMobileNumber = value; }
    }
    #endregion

    #region Internal Methods
    public PayBillResult Process()
    {
        PayBillResult returnValue = new PayBillResult();

        decimal commissionFee = 0;

        // Important: This function validates the request credential
        this.AuthenticateRequest();

        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
            #region Validate Partner Record
            PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
            partner.PartnerCode = this.PartnerCode;
            partner = serviceClient.FindPartnerCode(partner);

            if (partner.PartnerID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else { }

            if (partner.Token == this.Token) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (partner.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            #endregion

            #region Validate Agent Record
            PeraLinkCoreWcf.Agent agent = new PeraLinkCoreWcf.Agent();
            agent.AgentCode = this.AgentCode;
            agent.Partner = partner;
            agent = serviceClient.FindAgentCodeOfPartner(agent);

            if (agent.AgentID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }

            if (agent.Partner.PartnerID == partner.PartnerID)
            { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(43);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (agent.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(46);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            #endregion

            #region Validate Terminal Record
            PeraLinkCoreWcf.Terminal terminal = new PeraLinkCoreWcf.Terminal();

            if (serviceClient.GetAgentTerminalCollection(agent).Length > 0)
            {
                terminal.TerminalCode = this.TerminalCode;
                terminal.Agent = agent;
                terminal.Agent.Partner = partner;
                terminal = serviceClient.FindTerminal(terminal);

                if (terminal.TerminalID == 0)
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(104);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
                else
                {
                    // Proceed
                }

                if (terminal.Agent.AgentID == agent.AgentID)
                { }
                else
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(105);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }

                if (terminal.Activated) { }
                else
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(106);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
            }
            else
            {
                //do nothing
            }
            #endregion

            #region Validate User ID
            if (string.IsNullOrWhiteSpace(this.UserID))
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(130);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
            #endregion

            #region Validate Request Param
            if (string.IsNullOrWhiteSpace(this.RequestParam))
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(129);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
            #endregion

            #region Validate Mobile Number
            if (string.IsNullOrWhiteSpace(this.ClientMobileNumber))
            {
                //do nothing
            }
            else
            {
                if (RDFramework.Utility.Validation.IsNumeric(this.ClientMobileNumber))
                {
                    //do nothing
                }
                else
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(134);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
            }
            #endregion

            #region Get Biller Code & Bill Amount
            decimal billAmount;
            string billerCode;
            JObject jsonObject;

            try
            {
                jsonObject = JObject.Parse(this.RequestParam);
                billAmount = jsonObject.Value<decimal>("amount");
                billerCode = jsonObject.Value<string>("billercode");
                
                
            }
            catch (Exception error)
            {
                RDFramework.Utility.EventLog.SaveError(string.Format("Request Param: {0} \nError: {1}", this.RequestParam, error.ToString()));

                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(129);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
            #endregion

            #region Validate Partner's Access To BillsPayment Service
            PeraLinkCoreWcf.PartnerServiceMapping[] partnerServiceMappingCollection = serviceClient.GetPartnerServiceMappingCollection(partner);

            PeraLinkCoreWcf.PartnerServiceMapping partnerServiceMapping = Array.Find<PeraLinkCoreWcf.PartnerServiceMapping>(partnerServiceMappingCollection
                , delegate(PeraLinkCoreWcf.PartnerServiceMapping eachPartnerServiceMapping)
                {
                    return eachPartnerServiceMapping.ServiceType.ServiceTypeID == ServiceTypeID.BillsPayment;
                });


            if (partnerServiceMapping == null)
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(128);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
            else
            {
                // Proceed
            }
            #endregion

            #region Validate Partner's Access to Biller
            PeraLinkCoreWcf.PartnerServicePair[] partnerServicePairCollection = serviceClient.GetPartnerServicePairCollection(partnerServiceMapping);

            PeraLinkCoreWcf.PartnerServicePair pairing = Array.Find<PeraLinkCoreWcf.PartnerServicePair>(partnerServicePairCollection
                    , delegate(PeraLinkCoreWcf.PartnerServicePair eachPartnerServicePair)
                    {
                        if (eachPartnerServicePair.PairedPartner == null)
                        {
                            PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(131);
                            throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                        }
                        else
                        {
                            return eachPartnerServicePair.PairedPartner.PartnerCode == billerCode;
                        }
                    });

            if (pairing == null)
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(131);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
            else
            {
                //Proceed
            }

            if (pairing.PairedPartner.Activated)
            { }
            else
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(132);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
            #endregion

            #region Validate Funding
            PeraLinkCoreWcf.EMoneyProfile eMoneyProfile = new PeraLinkCoreWcf.EMoneyProfile();
            if (partner.SourceOfFundID == SourceOfFundID.EMoney)
            {
                #region Get E-Money Profile
                PeraLinkCoreWcf.GetEMoneyProfileRequest request = new PeraLinkCoreWcf.GetEMoneyProfileRequest();
                request.Partner = partner;

                eMoneyProfile = serviceClient.GetEMoneyProfile(request).EMoneyProfile;

                if (string.IsNullOrWhiteSpace(eMoneyProfile.AccountNumber))
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(2);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }

                if (string.IsNullOrWhiteSpace(eMoneyProfile.ClientNumber))
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(3);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }

                #region Validate Balance
                EMoneyWSClient eMoneyServiceClient = new EMoneyWSClient();
#if CONVERTION_TO_BUSINESS_ACCOUNT
                decimal balance = eMoneyServiceClient.EMoneyInquiry(eMoneyProfile.AccountNumber).AvailableBalance;
#else
                decimal balance = eMoneyServiceClient.RetrieveAccountDetails(eMoneyProfile.ClientNumber).DcAvailableBalance;
#endif

                if ((balance - billAmount) < 0)
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(6);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
                #endregion
                #endregion
            }
            #endregion

            #region Get Service Fee
            PeraLinkCoreWcf.GetPartnerPairingServiceFeeRequest feeRequest = new PeraLinkCoreWcf.GetPartnerPairingServiceFeeRequest();
            feeRequest.PartnerPairingServiceFee = new PeraLinkCoreWcf.PartnerPairingServiceFee();
            feeRequest.PartnerPairingServiceFee.Amount = billAmount;
            feeRequest.PartnerPairingServiceFee.PartnerServicePair = pairing;

            PeraLinkCoreWcf.GetPartnerPairingServiceFeeResult feeResult = new PeraLinkCoreWcf.GetPartnerPairingServiceFeeResult();
            feeResult = serviceClient.GetPartnerPairingServiceFee(feeRequest);

            if (feeResult.PartnerPairingServiceFee.FeeType.FeeTypeID == FeeTypeID.Fixed)
            {
                returnValue.ServiceFee = feeResult.PartnerPairingServiceFee.Fee;
            }
            else
            {
                returnValue.ServiceFee = billAmount * (feeResult.PartnerPairingServiceFee.Fee / 100);
            }

            

            JToken convFee; 
            if (jsonObject.TryGetValue("convfee", out convFee))
            {
                dynamic jsonObj = JsonConvert.DeserializeObject(this.RequestParam);
                jsonObj["convfee"] = returnValue.ServiceFee;
                this.RequestParam = JsonConvert.SerializeObject(jsonObj);
            }
            else
            {
                jsonObject.Add("convfee", returnValue.ServiceFee);
                this.RequestParam = JsonConvert.SerializeObject(jsonObject);
            }
           
            #endregion

            #region Insert to Database
            PeraLinkCoreWcf.CreateBillsPaymentRequest createBillsPaymentRequest = new PeraLinkCoreWcf.CreateBillsPaymentRequest();

            createBillsPaymentRequest.AuditTrail = new PeraLinkCoreWcf.AuditTrail();
            createBillsPaymentRequest.AuditTrail.CreatedByAgent = agent;
            createBillsPaymentRequest.AuditTrail.CreatedByUserID = this.UserID;
            createBillsPaymentRequest.AuditTrail.DateTimeCreated = DateTime.Now;
            createBillsPaymentRequest.AuditTrail.IPAddress = SystemUtility.GetClientIPAddress();

            createBillsPaymentRequest.BillsPayment = new PeraLinkCoreWcf.BillsPayment();
            createBillsPaymentRequest.BillsPayment.DateTimeCreated = createBillsPaymentRequest.AuditTrail.DateTimeCreated;
            createBillsPaymentRequest.BillsPayment.ClientMobileNumber = this.ClientMobileNumber;
            createBillsPaymentRequest.BillsPayment.ProcessedByAgent = agent;
            createBillsPaymentRequest.BillsPayment.ProcessedByTerminal = terminal;
            createBillsPaymentRequest.BillsPayment.ProcessedByUserID = this.UserID;
            createBillsPaymentRequest.BillsPayment.RequestParam = this.RequestParam;

            PeraLinkCoreWcf.CreateBillsPaymentResult createBillsPaymentResult = serviceClient.CreateBillsPayment(createBillsPaymentRequest);
            returnValue.ReferenceNumber = createBillsPaymentResult.BillsPayment.ReferenceNumber;
            #endregion

            #region Debit E-Money
            if (partner.SourceOfFundID == SourceOfFundID.EMoney)
            {
#if REPLACE_COMMISSION_FEE_BY_PAIRING_COMMISSION_FEE
                #region Commission Fee
                PeraLinkCoreWcf.GetPartnerPairingCommissionFeeRequest pairingfeeRequest = new PeraLinkCoreWcf.GetPartnerPairingCommissionFeeRequest();
                pairingfeeRequest.PartnerPairingCommissionFee = new PeraLinkCoreWcf.PartnerPairingCommissionFee();
                pairingfeeRequest.PartnerPairingCommissionFee.Amount = billAmount;
                pairingfeeRequest.PartnerPairingCommissionFee.PartnerServicePair = pairing;

                PeraLinkCoreWcf.GetPartnerPairingCommissionFeeResult pairingfeeResult = new PeraLinkCoreWcf.GetPartnerPairingCommissionFeeResult();
                pairingfeeResult = serviceClient.GetPartnerPairingCommissionFee(pairingfeeRequest);

                if (pairingfeeResult.PartnerPairingCommissionFee.PartnerPairingCommissionFeeID == 0)
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(137);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
                else
                {

                    if (pairingfeeResult.PartnerPairingCommissionFee.FeeType.FeeTypeID == FeeTypeID.Fixed)
                    {
                       commissionFee = pairingfeeResult.PartnerPairingCommissionFee.Fee;
                    }
                    else
                    {
                        commissionFee = returnValue.ServiceFee * (pairingfeeResult.PartnerPairingCommissionFee.Fee / 100);             
                    }

                    returnValue.CommissionFee = commissionFee;
                }
                #endregion
#else
                #region Commission Fee
                GetCommissionFeeRequest getCommissionFeeRequest = new GetCommissionFeeRequest();
                getCommissionFeeRequest.PartnerCode = partner.PartnerCode;
                getCommissionFeeRequest.CurrencyID = 6; //PHP
                getCommissionFeeRequest.ServiceTypeID = ServiceType.BillsPayment;
                getCommissionFeeRequest.PrincipalAmount = billAmount;
                getCommissionFeeRequest.ServiceFee = returnValue.ServiceFee;


                GetCommissionFeeResult getCommissionFeeResult = new GetCommissionFeeResult();

                getCommissionFeeResult = getCommissionFeeRequest.Process();
                commissionFee = getCommissionFeeResult.CommissionFee;
                #endregion
#endif

                EMoneyWSClient eMoneyServiceClient = new EMoneyWSClient();
                EMoneyWS.UpdateEMoneyRequest request = new EMoneyWS.UpdateEMoneyRequest();

                List<EMoneyWS.UpdateEMoneyRequest> requestList = new List<EMoneyWS.UpdateEMoneyRequest>();
                EMoney eMoneyRequest = new EMoney();
                eMoneyRequest.AccountNumber = eMoneyProfile.AccountNumber;
                eMoneyRequest.ControlNumber = returnValue.ReferenceNumber;
                eMoneyRequest.Remarks = "BillsPayment";
                eMoneyRequest.PeraLinkAgentCode = this.AgentCode;
                eMoneyRequest.CashInAmount = 0;
                eMoneyRequest.CashOutAmount = billAmount;

                requestList.Add(eMoneyRequest.Principal());

                eMoneyRequest.CashInAmount = 0;
                eMoneyRequest.CashOutAmount = returnValue.ServiceFee; 
                requestList.Add(eMoneyRequest.ServiceCharge());

                eMoneyRequest.CashInAmount = commissionFee;
                eMoneyRequest.CashOutAmount = 0;
                requestList.Add(eMoneyRequest.CommissionFee());

                EMoneyWS.UpdateEMoneyResult result = eMoneyServiceClient.UpdateEMoney(requestList);

                switch (result.ResultCode)
                {
                    case EMoneyWS.ResultCodes.Successful:
                        //do nothing
                        break;
                    case EMoneyWS.ResultCodes.Failed:
                    case EMoneyWS.ResultCodes.Error:
                        {
                             RDFramework.Utility.EventLog.SaveError(string.Format("{0}: {1} {2}",returnValue.ReferenceNumber, result.ResultMessageCode, result.ResultMessage));
                            throw new RDFramework.ClientException("Unsuccessful E-Money debiting");
                        }
                }
            }
            #endregion

            #region Process Bills Payment
            PeraLinkBillsPaymentWcfClient billsPaymentWcfClient = new PeraLinkBillsPaymentWcfClient();
            PeraLinkBillsPaymentWcf.ProcessBillsPaymentRequest processBillsPaymentRequest = new PeraLinkBillsPaymentWcf.ProcessBillsPaymentRequest();
            PeraLinkBillsPaymentWcf.ProcessBillsPaymentResult processBillsPaymentResult = new PeraLinkBillsPaymentWcf.ProcessBillsPaymentResult();
            
            try
            {    
                processBillsPaymentRequest.RequestParam = this.RequestParam;
                processBillsPaymentResult = billsPaymentWcfClient.ProcessBillsPayment(processBillsPaymentRequest);
            }
            catch (Exception error)
            {
                #region E-Money Reversal
                if (partner.SourceOfFundID == SourceOfFundID.EMoney)
                {
                    EMoneyWSClient eMoneyServiceClient = new EMoneyWSClient();
                    EMoneyWS.UpdateEMoneyRequest request = new EMoneyWS.UpdateEMoneyRequest();

                    List<EMoneyWS.UpdateEMoneyRequest> requestList = new List<EMoneyWS.UpdateEMoneyRequest>();
                    EMoney eMoneyRequest = new EMoney();
                    eMoneyRequest.AccountNumber = eMoneyProfile.AccountNumber;
                    eMoneyRequest.ControlNumber = returnValue.ReferenceNumber;
                    eMoneyRequest.Remarks = "BillsPaymentReversal";
                    eMoneyRequest.PeraLinkAgentCode = this.AgentCode;
                    
                    eMoneyRequest.CashInAmount = billAmount;
                    eMoneyRequest.CashOutAmount = 0;
                    requestList.Add(eMoneyRequest.Principal());

                    eMoneyRequest.CashInAmount = returnValue.ServiceFee;
                    eMoneyRequest.CashOutAmount = 0;
                    requestList.Add(eMoneyRequest.ServiceCharge());

                    eMoneyRequest.CashInAmount = 0;
                    eMoneyRequest.CashOutAmount = commissionFee;
                    requestList.Add(eMoneyRequest.CommissionFee());

                    EMoneyWS.UpdateEMoneyResult result = eMoneyServiceClient.UpdateEMoney(requestList);

                    switch (result.ResultCode)
                    {
                        case EMoneyWS.ResultCodes.Successful:
                            //do nothing
                            break;
                        case EMoneyWS.ResultCodes.Failed:
                        case EMoneyWS.ResultCodes.Error:
                            {
                                throw new RDFramework.ClientException(result.ResultMessage);
                            }
                    }
                }
                #endregion

                throw new RDFramework.ClientException(error.Message);
            }
            #endregion

            #region Save to Database
            PeraLinkCoreWcf.SaveBillsPaymentTransactionRequest saveBillsPaymentTransactionRequest = new PeraLinkCoreWcf.SaveBillsPaymentTransactionRequest();

            saveBillsPaymentTransactionRequest.AuditTrail = new PeraLinkCoreWcf.AuditTrail();
            saveBillsPaymentTransactionRequest.AuditTrail.CreatedByAgent = agent;
            saveBillsPaymentTransactionRequest.AuditTrail.CreatedByUserID = this.UserID;
            saveBillsPaymentTransactionRequest.AuditTrail.DateTimeCreated = DateTime.Now;
            saveBillsPaymentTransactionRequest.AuditTrail.IPAddress = SystemUtility.GetClientIPAddress();

            saveBillsPaymentTransactionRequest.BillsPayment = createBillsPaymentResult.BillsPayment;
            saveBillsPaymentTransactionRequest.BillsPayment.ApiTransactionNumber = processBillsPaymentResult.ReferenceNumber;

            PeraLinkCoreWcf.SaveBillsPaymentTransactionResult saveBillsPaymentTransactionResult = serviceClient.SaveBillsPayment(saveBillsPaymentTransactionRequest);

            returnValue.DateTimeProcessed = saveBillsPaymentTransactionRequest.AuditTrail.DateTimeCreated;
            #endregion

            #region Insert Total Commission
            AddCommissionTransactionRequest addCommissionTransactionRequest = new AddCommissionTransactionRequest();
            addCommissionTransactionRequest.ISControlNo = returnValue.ReferenceNumber;
            addCommissionTransactionRequest.PartnerID = partner.PartnerID;

            AddCommissionTransactionResult addCommissionTransactionResult = new AddCommissionTransactionResult();
            addCommissionTransactionResult = addCommissionTransactionRequest.Process();
            #endregion
        }
        returnValue.ResultStatus = ResultStatus.Successful;
        return returnValue;

    }
    #endregion
}