using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Partner
/// </summary>
public class Partner
{
    #region Constructor
    public Partner()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private Int64 _partnerID;

    public Int64 PartnerID
    {
        get { return _partnerID; }
        set { _partnerID = value; }
    }
    private string _partnerCode;

    public string PartnerCode
    {
        get { return _partnerCode; }
        set { _partnerCode = value; }
    }

    private string _name;

    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }

	private int _sourceOfFundID;

	public int SourceOfFundID
	{
		get { return _sourceOfFundID; }
		set { _sourceOfFundID = value; }
	}
    private string _prefix;

    public string Prefix
    {
        get { return _prefix; }
        set { _prefix = value; }
    }  
    #endregion

    #region Public Methods
    public void Load(PeraLinkCoreWcf.Partner partner)
    {
        this.Name = partner.Name;
        this.PartnerCode = partner.PartnerCode;
        this.PartnerID = partner.PartnerID;
		this.SourceOfFundID = partner.SourceOfFundID;
    }
    #endregion
}