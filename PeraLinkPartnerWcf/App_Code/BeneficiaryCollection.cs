using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BeneficiaryCollection
/// </summary>
public class BeneficiaryCollection: List<Beneficiary>
{
    #region Constructor
    public BeneficiaryCollection()
	{
		//

		//
    }
    #endregion

    #region Public Methods
    public void AddList(PeraLinkCoreWcf.Beneficiary[] beneficiaryCollection)
    {
        Array.ForEach<PeraLinkCoreWcf.Beneficiary>(beneficiaryCollection
            , delegate(PeraLinkCoreWcf.Beneficiary eachBeneficiary)
            {
                Beneficiary mappedBeneficiary = new Beneficiary();
                mappedBeneficiary.Load(eachBeneficiary);
                this.Add(mappedBeneficiary);
            });
    }
    #endregion
}