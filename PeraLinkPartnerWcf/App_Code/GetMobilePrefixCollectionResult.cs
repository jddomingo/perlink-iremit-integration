using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for GetMobilePrefixCollectionResult
/// </summary>
[DataContract]
public class GetMobilePrefixCollectionResult: ServiceResult
{
    #region Constructor
    public GetMobilePrefixCollectionResult()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion

    #region Fields/Properties
    private MobilePrefixCollection _mobilePrefixCollection;
    [DataMember]
    public MobilePrefixCollection MobilePrefixCollection
    {
        get { return _mobilePrefixCollection; }
        set { _mobilePrefixCollection = value; }
    }
    #endregion
}