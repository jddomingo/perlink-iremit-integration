using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetAgentCollectionResult: BaseResult
{
    #region Constructor
    public GetAgentCollectionResult()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private AgentCollection _agentCollection;
    [DataMember]
    public AgentCollection AgentCollection
    {
        get { return _agentCollection; }
        set { _agentCollection = value; }
    }
    #endregion
}