using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class ValidateBeneficiaryDetailsRequest: BaseRequest
{
	#region Constructor
	public ValidateBeneficiaryDetailsRequest()
	{ }
	#endregion

	#region Fields/Properties
	private Beneficiary _beneficiary;
	public Beneficiary Beneficiary
	{
		get { return _beneficiary; }
		set { _beneficiary = value; }
	}

	private string _partnerCode;
	public string PartnerCode
	{
		get { return _partnerCode; }
		set { _partnerCode = value; }
	}
	#endregion

	#region Internal Methods
	internal ValidateBeneficiaryDetailsResult Process()
	{
		ValidateBeneficiaryDetailsResult returnValue = new ValidateBeneficiaryDetailsResult();

		this.AuthenticateRequest();

		if (this.PartnerCode == SystemSetting.MoneyGramPartnerCode)
		{
			if (String.IsNullOrWhiteSpace(this.Beneficiary.Address))
			{
				PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(87);
				throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
			}

            if (String.IsNullOrWhiteSpace(this.Beneficiary.ProvinceAddress))
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(88);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }

			if (String.IsNullOrWhiteSpace(this.Beneficiary.ZipCode))
			{
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(89);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
			}
            
			if (String.IsNullOrWhiteSpace(this.Beneficiary.TelephoneNumber)
				&& String.IsNullOrWhiteSpace(this.Beneficiary.CellphoneNumber))
			{
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(95);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
			}

			if (String.IsNullOrWhiteSpace(this.Beneficiary.Occupation))
			{
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(90);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
			}
		}

		returnValue.ResultStatus = ResultStatus.Successful;
		return returnValue;
	}
	#endregion
}