using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;


public class TerminalTransactionSummary
{
	#region Constructor
	public TerminalTransactionSummary()
	{ }
	#endregion

	#region Fields/Properties
	private Int64 _terminalID;

	public Int64 TerminalID
	{
		get { return _terminalID; }
		set { _terminalID = value; }
	}

	private string _terminalCode;

	public string TerminalCode
	{
		get { return _terminalCode; }
		set { _terminalCode = value; }
	}

	private string _terminalMachineIdentificationNumber;

	public string TerminalMachineIdentificationNumber
	{
		get { return _terminalMachineIdentificationNumber; }
		set { _terminalMachineIdentificationNumber = value; }
	}

	private string _terminalPermitNumber;

	public string TerminalPermitNumber
	{
		get { return _terminalPermitNumber; }
		set { _terminalPermitNumber = value; }
	}

	private int _transactionCount;

	public int TransactionCount
	{
		get { return _transactionCount; }
		set { _transactionCount = value; }
	}

	private decimal _oldGrandTotal;

	public decimal OldGrandTotal
	{
		get { return _oldGrandTotal; }
		set { _oldGrandTotal = value; }
	}

	private decimal _principalAmount;

	public decimal PrincipalAmount
	{
		get { return _principalAmount; }
		set { _principalAmount = value; }
	}

	private decimal _serviceFee;

	public decimal ServiceFee
	{
		get { return _serviceFee; }
		set { _serviceFee = value; }
	}

	private decimal _totalTransactionAmount;

	public decimal TotalTransactionAmount
	{
		get { return _totalTransactionAmount; }
		set { _totalTransactionAmount = value; }
	}

	private decimal _newGrandTotal;

	public decimal NewGrandTotal
	{
		get { return _newGrandTotal; }
		set { _newGrandTotal = value; }
	}

	private int _voidTransaction;

	public int VoidTransaction
	{
		get { return _voidTransaction; }
		set { _voidTransaction = value; }
	}
	#endregion

	#region Public Methods
	
	public void Load(PeraLinkCoreWcf.TerminalTransactionSummary terminalTransactionSummary)
	{
		this.TerminalCode = terminalTransactionSummary.Terminal.TerminalCode;
		this.TerminalID = terminalTransactionSummary.Terminal.TerminalID;
		this.TerminalMachineIdentificationNumber = terminalTransactionSummary.Terminal.MachineIdentificationNumber;
		this.TerminalPermitNumber = terminalTransactionSummary.Terminal.TerminalPermitNumber;
		this.TransactionCount = terminalTransactionSummary.TransactionCount;
		this.PrincipalAmount = terminalTransactionSummary.PrincipalAmount;
		this.ServiceFee = terminalTransactionSummary.ServiceFee;
		this.TotalTransactionAmount = terminalTransactionSummary.TotalTransactionAmount;
		this.NewGrandTotal = terminalTransactionSummary.NewGrandTotal;
		this.VoidTransaction = terminalTransactionSummary.VoidTransaction;
	}
	#endregion

	#region Private Methods
	
	#endregion
}