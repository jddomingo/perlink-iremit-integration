using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetLastProcessedTransactionResult : ServiceResult
{
    #region Constructor
    public GetLastProcessedTransactionResult()
    { }
    #endregion

    #region Fields/Properties
    private string _controlNumber;
    [DataMember]
    public string ControlNumber
    {
        get { return _controlNumber; }
        set { _controlNumber = value; }
    }
    #endregion
}