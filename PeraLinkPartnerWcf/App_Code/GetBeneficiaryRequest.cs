using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetBeneficiaryRequest: BaseRequest
{
    #region Constructor
    public GetBeneficiaryRequest()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private Int64 _beneficiaryID;
    [DataMember]
    public Int64 BeneficiaryID
    {
        get { return _beneficiaryID; }
        set { _beneficiaryID = value; }
    }
    #endregion

    #region Internal Methods
    internal GetBeneficiaryResult Process()
    {
        GetBeneficiaryResult returnValue = new GetBeneficiaryResult();

        // Important: This function validates the request credential
        this.AuthenticateRequest();

        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
            PeraLinkCoreWcf.Beneficiary beneficiary = new PeraLinkCoreWcf.Beneficiary();
            beneficiary.BeneficiaryID = this.BeneficiaryID;
            beneficiary = serviceClient.GetBeneficiary(beneficiary);

            returnValue.Beneficiary = new Beneficiary();
            returnValue.Beneficiary.Load(beneficiary);
        }

        returnValue.ResultStatus = ResultStatus.Successful;

        return returnValue;
    }
    #endregion
}