﻿#define PeraLink_Agent_Insurance_Renewal

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
#if PeraLink_Agent_Insurance_Renewal
using System.Text.RegularExpressions;
#endif

/// <summary>
/// Summary description for ProcessMiCareInsuranceRequest
/// </summary>
public class ProcessInsuranceRequest : BaseRequest
{
    #region Constructor
    public ProcessInsuranceRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion

    #region Fields/Properties

    private string _insuranceType;
    public string InsuranceType
    {
        get { return _insuranceType; }
        set { _insuranceType = value; }
    }

    private string _firstName;
    public string FirstName
    {
        get { return _firstName; }
        set { _firstName = value; }
    }

    private string _middleName;
    public string MiddleName
    {
        get { return _middleName; }
        set { _middleName = value; }
    }

    private string _lastName;
    public string LastName
    {
        get { return _lastName; }
        set { _lastName = value; }
    }

    private string _address;
    public string Address
    {
        get { return _address; }
        set { _address = value; }
    }

    private string _birthday;
    public string Birthday
    {
        get { return _birthday; }
        set { _birthday = value; }
    }

    private string _mobileNumber;
    public string MobileNumber
    {
        get { return _mobileNumber; }
        set { _mobileNumber = value; }
    }

    private string _gender;
    public string Gender
    {
        get { return _gender; }
        set { _gender = value; }
    }

    private string _numberOfMonths;
    public string NumberOfMonths
    {
        get { return _numberOfMonths; }
        set { _numberOfMonths = value; }
    }

    private decimal _amount;
    public decimal Amount
    {
        get { return _amount; }
        set { _amount = value; }
    }

    private string _partnerCode;
    public string PartnerCode
    {
        get { return _partnerCode; }
        set { _partnerCode = value; }
    }

    private string _token;
    public string Token
    {
        get { return _token; }
        set { _token = value; }
    }

    private string _agentCode;
    public string AgentCode
    {
        get { return _agentCode; }
        set { _agentCode = value; }
    }

    private string _terminalCode;
    public string TerminalCode
    {
        get { return _terminalCode; }
        set { _terminalCode = value; }
    }

    private string _userID;
    public string UserID
    {
        get { return _userID; }
        set { _userID = value; }
    }

    private string _peraLinkReferenceNumber;
    public string PeraLinkReferenceNumber
    {
        get { return _peraLinkReferenceNumber; }
        set { _peraLinkReferenceNumber = value; }
    }
#if PeraLink_Agent_Insurance_Renewal
    private string _dateTimeExpiration;
    public string DateTimeExpiration
    {
        get { return _dateTimeExpiration; }
        set { _dateTimeExpiration = value; }
    }

    private bool _forRenewal;
    public bool ForRenewal
    {
        get { return _forRenewal; }
        set { _forRenewal = value; }
    }

    private string _productDescription;
    public string ProductDescription
    {
        get { return _productDescription; }
        set { _productDescription = value; }
    }

    private string _dateTimeInception;
    public string DateTimeInception
    {
        get { return _dateTimeInception; }
        set { _dateTimeInception = value; }
    }

#endif
    #endregion

    #region Internal Method
    internal ProcessInsuranceResult Process()
    {
        ProcessInsuranceResult returnValue = new ProcessInsuranceResult();

        decimal commissionFee = 0;

        this.AuthenticateRequest();

        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
            #region Validate Partner Record
            PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
            partner.PartnerCode = this.PartnerCode;
            partner = serviceClient.FindPartnerCode(partner);

            if (partner.PartnerID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else { }

            if (partner.Token == this.Token) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (partner.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            #endregion

            #region Validate Agent Record
            PeraLinkCoreWcf.Agent agent = new PeraLinkCoreWcf.Agent();
            agent.AgentCode = this.AgentCode;
            agent.Partner = partner;
            agent = serviceClient.FindAgentCodeOfPartner(agent);

            if (agent.AgentID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }

            if (agent.Partner.PartnerID == partner.PartnerID)
            { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(43);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (agent.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(46);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            #endregion

            #region Validate Terminal Record
            PeraLinkCoreWcf.Terminal terminal = new PeraLinkCoreWcf.Terminal();

            if (serviceClient.GetAgentTerminalCollection(agent).Length > 0)
            {
                terminal.TerminalCode = this.TerminalCode;
                terminal.Agent = agent;
                terminal.Agent.Partner = partner;
                terminal = serviceClient.FindTerminal(terminal);

                if (terminal.TerminalID == 0)
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(104);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
                else
                {
                    // Proceed
                }

                if (terminal.Agent.AgentID == agent.AgentID)
                { }
                else
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(105);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }

                if (terminal.Activated) { }
                else
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(106);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
            }
            else
            {
                //do nothing
            }
            #endregion

            #region Validate User ID
            if (string.IsNullOrWhiteSpace(this.UserID))
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(130);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
            #endregion

            #region Validate Insurance Request

            if (string.IsNullOrWhiteSpace(this.FirstName) || string.IsNullOrWhiteSpace(this.MiddleName) || string.IsNullOrWhiteSpace(this.LastName) || string.IsNullOrWhiteSpace(this.Birthday))
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(10);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
#if PeraLink_Agent_Insurance_Renewal
            if (this.InsuranceType == "MYCARE")
            {
                int age = 0;
                age = DateTime.Now.Year - Convert.ToDateTime(this.Birthday).Year;

                if (DateTime.Now.DayOfYear < Convert.ToDateTime(this.Birthday).DayOfYear)
                {
                    age = age - 1;
                }

                if (age < 18 || age > 70)
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(139);
                    throw new RDFramework.ClientException(string.Format(message.Description, 18, 70), message.SystemMessageID);
                }

            }
            else if (this.InsuranceType == "RENEW")
            {
                int age = 0;
                var format = "dd/MM/yyyy";
                DateTime bday = DateTime.ParseExact(this.Birthday, format, null);
                age = DateTime.Now.Year - bday.Year;

                if (DateTime.Now.DayOfYear < bday.DayOfYear)
                {
                    age = age - 1;
                }

                if (age < 18 || age > 70)
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(139);
                    throw new RDFramework.ClientException(string.Format(message.Description, 18, 70), message.SystemMessageID);
                }
            }
            else{}
#else
             int age = 0;
                age = DateTime.Now.Year - Convert.ToDateTime(this.Birthday).Year;

                if (DateTime.Now.DayOfYear < Convert.ToDateTime(this.Birthday).DayOfYear)
                {
                    age = age - 1;
                }

                if (age < 18 || age > 70)
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(139);
                    throw new RDFramework.ClientException(string.Format(message.Description, 18, 70), message.SystemMessageID);
                }
#endif


#if PeraLink_Agent_Insurance_Renewal
            PeraLinkCoreWcf.GetInsuranceProductNameRequest getInsuranceProductNameRequest = new PeraLinkCoreWcf.GetInsuranceProductNameRequest();
            getInsuranceProductNameRequest.Insurance = new PeraLinkCoreWcf.Insurance();
            getInsuranceProductNameRequest.Insurance.ProductDescription = this.InsuranceType;

            PeraLinkCoreWcf.GetInsuranceProductNameResult getInsuranceProductNameResult = new PeraLinkCoreWcf.GetInsuranceProductNameResult();
            getInsuranceProductNameResult = serviceClient.GetInsuranceProductName(getInsuranceProductNameRequest);

            if (this.InsuranceType == "MYCARE")
            {
                PeraLinkCoreWcf.GetInsuranceDetailsByNameRequest getInsuranceDetailsByNameRequest = new PeraLinkCoreWcf.GetInsuranceDetailsByNameRequest();
                getInsuranceDetailsByNameRequest.Insurance = new PeraLinkCoreWcf.Insurance();
                getInsuranceDetailsByNameRequest.Insurance.FirstName = this.FirstName;
                getInsuranceDetailsByNameRequest.Insurance.LastName = this.LastName;
                getInsuranceDetailsByNameRequest.Insurance.Birthday = Convert.ToDateTime(this.Birthday);
                getInsuranceDetailsByNameRequest.Insurance.ProductName = getInsuranceProductNameResult.Insurance.ProductName;

                PeraLinkCoreWcf.GetInsuranceDetailsByNameResult getInsuranceDetailsByNameResult = new PeraLinkCoreWcf.GetInsuranceDetailsByNameResult();
                getInsuranceDetailsByNameResult = serviceClient.GetInsuranceDetailsByName(getInsuranceDetailsByNameRequest);
                if (int.Parse(this.NumberOfMonths) + getInsuranceDetailsByNameResult.Insurance.ActiveECOC > 12)
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(138);
                    throw new RDFramework.ClientException(String.Format(message.Description, 12), message.SystemMessageID);
                }
            }
            else if (this.InsuranceType == "RENEW")
            {
                PeraLinkCoreWcf.GetInsuranceDateRequest getInsuranceDateRequest = new PeraLinkCoreWcf.GetInsuranceDateRequest();
                getInsuranceDateRequest.Insurance = new PeraLinkCoreWcf.Insurance();
                getInsuranceDateRequest.Insurance.FirstName = this.FirstName;
                getInsuranceDateRequest.Insurance.LastName = this.LastName;
                getInsuranceDateRequest.Insurance.Birthday = DateTime.ParseExact(this.Birthday, "dd/MM/yyyy", null);
                getInsuranceDateRequest.Insurance.ProductName = getInsuranceProductNameResult.Insurance.ProductName;

                PeraLinkCoreWcf.GetInsuranceDateResult getInsuranceDateResult = new PeraLinkCoreWcf.GetInsuranceDateResult();
                getInsuranceDateResult = serviceClient.GetInsuranceDate(getInsuranceDateRequest);
                if (getInsuranceDateResult.Insurance.ECOCCount == 0)
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(142);
                    throw new RDFramework.ClientException(String.Format(message.Description, 12), message.SystemMessageID);
                }
                else
                {
                    DateTime dateofExpiration = Convert.ToDateTime(getInsuranceDateResult.Insurance.DateTimeExpiration);
                    DateTime dateofInception = Convert.ToDateTime(getInsuranceDateResult.Insurance.DateTimeActivation);
                    if (DateTime.Now.Date < Convert.ToDateTime(getInsuranceDateResult.Insurance.DateTimeExpiration).Date)
                    {
                        TimeSpan daysDifference = Convert.ToDateTime(getInsuranceDateResult.Insurance.DateTimeExpiration).Date - DateTime.Now.Date;
                        double numberOfDays = daysDifference.TotalDays;
                        if (numberOfDays > 30)
                        {
                            PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(141);
                            throw new RDFramework.ClientException(String.Format(message.Description, 12), message.SystemMessageID);
                        }
                        else
                        {
                            if (dateofExpiration < DateTime.Now.Date)
                            {
                                dateofInception = DateTime.Now.Date;
                            }
                            else if (dateofExpiration > DateTime.Now.Date)
                            {
                                dateofInception = dateofExpiration.AddDays(1);
                            }
                            else
                            {
                                dateofInception = dateofExpiration.AddDays(1);
                            }
                            dateofExpiration = dateofInception.AddYears(1);
                            this.DateTimeInception = Convert.ToString(dateofInception);
                            this.DateTimeExpiration = Convert.ToString(dateofExpiration);

                        }
                    }
                    else
                    {
                        PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(138);
                        throw new RDFramework.ClientException(String.Format(message.Description, 12), message.SystemMessageID);

                    }

                }
            }
            else
            {

            }
#else
                PeraLinkCoreWcf.GetInsuranceDetailsByNameRequest getInsuranceDetailsByNameRequest = new PeraLinkCoreWcf.GetInsuranceDetailsByNameRequest();
                getInsuranceDetailsByNameRequest.Insurance = new PeraLinkCoreWcf.Insurance();
                getInsuranceDetailsByNameRequest.Insurance.FirstName = this.FirstName;
                getInsuranceDetailsByNameRequest.Insurance.LastName = this.LastName;
                getInsuranceDetailsByNameRequest.Insurance.Birthday = Convert.ToDateTime(this.Birthday);

                PeraLinkCoreWcf.GetInsuranceDateResult getInsuranceDateResult = new PeraLinkCoreWcf.GetInsuranceDateResult();
                getInsuranceDateResult = serviceClient.GetInsuranceDate(getInsuranceDateRequest);

            if (int.Parse(this.NumberOfMonths) + getInsuranceDetailsByNameResult.Insurance.ActiveECOC > 12)
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(138);
                throw new RDFramework.ClientException(String.Format(message.Description, 12), message.SystemMessageID);
            }
#endif
#endregion

#region Validate Mobile Number
            if (string.IsNullOrWhiteSpace(this.MobileNumber))
            {
                //do nothing
            }
            else
            {
                if (RDFramework.Utility.Validation.IsNumeric(this.MobileNumber))
                {
                    //do nothing
                }
                else
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(134);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
            }
#endregion

#region Validate Partner's Access To Insurance Service
            PeraLinkCoreWcf.PartnerServiceMapping[] partnerServiceMappingCollection = serviceClient.GetPartnerServiceMappingCollection(partner);

            PeraLinkCoreWcf.PartnerServiceMapping partnerServiceMapping = Array.Find<PeraLinkCoreWcf.PartnerServiceMapping>(partnerServiceMappingCollection
                , delegate (PeraLinkCoreWcf.PartnerServiceMapping eachPartnerServiceMapping)
                {
                    return eachPartnerServiceMapping.ServiceType.ServiceTypeID == ServiceTypeID.Insurance;
                });


            if (partnerServiceMapping == null)
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(128);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
            else
            {
                // Proceed
            }
#endregion

#region Validate Partner's Access to Insurance Partner
            PeraLinkCoreWcf.PartnerServicePair[] partnerServicePairCollection = serviceClient.GetPartnerServicePairCollection(partnerServiceMapping);

            PeraLinkCoreWcf.PartnerServicePair pairing = Array.Find<PeraLinkCoreWcf.PartnerServicePair>(partnerServicePairCollection
                    , delegate (PeraLinkCoreWcf.PartnerServicePair eachPartnerServicePair)
                    {
                        if (eachPartnerServicePair.PairedPartner == null)
                        {
                            PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(131);
                            throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                        }
                        else
                        {
                            return eachPartnerServicePair.PairedPartner.PartnerCode == this.InsuranceType;
                        }

                    });

            if (pairing == null)
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(131);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
            else
            {
                //Proceed
            }

            if (pairing.PairedPartner.Activated)
            { }
            else
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(132);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
#endregion

#region Validate Funding
            PeraLinkCoreWcf.EMoneyProfile eMoneyProfile = new PeraLinkCoreWcf.EMoneyProfile();
            if (partner.SourceOfFundID == SourceOfFundID.EMoney)
            {
#region Get E-Money Profile
                PeraLinkCoreWcf.GetEMoneyProfileRequest request = new PeraLinkCoreWcf.GetEMoneyProfileRequest();
                request.Partner = partner;

                eMoneyProfile = serviceClient.GetEMoneyProfile(request).EMoneyProfile;

                if (string.IsNullOrWhiteSpace(eMoneyProfile.AccountNumber))
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(2);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }

                if (string.IsNullOrWhiteSpace(eMoneyProfile.ClientNumber))
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(3);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }

#region Validate Balance
                EMoneyWSClient eMoneyServiceClient = new EMoneyWSClient();

                decimal balance = eMoneyServiceClient.EMoneyInquiry(eMoneyProfile.AccountNumber).AvailableBalance;

                if ((balance - this.Amount) < 0)
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(6);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
#endregion
#endregion
            }
#endregion

#region Get Service Fee
            PeraLinkCoreWcf.GetPartnerPairingServiceFeeRequest feeRequest = new PeraLinkCoreWcf.GetPartnerPairingServiceFeeRequest();
            feeRequest.PartnerPairingServiceFee = new PeraLinkCoreWcf.PartnerPairingServiceFee();
            feeRequest.PartnerPairingServiceFee.Amount = this.Amount;
            feeRequest.PartnerPairingServiceFee.PartnerServicePair = pairing;

            PeraLinkCoreWcf.GetPartnerPairingServiceFeeResult feeResult = new PeraLinkCoreWcf.GetPartnerPairingServiceFeeResult();
            feeResult = serviceClient.GetPartnerPairingServiceFee(feeRequest);

            if (feeResult.PartnerPairingServiceFee.FeeType.FeeTypeID == FeeTypeID.Fixed)
            {
                returnValue.ServiceFee = feeResult.PartnerPairingServiceFee.Fee;
            }
            else
            {
                returnValue.ServiceFee = 0;
            }
#endregion

#region Insert to Database
            PeraLinkCoreWcf.CreateInsuranceRequest createInsuranceRequest = new PeraLinkCoreWcf.CreateInsuranceRequest();

            createInsuranceRequest.AuditTrail = new PeraLinkCoreWcf.AuditTrail();
            createInsuranceRequest.AuditTrail.CreatedByAgent = agent;
            createInsuranceRequest.AuditTrail.CreatedByUserID = this.UserID;
            createInsuranceRequest.AuditTrail.DateTimeCreated = DateTime.Now;
            createInsuranceRequest.AuditTrail.IPAddress = SystemUtility.GetClientIPAddress();
            createInsuranceRequest.Insurance = new PeraLinkCoreWcf.Insurance();
            createInsuranceRequest.Insurance.DateTimeCreated = createInsuranceRequest.AuditTrail.DateTimeCreated;

            createInsuranceRequest.Insurance.ClientMobileNumber = this.MobileNumber;
            createInsuranceRequest.Insurance.ProcessedByAgent = agent;
            createInsuranceRequest.Insurance.ProcessedByTerminal = terminal;
            createInsuranceRequest.Insurance.ProcessedByUserID = this.UserID;

            createInsuranceRequest.Insurance.FirstName = this.FirstName;
            createInsuranceRequest.Insurance.MiddleName = this.MiddleName;
            createInsuranceRequest.Insurance.LastName = this.LastName;
            createInsuranceRequest.Insurance.Address = this.Address;
            createInsuranceRequest.Insurance.ClientMobileNumber = this.MobileNumber;
            createInsuranceRequest.Insurance.ECOCCount = int.Parse(this.NumberOfMonths);
            createInsuranceRequest.Insurance.Gender = this.Gender;
            createInsuranceRequest.Insurance.Birthday = Convert.ToDateTime(this.Birthday);
            createInsuranceRequest.Insurance.PremiumFee = this.Amount;
#if PeraLink_Agent_Insurance_Renewal
            if (this.InsuranceType == "MYCARE")
            {
                createInsuranceRequest.Insurance.InsuranceProductID = (int)InsuranceProductType.MyCare;
            }
            else if (this.InsuranceType == "RENEW")
            {

                createInsuranceRequest.Insurance.InsuranceProductID = (int)InsuranceProductType.Renew;
            }
#else
            createInsuranceRequest.Insurance.InsuranceProductID = (int)InsuranceProductType.MyCare;
#endif
            PeraLinkCoreWcf.CreateInsuranceResult createInsuranceResult = serviceClient.CreateInsurance(createInsuranceRequest);
            returnValue.PeraLinkReferenceNumber = createInsuranceResult.Insurance.PeraLinkReferenceNumber;
#endregion

#region Debit E-Money
            if (partner.SourceOfFundID == SourceOfFundID.EMoney)
            {
#region Commission Fee
                PeraLinkCoreWcf.GetPartnerPairingCommissionFeeRequest pairingfeeRequest = new PeraLinkCoreWcf.GetPartnerPairingCommissionFeeRequest();
                pairingfeeRequest.PartnerPairingCommissionFee = new PeraLinkCoreWcf.PartnerPairingCommissionFee();
                pairingfeeRequest.PartnerPairingCommissionFee.Amount = this.Amount;
                pairingfeeRequest.PartnerPairingCommissionFee.PartnerServicePair = pairing;

                PeraLinkCoreWcf.Insurance insurance = new PeraLinkCoreWcf.Insurance();

                PeraLinkCoreWcf.GetPartnerPairingCommissionFeeResult pairingfeeResult = new PeraLinkCoreWcf.GetPartnerPairingCommissionFeeResult();
                pairingfeeResult = serviceClient.GetPartnerPairingCommissionFee(pairingfeeRequest);

                if (pairingfeeResult.PartnerPairingCommissionFee.PartnerPairingCommissionFeeID == 0)
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(137);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
                else
                {

                    if (pairingfeeResult.PartnerPairingCommissionFee.FeeType.FeeTypeID == FeeTypeID.Fixed)
                    {
                        commissionFee = pairingfeeResult.PartnerPairingCommissionFee.Fee * createInsuranceRequest.Insurance.ECOCCount;
                    }
                    else
                    {
                        commissionFee = 3 * createInsuranceRequest.Insurance.ECOCCount;
                    }
                    returnValue.AdminFee = commissionFee;
                }


#endregion

                EMoneyWSClient eMoneyServiceClient = new EMoneyWSClient();
                EMoneyWS.UpdateEMoneyRequest request = new EMoneyWS.UpdateEMoneyRequest();


                List<EMoneyWS.UpdateEMoneyRequest> requestList = new List<EMoneyWS.UpdateEMoneyRequest>();
                EMoney eMoneyRequest = new EMoney();
                eMoneyRequest.AccountNumber = eMoneyProfile.AccountNumber;
                eMoneyRequest.ControlNumber = createInsuranceResult.Insurance.PeraLinkReferenceNumber;
                eMoneyRequest.Remarks = "Insurance";
                eMoneyRequest.PeraLinkAgentCode = this.AgentCode;
                eMoneyRequest.CashInAmount = 0;
                eMoneyRequest.CashOutAmount = this.Amount;

                requestList.Add(eMoneyRequest.Principal());

                eMoneyRequest.CashInAmount = 0;
                eMoneyRequest.CashOutAmount = returnValue.ServiceFee;
                requestList.Add(eMoneyRequest.ServiceCharge());

                eMoneyRequest.CashInAmount = returnValue.AdminFee;
                eMoneyRequest.CashOutAmount = 0;
                requestList.Add(eMoneyRequest.CommissionFee());

                EMoneyWS.UpdateEMoneyResult result = eMoneyServiceClient.UpdateEMoney(requestList);

                switch (result.ResultCode)
                {
                    case EMoneyWS.ResultCodes.Successful:
                        //do nothing
                        break;
                    case EMoneyWS.ResultCodes.Failed:
                    case EMoneyWS.ResultCodes.Error:
                        {
                            RDFramework.Utility.EventLog.SaveError(string.Format("{0}: {1} {2}", createInsuranceResult.Insurance.PeraLinkReferenceNumber, result.ResultMessageCode, result.ResultMessage));
                            throw new RDFramework.ClientException("Unsuccessful E-Money debiting");
                        }
                }
            }
#endregion

#region Process Insurance
            PeraLinkInsuranceWcfClient peraLinkInsuranceWcfClient = new PeraLinkInsuranceWcfClient();
            PeraLinkInsuranceWcf.ProcessMiCareRequest processMiCareRequest = new PeraLinkInsuranceWcf.ProcessMiCareRequest();
            PeraLinkInsuranceWcf.ProcessMiCareResult processMiCareResult = new PeraLinkInsuranceWcf.ProcessMiCareResult();
            PeraLinkInsuranceWcf.ProcessRenewRequest processRenewRequest = new PeraLinkInsuranceWcf.ProcessRenewRequest();
            PeraLinkInsuranceWcf.ProcessRenewResult processRenewResult = new PeraLinkInsuranceWcf.ProcessRenewResult();
            try
            {
                switch (this.InsuranceType)
                {
#region MiCare
                    case "MYCARE":
                        processMiCareRequest.MicroInsuranceMain = new PeraLinkInsuranceWcf.MicroInsuranceMain();
                        processMiCareRequest.MicroInsuranceMain.Birthdate = DateTime.Parse(this.Birthday);
                        processMiCareRequest.MicroInsuranceMain.FirstName = this.FirstName;
                        processMiCareRequest.MicroInsuranceMain.MiddleName = this.MiddleName;
                        processMiCareRequest.MicroInsuranceMain.LastName = this.LastName;
                        processMiCareRequest.MicroInsuranceMain.Address = this.Address;
                        processMiCareRequest.MicroInsuranceMain.MobileNumber = this.MobileNumber;
                        processMiCareRequest.MicroInsuranceMain.Count = this.NumberOfMonths;
                        processMiCareRequest.MicroInsuranceMain.Gender = this.Gender;
                        processMiCareRequest.MicroInsuranceMain.ProductName = getInsuranceProductNameResult.Insurance.ProductName;
                        processMiCareRequest.MicroInsuranceMain.ForRenewal = false;
                        processMiCareResult = peraLinkInsuranceWcfClient.ProcessMiCare(processMiCareRequest);

                        break;
#if PeraLink_Agent_Insurance_Renewal
                    case "RENEW":
                        processRenewRequest.MicroInsuranceMainRenew = new PeraLinkInsuranceWcf.MicroInsuranceMainRenew();
                        processRenewRequest.MicroInsuranceMainRenew.Birthdate = DateTime.Parse(this.Birthday);
                        processRenewRequest.MicroInsuranceMainRenew.FirstName = this.FirstName;
                        processRenewRequest.MicroInsuranceMainRenew.MiddleName = this.MiddleName;
                        processRenewRequest.MicroInsuranceMainRenew.LastName = this.LastName;
                        processRenewRequest.MicroInsuranceMainRenew.Address = this.Address;
                        processRenewRequest.MicroInsuranceMainRenew.MobileNumber = this.MobileNumber;
                        processRenewRequest.MicroInsuranceMainRenew.Count = this.NumberOfMonths;
                        processRenewRequest.MicroInsuranceMainRenew.Gender = this.Gender;
                        processRenewRequest.MicroInsuranceMainRenew.ProductName = getInsuranceProductNameResult.Insurance.ProductName;
                        processRenewRequest.MicroInsuranceMainRenew.ForRenewal = true;
                        processRenewRequest.MicroInsuranceMainRenew.InceptionDate = DateTime.Parse(this.DateTimeInception);
                        processRenewRequest.MicroInsuranceMainRenew.ExpiryDate = DateTime.Parse(this.DateTimeExpiration);
                        processRenewResult = peraLinkInsuranceWcfClient.ProcessRenew(processRenewRequest);

                        break;
#endif
                    default:
                        break;
                }
            }

#endregion
            catch (Exception error)
            {
#region E-Money Reversal
                if (partner.SourceOfFundID == SourceOfFundID.EMoney)
                {
                    EMoneyWSClient eMoneyServiceClient = new EMoneyWSClient();
                    EMoneyWS.UpdateEMoneyRequest request = new EMoneyWS.UpdateEMoneyRequest();

                    List<EMoneyWS.UpdateEMoneyRequest> requestList = new List<EMoneyWS.UpdateEMoneyRequest>();
                    EMoney eMoneyRequest = new EMoney();
                    eMoneyRequest.AccountNumber = eMoneyProfile.AccountNumber;
                    eMoneyRequest.ControlNumber = createInsuranceResult.Insurance.PeraLinkReferenceNumber;
                    eMoneyRequest.Remarks = "InsuranceReversal";
                    eMoneyRequest.PeraLinkAgentCode = this.AgentCode;
                    eMoneyRequest.CashInAmount = this.Amount;
                    eMoneyRequest.CashOutAmount = 0;
                    requestList.Add(eMoneyRequest.Principal());
                    eMoneyRequest.CashInAmount = 0;
                    eMoneyRequest.CashOutAmount = returnValue.AdminFee;
                    requestList.Add(eMoneyRequest.CommissionFee());

                    EMoneyWS.UpdateEMoneyResult result = eMoneyServiceClient.UpdateEMoney(requestList);

                    switch (result.ResultCode)
                    {
                        case EMoneyWS.ResultCodes.Successful:
                            //do nothing
                            break;
                        case EMoneyWS.ResultCodes.Failed:
                        case EMoneyWS.ResultCodes.Error:
                            {
                                throw new RDFramework.ClientException(result.ResultMessage);
                            }
                    }
                }
#endregion

#region Update Database Status
                PeraLinkCoreWcf.UpdateInsuranceStatusRequest updateInsuranceStatusRequest = new PeraLinkCoreWcf.UpdateInsuranceStatusRequest();
                updateInsuranceStatusRequest.AuditTrail = new PeraLinkCoreWcf.AuditTrail();
                updateInsuranceStatusRequest.AuditTrail.CreatedByAgent = agent;
                updateInsuranceStatusRequest.AuditTrail.CreatedByUserID = this.UserID;
                updateInsuranceStatusRequest.AuditTrail.DateTimeCreated = DateTime.Now;
                updateInsuranceStatusRequest.AuditTrail.IPAddress = SystemUtility.GetClientIPAddress();
                updateInsuranceStatusRequest.Insurance = createInsuranceResult.Insurance;
                PeraLinkCoreWcf.UpdateInsuranceStatusResult updateInsuranceStatusResult = serviceClient.UpdateInsuranceStatus(updateInsuranceStatusRequest);
#endregion
                throw new RDFramework.ClientException(error.Message);
            }

#endregion

#region Save to Database
            PeraLinkCoreWcf.SaveInsuranceTransactionRequest saveInsuranceTransactionRequest = new PeraLinkCoreWcf.SaveInsuranceTransactionRequest();
            saveInsuranceTransactionRequest.AuditTrail = new PeraLinkCoreWcf.AuditTrail();
            saveInsuranceTransactionRequest.AuditTrail.CreatedByAgent = agent;
            saveInsuranceTransactionRequest.AuditTrail.CreatedByUserID = this.UserID;
            saveInsuranceTransactionRequest.AuditTrail.DateTimeCreated = DateTime.Now;
            saveInsuranceTransactionRequest.AuditTrail.IPAddress = SystemUtility.GetClientIPAddress();
            saveInsuranceTransactionRequest.Insurance = createInsuranceResult.Insurance;
            saveInsuranceTransactionRequest.Insurance.AdminFee = returnValue.AdminFee;
            List<PeraLinkCoreWcf.CocDetails> cocDetailsList = new List<PeraLinkCoreWcf.CocDetails>();
#if PeraLink_Agent_Insurance_Renewal
            if (this.InsuranceType == "MYCARE")
            {
                foreach (PeraLinkInsuranceWcf.CocDetailsCollection cocDetailsCollection in processMiCareResult.CocDetailsCollection)
                {
                    PeraLinkCoreWcf.CocDetails cocDetails = new PeraLinkCoreWcf.CocDetails();
                    cocDetails.CocNumber = cocDetailsCollection.CocNumber;
                    cocDetails.EndDate = cocDetailsCollection.EndDate;
                    cocDetails.ProductName = getInsuranceProductNameResult.Insurance.ProductName; ;
                    cocDetails.StartDate = cocDetailsCollection.StartDate;
                    cocDetailsList.Add(cocDetails);
                }
                saveInsuranceTransactionRequest.Insurance.CocDetailsCollection = cocDetailsList.ToArray();

            }
            else if (this.InsuranceType == "RENEW")
            {
                foreach (PeraLinkInsuranceWcf.CocDetailsCollection cocDetailsCollection in processRenewResult.CocDetailsCollection)
                {
                    PeraLinkCoreWcf.CocDetails cocDetails = new PeraLinkCoreWcf.CocDetails();
                    cocDetails.CocNumber = cocDetailsCollection.CocNumber;
                    cocDetails.EndDate = cocDetailsCollection.EndDate;
                    cocDetails.ProductName = getInsuranceProductNameResult.Insurance.ProductName; ;
                    cocDetails.StartDate = cocDetailsCollection.StartDate;
                    cocDetailsList.Add(cocDetails);
                }
                saveInsuranceTransactionRequest.Insurance.CocDetailsCollection = cocDetailsList.ToArray();

            }
#else
             foreach (PeraLinkInsuranceWcf.CocDetailsCollection cocDetailsCollection in processMiCareResult.CocDetailsCollection)
                {
                    PeraLinkCoreWcf.CocDetails cocDetails = new PeraLinkCoreWcf.CocDetails();
                    cocDetails.CocNumber = cocDetailsCollection.CocNumber;
                    cocDetails.EndDate = cocDetailsCollection.EndDate;
                    cocDetails.ProductName = getInsuranceProductNameResult.Insurance.ProductName; ;
                    cocDetails.StartDate = cocDetailsCollection.StartDate;
                    cocDetailsList.Add(cocDetails);
                }
                saveInsuranceTransactionRequest.Insurance.CocDetailsCollection = cocDetailsList.ToArray();
#endif
            PeraLinkCoreWcf.SaveInsuranceTransactionResult saveInsuranceTransactionResult = serviceClient.SaveInsurance(saveInsuranceTransactionRequest);
            returnValue.DateTimeProcessed = saveInsuranceTransactionRequest.AuditTrail.DateTimeCreated;
            if (this.InsuranceType == "MYCARE")
            {
                if (processMiCareResult.CocDetailsCollection.Count() > 1)
                {
                    returnValue.ReferenceNumber = processMiCareResult.CocDetailsCollection.First().CocNumber.ToString() + "-" +
                    processMiCareResult.CocDetailsCollection.Last().CocNumber.ToString();
                }
                else
                {
                    returnValue.ReferenceNumber = processMiCareResult.CocDetailsCollection.First().CocNumber.ToString();
                }
            }
#if PeraLink_Agent_Insurance_Renewal
            else if (this.InsuranceType == "RENEW")
            {
                if (processRenewResult.CocDetailsCollection.Count() > 1)
                {
                    returnValue.ReferenceNumber = processRenewResult.CocDetailsCollection.First().CocNumber.ToString() + "-" +
                        processRenewResult.CocDetailsCollection.Last().CocNumber.ToString();
                }
                else
                {
                    returnValue.ReferenceNumber = processRenewResult.CocDetailsCollection.First().CocNumber.ToString();
                }

            }
#endif
#endregion

#region Insert Total Commission
            AddCommissionTransactionRequest addCommissionTransactionRequest = new AddCommissionTransactionRequest();
            addCommissionTransactionRequest.ISControlNo = returnValue.ReferenceNumber;
            addCommissionTransactionRequest.PartnerID = partner.PartnerID;

            AddCommissionTransactionResult addCommissionTransactionResult = new AddCommissionTransactionResult();
            addCommissionTransactionResult = addCommissionTransactionRequest.Process();
#endregion
        }
        returnValue.ResultStatus = ResultStatus.Successful;
        return returnValue;
    }
#endregion
}