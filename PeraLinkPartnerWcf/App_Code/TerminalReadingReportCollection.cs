using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class TerminalReadingReportCollection: List<TerminalReadingReport>
{
	#region Constructor
	public TerminalReadingReportCollection()
	{ }
	#endregion

	#region Public Methods
	public void AddList(PeraLinkCoreWcf.TerminalReadingReport[] terminalReadingReportCollection)
	{
		Array.ForEach<PeraLinkCoreWcf.TerminalReadingReport>(terminalReadingReportCollection
			, delegate(PeraLinkCoreWcf.TerminalReadingReport eachTerminalReadingReport)
			{
				TerminalReadingReport mappedTerminalReadingReport = new TerminalReadingReport();
				mappedTerminalReadingReport.Load(eachTerminalReadingReport);
				this.Add(mappedTerminalReadingReport);
			});
	}
	#endregion
}