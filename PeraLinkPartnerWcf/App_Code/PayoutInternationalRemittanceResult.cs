#define INTERNATIONAL_COMMISSION_FEE

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class PayoutInternationalRemittanceResult: BaseResult
{
    #region Constructor
    public PayoutInternationalRemittanceResult()
	{
		//

		//
    }
    #endregion

    private string _authorizationCode;
    [DataMember]
    public string AuthorizationCode
    {
        get { return _authorizationCode; }
        set { _authorizationCode = value; }
    }
#if INTERNATIONAL_COMMISSION_FEE
    private decimal _commissionFee;
    [DataMember]
    public decimal CommissionFee
    {
        get { return _commissionFee; }
        set { _commissionFee = value; }
    }
#endif
}