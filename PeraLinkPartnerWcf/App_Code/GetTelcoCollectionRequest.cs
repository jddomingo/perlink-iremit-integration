using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GetTelcoCollectionRequest
/// </summary>
public class GetTelcoCollectionRequest: BaseRequest
{
    #region Constructor
    public GetTelcoCollectionRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion

    #region Fields/Properties
    private string _transactionType;

    public string TransactionType
    {
        get { return _transactionType; }
        set { _transactionType = value; }
    }
    #endregion

    #region Internal Methods
    public GetTelcoCollectionResult Process()
    {
        GetTelcoCollectionResult returnValue = new GetTelcoCollectionResult();
        PeraLinkELoadGatewayWcf.GetTelcoCollectionResult getTelcoCollectionResult = new PeraLinkELoadGatewayWcf.GetTelcoCollectionResult();

        // Important: This function validates the request credential
        this.AuthenticateRequest();

        using (ELoadWSClient serviceClient = new ELoadWSClient())
        {
            PeraLinkELoadGatewayWcf.GetTelcoCollectionRequest getTelcoCollectionRequest = new PeraLinkELoadGatewayWcf.GetTelcoCollectionRequest();
            getTelcoCollectionRequest.TransactionType = this.TransactionType;
            getTelcoCollectionResult = serviceClient.GetTelcoCollection(getTelcoCollectionRequest);


            returnValue.TelcoCollection = new TelcoCollection();
            returnValue.TelcoCollection.AddList(getTelcoCollectionResult.TelcoCollection.ToArray());

            returnValue.ResultStatus = ResultStatus.Successful;

            return returnValue;

        }


    }
    #endregion
}