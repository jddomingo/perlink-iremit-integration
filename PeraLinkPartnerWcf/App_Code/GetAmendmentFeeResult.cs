using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetAmendmentFeeResult: BaseResult
{
    #region Constructor
    public GetAmendmentFeeResult()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private decimal _serviceFee;
    [DataMember]
    public decimal ServiceFee
    {
        get { return _serviceFee; }
        set { _serviceFee = value; }
    }
    #endregion
}