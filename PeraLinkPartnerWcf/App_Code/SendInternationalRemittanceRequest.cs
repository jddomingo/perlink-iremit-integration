using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class SendInternationalRemittanceRequest: BaseRequest
{
    #region Constructor
    public SendInternationalRemittanceRequest()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private Int64 _beneficiaryID;
    [DataMember]
    public Int64 BeneficiaryID
    {
        get { return _beneficiaryID; }
        set { _beneficiaryID = value; }
    }

    private int _sendCurrencyID;
    [DataMember]
    public int SendCurrencyID
    {
        get { return _sendCurrencyID; }
        set { _sendCurrencyID = value; }
    }

    private decimal _principalAmount;
    [DataMember]
    public decimal PrincipalAmount
    {
        get { return _principalAmount; }
        set { _principalAmount = value; }
    }

    private decimal _serviceFee;
    [DataMember]
    public decimal ServiceFee
    {
        get { return _serviceFee; }
        set { _serviceFee = value; }
    }

    private string _partnerCode;
    [DataMember]
    public string PartnerCode
    {
        get { return _partnerCode; }
        set { _partnerCode = value; }
    }

    private string _token;
    [DataMember]
    public string Token
    {
        get { return _token; }
        set { _token = value; }
    }

    private string _agentCode;
    [DataMember]
    public string AgentCode
    {
        get { return _agentCode; }
        set { _agentCode = value; }
    }

    private string _userID;
    [DataMember]
    public string UserID
    {
        get { return _userID; }
        set { _userID = value; }
    }
    #endregion

    #region Internal Methods
    internal SendDomesticRemittanceResult Process()
    {
        SendDomesticRemittanceResult returnValue = new SendDomesticRemittanceResult();

        // Important: This function validates the request credential
        this.AuthenticateRequest();

        // Retrieve agent information
        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
            PeraLinkCoreWcf.InternationalRemittance internationalRemittance = new PeraLinkCoreWcf.InternationalRemittance();

            #region Validate requesting partner's information
            PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
            partner.PartnerCode = this.PartnerCode;
            partner = serviceClient.FindPartnerCode(partner);

            if (partner.PartnerID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else { }

            if (partner.Token == this.Token) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (partner.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            #endregion

            #region Validate requesting agent's information
            PeraLinkCoreWcf.Agent agent = new PeraLinkCoreWcf.Agent();
            agent.AgentCode = this.AgentCode;
            agent.Partner = partner;
            agent = serviceClient.FindAgentCodeOfPartner(agent);

            if (agent.AgentID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }

            if (agent.Partner.PartnerID == partner.PartnerID)
            { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(43);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (agent.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(46);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            #endregion
            
            #region Validate requesting partner's access to method (SendInternationalRemittance)
            PeraLinkCoreWcf.PartnerServiceMapping[] partnerServiceMappingCollection = serviceClient.GetPartnerServiceMappingCollection(partner);

            PeraLinkCoreWcf.PartnerServiceMapping partnerServiceMapping = Array.Find<PeraLinkCoreWcf.PartnerServiceMapping>(partnerServiceMappingCollection
                , delegate(PeraLinkCoreWcf.PartnerServiceMapping eachPartnerServiceMapping)
                {
                    return eachPartnerServiceMapping.ServiceType.ServiceTypeID == ServiceTypeID.SendRemittance;   
                });


            if (partnerServiceMapping == null)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(1);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }

            if (partner.PartnerType.PartnerTypeID == PartnerTypeID.Domestic)
            {
                // Proceed
            }
            else
            {
                PeraLinkCoreWcf.PartnerServicePair[] partnerServicePairCollection = serviceClient.GetPartnerServicePairCollection(partnerServiceMapping);

                PeraLinkCoreWcf.PartnerServicePair domesticPartner = Array.Find<PeraLinkCoreWcf.PartnerServicePair>(partnerServicePairCollection
                    , delegate(PeraLinkCoreWcf.PartnerServicePair eachPartnerServicePair)
                    {
                        if (eachPartnerServicePair.PairedPartner == null)
                        {
                            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(2);
                            throw new RDFramework.ClientException(message.Content, message.MessageID);
                        }
                        else
                        {
                            return eachPartnerServicePair.PairedPartner.PartnerType.PartnerTypeID == PartnerTypeID.Domestic;
                        }
                    });

                if (domesticPartner == null)
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(3);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
                else
                {
                    // Proceed
                }                
            }
            #endregion

            #region Validate partner service fee
            
            GetSendServiceFeeRequest feeRequest = new GetSendServiceFeeRequest();
            feeRequest.AgentCode = this.AgentCode;
            feeRequest.PartnerCode = this.PartnerCode;
            feeRequest.Token = this.Token;
            feeRequest.CurrencyID = this.SendCurrencyID;
            feeRequest.ServiceCredential = this.ServiceCredential;
            feeRequest.PrincipalAmount = this.PrincipalAmount;

            GetSendServiceFeeResult feeResult = new GetSendServiceFeeResult();

            try
            {
                feeResult = feeRequest.Process();
                internationalRemittance.ServiceFee = feeResult.ServiceFee;
            }
            catch (RDFramework.ClientException clientEx)
            {
                if (clientEx.MessageID == SystemResource.GetMessageWithListID(28).MessageID)
                {
                    internationalRemittance.ServiceFee = this.ServiceFee;
                }
                else
                {
                    throw;
                }
            }
            #endregion

            PeraLinkCoreWcf.Beneficiary beneficiary = new PeraLinkCoreWcf.Beneficiary();
            beneficiary.BeneficiaryID = this.BeneficiaryID;
            internationalRemittance.Beneficiary = beneficiary;

            internationalRemittance.DateTimeSent = DateTime.Now;
            internationalRemittance.PrincipalAmount = this.PrincipalAmount;
            internationalRemittance.SendCurrency = new PeraLinkCoreWcf.Currency();
            internationalRemittance.SendCurrency.CurrencyID = this.SendCurrencyID;

            internationalRemittance.SenderClient = serviceClient.FindClientByBeneficiaryID(beneficiary);

            if (internationalRemittance.SenderClient.ClientID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(12);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }

            agent.Partner = partner;
            internationalRemittance.SentByAgent = agent;
            internationalRemittance.SentByUserID = this.UserID;

            PeraLinkCoreWcf.SendInternationalRemittanceRequest sendInternationalRemittanceRequest = new PeraLinkCoreWcf.SendInternationalRemittanceRequest();
            sendInternationalRemittanceRequest.InternationalRemittance = internationalRemittance;
            sendInternationalRemittanceRequest.UseAssignedControlNumber = false;

            returnValue.ControlNumber = serviceClient.SendInternationalRemittance(sendInternationalRemittanceRequest).ControlNumber;
            returnValue.ResultStatus = ResultStatus.Successful;

            return returnValue;
        }
    }
    #endregion
}