#define CONVERTION_TO_BUSINESS_ACCOUNT
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using AppCryptor;

/// <summary>
/// Summary description for RemitToAccountRequest
/// </summary>
[DataContract]
public class RemitToAccountRequest : BaseRequest
{
    #region Constructor
    public RemitToAccountRequest()
    {
    }
    #endregion

    #region Fields/Properties
    private string _partnerCode;
    [DataMember]
    public string PartnerCode
    {
        get { return _partnerCode; }
        set { _partnerCode = value; }
    }

    private string _token;
    [DataMember]
    public string Token
    {
        get { return _token; }
        set { _token = value; }
    }

    private string _agentCode;
    [DataMember]
    public string AgentCode
    {
        get { return _agentCode; }
        set { _agentCode = value; }
    }

    private string _userID;
    [DataMember]
    public string UserID
    {
        get { return _userID; }
        set { _userID = value; }
    }

    private Int64 _clientID;
    [DataMember]
    public Int64 ClientID
    {
        get { return _clientID; }
        set { _clientID = value; }
    }

    private string _bankCode;
    [DataMember]
    public string BankCode
    {
        get { return _bankCode; }
        set { _bankCode = value; }
    }

    private string _accountNumber;
    [DataMember]
    public string AccountNumber
    {
        get { return _accountNumber; }
        set { _accountNumber = value; }
    }

    private decimal _amount;
    [DataMember]
    public decimal Amount
    {
        get { return _amount; }
        set { _amount = value; }
    }

    private decimal _serviceFee;
    [DataMember]
    public decimal ServiceFee
    {
        get { return _serviceFee; }
        set { _serviceFee = value; }
    }

    private string _accountHolderLastName;
    [DataMember]
    public string AccountHolderLastName
    {
        get { return _accountHolderLastName; }
        set { _accountHolderLastName = value; }
    }

    private string _accountHolderFirstName;
    [DataMember]
    public string AccountHolderFirstName
    {
        get { return _accountHolderFirstName; }
        set { _accountHolderFirstName = value; }
    }

    private string _accountHolderMiddleName;
    [DataMember]
    public string AccountHolderMiddleName
    {
        get { return _accountHolderMiddleName; }
        set { _accountHolderMiddleName = value; }
    }

    private string _accountHolderHouseStreet;
    [DataMember]
    public string AccountHolderHouseStreet
    {
        get { return _accountHolderHouseStreet; }
        set { _accountHolderHouseStreet = value; }
    }

    private string _accountHolderCityProvince;
    [DataMember]
    public string AccountHolderCityProvince
    {
        get { return _accountHolderCityProvince; }
        set { _accountHolderCityProvince = value; }
    }

    private string _accountHolderMobileNumber;
    [DataMember]
    public string AccountHolderMobileNumber
    {
        get { return _accountHolderMobileNumber; }
        set { _accountHolderMobileNumber = value; }
    }

    private int _sendCurrencyID;
    [DataMember]
    public int SendCurrencyID
    {
        get { return _sendCurrencyID; }
        set { _sendCurrencyID = value; }
    }

	private string _pin;
	[DataMember]
	public string Pin
	{
		get { return _pin; }
		set { _pin = value; }
	}

	private string _terminalCode;

	public string TerminalCode
	{
		get { return _terminalCode; }
		set { _terminalCode = value; }
	}
    #endregion

    #region Internal Method/s
    internal RemitToAccountResult Process()
    {
        RemitToAccountResult returnValue = new RemitToAccountResult();

        // Important: This function validates the request credential
        this.AuthenticateRequest();

        string bankPartnerCode = string.Empty;
        string bankAgentCode = string.Empty;

        string[] bankInfo = this.BankCode.Split('-');
        if (bankInfo != null && bankInfo.Length == 2)
        {
            bankPartnerCode = bankInfo[0];
            bankAgentCode = bankInfo[1];
        }
        else
        {
            PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(8);
            throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
        }

        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
            #region Validate requesting partner information
            PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
            partner.PartnerCode = this.PartnerCode;
            partner = serviceClient.FindPartnerCode(partner);

            if (partner.PartnerID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else { }

            if (partner.Token == this.Token) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (partner.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            #endregion

            #region Validate requesting agent information
            PeraLinkCoreWcf.Agent agent = new PeraLinkCoreWcf.Agent();
            agent.AgentCode = this.AgentCode;
            agent.Partner = partner;
            agent = serviceClient.FindAgentCodeOfPartner(agent);

            if (agent.AgentID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }

            if (agent.Partner.PartnerID == partner.PartnerID)
            { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(43);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (agent.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(46);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            #endregion

			#region Validate Terminal Record
			PeraLinkCoreWcf.Terminal terminal = new PeraLinkCoreWcf.Terminal();

			if (serviceClient.GetAgentTerminalCollection(agent).Length > 0)
			{
				terminal.TerminalCode = this.TerminalCode;
				terminal.Agent = agent;
				terminal.Agent.Partner = partner;
				terminal = serviceClient.FindTerminal(terminal);

				if (terminal.TerminalID == 0)
				{
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(104);
					throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
				}
				else
				{
					// Proceed
				}

				if (terminal.Agent.AgentID == agent.AgentID)
				{ }
				else
				{
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(105);
					throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
				}

				if (terminal.Activated) { }
				else
				{
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(106);
					throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
				}
			}
			else
			{
				//do nothing
			}
			#endregion

            #region Validate requesting partner's access to method (RemitToAccount)
            PeraLinkCoreWcf.PartnerServiceMapping[] partnerServiceMappingCollection = serviceClient.GetPartnerServiceMappingCollection(partner);

            PeraLinkCoreWcf.PartnerServiceMapping partnerServiceMapping = Array.Find<PeraLinkCoreWcf.PartnerServiceMapping>(partnerServiceMappingCollection
                , delegate(PeraLinkCoreWcf.PartnerServiceMapping eachPartnerServiceMapping)
                {
                    return eachPartnerServiceMapping.ServiceType.ServiceTypeID == ServiceTypeID.RemitToAccount;
                });


            if (partnerServiceMapping == null)
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(36);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
            else
            {
                // Proceed
            }
            #endregion

            #region Validate bank code
            PeraLinkCoreWcf.Partner partnerBank = new PeraLinkCoreWcf.Partner();
            partnerBank.PartnerCode = bankPartnerCode;
            partnerBank = serviceClient.FindPartnerCode(partnerBank);

            if (partnerBank.PartnerID == 0)
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(38);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
            else { }

            if (partnerBank.Activated) { }
            else
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(39);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }

            PeraLinkCoreWcf.Agent partnerSubBank = new PeraLinkCoreWcf.Agent();
            partnerSubBank.AgentCode = bankAgentCode;
            partnerSubBank.Partner = partnerBank;
            partnerSubBank = serviceClient.FindAgentCodeOfPartner(partnerSubBank);

            if (partnerSubBank.AgentID == 0)
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(38);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
            else
            {
                // Proceed
            }

            if (partnerSubBank.Partner.PartnerID == partnerBank.PartnerID)
            { }
            else
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(38);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }

            if (partnerSubBank.Activated) { }
            else
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(39);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
            #endregion

            #region Validate partner pairing

            PeraLinkCoreWcf.PartnerServicePair[] partnerServicePairCollection = serviceClient.GetPartnerServicePairCollection(partnerServiceMapping);

            PeraLinkCoreWcf.PartnerServicePair pairing = Array.Find<PeraLinkCoreWcf.PartnerServicePair>(partnerServicePairCollection
                , delegate(PeraLinkCoreWcf.PartnerServicePair eachPartnerServicePair)
                {
                    if (eachPartnerServicePair.PairedPartner == null)
                    {
                        PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(40);
                        throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                    }
                    else
                    {
                        return eachPartnerServicePair.PairedPartner.PartnerID == partnerBank.PartnerID;
                    }
                });

            if (pairing == null)
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(38);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
            else
            {
                //    Proceed
            }

            if (pairing.PairedPartner.Activated)
            { }
            else
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(39);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
            #endregion

			PeraLinkCoreWcf.EMoneyProfile eMoneyProfile = new PeraLinkCoreWcf.EMoneyProfile();
			if (partner.SourceOfFundID == SourceOfFundID.EMoney)
			{
				#region Get E-Money Profile
				PeraLinkCoreWcf.GetEMoneyProfileRequest request = new PeraLinkCoreWcf.GetEMoneyProfileRequest();
				request.Partner = partner;

				eMoneyProfile = serviceClient.GetEMoneyProfile(request).EMoneyProfile;

				if (string.IsNullOrWhiteSpace(eMoneyProfile.AccountNumber))
				{
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(2);
					throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
				}

				if (string.IsNullOrWhiteSpace(eMoneyProfile.ClientNumber))
				{
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(3);
					throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
				}

				#endregion

				#region Validate PIN
				CustomerPortalWSClient customerPortalWSClient = new CustomerPortalWSClient();

				string pin = customerPortalWSClient.GetEnrolledCustomer(eMoneyProfile.ClientNumber);
				if (String.IsNullOrWhiteSpace(pin))
				{
					PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(52);
					throw new RDFramework.ClientException(message.Content, message.MessageID);
				}

				if (AESCrypt.EncryptDecrypt(pin, "Decrypt") != this.Pin || String.IsNullOrWhiteSpace(this.Pin))
				{
					PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(53);
					throw new RDFramework.ClientException(message.Content, message.MessageID);
				}
				#endregion

				#region Validate Balance
				EMoneyWSClient eMoneyServiceClient = new EMoneyWSClient();
#if CONVERTION_TO_BUSINESS_ACCOUNT
                decimal balance = eMoneyServiceClient.EMoneyInquiry(eMoneyProfile.AccountNumber).AvailableBalance;
#else
                decimal balance = eMoneyServiceClient.RetrieveAccountDetails(eMoneyProfile.ClientNumber).DcAvailableBalance;
#endif

				if ((balance - this.Amount) < 0)
				{
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(6);
					throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
				}
				#endregion
			}

			#region ValidateCapping
			if (partner.RemitToAccountCapAmount > 0)
			{
				PeraLinkCoreWcf.GetPartnerTotalRemitToAccountAmountRequest GetPartnerTotalRemitToAccountAmountRequest = new PeraLinkCoreWcf.GetPartnerTotalRemitToAccountAmountRequest();
				GetPartnerTotalRemitToAccountAmountRequest.Partner = partner;
				GetPartnerTotalRemitToAccountAmountRequest.StartDate = DateTime.Today;
				GetPartnerTotalRemitToAccountAmountRequest.EndDate = DateTime.Today.AddDays(1);

				PeraLinkCoreWcf.GetPartnerTotalRemitToAccountAmountResult GetPartnerTotalRemitToAccountAmountResult = new PeraLinkCoreWcf.GetPartnerTotalRemitToAccountAmountResult();
				GetPartnerTotalRemitToAccountAmountResult = serviceClient.GetPartnerTotalRemitToAccountAmount(GetPartnerTotalRemitToAccountAmountRequest);

				decimal totalAmount = this.Amount + GetPartnerTotalRemitToAccountAmountResult.RemitToAccountTotalAmount;

				if (partner.RemitToAccountCapAmount < totalAmount)
				{
					decimal remainingAmount = partner.RemitToAccountCapAmount - GetPartnerTotalRemitToAccountAmountResult.RemitToAccountTotalAmount;
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(5);
					throw new RDFramework.ClientException(string.Format(message.Description, remainingAmount), message.SystemMessageID);
				}
				else
				{
					/* Do nothing*/
				}
			}
			
			if (agent.CapAmount > 0)
			{
				PeraLinkCoreWcf.GetAgentTotalTransactionAmountRequest getAgentTotalTransactionAmountRequest = new PeraLinkCoreWcf.GetAgentTotalTransactionAmountRequest();
				getAgentTotalTransactionAmountRequest.Agent = agent;
				getAgentTotalTransactionAmountRequest.StartDate = DateTime.Today;
				getAgentTotalTransactionAmountRequest.EndDate = DateTime.Today.AddDays(1);

				PeraLinkCoreWcf.GetAgentTotalTransactionAmountResult getAgentTotalTransactionAmountResult = new PeraLinkCoreWcf.GetAgentTotalTransactionAmountResult();
				getAgentTotalTransactionAmountResult = serviceClient.GetAgentTotalTransactionAmount(getAgentTotalTransactionAmountRequest);

				decimal totalAmount = this.Amount + getAgentTotalTransactionAmountResult.TotalTransactionAmount;

				if (agent.CapAmount < totalAmount)
				{
					decimal remainingAmount = agent.CapAmount - getAgentTotalTransactionAmountResult.TotalTransactionAmount;
					remainingAmount = (remainingAmount < 0) ? 0 : remainingAmount;
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(102);
					throw new RDFramework.ClientException(string.Format(message.Description, remainingAmount), message.SystemMessageID);
				}
			}
			#endregion

            #region Validate currency
            //PeraLinkCoreWcf.Currency rtaCurrency = new PeraLinkCoreWcf.Currency();
            //rtaCurrency.CurrencyID = this.SendCurrencyID;
            // TODO: Get currency details from DB

            if (this.SendCurrencyID == 6)
            {
                // do nothing
            }
            else
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(41);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
            #endregion

            #region Validate Bank Details
            Bank bank = new Bank();

            PeraLinkCoreWcf.Bank coreBank = new PeraLinkCoreWcf.Bank();
            coreBank.BankCode = this.BankCode;
            coreBank = serviceClient.GetBank(coreBank);
            bank.Load(coreBank);

            if (bank.AccountNumberMinLength.HasValue && bank.AccountNumberMaxLength.HasValue)
            {
                if (bank.AccountNumberMinLength.Value == bank.AccountNumberMaxLength.Value)
                {
                    if (!RDFramework.Utility.Validation.IsCharacterLengthValid(this.AccountNumber, true, bank.AccountNumberMinLength.Value))
                    {
                        PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(60);
						throw new RDFramework.ClientException(String.Format(message.Description, bank.AccountNumberMinLength.Value.ToString()), message.SystemMessageID);
                    }
                }
                else if (bank.AccountNumberMinLength.Value < bank.AccountNumberMaxLength.Value)
                {
                    if (!RDFramework.Utility.Validation.IsCharacterLengthValid(this.AccountNumber, bank.AccountNumberMinLength.Value, bank.AccountNumberMaxLength.Value))
                    {
                        PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(61);
						throw new RDFramework.ClientException(String.Format(message.Description, bank.AccountNumberMinLength.Value.ToString(), bank.AccountNumberMaxLength.Value.ToString()), message.SystemMessageID);
                    }
                }
                else if (bank.AccountNumberMinLength.Value > bank.AccountNumberMaxLength.Value)
                {
                    throw SystemUtility.BuildClientError(58);
                }
            }
            else if (bank.AccountNumberMinLength.HasValue)
            {
                if (!RDFramework.Utility.Validation.IsCharacterLengthValid(this.AccountNumber, false, bank.AccountNumberMinLength.Value))
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(62);
					throw new RDFramework.ClientException(String.Format(message.Description, bank.AccountNumberMinLength.Value.ToString()), message.SystemMessageID);
                }
            }
            else if (bank.AccountNumberMaxLength.HasValue)
            {
                if (!RDFramework.Utility.Validation.IsCharacterLengthValid(this.AccountNumber, 0, bank.AccountNumberMaxLength.Value))
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(63);
					throw new RDFramework.ClientException(String.Format(message.Description, bank.AccountNumberMaxLength.Value.ToString()), message.SystemMessageID);
                }
            }

            if (!string.IsNullOrEmpty(bank.RegExValidator))
            {
                Regex regEx = new Regex(bank.RegExValidator);

                if (!regEx.IsMatch(this.AccountNumber))
                {
                    throw SystemUtility.BuildClientError(64);
                }
            }

            // validate with bank.MinAmount
            // validate with bank.MaxAmount
            if (bank.MinAmount.HasValue && bank.MaxAmount.HasValue)
            {
                if (bank.MinAmount.Value == bank.MaxAmount.Value)
                {
                    if (bank.MinAmount.Value != this.Amount)
                    {
                        PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(65);
						throw new RDFramework.ClientException(String.Format(message.Description, bank.MinAmount.Value.ToString()), message.SystemMessageID);
                    }
                }
                else if (bank.MinAmount.Value < bank.MaxAmount.Value)
                {
                    if (bank.MinAmount.Value > this.Amount || bank.MaxAmount.Value < this.Amount)
                    {
                        PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(66);
                        message.Description = String.Format(message.Description, bank.MinAmount.Value.ToString(), bank.MaxAmount.Value.ToString());
                        throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                    }
                }
                else if (bank.MinAmount.Value > bank.MaxAmount.Value)
                {
                    throw SystemUtility.BuildClientError(59);
                }
            }
            else if (bank.MinAmount.HasValue)
            {
                if (bank.MinAmount.Value > this.Amount)
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(67);
					throw new RDFramework.ClientException(String.Format(message.Description, bank.MinAmount.Value.ToString()), message.SystemMessageID);
                }
            }
            else if (bank.AccountNumberMaxLength.HasValue)
            {
                if (bank.MaxAmount.Value < this.Amount)
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(68);
					throw new RDFramework.ClientException(String.Format(message.Description, bank.MaxAmount.Value.ToString()), message.SystemMessageID);
                }
            }
            #endregion

            #region Get client details
            PeraLinkCoreWcf.Client remitter = new PeraLinkCoreWcf.Client();
            remitter.ClientID = this.ClientID;
            remitter = serviceClient.GetClient(remitter);
            #endregion

			#region Validate Partner Service Fee
			PeraLinkCoreWcf.CreateRemitToAccountTransactionRequest createRemitToAccountTransactionRequest = new PeraLinkCoreWcf.CreateRemitToAccountTransactionRequest();
			createRemitToAccountTransactionRequest.RemitToAccountTransaction = new PeraLinkCoreWcf.RemitToAccountTransaction();

			GetRemitToAccountServiceFeeRequest feeRequest = new GetRemitToAccountServiceFeeRequest();
			feeRequest.AgentCode = this.AgentCode;
			feeRequest.PartnerCode = this.PartnerCode;
			feeRequest.Token = this.Token;
			feeRequest.CurrencyID = this.SendCurrencyID;
			feeRequest.ServiceCredential = this.ServiceCredential;
			feeRequest.PrincipalAmount = this.Amount;

			GetRemitToAccountServiceFeeResult feeResult = new GetRemitToAccountServiceFeeResult();

			try
			{
				feeResult = feeRequest.Process();
				createRemitToAccountTransactionRequest.RemitToAccountTransaction.ServiceFee = feeResult.ServiceFee;
			}
			catch (RDFramework.ClientException clientEx)
			{
				if (clientEx.MessageID == SystemResource.GetMessageWithListID(28).MessageID)
				{
					createRemitToAccountTransactionRequest.RemitToAccountTransaction.ServiceFee = this.ServiceFee;
				}
				else
				{
					throw;
				}
			}
			#endregion

            #region Insert to Database
            PeraLinkCoreWcf.AuditTrail auditTrail = new PeraLinkCoreWcf.AuditTrail();
            auditTrail.CreatedByAgent = agent;
            auditTrail.CreatedByUserID = this.UserID;
            auditTrail.DateTimeCreated = DateTime.Now;
            auditTrail.IPAddress = SystemUtility.GetClientIPAddress();

            createRemitToAccountTransactionRequest.RemitToAccountTransaction.AccountNumber = this.AccountNumber;
            createRemitToAccountTransactionRequest.RemitToAccountTransaction.AgentBank = partnerSubBank;
            createRemitToAccountTransactionRequest.RemitToAccountTransaction.BeneficiaryAddress = this.AccountHolderHouseStreet;
            createRemitToAccountTransactionRequest.RemitToAccountTransaction.BeneficiaryFirstName = this.AccountHolderFirstName;
            createRemitToAccountTransactionRequest.RemitToAccountTransaction.BeneficiaryLastName = this.AccountHolderLastName;
            createRemitToAccountTransactionRequest.RemitToAccountTransaction.BeneficiaryMiddleName = this.AccountHolderMiddleName;
            createRemitToAccountTransactionRequest.RemitToAccountTransaction.BeneficiaryMobileNumber = this.AccountHolderMobileNumber;
            createRemitToAccountTransactionRequest.RemitToAccountTransaction.DateTimeSent = DateTime.Now;
            createRemitToAccountTransactionRequest.RemitToAccountTransaction.SendCurrency = new PeraLinkCoreWcf.Currency();
            createRemitToAccountTransactionRequest.RemitToAccountTransaction.SendCurrency.CurrencyID = this.SendCurrencyID;
            createRemitToAccountTransactionRequest.RemitToAccountTransaction.PrincipalAmount = this.Amount;
            createRemitToAccountTransactionRequest.RemitToAccountTransaction.SenderClient = remitter;
            createRemitToAccountTransactionRequest.RemitToAccountTransaction.SentByAgent = agent;
            createRemitToAccountTransactionRequest.RemitToAccountTransaction.SentByUserID = this.UserID;
			createRemitToAccountTransactionRequest.RemitToAccountTransaction.SentByTerminal = terminal;
            createRemitToAccountTransactionRequest.AuditTrail = auditTrail;

            PeraLinkCoreWcf.CreateRemitToAccountTransactionResult createRemitToAccountTransactionResult = serviceClient.CreateRemitToAccountTransaction(createRemitToAccountTransactionRequest);
            returnValue.ReferenceNumber = createRemitToAccountTransactionResult.RemitToAccountTransaction.ControlNumber;
            #endregion

            #region Remit to Account
            PeraLinkRemitToAccountWcf.RemitToAccount remitToAccountDetails = new PeraLinkRemitToAccountWcf.RemitToAccount();
            remitToAccountDetails.AccountNumber = this.AccountNumber;
            remitToAccountDetails.BeneficiaryAddress1 = this.AccountHolderHouseStreet;
            remitToAccountDetails.BeneficiaryFirstName = this.AccountHolderFirstName;
            remitToAccountDetails.BeneficiaryLastName = this.AccountHolderLastName;
            remitToAccountDetails.BeneficiaryMiddleName = this.AccountHolderMiddleName;
            remitToAccountDetails.OriginalAmount = this.Amount;
            remitToAccountDetails.OriginalCurrency = "PHP"; //TODO: Get currency from data, not hard coded
            remitToAccountDetails.Rate = 1;
            remitToAccountDetails.NetAmount = this.Amount * remitToAccountDetails.Rate;
            remitToAccountDetails.NetCurrency = "PHP";
            remitToAccountDetails.RemitterAddress1 = remitter.Address;
            remitToAccountDetails.RemitterCountry = "PH"; //TODO: Get country code (iso alpha-2) from db
            remitToAccountDetails.RemitterFirstName = remitter.FirstName;
            remitToAccountDetails.RemitterLastName = remitter.LastName;
            remitToAccountDetails.RemitterMiddleName = remitter.MiddleName;
            remitToAccountDetails.SourceOfFundsCode = String.Empty;
            remitToAccountDetails.ReferenceNumber = returnValue.ReferenceNumber;

            RemitToAccountTransaction remitToAccountTransaction = new RemitToAccountTransaction();
            remitToAccountTransaction.DateTimeSent = createRemitToAccountTransactionRequest.RemitToAccountTransaction.DateTimeSent;
            remitToAccountTransaction.ControlNumber = returnValue.ReferenceNumber;
            remitToAccountTransaction.AccountNumber = this.AccountNumber;
            remitToAccountTransaction.SenderFirstName = remitter.FirstName;
            remitToAccountTransaction.SenderLastName = remitter.LastName;
            remitToAccountTransaction.BeneficiaryFirstName = this.AccountHolderFirstName;
            remitToAccountTransaction.BeneficiaryLastName = this.AccountHolderLastName;
            remitToAccountTransaction.PrincipalAmount = this.Amount;
            remitToAccountTransaction.SentByPartnerCode = this.PartnerCode;
            remitToAccountTransaction.SentByAgentCode = this.AgentCode;
			remitToAccountTransaction.SentByTerminalCode = this.TerminalCode;
			

            PeraLinkCoreWcf.UpdateRemitToAccountTransactionStatusRequest updateRemitToAccountTransactionStatusRequest = new PeraLinkCoreWcf.UpdateRemitToAccountTransactionStatusRequest();
            updateRemitToAccountTransactionStatusRequest.AuditTrail = auditTrail;
            updateRemitToAccountTransactionStatusRequest.RemitToAccountTransaction = createRemitToAccountTransactionResult.RemitToAccountTransaction;

            try
            {
                if (bankPartnerCode.ToUpper() == SystemSetting.MetrobankPartnerCode)
                {
                    PeraLinkRemitToAccountWcf.MetrobankTransactionRequest metrobankTransactionRequest = new PeraLinkRemitToAccountWcf.MetrobankTransactionRequest();
                    metrobankTransactionRequest.RemitToAccount = remitToAccountDetails;
                    metrobankTransactionRequest.BeneficiaryMobileOrTelNumber = AccountHolderMobileNumber;

                    PeraLinkRemitToAccountWcf.MetrobankTransactionResult metrobankTransactionResult = new PeraLinkRemitToAccountWcf.MetrobankTransactionResult();
                    using (PeraLinkRemitToAccountWcfClient peraLinkRemitToAccount = new PeraLinkRemitToAccountWcfClient())
                    {
                        metrobankTransactionResult = peraLinkRemitToAccount.MetrobankTransaction(metrobankTransactionRequest);

                        updateRemitToAccountTransactionStatusRequest.RemitToAccountTransaction.PartnerReferenceNumber = metrobankTransactionResult.ReferenceNumber;
                        updateRemitToAccountTransactionStatusRequest.RemitToAccountTransaction.PartnerStatusCode = metrobankTransactionResult.ResponseCode;

                        switch (metrobankTransactionResult.ResultStatus)
                        {
                            case ResultStatus.Successful:
                                {
                                    if (metrobankTransactionResult.SuspectedTransaction)
                                    {
                                        if (metrobankTransactionResult.ResponseCode == "TMOUT")
                                        {
                                            updateRemitToAccountTransactionStatusRequest.RemitToAccountTransaction.RemitToAccountStatus.RemitToAccountStatusID = RemitToAccountStatusID.SuccessfulDueToTimeout;
                                        }

                                        SystemUtility.SendEmailAdvisory(remitToAccountTransaction, string.Format("{0}: {1}", metrobankTransactionResult.ResponseCode, metrobankTransactionResult.Message));
                                    }
                                    else
                                    {
                                        updateRemitToAccountTransactionStatusRequest.RemitToAccountTransaction.RemitToAccountStatus.RemitToAccountStatusID = RemitToAccountStatusID.Successful;
                                    }
                                    break;
                                }
                            case ResultStatus.Failed:
                            case ResultStatus.Error:
                                {
                                    updateRemitToAccountTransactionStatusRequest.RemitToAccountTransaction.RemitToAccountStatus.RemitToAccountStatusID = RemitToAccountStatusID.Failed;
                                    break;
                                }
                        }
                    }
                }
                else if (bankPartnerCode.ToUpper() == SystemSetting.SecurityBankPartnerCode)
                {
                    PeraLinkRemitToAccountWcf.SecurityBankTransactionRequest securityBankTransactionRequest = new PeraLinkRemitToAccountWcf.SecurityBankTransactionRequest();
                    securityBankTransactionRequest.BankCode = partnerSubBank.AgentCode;
                    securityBankTransactionRequest.BeneficiaryCity = this.AccountHolderCityProvince;
                    securityBankTransactionRequest.RemitterCity = remitter.ProvinceAddress;
                    securityBankTransactionRequest.RemitToAccount = remitToAccountDetails;

                    PeraLinkRemitToAccountWcf.SecurityBankTransactionResult securityBankTransactionResult = new PeraLinkRemitToAccountWcf.SecurityBankTransactionResult();
                    using (PeraLinkRemitToAccountWcfClient peraLinkRemitToAccount = new PeraLinkRemitToAccountWcfClient())
                    {
						try
						{
                        securityBankTransactionResult = peraLinkRemitToAccount.SecurityBankTransaction(securityBankTransactionRequest);
						}
						catch (TimeoutException error)
						{
							securityBankTransactionResult.SuspectedTransaction = true;
							securityBankTransactionResult.ResponseCode = "TMOUT";
							RDFramework.Utility.EventLog.SaveWarning(error.Message);
						}

                        updateRemitToAccountTransactionStatusRequest.RemitToAccountTransaction.PartnerReferenceNumber = securityBankTransactionResult.TransactionNumber;
                        updateRemitToAccountTransactionStatusRequest.RemitToAccountTransaction.PartnerStatusCode = securityBankTransactionResult.ResponseCode;

                        switch (securityBankTransactionResult.ResultStatus)
                        {
                            case ResultStatus.Successful:
                                {
                                    if (securityBankTransactionResult.SuspectedTransaction)
                                    {
                                        if (securityBankTransactionResult.ResponseCode == "TMOUT")
                                            updateRemitToAccountTransactionStatusRequest.RemitToAccountTransaction.RemitToAccountStatus.RemitToAccountStatusID = RemitToAccountStatusID.SuccessfulDueToTimeout;
                                        else
                                            updateRemitToAccountTransactionStatusRequest.RemitToAccountTransaction.RemitToAccountStatus.RemitToAccountStatusID = RemitToAccountStatusID.SuccessfulWithIssue;

                                        SystemUtility.SendEmailAdvisory(remitToAccountTransaction, string.Format("{0}: {1}", securityBankTransactionResult.ResponseCode, securityBankTransactionResult.Message));
                                    }
                                    else
                                    {
                                        updateRemitToAccountTransactionStatusRequest.RemitToAccountTransaction.RemitToAccountStatus.RemitToAccountStatusID = RemitToAccountStatusID.Successful;
                                    }
                                    break;
                                }
                            case ResultStatus.Failed:
                            case ResultStatus.Error:
                                {
                                    updateRemitToAccountTransactionStatusRequest.RemitToAccountTransaction.RemitToAccountStatus.RemitToAccountStatusID = RemitToAccountStatusID.Failed;
                                    break;
                                }
                        }
                    }
                }
                else
                {
                    //Bank has no remit to account API set.
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(37);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }

                PeraLinkCoreWcf.UpdateRemitToAccountTransactionStatusResult updateRemitToAccountTransactionStatusResult = serviceClient.UpdateRemitToAccountTransactionStatus(updateRemitToAccountTransactionStatusRequest);
            }
            catch (Exception error)
            {
                updateRemitToAccountTransactionStatusRequest.RemitToAccountTransaction.RemitToAccountStatus.RemitToAccountStatusID = RemitToAccountStatusID.Error;
                updateRemitToAccountTransactionStatusRequest.RemitToAccountTransaction.PartnerStatusCode = "NONE";
                PeraLinkCoreWcf.UpdateRemitToAccountTransactionStatusResult updateRemitToAccountTransactionStatusResult = serviceClient.UpdateRemitToAccountTransactionStatus(updateRemitToAccountTransactionStatusRequest);
                throw error;
            }

            #endregion
        }

        return returnValue;
    }
    #endregion
}