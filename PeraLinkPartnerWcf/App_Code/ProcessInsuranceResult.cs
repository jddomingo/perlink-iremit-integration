﻿#define INSURANCE_MICARE

#if INSURANCE_MICARE
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ProcessMiCareInsuranceResult
/// </summary>
public class ProcessInsuranceResult : ServiceResult
{
    #region Constructor
    public ProcessInsuranceResult()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion

    #region Fields/Properties
    private string _referenceNumber;

    public string ReferenceNumber
    {
        get { return _referenceNumber; }
        set { _referenceNumber = value; }
    }

    private DateTime _dateTimeProcessed;
    public DateTime DateTimeProcessed
    {
        get { return _dateTimeProcessed; }
        set { _dateTimeProcessed = value; }
    }

    private decimal _adminFee;
    public decimal AdminFee
    {
        get { return _adminFee; }
        set { _adminFee = value; }
    }

    private decimal _serviceFee;
    public decimal ServiceFee
    {
        get { return _serviceFee; }
        set { _serviceFee = value; }
    }

    private string _peraLinkReferenceNumber;
    public string PeraLinkReferenceNumber
    {
        get { return _peraLinkReferenceNumber; }
        set { _peraLinkReferenceNumber = value; }
    }

    #endregion
}
#else
#endif