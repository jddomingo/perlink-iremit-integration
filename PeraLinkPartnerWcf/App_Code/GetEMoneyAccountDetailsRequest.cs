#define CONVERTION_TO_BUSINESS_ACCOUNT
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for GetEMoneyAccountDetailsRequest
/// </summary>
/// 

[DataContract]
public class GetEMoneyAccountDetailsRequest : BaseRequest
{
    #region Constructor
    public GetEMoneyAccountDetailsRequest()
    {
        //

        //
    }
    #endregion

    #region Fields/Properties
    private string _partnerCode;
    [DataMember]
    public string PartnerCode
    {
        get { return _partnerCode; }
        set { _partnerCode = value; }
    }

    private string _token;
    [DataMember]
    public string Token
    {
        get { return _token; }
        set { _token = value; }
    }
    #endregion

    #region Internal Method/s
    internal GetEMoneyAccountDetailsResult Process()
    {
        GetEMoneyAccountDetailsResult returnValue = new GetEMoneyAccountDetailsResult();
        PeraLinkCoreWcf.EMoneyProfile eMoneyProfile = new PeraLinkCoreWcf.EMoneyProfile();



        // Important: This function validates the request credential
        this.AuthenticateRequest();

       
        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
            #region Validate Partner Record
            PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
            partner.PartnerCode = this.PartnerCode;
            partner = serviceClient.FindPartnerCode(partner);

            if (partner.PartnerID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else { }

            if (partner.Token == this.Token) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (partner.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
        #endregion

            #region Get E-Money Profile
            PeraLinkCoreWcf.GetEMoneyProfileRequest profileRequest = new PeraLinkCoreWcf.GetEMoneyProfileRequest();
            profileRequest.Partner = partner;

            eMoneyProfile = serviceClient.GetEMoneyProfile(profileRequest).EMoneyProfile;

            if (string.IsNullOrWhiteSpace(eMoneyProfile.AccountNumber))
            {
                //NOTE: THROW ERROR
            }

            //commission

            PeraLinkCoreWcf.GetTotalCommissionRequest getTotalCommissionRequest = new PeraLinkCoreWcf.GetTotalCommissionRequest();
            getTotalCommissionRequest.EMoneyProfile = eMoneyProfile;

            try
            {
                PeraLinkCoreWcf.GetTotalCommissionResult getTotalCommissionResult = serviceClient.GetTotalCommission(getTotalCommissionRequest);
                returnValue.TotalCommission = getTotalCommissionResult.TotalCommission;
            }
            catch (Exception ex)
            {
               
                RDFramework.Utility.EventLog.SaveError(string.Format("Partner: {0}. {1}", partner.PartnerCode, ex.ToString()));

                returnValue.TotalCommission = -1;
            }

            #endregion
        }
        

        using (PeraLinkCoreWcfClient peralinkCoreWcfClient = new PeraLinkCoreWcfClient())
        {

            //commissionfee
            //returnValue.TotalCommission = peralinkCoreWcfClient.partn
        }

        #region E-Money Account Details
        using (EMoneyWSClient eMoneyServiceClient = new EMoneyWSClient())
        {
#if CONVERTION_TO_BUSINESS_ACCOUNT
            EMoneyWS.EMoneyInquiryDetails eMoneyDetails = eMoneyServiceClient.EMoneyInquiry(eMoneyProfile.AccountNumber);

            switch (eMoneyDetails.ResultCode)
            {
                case EMoneyWS.ResultCodes.Successful:
                    //returnValue.AccountName = eMoneyDetails..StrAccountName;
                    returnValue.AvailableBalance = eMoneyDetails.AvailableBalance;
                    returnValue.CurrentBalance = eMoneyDetails.CurrentBalance;
                    //returnValue.TotalCredit = accountInformation.DcTotalCreditedAmount;
                    //returnValue.TotalDebit = accountInformation.DcTotalEncashedAmount;

                    //returnValue.TotalCommission = eMoneyDetails.DcTotalCommission;
                    returnValue.ResultStatus = ResultStatus.Successful;
                    break;
                case EMoneyWS.ResultCodes.Failed:
                case EMoneyWS.ResultCodes.Error:
                default:
                    throw new RDFramework.ClientException(eMoneyDetails.ResultMessage);
            }
#else
            EMoneyWS.AccountInfo accountInformation = eMoneyServiceClient.RetrieveAccountDetails(eMoneyProfile.ClientNumber);

            switch (accountInformation.ResultCode)
            {
                case (int)EMoneyWS.ResultCodes.Successful:
                    returnValue.AccountName = accountInformation.StrAccountName;
                    returnValue.AvailableBalance = accountInformation.DcAvailableBalance;
                    returnValue.CurrentBalance = accountInformation.DcCurrentBalance;
                    returnValue.TotalCredit = accountInformation.DcTotalCreditedAmount;
                    returnValue.TotalDebit = accountInformation.DcTotalEncashedAmount;

                    //returnValue.TotalCommission = accountInformation.DcTotalCommission;
                    returnValue.ResultStatus = ResultStatus.Successful;
                    break;
                case (int)EMoneyWS.ResultCodes.Failed:
                case (int)EMoneyWS.ResultCodes.Error:
                default:
                    throw new RDFramework.ClientException(accountInformation.ResultMessage);
            }
#endif
        }
        #endregion

        return returnValue;
    }

    #endregion
}