using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TelcoCollection
/// </summary>
public class TelcoCollection: List<Telco>
{
    #region Constructor
    public TelcoCollection()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion

    #region Public Methods
    public void AddList(PeraLinkELoadGatewayWcf.Telco[] telcoCollection)
    {
        Array.ForEach<PeraLinkELoadGatewayWcf.Telco>(telcoCollection
            , delegate(PeraLinkELoadGatewayWcf.Telco eachTelco)
            {
                Telco mappedTelco = new Telco();
                mappedTelco.Load(eachTelco);
                this.Add(mappedTelco);
            });
    }
    #endregion
}