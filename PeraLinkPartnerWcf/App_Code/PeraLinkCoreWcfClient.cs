#define INTERNATIONAL_COMMISSION_FEE
#define PeraLink_Agent_Insurance_Renewal

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

/// <summary>
/// Summary description for PeraLinkCoreWcfClient
/// </summary>
public class PeraLinkCoreWcfClient : IDisposable
{
    #region Constructor
    public PeraLinkCoreWcfClient()
    {
        _serviceClient = new PeraLinkCoreWcf.ServiceClient();
    }
    #endregion

    #region Fields/Properties
    private PeraLinkCoreWcf.ServiceClient _serviceClient;
    #endregion

    #region Public Methods
    public PeraLinkCoreWcf.GetPartnerServiceMappingResult GetPartnerServiceMapping(PeraLinkCoreWcf.GetPartnerServiceMappingRequest getPartnerServiceMappingRequest)
    {
        getPartnerServiceMappingRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkCoreWcf.GetPartnerServiceMappingResult returnValue = _serviceClient.GetPartnerServiceMapping(getPartnerServiceMappingRequest);
        switch (returnValue.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return returnValue;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(returnValue.Message, returnValue.MessageID, returnValue.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(returnValue.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.GetSystemMessageResult GetSystemMessage(PeraLinkCoreWcf.GetSystemMessageRequest getSystemMessageRequest)
    {
        getSystemMessageRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkCoreWcf.GetSystemMessageResult returnValue = _serviceClient.GetSystemMessage(getSystemMessageRequest);
        switch (returnValue.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return returnValue;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(returnValue.Message, returnValue.MessageID, returnValue.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(returnValue.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.GetEMoneyProfileResult GetEMoneyProfile(PeraLinkCoreWcf.GetEMoneyProfileRequest getEMoneyProfileRequest)
    {
        getEMoneyProfileRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkCoreWcf.GetEMoneyProfileResult returnValue = _serviceClient.GetEMoneyProfile(getEMoneyProfileRequest);
        switch (returnValue.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return returnValue;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(returnValue.Message, returnValue.MessageID, returnValue.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(returnValue.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.Agent GetAgent(PeraLinkCoreWcf.Agent agent)
    {
        PeraLinkCoreWcf.GetAgentRequest getAgentRequest = new PeraLinkCoreWcf.GetAgentRequest();
        getAgentRequest.PassKey = Cryptor.GeneratePassKey();
        getAgentRequest.Agent = agent;

        PeraLinkCoreWcf.GetAgentResult getAgentResult = _serviceClient.GetAgent(getAgentRequest);


        switch (getAgentResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getAgentResult.Agent;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getAgentResult.Message, getAgentResult.MessageID, getAgentResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getAgentResult.ResultStatus.ToString());
                }
        }

    }

    public PeraLinkCoreWcf.GetAgentAccessDetailResult GetAgentAccessDetail(PeraLinkCoreWcf.AgentAccessDetail agentAccessDetail)
    {
        PeraLinkCoreWcf.GetAgentAccessDetailRequest getAgentAccessDetailRequest = new PeraLinkCoreWcf.GetAgentAccessDetailRequest();
        getAgentAccessDetailRequest.PassKey = Cryptor.GeneratePassKey();
        getAgentAccessDetailRequest.AgentAccessDetail = agentAccessDetail;

        PeraLinkCoreWcf.GetAgentAccessDetailResult getAgentAccessDetailResult = _serviceClient.GetAgentAccessDetail(getAgentAccessDetailRequest);


        switch (getAgentAccessDetailResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getAgentAccessDetailResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getAgentAccessDetailResult.Message, getAgentAccessDetailResult.MessageID, getAgentAccessDetailResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getAgentAccessDetailResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.PartnerServiceFee GetServiceFee(PeraLinkCoreWcf.GetServiceFeeRequest getServiceFeeRequest)
    {
        getServiceFeeRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkCoreWcf.GetServiceFeeResult getServiceFeeResult = _serviceClient.GetServiceFee(getServiceFeeRequest);

        switch (getServiceFeeResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getServiceFeeResult.PartnerServiceFee;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getServiceFeeResult.Message, getServiceFeeResult.MessageID, getServiceFeeResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getServiceFeeResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.Currency FindCurrency(PeraLinkCoreWcf.Currency currency)
    {
        PeraLinkCoreWcf.FindCurrencyRequest findCurrencyRequest = new PeraLinkCoreWcf.FindCurrencyRequest();
        findCurrencyRequest.PassKey = Cryptor.GeneratePassKey();
        findCurrencyRequest.Currency = currency;

        PeraLinkCoreWcf.FindCurrencyResult findCurrencyResult = _serviceClient.FindCurrency(findCurrencyRequest);

        switch (findCurrencyResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return findCurrencyResult.Currency;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(findCurrencyResult.Message, findCurrencyResult.MessageID, findCurrencyResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(findCurrencyResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.Partner FindPartnerCode(PeraLinkCoreWcf.Partner partner)
    {
        PeraLinkCoreWcf.FindPartnerCodeRequest findPartnerCodeRequest = new PeraLinkCoreWcf.FindPartnerCodeRequest();
        findPartnerCodeRequest.PassKey = Cryptor.GeneratePassKey();
        findPartnerCodeRequest.Partner = partner;

        PeraLinkCoreWcf.FindPartnerCodeResult findPartnerCodeResult = _serviceClient.FindPartnerCode(findPartnerCodeRequest);

        switch (findPartnerCodeResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return findPartnerCodeResult.Partner;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(findPartnerCodeResult.Message, findPartnerCodeResult.MessageID, findPartnerCodeResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(findPartnerCodeResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.Beneficiary GetBeneficiary(PeraLinkCoreWcf.Beneficiary beneficiary)
    {
        PeraLinkCoreWcf.GetBeneficiaryRequest getBeneficiaryRequest = new PeraLinkCoreWcf.GetBeneficiaryRequest();
        getBeneficiaryRequest.PassKey = Cryptor.GeneratePassKey();
        getBeneficiaryRequest.Beneficiary = beneficiary;

        PeraLinkCoreWcf.GetBeneficiaryResult getBeneficiaryResult = _serviceClient.GetBeneficiary(getBeneficiaryRequest);

        switch (getBeneficiaryResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getBeneficiaryResult.Beneficiary;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getBeneficiaryResult.Message, getBeneficiaryResult.MessageID, getBeneficiaryResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getBeneficiaryResult.ResultStatus.ToString());
                }
        }
    }

    public void UpdateBeneficiary(PeraLinkCoreWcf.Beneficiary beneficiary)
    {
        PeraLinkCoreWcf.UpdateBeneficiaryRequest updateBeneficiaryRequest = new PeraLinkCoreWcf.UpdateBeneficiaryRequest();
        updateBeneficiaryRequest.PassKey = Cryptor.GeneratePassKey();
        updateBeneficiaryRequest.Beneficiary = beneficiary;

        PeraLinkCoreWcf.UpdateBeneficiaryResult updateBeneficiaryResult = _serviceClient.UpdateBeneficiary(updateBeneficiaryRequest);

        switch (updateBeneficiaryResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(updateBeneficiaryResult.Message, updateBeneficiaryResult.MessageID, updateBeneficiaryResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(updateBeneficiaryResult.ResultStatus.ToString());
                }
        }
    }

    public void UpdateClient(PeraLinkCoreWcf.Client client)
    {
        PeraLinkCoreWcf.UpdateClientRequest updateClientRequest = new PeraLinkCoreWcf.UpdateClientRequest();
        updateClientRequest.PassKey = Cryptor.GeneratePassKey();
        updateClientRequest.Client = client;

        PeraLinkCoreWcf.UpdateClientResult updateClientResult = _serviceClient.UpdateClient(updateClientRequest);

        switch (updateClientResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(updateClientResult.Message, updateClientResult.MessageID, updateClientResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(updateClientResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.Country FindCountry(PeraLinkCoreWcf.Country country)
    {
        PeraLinkCoreWcf.FindCountryRequest findCountryRequest = new PeraLinkCoreWcf.FindCountryRequest();
        findCountryRequest.PassKey = Cryptor.GeneratePassKey();
        findCountryRequest.Country = country;
        PeraLinkCoreWcf.FindCountryResult findCountryResult = _serviceClient.FindCountry(findCountryRequest);

        switch (findCountryResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return findCountryResult.Country;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(findCountryResult.Message, findCountryResult.MessageID, findCountryResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(findCountryResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.State FindState(PeraLinkCoreWcf.State state)
    {
        PeraLinkCoreWcf.FindStateRequest findStateRequest = new PeraLinkCoreWcf.FindStateRequest();
        findStateRequest.PassKey = Cryptor.GeneratePassKey();
        findStateRequest.State = state;
        PeraLinkCoreWcf.FindStateResult findStateResult = _serviceClient.FindState(findStateRequest);

        switch (findStateResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return findStateResult.State;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(findStateResult.Message, findStateResult.MessageID, findStateResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(findStateResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.Country[] GetCountryCollection()
    {
        PeraLinkCoreWcf.GetCountryCollectionRequest getCountryCollectionRequest = new PeraLinkCoreWcf.GetCountryCollectionRequest();
        getCountryCollectionRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkCoreWcf.GetCountryCollectionResult getCountryCollectionResult = _serviceClient.GetCountryCollection(getCountryCollectionRequest);

        switch (getCountryCollectionResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getCountryCollectionResult.CountryCollection;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getCountryCollectionResult.Message, getCountryCollectionResult.MessageID, getCountryCollectionResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getCountryCollectionResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.State[] GetStateCollectionByCountry(PeraLinkCoreWcf.State stateCountry)
    {
        PeraLinkCoreWcf.GetStateCollectionByCountryRequest getStateCollectionByCountryRequest = new PeraLinkCoreWcf.GetStateCollectionByCountryRequest();
        getStateCollectionByCountryRequest.PassKey = Cryptor.GeneratePassKey();
        getStateCollectionByCountryRequest.CountryID = stateCountry;

        PeraLinkCoreWcf.GetStateCollectionByCountryResult getStateCollectionByCountryResult = _serviceClient.GetStateCollectionByCountry(getStateCollectionByCountryRequest);


        switch (getStateCollectionByCountryResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getStateCollectionByCountryResult.StateCollectionByCountry;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getStateCollectionByCountryResult.Message, getStateCollectionByCountryResult.MessageID, getStateCollectionByCountryResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getStateCollectionByCountryResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.Agent[] GetPartnerAgentCollection(PeraLinkCoreWcf.Partner partner)
    {
        PeraLinkCoreWcf.GetPartnerAgentCollectionRequest getPartnerAgentCollectionRequest = new PeraLinkCoreWcf.GetPartnerAgentCollectionRequest();
        getPartnerAgentCollectionRequest.PassKey = Cryptor.GeneratePassKey();
        getPartnerAgentCollectionRequest.Partner = partner;
        PeraLinkCoreWcf.GetPartnerAgentCollectionResult getPartnerAgentCollectionResult = _serviceClient.GetPartnerAgentCollection(getPartnerAgentCollectionRequest);

        switch (getPartnerAgentCollectionResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getPartnerAgentCollectionResult.AgentCollection;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getPartnerAgentCollectionResult.Message, getPartnerAgentCollectionResult.MessageID, getPartnerAgentCollectionResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getPartnerAgentCollectionResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.Partner[] GetDomesticPartnerWithoutPrefix()
    {
        PeraLinkCoreWcf.GetDomesticPartnerWithoutPrefixRequest getDomesticPartnerWithoutPrefixRequest = new PeraLinkCoreWcf.GetDomesticPartnerWithoutPrefixRequest();
        getDomesticPartnerWithoutPrefixRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkCoreWcf.GetDomesticPartnerWithoutPrefixResult getDomesticPartnerWithoutPrefixResult = _serviceClient.GetDomesticPartnerWithoutPrefix(getDomesticPartnerWithoutPrefixRequest);

        switch (getDomesticPartnerWithoutPrefixResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getDomesticPartnerWithoutPrefixResult.PartnerCollection;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getDomesticPartnerWithoutPrefixResult.Message, getDomesticPartnerWithoutPrefixResult.MessageID, getDomesticPartnerWithoutPrefixResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getDomesticPartnerWithoutPrefixResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.Client FindClientByBeneficiaryID(PeraLinkCoreWcf.Beneficiary beneficiary)
    {
        PeraLinkCoreWcf.FindClientByBeneficiaryIDRequest findClientByBeneficiaryIDRequest = new PeraLinkCoreWcf.FindClientByBeneficiaryIDRequest();
        findClientByBeneficiaryIDRequest.PassKey = Cryptor.GeneratePassKey();
        findClientByBeneficiaryIDRequest.Beneficiary = beneficiary;

        PeraLinkCoreWcf.FindClientByBeneficiaryIDResult findClientByBeneficiaryIDResult = _serviceClient.FindClientByBeneficiaryID(findClientByBeneficiaryIDRequest);

        switch (findClientByBeneficiaryIDResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return findClientByBeneficiaryIDResult.Client;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(findClientByBeneficiaryIDResult.Message, findClientByBeneficiaryIDResult.MessageID, findClientByBeneficiaryIDResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(findClientByBeneficiaryIDResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.Client GetClient(PeraLinkCoreWcf.Client client)
    {
        PeraLinkCoreWcf.GetClientRequest getClientRequest = new PeraLinkCoreWcf.GetClientRequest();
        getClientRequest.PassKey = Cryptor.GeneratePassKey();
        getClientRequest.Client = client;

        PeraLinkCoreWcf.GetClientResult getClientResult = _serviceClient.GetClient(getClientRequest);


        switch (getClientResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getClientResult.Client;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getClientResult.Message, getClientResult.MessageID, getClientResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getClientResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.PartnerServiceFee[] GetPartnerServiceFeeCollection(PeraLinkCoreWcf.PartnerServiceCurrency partnerServiceCurrency)
    {
        PeraLinkCoreWcf.GetPartnerServiceFeeCollectionRequest getPartnerServiceFeeCollectionRequest = new PeraLinkCoreWcf.GetPartnerServiceFeeCollectionRequest();
        getPartnerServiceFeeCollectionRequest.PassKey = Cryptor.GeneratePassKey();
        getPartnerServiceFeeCollectionRequest.PartnerServiceCurrency = partnerServiceCurrency;

        PeraLinkCoreWcf.GetPartnerServiceFeeCollectionResult getPartnerServiceFeeCollectionResult = _serviceClient.GetPartnerServiceFeeCollection(getPartnerServiceFeeCollectionRequest);


        switch (getPartnerServiceFeeCollectionResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getPartnerServiceFeeCollectionResult.PartnerServiceFeeCollection;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getPartnerServiceFeeCollectionResult.Message, getPartnerServiceFeeCollectionResult.MessageID, getPartnerServiceFeeCollectionResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getPartnerServiceFeeCollectionResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.PartnerServiceCurrency[] GetPartnerServiceCurrencyCollection(PeraLinkCoreWcf.PartnerServiceMapping partnerServiceMapping)
    {
        PeraLinkCoreWcf.GetPartnerServiceCurrencyCollectionRequest getPartnerServiceCurrencyCollectionRequest = new PeraLinkCoreWcf.GetPartnerServiceCurrencyCollectionRequest();
        getPartnerServiceCurrencyCollectionRequest.PassKey = Cryptor.GeneratePassKey();
        getPartnerServiceCurrencyCollectionRequest.PartnerServiceMapping = partnerServiceMapping;

        PeraLinkCoreWcf.GetPartnerServiceCurrencyCollectionResult getPartnerServiceCurrencyCollectionResult = _serviceClient.GetPartnerServiceCurrencyCollection(getPartnerServiceCurrencyCollectionRequest);


        switch (getPartnerServiceCurrencyCollectionResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getPartnerServiceCurrencyCollectionResult.PartnerServiceCurrencyCollection;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getPartnerServiceCurrencyCollectionResult.Message, getPartnerServiceCurrencyCollectionResult.MessageID, getPartnerServiceCurrencyCollectionResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getPartnerServiceCurrencyCollectionResult.ResultStatus.ToString());
                }
        }

    }

    public PeraLinkCoreWcf.PartnerServicePair[] GetPartnerServicePairCollection(PeraLinkCoreWcf.PartnerServiceMapping partnerServiceMapping)
    {
        PeraLinkCoreWcf.GetPartnerServicePairCollectionRequest getPartnerServicePairCollectionRequest = new PeraLinkCoreWcf.GetPartnerServicePairCollectionRequest();
        getPartnerServicePairCollectionRequest.PassKey = Cryptor.GeneratePassKey();
        getPartnerServicePairCollectionRequest.PartnerServiceMapping = partnerServiceMapping;

        PeraLinkCoreWcf.GetPartnerServicePairCollectionResult getPartnerServicePairCollectionResult = _serviceClient.GetPartnerServicePairCollection(getPartnerServicePairCollectionRequest);


        switch (getPartnerServicePairCollectionResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getPartnerServicePairCollectionResult.PartnerServicePairCollection;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getPartnerServicePairCollectionResult.Message, getPartnerServicePairCollectionResult.MessageID, getPartnerServicePairCollectionResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getPartnerServicePairCollectionResult.ResultStatus.ToString());
                }
        }

    }

    public PeraLinkCoreWcf.Message[] GetMessageCollection(PeraLinkCoreWcf.SystemName systemName)
    {
        PeraLinkCoreWcf.GetMessageCollectionRequest getMessageCollectionRequest = new PeraLinkCoreWcf.GetMessageCollectionRequest();
        getMessageCollectionRequest.PassKey = Cryptor.GeneratePassKey();
        getMessageCollectionRequest.SystemName = systemName;

        PeraLinkCoreWcf.GetMessageCollectionResult getMessageCollectionResult = _serviceClient.GetMessageCollection(getMessageCollectionRequest);

        switch (getMessageCollectionResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getMessageCollectionResult.MessageCollection;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getMessageCollectionResult.Message, getMessageCollectionResult.MessageID, getMessageCollectionResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getMessageCollectionResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.Agent FindAgentCodeOfPartner(PeraLinkCoreWcf.Agent agent)
    {
        PeraLinkCoreWcf.FindAgentCodeOfPartnerRequest findAgentCodeOfPartnerRequest = new PeraLinkCoreWcf.FindAgentCodeOfPartnerRequest();
        findAgentCodeOfPartnerRequest.PassKey = Cryptor.GeneratePassKey();
        findAgentCodeOfPartnerRequest.Agent = agent;

        PeraLinkCoreWcf.FindAgentCodeOfPartnerResult findAgentCodeOfPartnerResult = _serviceClient.FindAgentCodeOfPartner(findAgentCodeOfPartnerRequest);

        switch (findAgentCodeOfPartnerResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return findAgentCodeOfPartnerResult.Agent;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(findAgentCodeOfPartnerResult.Message, findAgentCodeOfPartnerResult.MessageID, findAgentCodeOfPartnerResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(findAgentCodeOfPartnerResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.Agent FindAgentCode(PeraLinkCoreWcf.Agent agent)
    {
        PeraLinkCoreWcf.FindAgentCodeRequest findAgentCodeRequest = new PeraLinkCoreWcf.FindAgentCodeRequest();
        findAgentCodeRequest.PassKey = Cryptor.GeneratePassKey();
        findAgentCodeRequest.Agent = agent;

        PeraLinkCoreWcf.FindAgentCodeResult findAgentCodeResult = _serviceClient.FindAgentCode(findAgentCodeRequest);

        switch (findAgentCodeResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return findAgentCodeResult.Agent;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(findAgentCodeResult.Message, findAgentCodeResult.MessageID, findAgentCodeResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(findAgentCodeResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.PartnerServiceMapping[] GetPartnerServiceMappingCollection(PeraLinkCoreWcf.Partner partner)
    {
        PeraLinkCoreWcf.GetPartnerServiceMappingCollectionRequest getPartnerServiceMappingCollectionRequest = new PeraLinkCoreWcf.GetPartnerServiceMappingCollectionRequest();
        getPartnerServiceMappingCollectionRequest.PassKey = Cryptor.GeneratePassKey();
        getPartnerServiceMappingCollectionRequest.Partner = partner;

        PeraLinkCoreWcf.GetPartnerServiceMappingCollectionResult getPartnerServiceMappingCollectionResult = _serviceClient.GetPartnerServiceMappingCollection(getPartnerServiceMappingCollectionRequest);

        switch (getPartnerServiceMappingCollectionResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getPartnerServiceMappingCollectionResult.PartnerServiceMappingCollection;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getPartnerServiceMappingCollectionResult.Message, getPartnerServiceMappingCollectionResult.MessageID, getPartnerServiceMappingCollectionResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getPartnerServiceMappingCollectionResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.Partner GetPartner(PeraLinkCoreWcf.Partner partner)
    {
        PeraLinkCoreWcf.GetPartnerRequest getPartnerRequest = new PeraLinkCoreWcf.GetPartnerRequest();
        getPartnerRequest.PassKey = Cryptor.GeneratePassKey();
        getPartnerRequest.Partner = partner;

        PeraLinkCoreWcf.GetPartnerResult getPartnerResult = _serviceClient.GetPartner(getPartnerRequest);

        switch (getPartnerResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getPartnerResult.Partner;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getPartnerResult.Message, getPartnerResult.MessageID, getPartnerResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getPartnerResult.ResultStatus.ToString());
                }
        }
    }

    public void RefundDomesticRemittance(PeraLinkCoreWcf.DomesticRemittance domesticRemittance)
    {
        PeraLinkCoreWcf.RefundDomesticRemittanceRequest refundDomesticRemittanceRequest = new PeraLinkCoreWcf.RefundDomesticRemittanceRequest();
        refundDomesticRemittanceRequest.PassKey = Cryptor.GeneratePassKey();
        refundDomesticRemittanceRequest.DomesticRemittance = domesticRemittance;

        PeraLinkCoreWcf.AuditTrail auditTrail = new PeraLinkCoreWcf.AuditTrail();
        auditTrail.CreatedByAgent = domesticRemittance.UpdatedByAgent;
        auditTrail.CreatedByUserID = domesticRemittance.UpdatedByUserID;
        auditTrail.DateTimeCreated = DateTime.Now;
        auditTrail.IPAddress = SystemUtility.GetClientIPAddress();
        refundDomesticRemittanceRequest.AuditTrail = auditTrail;

        PeraLinkCoreWcf.RefundDomesticRemittanceResult refundDomesticRemittanceResult = _serviceClient.RefundDomesticRemittance(refundDomesticRemittanceRequest);

        switch (refundDomesticRemittanceResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(refundDomesticRemittanceResult.Message, refundDomesticRemittanceResult.MessageID, refundDomesticRemittanceResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(refundDomesticRemittanceResult.ResultStatus.ToString());
                }
        }
    }

    public void CancelDomesticRemittance(PeraLinkCoreWcf.DomesticRemittance domesticRemittance)
    {
        PeraLinkCoreWcf.CancelDomesticRemittanceRequest cancelDomesticRemittanceRequest = new PeraLinkCoreWcf.CancelDomesticRemittanceRequest();
        cancelDomesticRemittanceRequest.PassKey = Cryptor.GeneratePassKey();
        cancelDomesticRemittanceRequest.DomesticRemittance = domesticRemittance;

        PeraLinkCoreWcf.AuditTrail auditTrail = new PeraLinkCoreWcf.AuditTrail();
        auditTrail.CreatedByAgent = domesticRemittance.UpdatedByAgent;
        auditTrail.CreatedByUserID = domesticRemittance.UpdatedByUserID;
        auditTrail.DateTimeCreated = DateTime.Now;
        auditTrail.IPAddress = SystemUtility.GetClientIPAddress();
        cancelDomesticRemittanceRequest.AuditTrail = auditTrail;

        PeraLinkCoreWcf.CancelDomesticRemittanceResult cancelDomesticRemittanceResult = _serviceClient.CancelDomesticRemittance(cancelDomesticRemittanceRequest);

        switch (cancelDomesticRemittanceResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(cancelDomesticRemittanceResult.Message, cancelDomesticRemittanceResult.MessageID, cancelDomesticRemittanceResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(cancelDomesticRemittanceResult.ResultStatus.ToString());
                }
        }
    }

    public void CancelAndDeleteDomesticRemittance(PeraLinkCoreWcf.DomesticRemittance domesticRemittance)
    {
        PeraLinkCoreWcf.CancelAndDeleteDomesticRemittanceRequest cancelandDeleteDomesticRemittanceRequest = new PeraLinkCoreWcf.CancelAndDeleteDomesticRemittanceRequest();
        cancelandDeleteDomesticRemittanceRequest.PassKey = Cryptor.GeneratePassKey();
        cancelandDeleteDomesticRemittanceRequest.DomesticRemittance = domesticRemittance;

        PeraLinkCoreWcf.AuditTrail auditTrail = new PeraLinkCoreWcf.AuditTrail();
        auditTrail.CreatedByAgent = domesticRemittance.UpdatedByAgent;
        auditTrail.CreatedByUserID = domesticRemittance.UpdatedByUserID;
        auditTrail.DateTimeCreated = DateTime.Now;
        auditTrail.IPAddress = SystemUtility.GetClientIPAddress();
        cancelandDeleteDomesticRemittanceRequest.AuditTrail = auditTrail;

        PeraLinkCoreWcf.CancelAndDeleteDomesticRemittanceResult cancelAndDeleteDomesticRemittanceResult = _serviceClient.CancelAndDeleteDomesticRemittance(cancelandDeleteDomesticRemittanceRequest);

        switch (cancelAndDeleteDomesticRemittanceResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(cancelAndDeleteDomesticRemittanceResult.Message, cancelAndDeleteDomesticRemittanceResult.MessageID, cancelAndDeleteDomesticRemittanceResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(cancelAndDeleteDomesticRemittanceResult.ResultStatus.ToString());
                }
        }
    }

    public void CancelInternationalRemittance(PeraLinkCoreWcf.InternationalRemittance internationalRemittance)
    {
        PeraLinkCoreWcf.CancelInternationalRemittanceRequest cancelInternationalRemittanceRequest = new PeraLinkCoreWcf.CancelInternationalRemittanceRequest();
        cancelInternationalRemittanceRequest.PassKey = Cryptor.GeneratePassKey();
        cancelInternationalRemittanceRequest.InternationalRemittance = internationalRemittance;

        PeraLinkCoreWcf.AuditTrail auditTrail = new PeraLinkCoreWcf.AuditTrail();
        auditTrail.CreatedByAgent = internationalRemittance.UpdatedByAgent;
        auditTrail.CreatedByUserID = internationalRemittance.UpdatedByUserID;
        auditTrail.DateTimeCreated = DateTime.Now;
        auditTrail.IPAddress = SystemUtility.GetClientIPAddress();
        cancelInternationalRemittanceRequest.AuditTrail = auditTrail;

        PeraLinkCoreWcf.CancelInternationalRemittanceResult cancelInternationalRemittanceResult = _serviceClient.CancelInternationalRemittance(cancelInternationalRemittanceRequest);

        switch (cancelInternationalRemittanceResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(cancelInternationalRemittanceResult.Message, cancelInternationalRemittanceResult.MessageID, cancelInternationalRemittanceResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(cancelInternationalRemittanceResult.ResultStatus.ToString());
                }
        }
    }

    public void AmendDomesticRemittance(PeraLinkCoreWcf.DomesticRemittance domesticRemittance)
    {
        PeraLinkCoreWcf.AmendDomesticRemittanceRequest amendDomesticRemittanceRequest = new PeraLinkCoreWcf.AmendDomesticRemittanceRequest();
        amendDomesticRemittanceRequest.PassKey = Cryptor.GeneratePassKey();
        amendDomesticRemittanceRequest.DomesticRemittance = domesticRemittance;

        PeraLinkCoreWcf.AuditTrail auditTrail = new PeraLinkCoreWcf.AuditTrail();
        auditTrail.CreatedByAgent = domesticRemittance.UpdatedByAgent;
        auditTrail.CreatedByUserID = domesticRemittance.UpdatedByUserID;
        auditTrail.DateTimeCreated = DateTime.Now;
        auditTrail.IPAddress = SystemUtility.GetClientIPAddress();
        amendDomesticRemittanceRequest.AuditTrail = auditTrail;

        PeraLinkCoreWcf.AmendDomesticRemittanceResult amendDomesticRemittanceResult = _serviceClient.AmendDomesticRemittance(amendDomesticRemittanceRequest);

        switch (amendDomesticRemittanceResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(amendDomesticRemittanceResult.Message, amendDomesticRemittanceResult.MessageID, amendDomesticRemittanceResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(amendDomesticRemittanceResult.ResultStatus.ToString());
                }
        }
    }

    public void AmendInternationalRemittance(PeraLinkCoreWcf.InternationalRemittance internationalRemittance)
    {
        PeraLinkCoreWcf.AmendInternationalRemittanceRequest amendInternationalRemittanceRequest = new PeraLinkCoreWcf.AmendInternationalRemittanceRequest();
        amendInternationalRemittanceRequest.PassKey = Cryptor.GeneratePassKey();
        amendInternationalRemittanceRequest.InternationalRemittance = internationalRemittance;

        PeraLinkCoreWcf.AuditTrail auditTrail = new PeraLinkCoreWcf.AuditTrail();
        auditTrail.CreatedByAgent = internationalRemittance.UpdatedByAgent;
        auditTrail.CreatedByUserID = internationalRemittance.UpdatedByUserID;
        auditTrail.DateTimeCreated = DateTime.Now;
        auditTrail.IPAddress = SystemUtility.GetClientIPAddress();
        amendInternationalRemittanceRequest.AuditTrail = auditTrail;

        PeraLinkCoreWcf.AmendInternationalRemittanceResult amendInternationalRemittanceResult = _serviceClient.AmendInternationalRemittance(amendInternationalRemittanceRequest);

        switch (amendInternationalRemittanceResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(amendInternationalRemittanceResult.Message, amendInternationalRemittanceResult.MessageID, amendInternationalRemittanceResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(amendInternationalRemittanceResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.Beneficiary[] GetBeneficiaryBySender(PeraLinkCoreWcf.Client senderClient)
    {
        PeraLinkCoreWcf.GetBeneficiaryBySenderRequest getBeneficiaryBySenderRequest = new PeraLinkCoreWcf.GetBeneficiaryBySenderRequest();
        getBeneficiaryBySenderRequest.PassKey = Cryptor.GeneratePassKey();
        getBeneficiaryBySenderRequest.SenderClient = senderClient;

        PeraLinkCoreWcf.GetBeneficiaryBySenderResult getBeneficiaryBySenderResult = _serviceClient.GetBeneficiaryBySender(getBeneficiaryBySenderRequest);

        switch (getBeneficiaryBySenderResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getBeneficiaryBySenderResult.BeneficiaryCollection;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getBeneficiaryBySenderResult.Message, getBeneficiaryBySenderResult.MessageID, getBeneficiaryBySenderResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getBeneficiaryBySenderResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.Beneficiary[] FindBeneficiary(PeraLinkCoreWcf.Beneficiary beneficiary)
    {
        PeraLinkCoreWcf.FindBeneficiaryRequest findBeneficiaryRequest = new PeraLinkCoreWcf.FindBeneficiaryRequest();
        findBeneficiaryRequest.PassKey = Cryptor.GeneratePassKey();
        findBeneficiaryRequest.Beneficiary = beneficiary;

        PeraLinkCoreWcf.FindBeneficiaryResult findBeneficiaryResult = _serviceClient.FindBeneficiary(findBeneficiaryRequest);

        switch (findBeneficiaryResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return findBeneficiaryResult.BeneficiaryCollection;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(findBeneficiaryResult.Message, findBeneficiaryResult.MessageID, findBeneficiaryResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(findBeneficiaryResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.IdentificationType GetIdentificationType(PeraLinkCoreWcf.IdentificationType identificationType)
    {
        PeraLinkCoreWcf.GetIdentificationTypeRequest getIdentificationTypeRequest = new PeraLinkCoreWcf.GetIdentificationTypeRequest();
        getIdentificationTypeRequest.PassKey = Cryptor.GeneratePassKey();

        getIdentificationTypeRequest.IdentificationType = identificationType;

        PeraLinkCoreWcf.GetIdentificationTypeResult getIdentificationTypeResult = _serviceClient.GetIdentificationType(getIdentificationTypeRequest);

        switch (getIdentificationTypeResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getIdentificationTypeResult.IdentificationType;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getIdentificationTypeResult.Message, getIdentificationTypeResult.MessageID, getIdentificationTypeResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getIdentificationTypeResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.IdentificationType[] GetIdentificationTypeCollection()
    {
        PeraLinkCoreWcf.GetIdentificationTypeCollectionRequest getIdentificationTypeCollectionRequest = new PeraLinkCoreWcf.GetIdentificationTypeCollectionRequest();
        getIdentificationTypeCollectionRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkCoreWcf.GetIdentificationTypeCollectionResult getIdentificationTypeCollectionResult = _serviceClient.GetIdentificationTypeCollection(getIdentificationTypeCollectionRequest);

        switch (getIdentificationTypeCollectionResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getIdentificationTypeCollectionResult.IdentificationTypeCollection;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getIdentificationTypeCollectionResult.Message, getIdentificationTypeCollectionResult.MessageID, getIdentificationTypeCollectionResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getIdentificationTypeCollectionResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.IdentificationType[] GetIdentificationTypeCollection(long pullPartnerID)
    {
        PeraLinkCoreWcf.GetIdentificationTypeCollectionRequest getIdentificationTypeCollectionRequest = new PeraLinkCoreWcf.GetIdentificationTypeCollectionRequest();
        getIdentificationTypeCollectionRequest.PassKey = Cryptor.GeneratePassKey();
        getIdentificationTypeCollectionRequest.PullTransactionPartnerID = pullPartnerID;

        PeraLinkCoreWcf.GetIdentificationTypeCollectionResult getIdentificationTypeCollectionResult = _serviceClient.GetIdentificationTypeCollection(getIdentificationTypeCollectionRequest);

        switch (getIdentificationTypeCollectionResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getIdentificationTypeCollectionResult.IdentificationTypeCollection;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getIdentificationTypeCollectionResult.Message, getIdentificationTypeCollectionResult.MessageID, getIdentificationTypeCollectionResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getIdentificationTypeCollectionResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.DomesticRemittance FindDomesticRemittance(PeraLinkCoreWcf.DomesticRemittance domesticRemittance)
    {
        PeraLinkCoreWcf.FindDomesticRemittanceRequest findDomesticRemittanceRequest = new PeraLinkCoreWcf.FindDomesticRemittanceRequest();
        findDomesticRemittanceRequest.PassKey = Cryptor.GeneratePassKey();
        findDomesticRemittanceRequest.DomesticRemittance = domesticRemittance;

        PeraLinkCoreWcf.FindDomesticRemittanceResult findDomesticRemittanceResult = _serviceClient.FindDomesticRemittance(findDomesticRemittanceRequest);

        switch (findDomesticRemittanceResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return findDomesticRemittanceResult.DomesticRemittance;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(findDomesticRemittanceResult.Message, findDomesticRemittanceResult.MessageID, findDomesticRemittanceResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(findDomesticRemittanceResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.InternationalRemittance FindInternationalRemittance(PeraLinkCoreWcf.InternationalRemittance internationalRemittance)
    {
        PeraLinkCoreWcf.FindInternationalRemittanceRequest findInternationalRemittanceRequest = new PeraLinkCoreWcf.FindInternationalRemittanceRequest();
        findInternationalRemittanceRequest.PassKey = Cryptor.GeneratePassKey();
        findInternationalRemittanceRequest.InternationalRemittance = internationalRemittance;

        PeraLinkCoreWcf.FindInternationalRemittanceResult findInternationalRemittanceResult = _serviceClient.FindInternationalRemittance(findInternationalRemittanceRequest);

        switch (findInternationalRemittanceResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return findInternationalRemittanceResult.InternationalRemittance;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(findInternationalRemittanceResult.Message, findInternationalRemittanceResult.MessageID, findInternationalRemittanceResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(findInternationalRemittanceResult.ResultStatus.ToString());
                }
        }
    }

    public void PayoutDomesticRemittance(PeraLinkCoreWcf.PayoutDomesticRemittanceRequest payoutDomesticRemittanceRequest)
    {
        payoutDomesticRemittanceRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.PayoutDomesticRemittanceResult payoutDomesticRemittanceResult = _serviceClient.PayoutDomesticRemittance(payoutDomesticRemittanceRequest);

        switch (payoutDomesticRemittanceResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(payoutDomesticRemittanceResult.Message, payoutDomesticRemittanceResult.MessageID, payoutDomesticRemittanceResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(payoutDomesticRemittanceResult.ResultStatus.ToString());
                }
        }
    }

    public void PayoutInternationalRemittance(PeraLinkCoreWcf.PayoutInternationalRemittanceRequest payoutInternationalRemittanceRequest)
    {
        payoutInternationalRemittanceRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkCoreWcf.PayoutInternationalRemittanceResult payoutInternationalRemittanceResult = _serviceClient.PayoutInternationalRemittance(payoutInternationalRemittanceRequest);
        switch (payoutInternationalRemittanceResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(payoutInternationalRemittanceResult.Message, payoutInternationalRemittanceResult.MessageID, payoutInternationalRemittanceResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(payoutInternationalRemittanceResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.Client AddClient(PeraLinkCoreWcf.Client client)
    {
        PeraLinkCoreWcf.AddClientRequest addClientRequest = new PeraLinkCoreWcf.AddClientRequest();
        addClientRequest.PassKey = Cryptor.GeneratePassKey();
        addClientRequest.Client = client;

        PeraLinkCoreWcf.AddClientResult addClientResult = _serviceClient.AddClient(addClientRequest);

        switch (addClientResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return addClientResult.Client;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(addClientResult.Message, addClientResult.MessageID, addClientResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(addClientResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.Client[] FindClient(PeraLinkCoreWcf.Client client, Int64 partnerID)
    {
        PeraLinkCoreWcf.FindClientRequest findClientRequest = new PeraLinkCoreWcf.FindClientRequest();
        findClientRequest.PassKey = Cryptor.GeneratePassKey();
        findClientRequest.Client = client;
        findClientRequest.PartnerID = partnerID;

        PeraLinkCoreWcf.FindClientResult findClientResult = _serviceClient.FindClient(findClientRequest);

        switch (findClientResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return findClientResult.ClientCollection;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(findClientResult.Message, findClientResult.MessageID, findClientResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(findClientResult.ResultStatus.ToString());
                }
        }
    }

    public void AddAuditTrail(PeraLinkCoreWcf.AuditTrail auditTrail)
    {
        PeraLinkCoreWcf.AddAuditTrailRequest addAuditTrailRequest = new PeraLinkCoreWcf.AddAuditTrailRequest();
        addAuditTrailRequest.PassKey = Cryptor.GeneratePassKey();
        addAuditTrailRequest.AuditTrail = auditTrail;

        PeraLinkCoreWcf.AddAuditTrailResult addAuditTrailResult = _serviceClient.AddAuditTrail(addAuditTrailRequest);

        switch (addAuditTrailResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(addAuditTrailResult.Message, addAuditTrailResult.MessageID, addAuditTrailResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(addAuditTrailResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.Beneficiary AddBeneficiary(PeraLinkCoreWcf.Beneficiary beneficiary)
    {
        PeraLinkCoreWcf.AddBeneficiaryRequest addBeneficiaryRequest = new PeraLinkCoreWcf.AddBeneficiaryRequest();
        addBeneficiaryRequest.PassKey = Cryptor.GeneratePassKey();
        addBeneficiaryRequest.Beneficiary = beneficiary;

        PeraLinkCoreWcf.AddBeneficiaryResult addBeneficiaryResult = _serviceClient.AddBeneficiary(addBeneficiaryRequest);

        switch (addBeneficiaryResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return addBeneficiaryResult.Beneficiary;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(addBeneficiaryResult.Message, addBeneficiaryResult.MessageID, addBeneficiaryResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(addBeneficiaryResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.DomesticRemittance SendDomesticRemittance(PeraLinkCoreWcf.SendDomesticRemittanceRequest sendDomesticRemittanceRequest)
    {
        PeraLinkCoreWcf.AuditTrail auditTrail = new PeraLinkCoreWcf.AuditTrail();
        sendDomesticRemittanceRequest.PassKey = Cryptor.GeneratePassKey();
        auditTrail.CreatedByAgent = sendDomesticRemittanceRequest.DomesticRemittance.SentByAgent;
        auditTrail.CreatedByUserID = sendDomesticRemittanceRequest.DomesticRemittance.SentByUserID;
        auditTrail.DateTimeCreated = DateTime.Now;
        auditTrail.IPAddress = SystemUtility.GetClientIPAddress();
        sendDomesticRemittanceRequest.AuditTrail = auditTrail;

        PeraLinkCoreWcf.SendDomesticRemittanceResult sendDomesticRemittanceResult = _serviceClient.SendDomesticRemittance(sendDomesticRemittanceRequest);

        switch (sendDomesticRemittanceResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return sendDomesticRemittanceResult.DomesticRemittance;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(sendDomesticRemittanceResult.Message, sendDomesticRemittanceResult.MessageID, sendDomesticRemittanceResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(sendDomesticRemittanceResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.InternationalRemittance SendInternationalRemittance(PeraLinkCoreWcf.SendInternationalRemittanceRequest sendInternationalRemittanceRequest)
    {
        PeraLinkCoreWcf.AuditTrail auditTrail = new PeraLinkCoreWcf.AuditTrail();
        sendInternationalRemittanceRequest.PassKey = Cryptor.GeneratePassKey();
        auditTrail.CreatedByAgent = sendInternationalRemittanceRequest.InternationalRemittance.SentByAgent;
        auditTrail.CreatedByUserID = sendInternationalRemittanceRequest.InternationalRemittance.SentByUserID;
        auditTrail.DateTimeCreated = DateTime.Now;
        auditTrail.IPAddress = SystemUtility.GetClientIPAddress();
        sendInternationalRemittanceRequest.AuditTrail = auditTrail;

        PeraLinkCoreWcf.SendInternationalRemittanceResult sendInternationalRemittanceResult = _serviceClient.SendInternationalRemittance(sendInternationalRemittanceRequest);

        switch (sendInternationalRemittanceResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return sendInternationalRemittanceResult.InternationalRemittance;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(sendInternationalRemittanceResult.Message, sendInternationalRemittanceResult.MessageID, sendInternationalRemittanceResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(sendInternationalRemittanceResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.InternationalRemittance CacheInternationalRemittance(PeraLinkCoreWcf.CacheInternationalRemittanceRequest cacheInternationalRemittanceRequest)
    {
        PeraLinkCoreWcf.AuditTrail auditTrail = new PeraLinkCoreWcf.AuditTrail();
        cacheInternationalRemittanceRequest.PassKey = Cryptor.GeneratePassKey();
        auditTrail.CreatedByAgent = cacheInternationalRemittanceRequest.InternationalRemittance.SentByAgent;
        auditTrail.CreatedByUserID = cacheInternationalRemittanceRequest.InternationalRemittance.SentByUserID;
        auditTrail.DateTimeCreated = DateTime.Now;
        auditTrail.IPAddress = SystemUtility.GetClientIPAddress();
        cacheInternationalRemittanceRequest.AuditTrail = auditTrail;

        PeraLinkCoreWcf.CacheInternationalRemittanceResult cacheInternationalRemittanceResult = _serviceClient.CacheInternationalRemittance(cacheInternationalRemittanceRequest);

        switch (cacheInternationalRemittanceResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return cacheInternationalRemittanceResult.InternationalRemittance;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(cacheInternationalRemittanceResult.Message, cacheInternationalRemittanceResult.MessageID, cacheInternationalRemittanceResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(cacheInternationalRemittanceResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.GetDomesticRemittanceHistoryResult GetDomesticRemittanceHistory(PeraLinkCoreWcf.GetDomesticRemittanceHistoryRequest getDomesticRemittanceHistoryRequest)
    {
        getDomesticRemittanceHistoryRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkCoreWcf.GetDomesticRemittanceHistoryResult getDomesticRemittanceHistoryResult = _serviceClient.GetDomesticRemittanceHistory(getDomesticRemittanceHistoryRequest);

        switch (getDomesticRemittanceHistoryResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getDomesticRemittanceHistoryResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getDomesticRemittanceHistoryResult.Message, getDomesticRemittanceHistoryResult.MessageID, getDomesticRemittanceHistoryResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getDomesticRemittanceHistoryResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.GetInternationalRemittanceHistoryResult GetInternationalRemittanceHistory(PeraLinkCoreWcf.GetInternationalRemittanceHistoryRequest getInternationalRemittanceHistoryRequest)
    {
        getInternationalRemittanceHistoryRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkCoreWcf.GetInternationalRemittanceHistoryResult getInternationalRemittanceHistoryResult = _serviceClient.GetInternationalRemittanceHistory(getInternationalRemittanceHistoryRequest);

        switch (getInternationalRemittanceHistoryResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getInternationalRemittanceHistoryResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getInternationalRemittanceHistoryResult.Message, getInternationalRemittanceHistoryResult.MessageID, getInternationalRemittanceHistoryResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getInternationalRemittanceHistoryResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.GetPartnerTotalSendAmountResult GetPartnerTotalSendAmount(PeraLinkCoreWcf.GetPartnerTotalSendAmountRequest getPartnerTotalSendAmountRequest)
    {
        getPartnerTotalSendAmountRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.GetPartnerTotalSendAmountResult returnValue = _serviceClient.GetPartnerTotalSendAmount(getPartnerTotalSendAmountRequest);
        switch (returnValue.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return returnValue;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(returnValue.Message, returnValue.MessageID, returnValue.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(returnValue.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.GetPartnerTotalRemitToAccountAmountResult GetPartnerTotalRemitToAccountAmount(PeraLinkCoreWcf.GetPartnerTotalRemitToAccountAmountRequest getPartnerTotalRemitToAccountAmountRequest)
    {
        getPartnerTotalRemitToAccountAmountRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.GetPartnerTotalRemitToAccountAmountResult returnValue = _serviceClient.GetPartnerTotalRemitToAccountAmount(getPartnerTotalRemitToAccountAmountRequest);
        switch (returnValue.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return returnValue;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(returnValue.Message, returnValue.MessageID, returnValue.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(returnValue.ResultStatus.ToString());
                }
        }
    }

    //commissionfee here
    public PeraLinkCoreWcf.GetTotalCommissionResult GetTotalCommission(PeraLinkCoreWcf.GetTotalCommissionRequest getTotalCommissionRequest)
    {
        getTotalCommissionRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.GetTotalCommissionResult getTotalCommissionResult = _serviceClient.GetTotalCommission(getTotalCommissionRequest);

        switch (getTotalCommissionResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getTotalCommissionResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getTotalCommissionResult.Message, getTotalCommissionResult.MessageID, getTotalCommissionResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getTotalCommissionResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.GetClientSourceOfFundCollectionResult GetClientSourceOfFundCollection(PeraLinkCoreWcf.GetClientSourceOfFundCollectionRequest getClientSourceOfFundCollectionRequest)
    {
        getClientSourceOfFundCollectionRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.GetClientSourceOfFundCollectionResult getClientSourceOfFundCollectionResult = _serviceClient.GetClientSourceOfFundCollection(getClientSourceOfFundCollectionRequest);

        switch (getClientSourceOfFundCollectionResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getClientSourceOfFundCollectionResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getClientSourceOfFundCollectionResult.Message, getClientSourceOfFundCollectionResult.MessageID, getClientSourceOfFundCollectionResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getClientSourceOfFundCollectionResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.CreateRemitToAccountTransactionResult CreateRemitToAccountTransaction(PeraLinkCoreWcf.CreateRemitToAccountTransactionRequest createRemitToAccountTransactionRequest)
    {
        createRemitToAccountTransactionRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.CreateRemitToAccountTransactionResult createRemitToAccountTransactionResult = _serviceClient.CreateRemitToAccountTransaction(createRemitToAccountTransactionRequest);

        switch (createRemitToAccountTransactionResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return createRemitToAccountTransactionResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(createRemitToAccountTransactionResult.Message, createRemitToAccountTransactionResult.MessageID, createRemitToAccountTransactionResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(createRemitToAccountTransactionResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.UpdateRemitToAccountTransactionStatusResult UpdateRemitToAccountTransactionStatus(PeraLinkCoreWcf.UpdateRemitToAccountTransactionStatusRequest updateRemitToAccountTransactionStatusRequest)
    {
        updateRemitToAccountTransactionStatusRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.UpdateRemitToAccountTransactionStatusResult updateRemitToAccountTransactionStatusResult = _serviceClient.UpdateRemitToAccountTransactionStatus(updateRemitToAccountTransactionStatusRequest);

        switch (updateRemitToAccountTransactionStatusResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return updateRemitToAccountTransactionStatusResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(updateRemitToAccountTransactionStatusResult.Message, updateRemitToAccountTransactionStatusResult.MessageID, updateRemitToAccountTransactionStatusResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(updateRemitToAccountTransactionStatusResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.Bank[] GetBankCollection(PeraLinkCoreWcf.GetBankCollectionRequest getBankCollectionRequest)
    {
        getBankCollectionRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.GetBankCollectionResult getBankCollectionResult = _serviceClient.GetBankCollection(getBankCollectionRequest);

        switch (getBankCollectionResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getBankCollectionResult.BankCollection;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getBankCollectionResult.Message, getBankCollectionResult.MessageID, getBankCollectionResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getBankCollectionResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.GetRemitToAccountTransactionHistoryResult GetRemitToAccountTransactionHistory(PeraLinkCoreWcf.GetRemitToAccountTransactionHistoryRequest getRemitToAccountTransactionHistoryRequest)
    {
        getRemitToAccountTransactionHistoryRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.GetRemitToAccountTransactionHistoryResult getRemitToAccountTransactionHistoryResult = _serviceClient.GetRemitToAccountTransactionHistory(getRemitToAccountTransactionHistoryRequest);

        switch (getRemitToAccountTransactionHistoryResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getRemitToAccountTransactionHistoryResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getRemitToAccountTransactionHistoryResult.Message, getRemitToAccountTransactionHistoryResult.MessageID, getRemitToAccountTransactionHistoryResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getRemitToAccountTransactionHistoryResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.RemitToAccountTransaction FindRemitToAccountTransaction(PeraLinkCoreWcf.RemitToAccountTransaction remitToAccountTransaction)
    {
        PeraLinkCoreWcf.FindRemitToAccountTransactionRequest findRemitToAccountTransactionRequest = new PeraLinkCoreWcf.FindRemitToAccountTransactionRequest();
        findRemitToAccountTransactionRequest.PassKey = Cryptor.GeneratePassKey();

        findRemitToAccountTransactionRequest.RemitToAccountTransaction = remitToAccountTransaction;

        PeraLinkCoreWcf.FindRemitToAccountTransactionResult findRemitToAccountTransactionResult = _serviceClient.FindRemitToAccountTransaction(findRemitToAccountTransactionRequest);

        switch (findRemitToAccountTransactionResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return findRemitToAccountTransactionResult.RemitToAccountTransaction;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(findRemitToAccountTransactionResult.Message, findRemitToAccountTransactionResult.MessageID, findRemitToAccountTransactionResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(findRemitToAccountTransactionResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.Bank GetBank(PeraLinkCoreWcf.Bank bank)
    {
        PeraLinkCoreWcf.GetBankRequest getBankRequest = new PeraLinkCoreWcf.GetBankRequest();
        getBankRequest.PassKey = Cryptor.GeneratePassKey();

        getBankRequest.Bank = bank;

        PeraLinkCoreWcf.GetBankResult getBankResult = _serviceClient.GetBank(getBankRequest);

        switch (getBankResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getBankResult.Bank;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getBankResult.Message, getBankResult.MessageID, getBankResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getBankResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.PartnerRequestDetails FindPartnerRequestDetails(PeraLinkCoreWcf.PartnerRequestDetails partnerRequestDetails)
    {
        PeraLinkCoreWcf.FindPartnerRequestDetailsRequest findPartnerRequestDetailsRequest = new PeraLinkCoreWcf.FindPartnerRequestDetailsRequest();
        findPartnerRequestDetailsRequest.PassKey = Cryptor.GeneratePassKey();

        findPartnerRequestDetailsRequest.PartnerRequestDetails = partnerRequestDetails;

        PeraLinkCoreWcf.FindPartnerRequestDetailsResult findPartnerRequestDetailsResult = _serviceClient.FindPartnerRequestDetails(findPartnerRequestDetailsRequest);
        switch (findPartnerRequestDetailsResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return findPartnerRequestDetailsResult.PartnerRequestDetails;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(findPartnerRequestDetailsResult.Message, findPartnerRequestDetailsResult.MessageID, findPartnerRequestDetailsResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(findPartnerRequestDetailsResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.InsertPartnerRequestDetailsResult InsertPartnerRequestDetails(PeraLinkCoreWcf.InsertPartnerRequestDetailsRequest insertPartnerRequestDetailsRequest)
    {
        insertPartnerRequestDetailsRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.InsertPartnerRequestDetailsResult insertPartnerRequestDetailsResult = _serviceClient.InsertPartnerRequestDetails(insertPartnerRequestDetailsRequest);

        switch (insertPartnerRequestDetailsResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return insertPartnerRequestDetailsResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(insertPartnerRequestDetailsResult.Message, insertPartnerRequestDetailsResult.MessageID, insertPartnerRequestDetailsResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(insertPartnerRequestDetailsResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.UpdatePartnerRequestDetailsResult UpdatePartnerRequestDetails(PeraLinkCoreWcf.UpdatePartnerRequestDetailsRequest updatePartnerRequestDetailsRequest)
    {
        updatePartnerRequestDetailsRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.UpdatePartnerRequestDetailsResult updatePartnerRequestDetailsResult = _serviceClient.UpdatePartnerRequestDetails(updatePartnerRequestDetailsRequest);

        switch (updatePartnerRequestDetailsResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return updatePartnerRequestDetailsResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(updatePartnerRequestDetailsResult.Message, updatePartnerRequestDetailsResult.MessageID, updatePartnerRequestDetailsResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(updatePartnerRequestDetailsResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.GetAgentTotalTransactionAmountResult GetAgentTotalTransactionAmount(PeraLinkCoreWcf.GetAgentTotalTransactionAmountRequest getAgentTotalTransactionAmountRequest)
    {
        getAgentTotalTransactionAmountRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.GetAgentTotalTransactionAmountResult getAgentTotalTransactionAmountResult = _serviceClient.GetAgentTotalTransactionAmount(getAgentTotalTransactionAmountRequest);

        switch (getAgentTotalTransactionAmountResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getAgentTotalTransactionAmountResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getAgentTotalTransactionAmountResult.Message, getAgentTotalTransactionAmountResult.MessageID, getAgentTotalTransactionAmountResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getAgentTotalTransactionAmountResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.Terminal GetTerminal(PeraLinkCoreWcf.Terminal terminal)
    {
        PeraLinkCoreWcf.GetTerminalRequest getTerminalRequest = new PeraLinkCoreWcf.GetTerminalRequest();
        getTerminalRequest.PassKey = Cryptor.GeneratePassKey();

        getTerminalRequest.Terminal = terminal;

        PeraLinkCoreWcf.GetTerminalResult getTerminalResult = _serviceClient.GetTerminal(getTerminalRequest);


        switch (getTerminalResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getTerminalResult.Terminal;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getTerminalResult.Message, getTerminalResult.MessageID, getTerminalResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getTerminalResult.ResultStatus.ToString());
                }
        }

    }

    public PeraLinkCoreWcf.Terminal FindTerminal(PeraLinkCoreWcf.Terminal Terminal)
    {
        PeraLinkCoreWcf.FindTerminalRequest findTerminalRequest = new PeraLinkCoreWcf.FindTerminalRequest();
        findTerminalRequest.PassKey = Cryptor.GeneratePassKey();

        findTerminalRequest.Terminal = Terminal;

        PeraLinkCoreWcf.FindTerminalResult findTerminalResult = _serviceClient.FindTerminal(findTerminalRequest);

        switch (findTerminalResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return findTerminalResult.Terminal;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(findTerminalResult.Message, findTerminalResult.MessageID, findTerminalResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(findTerminalResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.Terminal[] GetAgentTerminalCollection(PeraLinkCoreWcf.Agent Agent)
    {
        PeraLinkCoreWcf.GetAgentTerminalCollectionRequest getAgentTerminalCollectionRequest = new PeraLinkCoreWcf.GetAgentTerminalCollectionRequest();
        getAgentTerminalCollectionRequest.PassKey = Cryptor.GeneratePassKey();

        getAgentTerminalCollectionRequest.Agent = Agent;

        PeraLinkCoreWcf.GetAgentTerminalCollectionResult getAgentTerminalCollectionResult = _serviceClient.GetAgentTerminalCollection(getAgentTerminalCollectionRequest);


        switch (getAgentTerminalCollectionResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getAgentTerminalCollectionResult.TerminalCollection;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getAgentTerminalCollectionResult.Message, getAgentTerminalCollectionResult.MessageID, getAgentTerminalCollectionResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getAgentTerminalCollectionResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.GetTerminalTransactionSummaryResult GetTerminalTransactionSummary(PeraLinkCoreWcf.GetTerminalTransactionSummaryRequest getTerminalTransactionHistoryRequest)
    {
        getTerminalTransactionHistoryRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.GetTerminalTransactionSummaryResult getTerminalTransactionSummaryResult = _serviceClient.GetTerminalTransactionSummary(getTerminalTransactionHistoryRequest);

        switch (getTerminalTransactionSummaryResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getTerminalTransactionSummaryResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getTerminalTransactionSummaryResult.Message, getTerminalTransactionSummaryResult.MessageID, getTerminalTransactionSummaryResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getTerminalTransactionSummaryResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.GetTerminalReadingReportResult GetTerminalReadingReport(PeraLinkCoreWcf.GetTerminalReadingReportRequest getTerminalReadingReportRequest)
    {
        getTerminalReadingReportRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.GetTerminalReadingReportResult getTerminalReadingReportResult = _serviceClient.GetTerminalReadingReport(getTerminalReadingReportRequest);

        switch (getTerminalReadingReportResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getTerminalReadingReportResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getTerminalReadingReportResult.Message, getTerminalReadingReportResult.MessageID, getTerminalReadingReportResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getTerminalReadingReportResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.GetSummaryReportResult GetSummaryReport(PeraLinkCoreWcf.GetSummaryReportRequest getSummaryReportRequest)
    {
        getSummaryReportRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.GetSummaryReportResult getSummaryReportResult = _serviceClient.GetSummaryReport(getSummaryReportRequest);

        switch (getSummaryReportResult.ResultStatus)
        {
            case ResultStatus.Successful:
                { return getSummaryReportResult; }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getSummaryReportResult.Message, getSummaryReportResult.MessageID, getSummaryReportResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getSummaryReportResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.GetSendingTransactionReportResult GetSendingTransactionReport(PeraLinkCoreWcf.GetSendingTransactionReportRequest getSendingTransactionReportRequest)
    {
        getSendingTransactionReportRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.GetSendingTransactionReportResult getSendingTransactionReportResult = _serviceClient.GetSendingTransactionReport(getSendingTransactionReportRequest);

        switch (getSendingTransactionReportResult.ResultStatus)
        {
            case ResultStatus.Successful:
                { return getSendingTransactionReportResult; }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getSendingTransactionReportResult.Message, getSendingTransactionReportResult.MessageID, getSendingTransactionReportResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getSendingTransactionReportResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.GetPayoutTransactionReportResult GetPayoutTransactionReport(PeraLinkCoreWcf.GetPayoutTransactionReportRequest getPayoutTransactionReportRequest)
    {
        getPayoutTransactionReportRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.GetPayoutTransactionReportResult getPayoutTransactionReportResult = _serviceClient.GetPayoutTransactionReport(getPayoutTransactionReportRequest);

        switch (getPayoutTransactionReportResult.ResultStatus)
        {
            case ResultStatus.Successful:
                { return getPayoutTransactionReportResult; }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getPayoutTransactionReportResult.Message, getPayoutTransactionReportResult.MessageID, getPayoutTransactionReportResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getPayoutTransactionReportResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.GetCancelTransactionReportResult GetCancelTransactionReport(PeraLinkCoreWcf.GetCancelTransactionReportRequest getCancelTransactionReportRequest)
    {
        getCancelTransactionReportRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.GetCancelTransactionReportResult getCancelTransactionReportResult = _serviceClient.GetCancelTransactionReport(getCancelTransactionReportRequest);

        switch (getCancelTransactionReportResult.ResultStatus)
        {
            case ResultStatus.Successful:
                { return getCancelTransactionReportResult; }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getCancelTransactionReportResult.Message, getCancelTransactionReportResult.MessageID, getCancelTransactionReportResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getCancelTransactionReportResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.GetRefundTransactionReportResult GetRefundTransactionReport(PeraLinkCoreWcf.GetRefundTransactionReportRequest getRefundTransactionReportRequest)
    {
        getRefundTransactionReportRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.GetRefundTransactionReportResult getRefundTransactionReportResult = _serviceClient.GetRefundTransactionReport(getRefundTransactionReportRequest);

        switch (getRefundTransactionReportResult.ResultStatus)
        {
            case ResultStatus.Successful:
                { return getRefundTransactionReportResult; }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getRefundTransactionReportResult.Message, getRefundTransactionReportResult.MessageID, getRefundTransactionReportResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getRefundTransactionReportResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.GetAmendTransactionReportResult GetAmendTransactionReport(PeraLinkCoreWcf.GetAmendTransactionReportRequest getAmendTransactionReportRequest)
    {
        getAmendTransactionReportRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.GetAmendTransactionReportResult getAmendTransactionReportResult = _serviceClient.GetAmendTransactionReport(getAmendTransactionReportRequest);

        switch (getAmendTransactionReportResult.ResultStatus)
        {
            case ResultStatus.Successful:
                { return getAmendTransactionReportResult; }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getAmendTransactionReportResult.Message, getAmendTransactionReportResult.MessageID, getAmendTransactionReportResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getAmendTransactionReportResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.GetLastDomesticRemittanceControlNumberByTerminalResult GetLastDomesticRemittanceControlNumberByTerminal(PeraLinkCoreWcf.GetLastDomesticRemittanceControlNumberByTerminalRequest getLastDomesticRemittanceControlNumberByTerminalRequest)
    {
        getLastDomesticRemittanceControlNumberByTerminalRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.GetLastDomesticRemittanceControlNumberByTerminalResult getLastDomesticRemittanceControlNumberByTerminalResult = _serviceClient.GetLastDomesticRemittanceControlNumberByTerminal(getLastDomesticRemittanceControlNumberByTerminalRequest);

        switch (getLastDomesticRemittanceControlNumberByTerminalResult.ResultStatus)
        {
            case ResultStatus.Successful:
                { return getLastDomesticRemittanceControlNumberByTerminalResult; }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getLastDomesticRemittanceControlNumberByTerminalResult.Message, getLastDomesticRemittanceControlNumberByTerminalResult.MessageID, getLastDomesticRemittanceControlNumberByTerminalResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getLastDomesticRemittanceControlNumberByTerminalResult.ResultStatus.ToString());
                }
        }

    }

    public PeraLinkCoreWcf.GetLastProcessedTransactionByTerminalResult GetLastProcessedTransactionByTerminal(PeraLinkCoreWcf.GetLastProcessedTransactionByTerminalRequest getLastProcessedTransactionByTerminalRequest)
    {
        getLastProcessedTransactionByTerminalRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.GetLastProcessedTransactionByTerminalResult getLastProcessedTransactionByTerminalResult = _serviceClient.GetLastProcessedTransactionByTerminal(getLastProcessedTransactionByTerminalRequest);

        switch (getLastProcessedTransactionByTerminalResult.ResultStatus)
        {
            case ResultStatus.Successful:
                { return getLastProcessedTransactionByTerminalResult; }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getLastProcessedTransactionByTerminalResult.Message, getLastProcessedTransactionByTerminalResult.MessageID, getLastProcessedTransactionByTerminalResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getLastProcessedTransactionByTerminalResult.ResultStatus.ToString());
                }
        }

    }

    public PeraLinkCoreWcf.ProcessLoadTransactionResult ProcessLoadTransaction(PeraLinkCoreWcf.ProcessLoadTransactionRequest processLoadTransactionRequest)
    {
        processLoadTransactionRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.ProcessLoadTransactionResult processLoadTransactionResult = _serviceClient.ProcessLoadTransaction(processLoadTransactionRequest);

        switch (processLoadTransactionResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return processLoadTransactionResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(processLoadTransactionResult.Message, processLoadTransactionResult.MessageID, processLoadTransactionResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(processLoadTransactionResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.CheckELoadTransactionResult CheckELoadTransaction(PeraLinkCoreWcf.CheckELoadTransactionRequest checkELoadTransactionRequest)
    {
        checkELoadTransactionRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.CheckELoadTransactionResult checkELoadTransactionResult = _serviceClient.CheckELoadTransaction(checkELoadTransactionRequest);

        switch (checkELoadTransactionResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return checkELoadTransactionResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(checkELoadTransactionResult.Message, checkELoadTransactionResult.MessageID, checkELoadTransactionResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(checkELoadTransactionResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.GetELoadTransactionHistoryResult GetELoadTransactionHistory(PeraLinkCoreWcf.GetELoadTransactionHistoryRequest getELoadTransactionHistoryRequest)
    {
        getELoadTransactionHistoryRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.GetELoadTransactionHistoryResult getELoadTransactionHistoryResult = _serviceClient.GetELoadTransactionHistory(getELoadTransactionHistoryRequest);

        switch (getELoadTransactionHistoryResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getELoadTransactionHistoryResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getELoadTransactionHistoryResult.Message, getELoadTransactionHistoryResult.MessageID, getELoadTransactionHistoryResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getELoadTransactionHistoryResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.PartnerCommissionFee GetCommissionFee(PeraLinkCoreWcf.GetPartnerCommissionFeeRequest getCommissionFeeRequest)
    {
        getCommissionFeeRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkCoreWcf.GetPartnerCommissionFeeResult getCommissionFeeResult = _serviceClient.GetPartnerCommissionFee(getCommissionFeeRequest);

        switch (getCommissionFeeResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getCommissionFeeResult.PartnerCommissionFee;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getCommissionFeeResult.Message, getCommissionFeeResult.MessageID, getCommissionFeeResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getCommissionFeeResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.AddCommissionTransactionResult AddCommissionTransaction(PeraLinkCoreWcf.AddCommissionTransactionRequest addCommissionTransactionRequest)
    {
        addCommissionTransactionRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.AddCommissionTransactionResult addCommissionTransactionResult = _serviceClient.AddCommissionTransaction(addCommissionTransactionRequest);

        switch (addCommissionTransactionResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return addCommissionTransactionResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(addCommissionTransactionResult.Message, addCommissionTransactionResult.MessageID, addCommissionTransactionResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(addCommissionTransactionResult.ResultStatus.ToString());
                }
        }
    }



    public PeraLinkCoreWcf.CreateBillsPaymentResult CreateBillsPayment(PeraLinkCoreWcf.CreateBillsPaymentRequest createBillsPaymentRequest)
    {
        createBillsPaymentRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkCoreWcf.CreateBillsPaymentResult createBillsPaymentResult = _serviceClient.CreateBillsPayment(createBillsPaymentRequest);

        switch (createBillsPaymentResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return createBillsPaymentResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(createBillsPaymentResult.Message, createBillsPaymentResult.MessageID, createBillsPaymentResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(createBillsPaymentResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.SaveBillsPaymentTransactionResult SaveBillsPayment(PeraLinkCoreWcf.SaveBillsPaymentTransactionRequest saveBillsPaymentTransactionRequest)
    {
        saveBillsPaymentTransactionRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkCoreWcf.SaveBillsPaymentTransactionResult saveBillsPaymentTransactionResult = _serviceClient.SaveBillsPaymentTransaction(saveBillsPaymentTransactionRequest);

        switch (saveBillsPaymentTransactionResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return saveBillsPaymentTransactionResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(saveBillsPaymentTransactionResult.Message, saveBillsPaymentTransactionResult.MessageID, saveBillsPaymentTransactionResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(saveBillsPaymentTransactionResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.GetPartnerPairingServiceFeeResult GetPartnerPairingServiceFee(PeraLinkCoreWcf.GetPartnerPairingServiceFeeRequest getPartnerPairingServiceFeeRequest)
    {
        getPartnerPairingServiceFeeRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkCoreWcf.GetPartnerPairingServiceFeeResult getPartnerRegionCollectionResult = _serviceClient.GetPartnerPairingServiceFee(getPartnerPairingServiceFeeRequest);

        switch (getPartnerRegionCollectionResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getPartnerRegionCollectionResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getPartnerRegionCollectionResult.Message, getPartnerRegionCollectionResult.MessageID, getPartnerRegionCollectionResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getPartnerRegionCollectionResult.ResultStatus.ToString());
                }
        }
    }


    public PeraLinkCoreWcf.Client[] FindClientMobile(PeraLinkCoreWcf.Client client)
    {
        PeraLinkCoreWcf.FindClientMobileRequest findClientMobileRequest = new PeraLinkCoreWcf.FindClientMobileRequest();
        findClientMobileRequest.PassKey = Cryptor.GeneratePassKey();
        findClientMobileRequest.Client = client;

        PeraLinkCoreWcf.FindClientMobileResult findClientMobileResult = _serviceClient.FindClientMobile(findClientMobileRequest);

        switch (findClientMobileResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return findClientMobileResult.ClientCollection;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(findClientMobileResult.Message, findClientMobileResult.MessageID, findClientMobileResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(findClientMobileResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.Client[] FindClientByClientNumber(PeraLinkCoreWcf.Client client)
    {
        PeraLinkCoreWcf.FindClientByClientNumberRequest findClientMobileRequest = new PeraLinkCoreWcf.FindClientByClientNumberRequest();
        findClientMobileRequest.PassKey = Cryptor.GeneratePassKey();

        findClientMobileRequest.Client = client;

        PeraLinkCoreWcf.FindClientByClientNumberResult findClientMobileResult = _serviceClient.FindClientByClientNumber(findClientMobileRequest);

        switch (findClientMobileResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return findClientMobileResult.ClientCollection;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(findClientMobileResult.Message, findClientMobileResult.MessageID, findClientMobileResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(findClientMobileResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.GetBillsPaymentTransactionHistoryResult GetBillsPaymentTransactionHistory(PeraLinkCoreWcf.GetBillsPaymentTransactionHistoryRequest getBillsPaymentTransactionHistoryRequest)
    {
        getBillsPaymentTransactionHistoryRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.GetBillsPaymentTransactionHistoryResult getBillsPaymentTransactionHistoryResult = _serviceClient.GetBillsPaymentTransactionHistory(getBillsPaymentTransactionHistoryRequest);

        switch (getBillsPaymentTransactionHistoryResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getBillsPaymentTransactionHistoryResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getBillsPaymentTransactionHistoryResult.Message, getBillsPaymentTransactionHistoryResult.MessageID, getBillsPaymentTransactionHistoryResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getBillsPaymentTransactionHistoryResult.ResultStatus.ToString());
                }
        }
    }

#if INTERNATIONAL_COMMISSION_FEE
    public PeraLinkCoreWcf.GetPartnerPairingCommissionFeeResult GetPartnerPairingCommissionFee(PeraLinkCoreWcf.GetPartnerPairingCommissionFeeRequest getPartnerPairingCommissionFeeRequest)
    {
        getPartnerPairingCommissionFeeRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkCoreWcf.GetPartnerPairingCommissionFeeResult getPartnerPairingCommissionFeeResult = _serviceClient.GetPartnerPairingCommissionFee(getPartnerPairingCommissionFeeRequest);

        switch (getPartnerPairingCommissionFeeResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getPartnerPairingCommissionFeeResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getPartnerPairingCommissionFeeResult.Message, getPartnerPairingCommissionFeeResult.MessageID, getPartnerPairingCommissionFeeResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getPartnerPairingCommissionFeeResult.ResultStatus.ToString());
                }
        }
    }
#endif
    public PeraLinkCoreWcf.CreateInsuranceResult CreateInsurance(PeraLinkCoreWcf.CreateInsuranceRequest createInsuranceRequest)
    {
        createInsuranceRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkCoreWcf.CreateInsuranceResult createInsuranceResult = _serviceClient.CreateInsurance(createInsuranceRequest);

        switch (createInsuranceResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return createInsuranceResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(createInsuranceResult.Message, createInsuranceResult.MessageID, createInsuranceResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(createInsuranceResult.ResultStatus.ToString());
                }
        }
    }
    public PeraLinkCoreWcf.SaveInsuranceTransactionResult SaveInsurance(PeraLinkCoreWcf.SaveInsuranceTransactionRequest saveInsuranceTransactionRequest)
    {
        saveInsuranceTransactionRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkCoreWcf.SaveInsuranceTransactionResult saveInsuranceTransactionResult = _serviceClient.SaveInsuranceTransaction(saveInsuranceTransactionRequest);

        switch (saveInsuranceTransactionResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return saveInsuranceTransactionResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(saveInsuranceTransactionResult.Message, saveInsuranceTransactionResult.MessageID, saveInsuranceTransactionResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(saveInsuranceTransactionResult.ResultStatus.ToString());
                }
        }

    }


    public PeraLinkCoreWcf.GetInsuranceTransactionHistoryResult GetInsuranceTransactionHistory(PeraLinkCoreWcf.GetInsuranceTransactionHistoryRequest getInsuranceTransactionHistoryRequest)
    {
        getInsuranceTransactionHistoryRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.GetInsuranceTransactionHistoryResult getInsuranceTransactionHistoryResult = _serviceClient.GetInsuranceTransactionHistory(getInsuranceTransactionHistoryRequest);

        switch (getInsuranceTransactionHistoryResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getInsuranceTransactionHistoryResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getInsuranceTransactionHistoryResult.Message, getInsuranceTransactionHistoryResult.MessageID, getInsuranceTransactionHistoryResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getInsuranceTransactionHistoryResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.GetInsuranceDetailsByNameResult GetInsuranceDetailsByName(PeraLinkCoreWcf.GetInsuranceDetailsByNameRequest getInsuranceDetailsByNameRequest)
    {
        getInsuranceDetailsByNameRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.GetInsuranceDetailsByNameResult getInsuranceDetailsByNameResult = _serviceClient.GetInsuranceDetailsByName(getInsuranceDetailsByNameRequest);

        switch (getInsuranceDetailsByNameResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getInsuranceDetailsByNameResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getInsuranceDetailsByNameResult.Message, getInsuranceDetailsByNameResult.MessageID, getInsuranceDetailsByNameResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getInsuranceDetailsByNameResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.UpdateInsuranceStatusResult UpdateInsuranceStatus(PeraLinkCoreWcf.UpdateInsuranceStatusRequest updateInsuranceStatusRequest)
    {
        updateInsuranceStatusRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.UpdateInsuranceStatusResult updateInsuranceStatusResult = _serviceClient.UpdateInsuranceStatus(updateInsuranceStatusRequest);

        switch (updateInsuranceStatusResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return updateInsuranceStatusResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(updateInsuranceStatusResult.Message, updateInsuranceStatusResult.MessageID, updateInsuranceStatusResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(updateInsuranceStatusResult.ResultStatus.ToString());
                }
        }
    }

#if PeraLink_Agent_Insurance_Renewal
    public PeraLinkCoreWcf.GetInsuranceDateResult GetInsuranceDate(PeraLinkCoreWcf.GetInsuranceDateRequest getInsuranceDateRequest)
    {
        getInsuranceDateRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.GetInsuranceDateResult getInsuranceDateResult = _serviceClient.GetInsuranceDate(getInsuranceDateRequest);

        switch (getInsuranceDateResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getInsuranceDateResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getInsuranceDateResult.Message, getInsuranceDateResult.MessageID, getInsuranceDateResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getInsuranceDateResult.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkCoreWcf.GetInsuranceProductNameResult GetInsuranceProductName(PeraLinkCoreWcf.GetInsuranceProductNameRequest getInsuranceProductNameRequest)
    {
        getInsuranceProductNameRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkCoreWcf.GetInsuranceProductNameResult getInsuranceProductNameResult = _serviceClient.GetInsuranceProductName(getInsuranceProductNameRequest);

        switch (getInsuranceProductNameResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return getInsuranceProductNameResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(getInsuranceProductNameResult.Message, getInsuranceProductNameResult.MessageID, getInsuranceProductNameResult.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getInsuranceProductNameResult.ResultStatus.ToString());
                }
        }
    }
#else
#endif
    #endregion


    void IDisposable.Dispose()
    {
        bool isClosed = false;

        try
        {
            if (this._serviceClient.State != System.ServiceModel.CommunicationState.Faulted)
            {
                this._serviceClient.Close();
                isClosed = true;
            }
            else
            {
                // Proceed with Abort in finally
            }
        }
        finally
        {
            if (!isClosed)
            {
                this._serviceClient.Abort();
            }
            else
            {
                // Do nothing since state is already closed
            }
        }
    }

}