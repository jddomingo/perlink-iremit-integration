using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CurrencyID
/// </summary>
public class CurrencyID
{
    #region Constructor
    public CurrencyID()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    public const int AED = 1;
    public const int CAD = 2;
    public const int HKD = 3;
    public const int ITL = 4;
    public const int NZD = 5;
    public const int PHP = 6;
    public const int SAR = 7;
    public const int SGD = 8;
    public const int TWD = 9;
    public const int USD = 10;
    public const int YEN = 11;
    #endregion
}