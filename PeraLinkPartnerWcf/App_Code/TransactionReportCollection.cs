using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class TransactionReportCollection: List<TransactionReport>
{
    #region Constructor
    public TransactionReportCollection()
	{ }
    #endregion

    #region Public Methods
    public void AddList(PeraLinkCoreWcf.TransactionReport[] transactionReportCollection)
    {
        Array.ForEach<PeraLinkCoreWcf.TransactionReport>(transactionReportCollection
            , delegate(PeraLinkCoreWcf.TransactionReport eachTransactionReport)
            {
                TransactionReport mappedTransactionReport = new TransactionReport();
                mappedTransactionReport.Load(eachTransactionReport);
                this.Add(mappedTransactionReport);
            });

    }
    #endregion
}