﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for PayBillResult
/// </summary>
public class PayBillResult: ServiceResult
{
    #region Constructor
    public PayBillResult()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion

    #region Fields/Properties
    private string _referenceNumber;
    [DataMember]
    public string ReferenceNumber
    {
        get { return _referenceNumber; }
        set { _referenceNumber = value; }
    }

    private decimal _serviceFee;
    [DataMember]
    public decimal ServiceFee
    {
        get { return _serviceFee; }
        set { _serviceFee = value; }
    }

    private DateTime _dateTimeProcessed;
    [DataMember]
    public DateTime DateTimeProcessed
    {
        get { return _dateTimeProcessed; }
        set { _dateTimeProcessed = value; }
    }

    private decimal _commissionFee;
    [DataMember]
    public decimal CommissionFee
    {
        get { return _commissionFee; }
        set { _commissionFee = value; }
    }
    #endregion

}