using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetDomesticPullPartnerResult: BaseResult
{
    #region Constructor
    public GetDomesticPullPartnerResult()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private PartnerCollection _partnerCollection;

    public PartnerCollection PartnerCollection
    {
        get { return _partnerCollection; }
        set { _partnerCollection = value; }
    }
    #endregion
}