﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BillsPaymentCollection
/// </summary>
public class BillsPaymentCollection:List<BillsPayment>
{
    #region Constructor
    public BillsPaymentCollection()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion

    #region Public Methods
    public void AddList(PeraLinkCoreWcf.BillsPayment[] billsPaymentTransactionCollection)
    {
        Array.ForEach<PeraLinkCoreWcf.BillsPayment>(billsPaymentTransactionCollection
            , delegate(PeraLinkCoreWcf.BillsPayment eachBillsPaymentTransaction)
            {
                BillsPayment mappedBillsPaymentTransaction = new BillsPayment();
                mappedBillsPaymentTransaction.Load(eachBillsPaymentTransaction);
                this.Add(mappedBillsPaymentTransaction);
            });
    }
    #endregion


}
