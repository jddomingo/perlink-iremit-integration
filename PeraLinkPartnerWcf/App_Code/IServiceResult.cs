using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for IServiceResult
/// </summary>
public interface IServiceResult
{
	string Message { get; set; }
	int MessageID { get; set; }
	int LogID { get; set; }
	ResultStatus ResultStatus { get; set; }
}