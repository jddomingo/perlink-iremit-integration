using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class UpdateBeneficiaryMobileRequest: BaseRequest
{
    #region Constructor
    public UpdateBeneficiaryMobileRequest()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private Int64 _beneficiaryID;
    [DataMember]
    public Int64 BeneficiaryID
    {
        get { return _beneficiaryID; }
        set { _beneficiaryID = value; }
    }

    private string _middleName;
    [DataMember]
    public string MiddleName
    {
        get { return _middleName; }
        set { _middleName = value; }
    }

    private DateTime? _birthDate;
    [DataMember]
    public DateTime? BirthDate
    {
        get { return _birthDate; }
        set { _birthDate = value; }
    }

    private int _cellphoneCountryID;
    [DataMember]
    public int CellphoneCountryID
    {
        get { return _cellphoneCountryID; }
        set { _cellphoneCountryID = value; }
    }

    [DataMember]
    public int BirthCountryID { get; set; }

    private string _cellphoneNumber;
    [DataMember]
    public string CellphoneNumber
    {
        get { return _cellphoneNumber; }
        set { _cellphoneNumber = value; }
    }

    private int _telephoneCountryID;
    [DataMember]
    public int TelephoneCountryID
    {
        get { return _telephoneCountryID; }
        set { _telephoneCountryID = value; }
    }

    private string _telephoneAreaCode;
    [DataMember]
    public string TelephoneAreaCode
    {
        get { return _telephoneAreaCode; }
        set { _telephoneAreaCode = value; }
    }

    private string _telephoneNumber;
    [DataMember]
    public string TelephoneNumber
    {
        get { return _telephoneNumber; }
        set { _telephoneNumber = value; }
    }

    private int _countryAddressID;
    [DataMember]
    public int CountryAddressID
    {
        get { return _countryAddressID; }
        set { _countryAddressID = value; }
    }

    private string _provinceAddress;
    [DataMember]
    public string ProvinceAddress
    {
        get { return _provinceAddress; }
        set { _provinceAddress = value; }
    }

    private string _address;
    [DataMember]
    public string Address
    {
        get { return _address; }
        set { _address = value; }
    }

    private string _userID;
    [DataMember]
    public string UserID
    {
        get { return _userID; }
        set { _userID = value; }
    }

	private string _zipCode;
	[DataMember]
	public string ZipCode
	{
		get { return _zipCode; }
		set { _zipCode = value; }
	}

	private string _occupation;
	[DataMember]
	public string Occupation
	{
		get { return _occupation; }
		set { _occupation = value; }
	}

	private string _sendingPartnerCode;
	[DataMember]
	public string SendingPartnerCode
	{
		get { return _sendingPartnerCode; }
		set { _sendingPartnerCode = value; }
	}

	private int? _stateIDAddress;
	[DataMember]
	public int? StateIDAddress
	{
		get { return _stateIDAddress; }
		set { _stateIDAddress = value; }
	}

	private string _tin;

	public string TIN
	{
		get { return _tin; }
		set { _tin = value; }
	}
    #endregion

    #region Internal Methods
    internal UpdateBeneficiaryMobileResult Process()
    {
        UpdateBeneficiaryMobileResult returnValue = new UpdateBeneficiaryMobileResult();

        // Important: This function validates the request credential
        this.AuthenticateRequest();

        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
            if (String.IsNullOrWhiteSpace(this.SendingPartnerCode)) { }
            else
            {
                #region Validate sending partner information
                PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
                partner.PartnerCode = this.SendingPartnerCode;
                partner = serviceClient.FindPartnerCode(partner);

                if (partner.PartnerID == 0)
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
                else { }
            }
                #endregion

            PeraLinkCoreWcf.Beneficiary beneficiary = new PeraLinkCoreWcf.Beneficiary();
            beneficiary.UpdatedBy = this.UserID;
            beneficiary.DateTimeUpdated = DateTime.Now;
            beneficiary.MiddleName = this.MiddleName;
            beneficiary.BeneficiaryID = this.BeneficiaryID;

            #region Validation

            if (this.BirthDate == null)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(40);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else { }

            #endregion

            if (this.CellphoneCountryID == 0) { }
            else
            {
                beneficiary.CellphoneCountry = new PeraLinkCoreWcf.Country();
                beneficiary.CellphoneCountry.CountryID = this.CellphoneCountryID;
            }
            beneficiary.CellphoneNumber = this.CellphoneNumber;

            if (this.TelephoneCountryID == 0) { }
            else
            {
                beneficiary.TelephoneCountry = new PeraLinkCoreWcf.Country();
                beneficiary.TelephoneCountry.CountryID = this.TelephoneCountryID;
            }
            beneficiary.TelephoneAreaCode = this.TelephoneAreaCode;
            beneficiary.TelephoneNumber = this.TelephoneNumber;

            if (this.CountryAddressID == 0) { }
            else
            {
                beneficiary.CountryAddress = new PeraLinkCoreWcf.Country();
                beneficiary.CountryAddress.CountryID = this.CountryAddressID;
            }

			if (this.StateIDAddress == null) { }
			else
			{
				beneficiary.StateIDAddress = this.StateIDAddress.Value;
			}

            if (this.BirthCountryID == 0) { /* Do nothing */ }
            else
            {
                beneficiary.BirthCountry = new PeraLinkCoreWcf.Country() { CountryID = this.BirthCountryID };
            }

            beneficiary.ProvinceAddress = this.ProvinceAddress;
            beneficiary.Address = this.Address;
            beneficiary.BirthDate = this.BirthDate;

            if (this.SendingPartnerCode == SystemSetting.MoneyGramPartnerCode)
            {
                if (String.IsNullOrWhiteSpace(this.Address))
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(49);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }

                if (String.IsNullOrWhiteSpace(this.ZipCode))
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(50);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }

                if (String.IsNullOrWhiteSpace(this.Occupation))
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(51);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
            }

            beneficiary.Address = this.Address;
            beneficiary.ZipCode = this.ZipCode;
            beneficiary.Occupation = this.Occupation;
			beneficiary.TIN = this.TIN;

            serviceClient.UpdateBeneficiary(beneficiary);
        }

        returnValue.ResultStatus = ResultStatus.Successful;

        return returnValue;
    }
    #endregion
}