using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Client
/// </summary>
public class Client
{
    #region Constructor
    public Client()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private Int64 _clientID;

    public Int64 ClientID
    {
        get { return _clientID; }
        set { _clientID = value; }
    }

    private string _clientNumber;

    public string ClientNumber
    {
        get { return _clientNumber; }
        set { _clientNumber = value; }
    }

    private string _firstName;

    public string FirstName
    {
        get { return _firstName; }
        set { _firstName = value; }
    }

    private string _middleName;

    public string MiddleName
    {
        get { return _middleName; }
        set { _middleName = value; }
    }

    private string _lastName;

    public string LastName
    {
        get { return _lastName; }
        set { _lastName = value; }
    }

    private DateTime? _birthDate;

    public DateTime? BirthDate
    {
        get { return _birthDate; }
        set { _birthDate = value; }
    }
    private Country _cellphoneCountry;

    public Country CellphoneCountry
    {
        get { return _cellphoneCountry; }
        set { _cellphoneCountry = value; }
    }

    private string _cellphoneNumber;

    public string CellphoneNumber
    {
        get { return _cellphoneNumber; }
        set { _cellphoneNumber = value; }
    }

    private Country _telephoneCountry;

    public Country TelephoneCountry
    {
        get { return _telephoneCountry; }
        set { _telephoneCountry = value; }
    }

    private string _telephoneAreaCode;

    public string TelephoneAreaCode
    {
        get { return _telephoneAreaCode; }
        set { _telephoneAreaCode = value; }
    }

    private string _telephoneNumber;

    public string TelephoneNumber
    {
        get { return _telephoneNumber; }
        set { _telephoneNumber = value; }
    }

    private Country _countryAddress;

    public Country CountryAddress
    {
        get { return _countryAddress; }
        set { _countryAddress = value; }
    }

    private string _provinceAddress;

    public string ProvinceAddress
    {
        get { return _provinceAddress; }
        set { _provinceAddress = value; }
    }

    private string _address;

    public string Address
    {
        get { return _address; }
        set { _address = value; }
    }

	private ClientSourceOfFund _clientSourceOfFund;

	public ClientSourceOfFund ClientSourceOfFund
	{
		get { return _clientSourceOfFund; }
		set { _clientSourceOfFund = value; }
	}

	private string _tin;

	public string TIN
	{
		get { return _tin; }
		set { _tin = value; }
	}
    #endregion

    #region Public Methods
    public void Load(PeraLinkCoreWcf.Client client)
    {
        if (client == null)
        { }
        else
        {
            this.ClientID = client.ClientID;
            this.BirthDate = client.BirthDate;
            this.ClientNumber = client.ClientNumber;
            this.FirstName = client.FirstName;
            this.LastName = client.LastName;
            this.MiddleName = client.MiddleName;
            this.CellphoneCountry = new Country();
            this.CellphoneCountry.Load(client.CellphoneCountry);
            this.CellphoneNumber = client.CellphoneNumber;
            this.TelephoneCountry = new Country();
            this.TelephoneCountry.Load(client.TelephoneCountry);
            this.TelephoneAreaCode = client.TelephoneAreaCode;
            this.TelephoneNumber = client.TelephoneNumber;
            this.CountryAddress = new Country();
            this.CountryAddress.Load(client.CountryAddress);
            this.ProvinceAddress = client.ProvinceAddress;
            this.Address = client.Address;
			this.ClientSourceOfFund = new ClientSourceOfFund();
			this.ClientSourceOfFund.Load(client.ClientSourceOfFund);
			this.TIN = client.TIN;
        }
    }
    #endregion
}