using System;
using System.Collections.Generic;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetInternationalRemittanceHistoryResult: BaseResult
{
    #region Constructor
    public GetInternationalRemittanceHistoryResult()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private InternationalRemittanceCollection _internationalRemittanceCollection;
    [DataMember]
    public InternationalRemittanceCollection InternationalRemittanceCollection
    {
        get { return _internationalRemittanceCollection; }
        set { _internationalRemittanceCollection = value; }
    }

    private int _totalPageCount;
    [DataMember]
    public int TotalPageCount
    {
        get { return _totalPageCount; }
        set { _totalPageCount = value; }
    }
    #endregion
}