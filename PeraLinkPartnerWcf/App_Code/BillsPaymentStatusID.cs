﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BillsPaymentStatusID
/// </summary>
public class BillsPaymentStatusID
{
    #region Constructor
    public BillsPaymentStatusID()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion

    #region Fields/Properties
    public const int InProcess = 1;
    public const int Successful = 2;
    public const int Failed = 3;
    #endregion
}