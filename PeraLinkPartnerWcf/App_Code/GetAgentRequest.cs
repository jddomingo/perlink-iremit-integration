using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;


[DataContract]
public class GetAgentRequest: BaseRequest
{
	#region Constructor
	public GetAgentRequest()
	{
		//

		//
	}
	#endregion

	#region Fields

	private Agent _agent;
	[DataMember]
	public Agent Agent
	{
		get { return _agent; }
		set { _agent = value; }
	}

	private string _partnerCode;
	[DataMember]
	public string PartnerCode
	{
		get { return _partnerCode; }
		set { _partnerCode = value; }
	}

	private string _token;
	[DataMember]
	public string Token
	{
		get { return _token; }
		set { _token = value; }
	}

	private string _agentCode;
	[DataMember]
	public string AgentCode
	{
		get { return _agentCode; }
		set { _agentCode = value; }
	}
	#endregion

	#region Internal Methods

	internal GetAgentResult Process()
	{
		GetAgentResult returnValue = new GetAgentResult();
        this.AuthenticateRequest();

		using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
		{
            #region Validate Partner Record
            PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
            partner.PartnerCode = this.PartnerCode;
            partner = serviceClient.FindPartnerCode(partner);

            if (partner.PartnerID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else { }

			if (partner.Token == this.Token) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (partner.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            #endregion

			PeraLinkCoreWcf.Agent agent = new PeraLinkCoreWcf.Agent();
			agent.AgentCode = this.AgentCode;
			agent.Partner = partner;
            
			agent = serviceClient.FindAgentCodeOfPartner(agent);
			returnValue.Partner = new Partner();
			returnValue.Partner.Load(partner);
            returnValue.Agent = new Agent();
			returnValue.Agent.Load(agent);
		}

		returnValue.ResultStatus = ResultStatus.Successful;
		return returnValue;
	}

	

	#endregion
}