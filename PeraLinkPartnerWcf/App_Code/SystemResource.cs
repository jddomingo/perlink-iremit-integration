using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class SystemResource
{
    #region Constructor
    public SystemResource()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    [Obsolete("The _messageCollection is deprecated, please use _systemMessageCollection instead.")]
    private static PeraLinkCoreWcf.Message[] _messageCollection;
    private static Dictionary<int, PeraLinkCoreWcf.SystemMessage> _systemMessageCollection = new Dictionary<int, PeraLinkCoreWcf.SystemMessage>();
    #endregion

    #region Public Methods
    [Obsolete("GetMessageWithListID is deprecated, please use GetSystemMessage instead.")]
    public static PeraLinkCoreWcf.Message GetMessageWithListID(int listID)
    {
        // Important: List ID must be equivalent to its value minus 1 to match its position in array or list.
        return _messageCollection[listID - 1];
    }

    public static PeraLinkCoreWcf.SystemMessage GetSystemMessage(int systemMessageID)
    {
        PeraLinkCoreWcf.SystemMessage returnValue;

        _systemMessageCollection.TryGetValue(systemMessageID, out returnValue);

        if (returnValue != null)
        {
            return returnValue;
        }
        else
        {
            using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
            {
                PeraLinkCoreWcf.GetSystemMessageRequest getSystemMessageRequest = new PeraLinkCoreWcf.GetSystemMessageRequest();
                getSystemMessageRequest.SystemMessageID = systemMessageID;
                PeraLinkCoreWcf.GetSystemMessageResult getSystemMessageResult = serviceClient.GetSystemMessage(getSystemMessageRequest);
                _systemMessageCollection.Add(systemMessageID, getSystemMessageResult.SystemMessage);
                returnValue = getSystemMessageResult.SystemMessage;

                return returnValue;
            }
        }
    }

    public static void Initialize()
    {
        
        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
            PeraLinkCoreWcf.SystemName peraLinkPartnerWcf = new PeraLinkCoreWcf.SystemName();
            peraLinkPartnerWcf.SystemNameID = SystemNameID.PeraLinkPartnerWcf;
            _messageCollection = serviceClient.GetMessageCollection(peraLinkPartnerWcf);
        }
    }
    #endregion
}