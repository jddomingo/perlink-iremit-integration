using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for IdentificationType
/// </summary>
public class IdentificationType
{
    #region Constructor
    public IdentificationType()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private int _identificationTypeID;

    public int IdentificationTypeID
    {
        get { return _identificationTypeID; }
        set { _identificationTypeID = value; }
    }
    private string _description;

    public string Description
    {
        get { return _description; }
        set { _description = value; }
    }
    private string _smsCode;

    public string SmsCode
    {
        get { return _smsCode; }
        set { _smsCode = value; }
    }
    #endregion

    #region Public Methods
    public void Load(PeraLinkCoreWcf.IdentificationType identificationType)
    {
        this.Description = identificationType.Description;
        this.IdentificationTypeID = identificationType.IdentificationTypeID;
        this.SmsCode = identificationType.SmsCode;
    }
    #endregion
}