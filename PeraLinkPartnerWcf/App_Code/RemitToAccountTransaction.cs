using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for RemitToAccountTransaction
/// </summary>
public class RemitToAccountTransaction
{
	#region Constructor
	public RemitToAccountTransaction()
	{ }
	#endregion

	#region Fields/Properties
	private Int64 _remitToAccountTransactionID;
	public Int64 RemitToAccountTransactionID { get { return _remitToAccountTransactionID; } set { _remitToAccountTransactionID = value; } }

	private string _controlNumber;
	public string ControlNumber { get { return _controlNumber; } set { _controlNumber = value; } }

	private Int64 _senderClientID;

	public Int64 SenderClientID
	{
		get { return _senderClientID; }
		set { _senderClientID = value; }
	}

	private string _senderClientNumber;

	public string SenderClientNumber
	{
		get { return _senderClientNumber; }
		set { _senderClientNumber = value; }
	}

	private string _senderFirstName;

	public string SenderFirstName
	{
		get { return _senderFirstName; }
		set { _senderFirstName = value; }
	}

	private string _senderMiddleName;

	public string SenderMiddleName
	{
		get { return _senderMiddleName; }
		set { _senderMiddleName = value; }
	}

	private string _senderLastName;

	public string SenderLastName
	{
		get { return _senderLastName; }
		set { _senderLastName = value; }
	}

	private string _beneficiaryLastName;
	public string BeneficiaryLastName { get { return RDFramework.Utility.Conversion.ToUpper(_beneficiaryLastName); } set { _beneficiaryLastName = value; } }

	private string _beneficiaryFirstName;
	public string BeneficiaryFirstName { get { return RDFramework.Utility.Conversion.ToUpper(_beneficiaryFirstName); } set { _beneficiaryFirstName = value; } }

	private string _beneficiaryMiddleName;
	public string BeneficiaryMiddleName { get { return RDFramework.Utility.Conversion.ToUpper(_beneficiaryMiddleName); } set { _beneficiaryMiddleName = value; } }

	private string _beneficiaryAddress;
	public string BeneficiaryAddress { get { return RDFramework.Utility.Conversion.ToUpper(_beneficiaryAddress); } set { _beneficiaryAddress = value; } }

	private string _beneficiaryMobileNumber;
	public string BeneficiaryMobileNumber { get { return _beneficiaryMobileNumber; } set { _beneficiaryMobileNumber = value; } }

	private string _accountNumber;
	public string AccountNumber { get { return _accountNumber; } set { _accountNumber = value; } }

	private decimal _principalAmount;
	public decimal PrincipalAmount { get { return _principalAmount; } set { _principalAmount = value; } }

	private int _sendCurrencyID;
	public int SendCurrencyID { get { return _sendCurrencyID; } set { _sendCurrencyID = value; } }

	private string _sendCurrencyCode;
	public string SendCurrencyCode { get { return _sendCurrencyCode; } set { _sendCurrencyCode = value; } }

	private decimal _serviceFee;
	public decimal ServiceFee { get { return _serviceFee; } set { _serviceFee = value; } }

	private string _promoCode;
	public string PromoCode { get { return _promoCode; } set { _promoCode = value; } }

	private DateTime _dateTimeSent;
	public DateTime DateTimeSent { get { return _dateTimeSent; } set { _dateTimeSent = value; } }

	private string _sentByPartnerCode;
	public string SentByPartnerCode { get { return _sentByPartnerCode; } set { _sentByPartnerCode = value; } }
	
	private Int64 _sentByAgentID;
	public Int64 SentByAgentID { get { return _sentByAgentID; } set { _sentByAgentID = value; } }

	private string _sentByAgentCode;
	public string SentByAgentCode { get { return _sentByAgentCode; } set { _sentByAgentCode = value; } }

	private string _sentByUserID;
	public string SentByUserID { get { return RDFramework.Utility.Conversion.ToUpper(_sentByUserID); } set { _sentByUserID = value; } }

	private int _remitToAccountStatusID;
	public int RemitToAccountStatusID { get { return _remitToAccountStatusID; } set { _remitToAccountStatusID = value; } }

	private bool _printable;
	public bool Printable { get { return _printable; } set { _printable = value; } }

	private string _remitToAccountStatusDescription;
	public string RemitToAccountStatusDescription { get { return _remitToAccountStatusDescription; } set { _remitToAccountStatusDescription = value; } }

	private Int64 _agentBankID;
	public Int64 AgentBankID { get { return _agentBankID; } set { _agentBankID = value; } }

	private string _agentBankCode;
	public string AgentBankCode { get { return _agentBankCode; } set { _agentBankCode = value; } }

	private string _agentBank;
	public string AgentBank { get { return _agentBank; } set { _agentBank = value; } }

	private string _partnerStatusCode;
	public string PartnerStatusCode { get { return _partnerStatusCode; } set { _partnerStatusCode = value; } }

	private string _partnerReferenceNumber;
	public string PartnerReferenceNumber { get { return _partnerReferenceNumber; } set { _partnerReferenceNumber = value; } }

	private DateTime? _dateTimeUpdated;
	public DateTime? DateTimeUpdated { get { return _dateTimeUpdated; } set { _dateTimeUpdated = value; } }

	private Int64 _sentByTerminalID;
	public Int64 SentByTerminalID { get { return _sentByTerminalID; } set { _sentByTerminalID = value; } }
	
	private string _sentByTerminalCode;
	public string SentByTerminalCode { get { return _sentByTerminalCode; } set { _sentByTerminalCode = value; } }
	#endregion

	#region Public Methods
	public void Load(PeraLinkCoreWcf.RemitToAccountTransaction remitToAccountTransaction)
	{
		this.AccountNumber = remitToAccountTransaction.AccountNumber;
		this.AgentBankCode = remitToAccountTransaction.AgentBank.AgentCode;
		this.AgentBankID = remitToAccountTransaction.AgentBank.AgentID;
		this.BeneficiaryAddress = remitToAccountTransaction.BeneficiaryAddress;
		this.BeneficiaryFirstName = remitToAccountTransaction.BeneficiaryFirstName;
		this.BeneficiaryLastName = remitToAccountTransaction.BeneficiaryLastName;
		this.BeneficiaryMiddleName = remitToAccountTransaction.BeneficiaryMiddleName;
		this.BeneficiaryMobileNumber = remitToAccountTransaction.BeneficiaryMobileNumber;
		this.ControlNumber = remitToAccountTransaction.ControlNumber;
		this.DateTimeSent = remitToAccountTransaction.DateTimeSent;
		this.DateTimeUpdated = remitToAccountTransaction.DateTimeUpdated;
		this.PartnerReferenceNumber = remitToAccountTransaction.PartnerReferenceNumber;
		this.PartnerStatusCode = remitToAccountTransaction.PartnerStatusCode;
		this.PrincipalAmount = remitToAccountTransaction.PrincipalAmount;
		this.PromoCode = remitToAccountTransaction.PromoCode;
		this.RemitToAccountStatusDescription = remitToAccountTransaction.RemitToAccountStatus.Description;
		this.RemitToAccountStatusID = remitToAccountTransaction.RemitToAccountStatus.RemitToAccountStatusID;
		this.Printable = remitToAccountTransaction.RemitToAccountStatus.Printable;
		this.RemitToAccountTransactionID = remitToAccountTransaction.RemitToAccountTransactionID;
		this.SendCurrencyCode = remitToAccountTransaction.SendCurrency.Code;
		this.SendCurrencyID = remitToAccountTransaction.SendCurrency.CurrencyID;
		this.SenderClientID = remitToAccountTransaction.SenderClient.ClientID;
		this.SenderClientNumber = remitToAccountTransaction.SenderClient.ClientNumber;
		this.SenderFirstName = remitToAccountTransaction.SenderClient.FirstName;
		this.SenderLastName = remitToAccountTransaction.SenderClient.LastName;
		this.SenderMiddleName = remitToAccountTransaction.SenderClient.MiddleName;
		this.SentByAgentCode = remitToAccountTransaction.SentByAgent.AgentCode;
		this.SentByAgentID = remitToAccountTransaction.SentByAgent.AgentID;
		this.SentByUserID = remitToAccountTransaction.SentByUserID;
		this.SentByPartnerCode = remitToAccountTransaction.SentByAgent.Partner.PartnerCode;
		this.AgentBank = remitToAccountTransaction.AgentBank.Name;
		this.ServiceFee = remitToAccountTransaction.ServiceFee;
		                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
		if (remitToAccountTransaction.SentByTerminal != null)
		{
			this.SentByTerminalID = remitToAccountTransaction.SentByTerminal.TerminalID;
			this.SentByTerminalCode = remitToAccountTransaction.SentByTerminal.TerminalCode;
		}
	}
	#endregion
}