using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class UpdateClientRequest: BaseRequest
{
    #region Constructor
    public UpdateClientRequest()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private Int64 _clientID;
    [DataMember]
    public Int64 ClientID
    {
        get { return _clientID; }
        set { _clientID = value; }
    }

    private string _clientNumber;
    [DataMember]
    public string ClientNumber
    {
        get { return _clientNumber; }
        set { _clientNumber = value; }
    }

    private string _middleName;
    [DataMember]
    public string MiddleName
    {
        get { return _middleName; }
        set { _middleName = value; }
    }

    private DateTime? _birthDate;
    [DataMember]
    public DateTime? BirthDate
    {
        get { return _birthDate; }
        set { _birthDate = value; }
    }

    private int _cellphoneCountryID;
    [DataMember]
    public int CellphoneCountryID
    {
        get { return _cellphoneCountryID; }
        set { _cellphoneCountryID = value; }
    }

    private string _cellphoneNumber;
    [DataMember]
    public string CellphoneNumber
    {
        get { return _cellphoneNumber; }
        set { _cellphoneNumber = value; }
    }

    private int _telephoneCountryID;
    [DataMember]
    public int TelephoneCountryID
    {
        get { return _telephoneCountryID; }
        set { _telephoneCountryID = value; }
    }

    private string _telephoneAreaCode;
    [DataMember]
    public string TelephoneAreaCode
    {
        get { return _telephoneAreaCode; }
        set { _telephoneAreaCode = value; }
    }

    private string _telephoneNumber;
    [DataMember]
    public string TelephoneNumber
    {
        get { return _telephoneNumber; }
        set { _telephoneNumber = value; }
    }

    private int _countryAddressID;
    [DataMember]
    public int CountryAddressID
    {
        get { return _countryAddressID; }
        set { _countryAddressID = value; }
    }

    private string _provinceAddress;
    [DataMember]
    public string ProvinceAddress
    {
        get { return _provinceAddress; }
        set { _provinceAddress = value; }
    }

    private string _address;
    [DataMember]
    public string Address
    {
        get { return _address; }
        set { _address = value; }
    }

    private string _partnerCode;
    [DataMember]
    public string PartnerCode
    {
        get { return _partnerCode; }
        set { _partnerCode = value; }
    }

    private string _token;
    [DataMember]
    public string Token
    {
        get { return _token; }
        set { _token = value; }
    }

    private string _agentCode;
    [DataMember]
    public string AgentCode
    {
        get { return _agentCode; }
        set { _agentCode = value; }
    }

    private string _userID;
    [DataMember]
    public string UserID
    {
        get { return _userID; }
        set { _userID = value; }
    }

	private int _clientSourceOfFundID;
	[DataMember]
	public int ClientSourceOfFundID
	{
		get { return _clientSourceOfFundID; }
		set { _clientSourceOfFundID = value; }
	}

	private string _tin;
	[DataMember]
	public string TIN
	{
		get { return _tin; }
		set { _tin = value; }
	}
    #endregion

    #region Internal Methods
    internal UpdateClientResult Process()
    {
        UpdateClientResult returnValue = new UpdateClientResult();

        // Important: This function validates the request credential
        this.AuthenticateRequest();

        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {

            #region Validate Partner Record
            PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
            partner.PartnerCode = this.PartnerCode;
            partner = serviceClient.FindPartnerCode(partner);

            if (partner.PartnerID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else { }

            if (partner.Token == this.Token) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (partner.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            #endregion

            #region Validate Agent Record
            PeraLinkCoreWcf.Agent agent = new PeraLinkCoreWcf.Agent();
            agent.AgentCode = this.AgentCode;
            agent.Partner = partner;
            agent = serviceClient.FindAgentCodeOfPartner(agent);

            if (agent.AgentID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }

            if (agent.Partner.PartnerID == partner.PartnerID)
            { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(43);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (agent.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(46);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            #endregion

            PeraLinkCoreWcf.Client clientRecord = new PeraLinkCoreWcf.Client();
            clientRecord.ClientID = this.ClientID;
            clientRecord = serviceClient.GetClient(clientRecord);

            if (clientRecord.ClientID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(24);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                if (clientRecord.ClientNumber == this.ClientNumber) { }
                else 
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(37);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
            }

            #region Validation

            #region Contact Information
            bool hasCellphoneCountryID = false;
            bool hasCellphoneNumber = false;
            bool hasCellphoneInformation = false;
            if (string.IsNullOrWhiteSpace(this.CellphoneNumber))
            {
                hasCellphoneNumber = false;
            }
            else
            {
                if (RDFramework.Utility.Validation.IsNumeric(this.CellphoneNumber))
                {
                    hasCellphoneNumber = true;
                }
                else
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(31);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
            }

            if (this.CellphoneCountryID == 0)
            {
                hasCellphoneCountryID = false;
            }
            else
            {
                hasCellphoneCountryID = true;
            }

            if (hasCellphoneCountryID == hasCellphoneNumber)
            {
                if (hasCellphoneCountryID)
                {
                    hasCellphoneInformation = true;
                }
                else
                {
                    hasCellphoneInformation = false;
                }
            }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(32);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            bool hasTelephoneCountryID = false;
            bool hasTelephoneAreaCode = false;
            bool hasTelephoneNumber = false;
            bool hasTelephoneInformation = false;
            if (this.TelephoneCountryID == 0)
            {
                hasTelephoneCountryID = false;
            }
            else
            {
                hasTelephoneCountryID = true;
            }
            if (string.IsNullOrWhiteSpace(this.TelephoneAreaCode))
            {
                hasTelephoneAreaCode = false;
            }
            else
            {
                if (RDFramework.Utility.Validation.IsNumeric(this.TelephoneAreaCode))
                {
                    hasTelephoneAreaCode = true;
                }
                else
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(34);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
            }

            if (string.IsNullOrWhiteSpace(this.TelephoneNumber))
            {
                hasTelephoneNumber = false;
            }
            else
            {
                if (RDFramework.Utility.Validation.IsNumeric(this.TelephoneNumber))
                {
                    hasTelephoneNumber = true;
                }
                else
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(33);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
            }

            if (hasTelephoneCountryID == hasTelephoneAreaCode
                && hasTelephoneAreaCode == hasTelephoneNumber
                && hasTelephoneCountryID == hasTelephoneNumber)
            {
                if (hasTelephoneCountryID)
                {
                    hasTelephoneInformation = true;
                }
                else
                {
                    hasTelephoneInformation = false;
                }
            }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(35);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (hasCellphoneInformation || hasTelephoneInformation)
            {
                // Proceed
            }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(36);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            #endregion

            if (this.BirthDate == null)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(40);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else { }

            #endregion

            PeraLinkCoreWcf.Client client = new PeraLinkCoreWcf.Client();
            client.BirthDate = this.BirthDate;
            client.MiddleName = this.MiddleName;
            client.UpdatedBy = this.UserID;
            client.DateTimeUpdated = DateTime.Now;
            client.ClientID = this.ClientID;
            client.BirthDate = this.BirthDate;

            if (this.CellphoneCountryID == 0) { }
            else
            {
                client.CellphoneCountry = new PeraLinkCoreWcf.Country();
                client.CellphoneCountry.CountryID = this.CellphoneCountryID;
            }
            client.CellphoneNumber = this.CellphoneNumber;

            if (this.TelephoneCountryID == 0) { }
            else
            {
                client.TelephoneCountry = new PeraLinkCoreWcf.Country();
                client.TelephoneCountry.CountryID = this.TelephoneCountryID;
            }
            client.TelephoneAreaCode = this.TelephoneAreaCode;
            client.TelephoneNumber = this.TelephoneNumber;

            if (this.CountryAddressID == 0) { }
            else
            {
                client.CountryAddress = new PeraLinkCoreWcf.Country();
                client.CountryAddress.CountryID = this.CountryAddressID;
            }

			if (this.ClientSourceOfFundID == 0) { }
			else
			{
				client.ClientSourceOfFund = new PeraLinkCoreWcf.ClientSourceOfFund();
				client.ClientSourceOfFund.SourceOfFundID = Convert.ToInt64(this.ClientSourceOfFundID);
			}
            client.ProvinceAddress = this.ProvinceAddress;
            client.Address = this.Address;
			client.TIN = this.TIN;

            serviceClient.UpdateClient(client);
        }

        returnValue.ResultStatus = ResultStatus.Successful;

        return returnValue;
    }
    #endregion
}