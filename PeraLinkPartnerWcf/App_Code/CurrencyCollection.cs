using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CurrencyCollection
/// </summary>
public class CurrencyCollection: List<Currency>
{
    #region Constructor
    public CurrencyCollection()
	{
		//

		//
    }
    #endregion

    #region Public Methods
    public void AddList(PeraLinkCoreWcf.Currency[] currencyCollection)
    {
        Array.ForEach<PeraLinkCoreWcf.Currency>(currencyCollection
            , delegate(PeraLinkCoreWcf.Currency eachCurrency)
            {
                Currency mappedCurrency = new Currency();
                mappedCurrency.Load(eachCurrency);
                this.Add(mappedCurrency);
            });
    }
    #endregion
}