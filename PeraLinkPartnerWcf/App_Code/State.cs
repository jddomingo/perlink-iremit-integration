using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for State
/// </summary>
public class State
{
    #region Constructor
    public State()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    #endregion

    #region Fields/Properties
    private int _stateID;

    public int StateID
    {
        get { return _stateID; }
        set { _stateID = value; }
    }

    private int _countryID;

    public int CountryID
    {
        get { return _countryID; }
        set { _countryID = value; }
    }

    private string _countryCodeIso2;

    public string CountryCodeIso2
    {
        get { return _countryCodeIso2; }
        set { _countryCodeIso2 = value; }
    }

    private string _countryCodeIso3;

    public string CountryCodeIso3
    {
        get { return _countryCodeIso3; }
        set { _countryCodeIso3 = value; }
    }

    private string _stateCode;

    public string StateCode
    {
        get { return _stateCode; }
        set { _stateCode = value; }
    }

    private string _stateName;

    public string StateName
    {
        get { return _stateName; }
        set { _stateName = value; }
    }
    #endregion

    #region Public Methods
    public void Load(PeraLinkCoreWcf.State state)
    {
        if (state == null)
        { }
        else
        {
            this.StateID = state.StateID;
            this.CountryID = state.CountryID;
            this.CountryCodeIso3 = state.CountryCodeIso3;
            this.CountryCodeIso2 = state.CountryCodeIso2;
            this.StateCode = state.StateCode;
            this.StateName = state.StateName;
        }
    }
    #endregion
}