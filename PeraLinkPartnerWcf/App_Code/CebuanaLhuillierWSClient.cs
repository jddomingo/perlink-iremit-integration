using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for CebuanaLhuillierWSClient
/// </summary>
public class CebuanaLhuillierWSClient : IDisposable
{
    #region Constructor
    public CebuanaLhuillierWSClient()
	{
        _serviceSoapClient = new CebuanaLhuillierWS.ServiceSoapClient();
    }
    #endregion

    #region Fields/Properties
    private CebuanaLhuillierWS.ServiceSoapClient _serviceSoapClient;
    #endregion

    #region Public Methods
    public void PayoutDomesticRemittance(CebuanaLhuillierWS.PayoutDomesticRemittanceRequest payoutDomesticRemittanceRequest)
    {
        string status = _serviceSoapClient.PayoutDomesticRemittance(payoutDomesticRemittanceRequest);

        if (status == "Success")
        {
            return;
        }
        else
        {
            throw new RDFramework.ClientException(status);
        }
    }

    public PeraLinkCoreWcf.DomesticRemittance FindDomesticRemittance(string controlNumber)
    {
        DataTable transaction = _serviceSoapClient.FindDomesticRemittance(controlNumber);

        if (transaction == null)
        {
            return null;
        }
        else
        {
            PeraLinkCoreWcf.DomesticRemittance returnValue = new PeraLinkCoreWcf.DomesticRemittance();
            returnValue.DomesticRemittanceID = Int64.Parse(transaction.Rows[0]["fldTUID"].ToString());
            returnValue.Beneficiary = new PeraLinkCoreWcf.Beneficiary();
            returnValue.Beneficiary.FirstName = transaction.Rows[0]["fldBFirstName"].ToString().Trim();
			returnValue.Beneficiary.LastName = transaction.Rows[0]["fldBlastName"].ToString().Trim();
			returnValue.Beneficiary.MiddleName = transaction.Rows[0]["fldBMiddleName"].ToString().Trim();
            returnValue.Beneficiary.BirthDate = DateTime.Parse(transaction.Rows[0]["fldBBirthdate"].ToString());
            returnValue.Beneficiary.Client = new PeraLinkCoreWcf.Client();
            returnValue.Beneficiary.Client.ClientNumber = transaction.Rows[0]["fldBeneNum"].ToString();
			
            returnValue.ControlNumber = transaction.Rows[0]["fldControlNum"].ToString();
            returnValue.DateTimeSent = DateTime.Parse(transaction.Rows[0]["fldTDateCreated"].ToString());
            returnValue.DateTimeUpdated = DateTime.Parse(transaction.Rows[0]["fldTLastUpdated"].ToString());
            returnValue.DomesticPayoutDetail = new PeraLinkCoreWcf.DomesticPayoutDetail();
            returnValue.DomesticPayoutDetail.PayoutAmount = decimal.Parse(transaction.Rows[0]["fldAmount"].ToString());
            returnValue.DomesticPayoutDetail.PayoutCurrency = new PeraLinkCoreWcf.Currency();
            returnValue.DomesticPayoutDetail.PayoutCurrency.Code = transaction.Rows[0]["fldCCY2"].ToString();
            returnValue.PrincipalAmount = decimal.Parse(transaction.Rows[0]["fldAmount"].ToString());
			returnValue.ServiceFee = decimal.Parse(transaction.Rows[0]["fldOtherCharges"].ToString());
            returnValue.RemittanceStatus = MapStatus(int.Parse(transaction.Rows[0]["fldTranStatus"].ToString()));
            returnValue.SendCurrency = new PeraLinkCoreWcf.Currency();
            returnValue.SendCurrency.Code = transaction.Rows[0]["fldCCY1"].ToString();

            returnValue.SenderClient = new PeraLinkCoreWcf.Client();
			returnValue.SenderClient.FirstName = transaction.Rows[0]["fldSFirstName"].ToString().Trim();
			returnValue.SenderClient.LastName = transaction.Rows[0]["fldSLastName"].ToString().Trim();
			returnValue.SenderClient.MiddleName = transaction.Rows[0]["fldSMiddleName"].ToString().Trim();
            returnValue.SenderClient.BirthDate = DateTime.Parse(transaction.Rows[0]["fldSBirthdate"].ToString());
            returnValue.SenderClient.ClientNumber = transaction.Rows[0]["fldCustomerNum"].ToString();

            returnValue.SentByAgent = new PeraLinkCoreWcf.Agent();
            returnValue.SentByAgent.Partner = new PeraLinkCoreWcf.Partner();
            returnValue.SentByAgent.Partner.PartnerCode = SystemSetting.CebuanaLhuillierPartnerCode;

            using (PeraLinkCoreWcfClient peraLinkCoreWcfClient = new PeraLinkCoreWcfClient())
            {
                returnValue.SentByAgent.Partner = peraLinkCoreWcfClient.FindPartnerCode(returnValue.SentByAgent.Partner);
            }
            returnValue.SentByAgent.AgentCode = transaction.Rows[0]["fldSendingBranch"].ToString();

            returnValue.SentByUserID = transaction.Rows[0]["fldCreatorID"].ToString();

            return returnValue;
        }
    }

    public static PeraLinkCoreWcf.RemittanceStatus MapStatus(int cebuanaLhuillierStatusID)
    {
        switch (cebuanaLhuillierStatusID)
        {
            case 5:
            case 6:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 1;
                    returnValue.Description = "Outstanding";

                    return returnValue;
                }
            case 1:
            case 22:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 4;
                    returnValue.Description = "Cancelled";

                    return returnValue;
                }
            case 2:
            case 3:
            case 20:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 2;
                    returnValue.Description = "Paid";

                    return returnValue;
                }
            case 7:
            case 8:
            case 24:
            case 25:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 3;
                    returnValue.Description = "Refunded";

                    return returnValue;
                }
            case 9:
            case 10:
            case 11:
            case 21:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 5;
                    returnValue.Description = "Amended";

                    return returnValue;
                }
            case 37:
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(14);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException();
                }
        }
    }

    public PeraLinkCoreWcf.InternationalRemittance FindInternationalRemittance(string controlNumber)
    {
        string[][] resultTransaction = _serviceSoapClient.GetRemittanceTieUpTransaction(controlNumber, SystemUtility.GenerateRemittanceAuditTrail());

        DataTable transaction = new DataTable();

        if (resultTransaction == null)
        {
            transaction = null;
        }
        else
        {
            transaction = SystemUtility.ConvertStringArrayToDataTable(resultTransaction);
        }

        if (transaction == null)
        {
            return null;
        }
        else
        {
            PeraLinkCoreWcf.InternationalRemittance returnValue = new PeraLinkCoreWcf.InternationalRemittance();
            returnValue.InternationalRemittanceID = Int64.Parse(transaction.Rows[0]["fldTUUID"].ToString());
            returnValue.Beneficiary = new PeraLinkCoreWcf.Beneficiary();
            returnValue.Beneficiary.FirstName = transaction.Rows[0]["fldBFirstName"].ToString().Trim();
            returnValue.Beneficiary.LastName = transaction.Rows[0]["fldBlastName"].ToString().Trim();
            //returnValue.Beneficiary.MiddleName = transaction.Rows[0]["fldBMiddleName"].ToString().Trim();  
            //returnValue.Beneficiary.BirthDate = DateTime.Parse(transaction.Rows[0]["fldBBirthdate"].ToString());  
            //returnValue.Beneficiary.Client = new PeraLinkCoreWcf.Client();  
            //returnValue.Beneficiary.Client.ClientNumber = transaction.Rows[0]["fldBUID"].ToString();  

            returnValue.ControlNumber = transaction.Rows[0]["fldControlNum"].ToString();
            returnValue.DateTimeSent = DateTime.Parse(transaction.Rows[0]["fldTDateCreated"].ToString());
            //returnValue.DateTimeUpdated = DateTime.Parse(transaction.Rows[0]["fldTLastUpdated"].ToString());  
            returnValue.InternationalPayoutDetail = new PeraLinkCoreWcf.InternationalPayoutDetail();
            returnValue.InternationalPayoutDetail.PayoutAmount = decimal.Parse(transaction.Rows[0]["fldAmount"].ToString());
            returnValue.InternationalPayoutDetail.PayoutCurrency = new PeraLinkCoreWcf.Currency();
            returnValue.InternationalPayoutDetail.PayoutCurrency.Code = transaction.Rows[0]["fldCCY2"].ToString();
            returnValue.PrincipalAmount = decimal.Parse(transaction.Rows[0]["fldAmount"].ToString());
            //returnValue.ServiceFee = decimal.Parse(transaction.Rows[0]["fldOtherCharges"].ToString());  
            returnValue.RemittanceStatus = MapStatus(int.Parse(transaction.Rows[0]["fldTranStatus"].ToString()));
            returnValue.SendCurrency = new PeraLinkCoreWcf.Currency();
            returnValue.SendCurrency.Code = transaction.Rows[0]["fldCCY2"].ToString();

            returnValue.SenderClient = new PeraLinkCoreWcf.Client();
            returnValue.SenderClient.FirstName = transaction.Rows[0]["fldSFirstName"].ToString().Trim();
            returnValue.SenderClient.LastName = transaction.Rows[0]["fldSLastName"].ToString().Trim();
            //returnValue.SenderClient.MiddleName = transaction.Rows[0]["fldSMiddleName"].ToString().Trim();  
            //returnValue.SenderClient.BirthDate = DateTime.Parse(transaction.Rows[0]["fldSBirthdate"].ToString());  
            //returnValue.SenderClient.ClientNumber = transaction.Rows[0]["fldSenderID"].ToString();  

            returnValue.SentByAgent = new PeraLinkCoreWcf.Agent();
            returnValue.SentByAgent.Partner = new PeraLinkCoreWcf.Partner();
            returnValue.SentByAgent.Partner.PartnerCode = transaction.Rows[0]["fldSendingBranch"].ToString();

            using (PeraLinkCoreWcfClient peraLinkCoreWcfClient = new PeraLinkCoreWcfClient())
            {
                returnValue.SentByAgent.Partner = peraLinkCoreWcfClient.FindPartnerCode(returnValue.SentByAgent.Partner);
            }
            //returnValue.SentByAgent.AgentCode = transaction.Rows[0]["fldSendingBranch"].ToString();  

            returnValue.SentByUserID = transaction.Rows[0]["fldReceiverID"].ToString();

            return returnValue;  
        }
    }
    #endregion

    void IDisposable.Dispose()
    {
        bool isClosed = false;

        try
        {
            if (this._serviceSoapClient.State != System.ServiceModel.CommunicationState.Faulted)
            {
                this._serviceSoapClient.Close();
                isClosed = true;
            }
            else
            {
                // Proceed with Abort in finally
            }
        }
        finally
        {
            if (!isClosed)
            {
                this._serviceSoapClient.Abort();
            }
            else
            {
                // Do nothing since state is already closed
            }
        }
    }
}