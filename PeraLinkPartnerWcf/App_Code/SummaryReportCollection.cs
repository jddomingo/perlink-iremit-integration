using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class SummaryReportCollection: List<SummaryReport>
{
	#region Constructor
	public SummaryReportCollection()
	{ }
	#endregion

	#region Public Methods
	public void AddList(PeraLinkCoreWcf.SummaryReport[] summaryReportCollection)
	{
		Array.ForEach<PeraLinkCoreWcf.SummaryReport>(summaryReportCollection
			, delegate(PeraLinkCoreWcf.SummaryReport eachSummaryReport)
			{
				SummaryReport mappedSummaryReport = new SummaryReport();
				mappedSummaryReport.Load(eachSummaryReport);
				this.Add(mappedSummaryReport);
			});
	}
	#endregion
}