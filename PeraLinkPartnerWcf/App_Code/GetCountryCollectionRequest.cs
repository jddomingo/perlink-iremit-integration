using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetCountryCollectionRequest: BaseRequest
{
    #region Constructor
    public GetCountryCollectionRequest()
	{
		//

		//
    }
    #endregion

    #region Internal Methods
    internal GetCountryCollectionResult Process()
    {
        GetCountryCollectionResult returnValue = new GetCountryCollectionResult();

        // Important: This function validates the request credential
        this.AuthenticateRequest();

        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
            returnValue.CountryCollection = new CountryCollection();
            returnValue.CountryCollection.AddList(serviceClient.GetCountryCollection());
        }

        returnValue.ResultStatus = ResultStatus.Successful;

        return returnValue;
    }
    #endregion
}