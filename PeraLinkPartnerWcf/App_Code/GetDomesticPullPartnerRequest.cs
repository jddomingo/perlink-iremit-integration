using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetDomesticPullPartnerRequest: BaseRequest
{
    #region Constructor
    public GetDomesticPullPartnerRequest()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private string _partnerCode;
    [DataMember]
    public string PartnerCode
    {
        get { return _partnerCode; }
        set { _partnerCode = value; }
    }

    private string _token;
    [DataMember]
    public string Token
    {
        get { return _token; }
        set { _token = value; }
    }

    private string _agentCode;
    [DataMember]
    public string AgentCode
    {
        get { return _agentCode; }
        set { _agentCode = value; }
    }
    #endregion

    #region Internal Methods
    internal GetDomesticPullPartnerResult Process()
    {
        GetDomesticPullPartnerResult returnValue = new GetDomesticPullPartnerResult();

        // Important: This function validates the request credential
        this.AuthenticateRequest();

        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
            #region Validate Partner Record
            PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
            partner.PartnerCode = this.PartnerCode;
            partner = serviceClient.FindPartnerCode(partner);

            if (partner.PartnerID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else { }

            if (partner.Token == this.Token) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (partner.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            #endregion

            #region Validate Agent Record
            PeraLinkCoreWcf.Agent agent = new PeraLinkCoreWcf.Agent();
            agent.AgentCode = this.AgentCode;
            agent.Partner = partner;
            agent = serviceClient.FindAgentCodeOfPartner(agent);

            if (agent.AgentID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }

            if (agent.Partner.PartnerID == partner.PartnerID)
            { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(43);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (agent.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(46);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            #endregion

            PeraLinkCoreWcf.PartnerServiceMapping[] partnerServiceMappingCollection = serviceClient.GetPartnerServiceMappingCollection(partner);

            PeraLinkCoreWcf.PartnerServiceMapping partnerServiceMapping = Array.Find<PeraLinkCoreWcf.PartnerServiceMapping>(partnerServiceMappingCollection
                , delegate(PeraLinkCoreWcf.PartnerServiceMapping eachPartnerServiceMapping)
                {
                    return eachPartnerServiceMapping.ServiceType.ServiceTypeID == ServiceTypeID.SendRemittance;
                });


            if (partnerServiceMapping == null)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(27);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }

            PeraLinkCoreWcf.PartnerServicePair[] partnerServicePairCollection = serviceClient.GetPartnerServicePairCollection(partnerServiceMapping);

            if (partnerServicePairCollection.Length == 0)
            {
                // No partners available
            }
            else
            {
                returnValue.PartnerCollection = new PartnerCollection();

                PeraLinkCoreWcf.Partner[] partnerCollection = serviceClient.GetDomesticPartnerWithoutPrefix();

                if (partnerCollection.Length == 0)
                {
                    // No available partners without prefix
                }
                else
                {
                    Array.ForEach<PeraLinkCoreWcf.Partner>(partnerCollection
                        , delegate(PeraLinkCoreWcf.Partner eachPartner)
                        {
                            if (IsPartner(partnerServicePairCollection, eachPartner))
                            {
                                Partner mappedPartner = new Partner();
                                mappedPartner.Load(eachPartner);
                                returnValue.PartnerCollection.Add(mappedPartner);
                            }
                            else
                            { 
                                // Do nothing
                            }
                        });
                }
            }
        }

        returnValue.ResultStatus = ResultStatus.Successful;

        return returnValue;
    }

    internal bool IsPartner(PeraLinkCoreWcf.PartnerServicePair[] partnerServicePairCollection, PeraLinkCoreWcf.Partner partner)
    {
        return Array.Exists<PeraLinkCoreWcf.PartnerServicePair>(partnerServicePairCollection
            , delegate(PeraLinkCoreWcf.PartnerServicePair eachPartnerServicePair)
            { 
                return eachPartnerServicePair.PairedPartner.PartnerID == partner.PartnerID;
            });
    }
    #endregion
}