#define PeraLink_Agent_Insurance_Renewal

public enum LookupType
{
     Amendment
    ,Payout
    ,Cancellation
    ,Refund
}

public enum SourceOfFund
{
	Accounting = 1
	,EMoney = 2
}

public enum PartnerType
{
    Push
    , Pull
}


public enum FeeType
{
    Fixed = 1,
    Percentage = 2
}

public enum ServiceType
{
    SendRemittance = 1,
    PayoutRemittance = 2,
    CancelRemittance = 3,
    RefundRemittance = 4,
    AmendRemittance = 5,
    RemitToAccount = 6,
    ELoad = 8,
    BillsPayment = 9,
    Insurance = 12
}
public enum InsuranceProductType
{
    MyCare = 1,
#if PeraLink_Agent_Insurance_Renewal
    Renew = 2
#endif
}
