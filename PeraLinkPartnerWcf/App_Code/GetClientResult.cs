using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetClientResult: BaseResult
{
    #region Constructor
    public GetClientResult()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private Client _client;
    [DataMember]
    public Client Client
    {
        get { return _client; }
        set { _client = value; }
    }
    #endregion
}