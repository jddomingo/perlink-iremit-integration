using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class PartnerCollection: List<Partner>
{
    #region Constructor
    public PartnerCollection()
	{
		//

		//
    }
    #endregion

    #region Public Methods
    public void AddList(PeraLinkCoreWcf.Partner[] partnerCollection)
    {
        Array.ForEach<PeraLinkCoreWcf.Partner>(partnerCollection
            , delegate(PeraLinkCoreWcf.Partner eachPartner)
            {
                Partner mappedPartner = new Partner();
                mappedPartner.Load(eachPartner);
                this.Add(mappedPartner);
            });
    }
    #endregion
}