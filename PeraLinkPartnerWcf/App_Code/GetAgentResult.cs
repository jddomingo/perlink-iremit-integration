using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;


[DataContract]
public class GetAgentResult : BaseResult
{
	#region Constructor
	public GetAgentResult()
	{
		//

		//
	}
	#endregion

	#region Fields/Properties
	private Agent _agent;
	[DataMember]
	public Agent Agent
	{
		get { return _agent; }
		set { _agent = value; }
	}

	private Partner _partner;
	[DataMember]
	public Partner Partner
	{
		get { return _partner; }
		set { _partner = value; }
	}
	#endregion
}