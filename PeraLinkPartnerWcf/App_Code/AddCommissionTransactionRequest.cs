using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for AddCommissionTransactionRequest
/// </summary>
public class AddCommissionTransactionRequest:BaseRequest
{
    #region Constructors
    public AddCommissionTransactionRequest()
	{ }
    #endregion

    #region Fields/Properties

    private Int64 _partnerID;

    [DataMember]
    public Int64 PartnerID
    {
        get { return _partnerID; }
        set { _partnerID = value; }
    }

    private string _iSControlNo;

    [DataMember]
    public string ISControlNo
    {
        get { return _iSControlNo; }
        set { _iSControlNo = value; }
    }

    #endregion

    #region Internal Methods
    internal AddCommissionTransactionResult Process()
    {
        AddCommissionTransactionResult returnValue = new AddCommissionTransactionResult();

        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
            PeraLinkCoreWcf.AddCommissionTransactionRequest addCommissionTransactionRequest = new PeraLinkCoreWcf.AddCommissionTransactionRequest();
            addCommissionTransactionRequest.PartnerID = this.PartnerID;
            addCommissionTransactionRequest.ISControlNo = this.ISControlNo;

            PeraLinkCoreWcf.AddCommissionTransactionResult addCommissionTransactionResult = new PeraLinkCoreWcf.AddCommissionTransactionResult();
            addCommissionTransactionResult = serviceClient.AddCommissionTransaction(addCommissionTransactionRequest);
        }

        returnValue.ResultStatus = ResultStatus.Successful;
        return returnValue;
    }

    #endregion

}