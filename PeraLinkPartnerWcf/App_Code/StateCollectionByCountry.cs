using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for StateCollectionByCountry
/// </summary>
public class StateCollectionByCountry: List<State>
{
    #region Constructor
    public StateCollectionByCountry()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion

    #region Public Methods
    public void AddList(PeraLinkCoreWcf.State[] stateCollectionByCountry)
    {
        Array.ForEach<PeraLinkCoreWcf.State>(stateCollectionByCountry
            , delegate(PeraLinkCoreWcf.State eachClient)
            {
                State mappedState = new State();
                mappedState.Load(eachClient);
                this.Add(mappedState);
            });
    }
    #endregion
}