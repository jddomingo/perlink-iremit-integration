using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PeraLinkCoreWcf;

/// <summary>
/// Summary description for FindPartnerRequestDetailResult
/// </summary>
public class FindPartnerRequestDetailsResult : BaseResult
{
    #region Constructors
    public FindPartnerRequestDetailsResult()
	{
    }
    #endregion

    #region Fields
    private PartnerRequestDetails _partnerRequestDetails;
    #endregion

    #region Properties
    public PartnerRequestDetails PartnerRequestDetails
    {
        get { return _partnerRequestDetails; }
        set { _partnerRequestDetails = value; }
    }
    #endregion

}