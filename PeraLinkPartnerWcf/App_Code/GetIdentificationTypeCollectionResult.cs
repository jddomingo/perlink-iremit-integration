using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetIdentificationTypeCollectionResult: BaseResult
{
    #region Constructor
    public GetIdentificationTypeCollectionResult()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private IdentificationTypeCollection _identificationTypeCollection;

    [DataMember]
    public IdentificationTypeCollection IdentificationTypeCollection
    {
        get { return _identificationTypeCollection; }
        set { _identificationTypeCollection = value; }
    }
    #endregion
}