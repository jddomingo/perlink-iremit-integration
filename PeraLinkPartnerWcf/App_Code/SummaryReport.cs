using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class SummaryReport
{
	#region Constructor
	public SummaryReport()
	{ }
	#endregion

	#region Fields/Properties
	private Int64 _terminalID;

	public Int64 TerminalID
	{
		get { return _terminalID; }
		set { _terminalID = value; }
	}

	private string _terminalCode;

	public string TerminalCode
	{
		get { return _terminalCode; }
		set { _terminalCode = value; }
	}

	private string _terminalName;

	public string TerminalName
	{
		get { return _terminalName; }
		set { _terminalName = value; }
	}

	private int _transactionCount;

	public int TransactionCount
	{
		get { return _transactionCount; }
		set { _transactionCount = value; }
	}

	private decimal _principalAmount;

	public decimal PrincipalAmount
	{
		get { return _principalAmount; }
		set { _principalAmount = value; }
	}

	private decimal _serviceFees;

	public decimal ServiceFees
	{
		get { return _serviceFees; }
		set { _serviceFees = value; }
	}
	#endregion

	#region Public Methods
	public void Load(PeraLinkCoreWcf.SummaryReport summaryReport)
	{
		this.PrincipalAmount = summaryReport.PrincipalAmount;
		this.ServiceFees = summaryReport.ServiceFees;
		this.TerminalCode = summaryReport.Terminal.Name;
		this.TerminalID = summaryReport.Terminal.TerminalID;
		this.TransactionCount = summaryReport.TransactionCount;
	}
	#endregion
}