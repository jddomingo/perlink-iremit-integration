using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EMoney
/// </summary>
public class EMoney
{
	#region Constructor
	public EMoney()
	{
		//

		//
	}
	#endregion

	#region Fields/Properties

	private decimal _cashInAmount;
	public decimal CashInAmount
	{
		get { return _cashInAmount; }
		set { _cashInAmount = value; }
	}

	private decimal _cashOutAmount;
	public decimal CashOutAmount
	{
		get { return _cashOutAmount; }
		set { _cashOutAmount = value; }
	}

	private string _accountNumber;
	public string AccountNumber
	{
		get { return _accountNumber; }
		set { _accountNumber = value; }
	}

	private string _controlNumber;
	public string ControlNumber
	{
		get { return _controlNumber; }
		set { _controlNumber = value; }
	}

	private string _remarks;

	public string Remarks
	{
		get { return _remarks; }
		set { _remarks = value; }
	}

	private string _peraLinkAgentCode;

	public string PeraLinkAgentCode
	{
		get { return _peraLinkAgentCode; }
		set { _peraLinkAgentCode = value; }
	}

    private DateTime _dateCreated;

    public DateTime DateCreated
    {
        get { return _dateCreated; }
        set { _dateCreated = value; }
    }

    private string _action;

    public string Action
    {
        get { return _action; }
        set { _action = value; }
    }

	private string _type;

	public string Type
	{
		get { return _type; }
		set { _type = value; }
	}
	#endregion

	#region Public Methods
	public EMoneyWS.UpdateEMoneyRequest Principal()
	{
		EMoneyWS.UpdateEMoneyRequest request = new EMoneyWS.UpdateEMoneyRequest();

		request.AccountNumber = this.AccountNumber;
		request.Action = "PeraLink Principal";
		request.BranchCode = SystemSetting.EMoneyBranchCode;
		request.CashInAmount = this.CashInAmount;
		request.CashOutAmount = this.CashOutAmount;
		request.ChannelCode = EMoneyWS.EMoneyChannelCode.PERALINK;
		request.CreatedBy = this.AccountNumber;
		request.Details = string.Format("{0}|{1}|{2}|{3}"
													, this.AccountNumber
													, this.CashInAmount.ToString()
													, this.CashOutAmount.ToString()
													, this.ControlNumber
												);
		request.IntegratedSystemControlNo = this.ControlNumber;
		request.IpAddress = SystemUtility.GetClientIPAddress(); 
		request.MacAddress = string.Empty;
		request.ProductCode = EMoneyWS.EMoneyProductCode.PERALINK;
		request.Remarks = string.Format("{0}|{1}|{2}|{3}", this.PeraLinkAgentCode, this.Remarks, "PrincipalAmount", this.ControlNumber);
		request.TotalExpectedCashInAmount = this.CashInAmount;
		request.TotalExpectedCashOutAmount = this.CashOutAmount;

		return request;
	}

	public EMoneyWS.UpdateEMoneyRequest ServiceCharge()
	{
		EMoneyWS.UpdateEMoneyRequest request = new EMoneyWS.UpdateEMoneyRequest();

		request.AccountNumber = this.AccountNumber;
		request.Action = "PeraLink Service Charge";
		request.BranchCode = SystemSetting.EMoneyBranchCode;
		request.CashInAmount = this.CashInAmount;
		request.CashOutAmount = this.CashOutAmount;
		request.ChannelCode = EMoneyWS.EMoneyChannelCode.PERALINK;//FOR THE MEANTIME
		request.CreatedBy = this.AccountNumber;
		request.Details = string.Format("{0}|{1}|{2}|{3}"
													, this.AccountNumber
													, this.CashInAmount.ToString()
													, this.CashOutAmount.ToString()
													, this.ControlNumber
												);
		request.IntegratedSystemControlNo = this.ControlNumber;
		request.IpAddress = SystemUtility.GetClientIPAddress();
		request.MacAddress = string.Empty;
		request.ProductCode = EMoneyWS.EMoneyProductCode.PERALINKSC;
		request.Remarks = string.Format("{0}|{1}|{2}|{3}", this.PeraLinkAgentCode, this.Remarks,  "ServiceCharge", this.ControlNumber);
		request.TotalExpectedCashInAmount = this.CashInAmount;
		request.TotalExpectedCashOutAmount = this.CashOutAmount;

		return request;
	}


	public EMoneyWS.UpdateEMoneyRequest ProcessFee()
	{
		EMoneyWS.UpdateEMoneyRequest request = new EMoneyWS.UpdateEMoneyRequest();

		request.AccountNumber = this.AccountNumber;
		request.Action = "PeraLink Process Fee";
		request.BranchCode = SystemSetting.EMoneyBranchCode;
		request.CashInAmount = this.CashInAmount;
		request.CashOutAmount = this.CashOutAmount;
		request.ChannelCode = EMoneyWS.EMoneyChannelCode.PERALINK;//FOR THE MEANTIME
		request.CreatedBy = this.AccountNumber;
		request.Details = string.Format("{0}|{1}|{2}|{3}"
													, this.AccountNumber
													, this.CashInAmount.ToString()
													, this.CashOutAmount.ToString()
													, this.ControlNumber
												);
		request.IntegratedSystemControlNo = this.ControlNumber;
		request.IpAddress = SystemUtility.GetClientIPAddress();
		request.MacAddress = string.Empty;
		request.ProductCode = EMoneyWS.EMoneyProductCode.PERALINKPF;
		request.Remarks = string.Format("{0}|{1}|{2}|{3}", this.PeraLinkAgentCode, this.Remarks,  "ProcessFee", this.ControlNumber);
		request.TotalExpectedCashInAmount = this.CashInAmount;
		request.TotalExpectedCashOutAmount = this.CashOutAmount;

		return request;
	}

    public EMoneyWS.UpdateEMoneyRequest CommissionFee()
    {
        EMoneyWS.UpdateEMoneyRequest request = new EMoneyWS.UpdateEMoneyRequest();

        request.AccountNumber = this.AccountNumber;
        request.Action = "PeraLink Commission Fee";
        request.BranchCode = SystemSetting.EMoneyBranchCode;
        request.CashInAmount = this.CashInAmount;
        request.CashOutAmount = this.CashOutAmount;
        request.ChannelCode = EMoneyWS.EMoneyChannelCode.PERALINK;
        request.CreatedBy = this.AccountNumber;
        request.Details = string.Format("{0}|{1}|{2}|{3}"
                                                    , this.AccountNumber
                                                    , this.CashInAmount.ToString()
                                                    , this.CashOutAmount.ToString()
                                                    , this.ControlNumber
                                                );
        request.IntegratedSystemControlNo = this.ControlNumber;
        request.IpAddress = SystemUtility.GetClientIPAddress();
        request.MacAddress = string.Empty;
        request.ProductCode = EMoneyWS.EMoneyProductCode.PERALINKCF;
        request.Remarks = string.Format("{0}|{1}|{2}|{3}", this.PeraLinkAgentCode, this.Remarks, "Commission", this.ControlNumber);
        request.TotalExpectedCashInAmount = this.CashInAmount;
        request.TotalExpectedCashOutAmount = this.CashOutAmount;

        return request;
    }
	#endregion

}