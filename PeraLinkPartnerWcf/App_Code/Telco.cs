using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Telco
/// </summary>
public class Telco
{
    #region Constructor
    public Telco()
	{
    }
    #endregion

    #region Fields/Properties
    private bool _isActivated;

    public bool IsActivated
    {
        get { return _isActivated; }
        set { _isActivated = value; }
    }

    private string _telcoCode;

    public string TelcoCode
    {
        get { return _telcoCode; }
        set { _telcoCode = value; }
    }

    private string _telcoName;

    public string TelcoName
    {
        get { return _telcoName; }
        set { _telcoName = value; }
    }
    #endregion

    #region Public Methods
    public void Load(PeraLinkELoadGatewayWcf.Telco telco)
    {
        this.IsActivated = telco.IsActivated;
        this.TelcoCode = telco.TelcoCode;
        this.TelcoName = telco.TelcoName;

    }
    #endregion
}