using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class TerminalTransactionSummaryCollection: List<TerminalTransactionSummary>
{
	#region Constructor
	public TerminalTransactionSummaryCollection()
	{ }
	#endregion

	#region Public Methods
	public void AddList(PeraLinkCoreWcf.TerminalTransactionSummary[] terminalTransactionSummaryCollection)
	{
		Array.ForEach<PeraLinkCoreWcf.TerminalTransactionSummary>(terminalTransactionSummaryCollection
			, delegate(PeraLinkCoreWcf.TerminalTransactionSummary eachTerminalTransactionSummary)
			{
				TerminalTransactionSummary mappedterminalTransactionSummary = new TerminalTransactionSummary();
				mappedterminalTransactionSummary.Load(eachTerminalTransactionSummary);
				this.Add(mappedterminalTransactionSummary);
			});
	}
	#endregion
}