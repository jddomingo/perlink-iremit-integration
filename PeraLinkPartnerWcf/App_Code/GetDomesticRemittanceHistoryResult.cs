using System;
using System.Collections.Generic;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetDomesticRemittanceHistoryResult: BaseResult
{
    #region Constructor
    public GetDomesticRemittanceHistoryResult()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private DomesticRemittanceCollection _domesticRemittanceCollection;
    [DataMember]
    public DomesticRemittanceCollection DomesticRemittanceCollection
    {
        get { return _domesticRemittanceCollection; }
        set { _domesticRemittanceCollection = value; }
    }

    private int _totalPageCount;
    [DataMember]
    public int TotalPageCount
    {
        get { return _totalPageCount; }
        set { _totalPageCount = value; }
    }
    #endregion
}