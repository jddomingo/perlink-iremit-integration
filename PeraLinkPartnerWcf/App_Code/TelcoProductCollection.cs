using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TelcoProductCollection
/// </summary>
public class TelcoProductCollection: List<TelcoProduct>
{
    #region Constructor
    public TelcoProductCollection()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion

    #region Public Methods
    public void AddList(PeraLinkELoadGatewayWcf.TelcoProduct[] telcoProductCollection)
    {
        Array.ForEach<PeraLinkELoadGatewayWcf.TelcoProduct>(telcoProductCollection
            , delegate(PeraLinkELoadGatewayWcf.TelcoProduct eachTelcoProduct)
            {
                TelcoProduct mappedTelcoProduct = new TelcoProduct();
                mappedTelcoProduct.Load(eachTelcoProduct);
                this.Add(mappedTelcoProduct);
            });
    }
    #endregion
}