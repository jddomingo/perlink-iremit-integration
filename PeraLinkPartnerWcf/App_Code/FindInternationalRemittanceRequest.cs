

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using Newtonsoft.Json;


[DataContract]
public class FindInternationalRemittanceRequest : BaseRequest
{
    #region Constructor
    public FindInternationalRemittanceRequest()
    {
        //

        //
    }
    #endregion

    #region Fields/Properties
    private string _controlNumber;
    [DataMember]
    public string ControlNumber
    {
        get { return _controlNumber; }
        set { _controlNumber = value; }
    }
    private Int64 _beneficiaryID;
    [DataMember]
    public Int64 BeneficiaryID
    {
        get { return _beneficiaryID; }
        set { _beneficiaryID = value; }
    }

    private string _partnerCode;
    [DataMember]
    public string PartnerCode
    {
        get { return _partnerCode; }
        set { _partnerCode = value; }
    }

    private string _token;
    [DataMember]
    public string Token
    {
        get { return _token; }
        set { _token = value; }
    }

    private string _agentCode;
    [DataMember]
    public string AgentCode
    {
        get { return _agentCode; }
        set { _agentCode = value; }
    }

    private string _pullTransactionPartnerCode;
    [DataMember]
    public string PullTransactionPartnerCode
    {
        get { return _pullTransactionPartnerCode; }
        set { _pullTransactionPartnerCode = value; }
    }

    private string _userID;
    [DataMember]
    public string UserID
    {
        get { return _userID; }
        set { _userID = value; }
    }

    private string _externalPartnerBeneficiaryNumber;

    internal string ExternalPartnerBeneficiaryNumber
    {
        get { return _externalPartnerBeneficiaryNumber; }
    }

    private string _externalPartnerClientNumber;

    internal string ExternalPartnerClientNumber
    {
        get { return _externalPartnerClientNumber; }
    }

    private Int64 _externalPartnerRemittanceID;

    internal Int64 ExternalPartnerRemittanceID
    {
        get { return _externalPartnerRemittanceID; }
    }

    private string _terminalCode;
    [DataMember]
    public string TerminalCode
    {
        get { return _terminalCode; }
        set { _terminalCode = value; }
    }
    #endregion

    #region Internal Methods
    internal FindInternationalRemittanceResult Process()
    {
        FindInternationalRemittanceResult returnValue = new FindInternationalRemittanceResult();
        PeraLinkCoreWcf.InternationalRemittance internationalRemittance = new PeraLinkCoreWcf.InternationalRemittance();
        string originatingCountry = string.Empty;
        PartnerType partnerType = PartnerType.Pull;

        // Important: This function validates the request credential
        this.AuthenticateRequest();

        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
            #region Validate requesting partner information
            PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
            partner.PartnerCode = this.PartnerCode;
            partner = serviceClient.FindPartnerCode(partner);

            if (partner.PartnerID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else { }

            if (partner.Token == this.Token) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (partner.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            #endregion

            #region Validate requesting agent information
            PeraLinkCoreWcf.Agent agent = new PeraLinkCoreWcf.Agent();
            agent.AgentCode = this.AgentCode;
            agent.Partner = partner;
            agent = serviceClient.FindAgentCodeOfPartner(agent);

            if (agent.AgentID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }

            if (agent.Partner.PartnerID == partner.PartnerID)
            { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(43);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (agent.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(46);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            #endregion

            #region Validate Terminal Record
            PeraLinkCoreWcf.Terminal terminal = new PeraLinkCoreWcf.Terminal();

            if (serviceClient.GetAgentTerminalCollection(agent).Length > 0)
            {
                terminal.TerminalCode = this.TerminalCode;
                terminal.Agent = agent;
                terminal.Agent.Partner = partner;
                terminal = serviceClient.FindTerminal(terminal);

                if (terminal.TerminalID == 0)
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(104);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
                else
                {
                    // Proceed
                }

                if (terminal.Agent.AgentID == agent.AgentID)
                { }
                else
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(105);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }

                if (terminal.Activated) { }
                else
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(106);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
            }
            else
            {
                //do nothing
            }
            #endregion

            #region Validate requesting partner's access to method (FindInternationalRemittance)
            PeraLinkCoreWcf.PartnerServiceMapping[] partnerServiceMappingCollection = serviceClient.GetPartnerServiceMappingCollection(partner);

            PeraLinkCoreWcf.PartnerServiceMapping partnerServiceMapping = Array.Find<PeraLinkCoreWcf.PartnerServiceMapping>(partnerServiceMappingCollection
                , delegate(PeraLinkCoreWcf.PartnerServiceMapping eachPartnerServiceMapping)
                {
                    return eachPartnerServiceMapping.ServiceType.ServiceTypeID == ServiceTypeID.PayoutRemittance
                        || eachPartnerServiceMapping.ServiceType.ServiceTypeID == ServiceTypeID.AmendRemittance
                        || eachPartnerServiceMapping.ServiceType.ServiceTypeID == ServiceTypeID.RefundRemittance
                        || eachPartnerServiceMapping.ServiceType.ServiceTypeID == ServiceTypeID.CancelRemittance;
                });

            if (partnerServiceMapping == null)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(48);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }
            #endregion

            #region Log Audit Trail
            PeraLinkCoreWcf.AuditTrail auditTrail = new PeraLinkCoreWcf.AuditTrail();
            auditTrail.CreatedByAgent = agent;
            auditTrail.CreatedByUserID = this.UserID;
            auditTrail.DateTimeCreated = DateTime.Now;
            auditTrail.IPAddress = SystemUtility.GetClientIPAddress();
            auditTrail.AuditAction = new PeraLinkCoreWcf.AuditAction();
            auditTrail.AuditAction.AuditActionID = AuditActionID.FindInternationalRemittanceTransaction;
            auditTrail.AuditDetails = string.Format("Control Number: {0}", this.ControlNumber);
            serviceClient.AddAuditTrail(auditTrail);
            #endregion

            #region Partner Type
            switch (this.PullTransactionPartnerCode)
            {
                case "ABS2":
                case "IRT":
                case "FRX-PS":
                case "WRT-PS":
                case "ACE-PS":
                case "OWC-PS":
                case "EGS-PS":
                case "LMI-PS":
                case "TMS-PS":
                case "LNR-PS":
                case "SWF-PS":
                case "BKK-PS":
                case "KEB-PS":
                case "AFX-PS":
                case "GSC-PS":
                case "BMX-PS":
                case "GMT-PS":
                case "PMT-PS":
                case "TMF-PS":
                case "IDX-PS":
                case "MFS-PS":
                case "EMQ-PS":
                case "CMT-PS":
                case "MPL-PS":
                case "LLU-PS":
                    partnerType = PartnerType.Push;
                    break;
                default:
                    break;
            }
            #endregion

            #region For non-pull partner
            if (string.IsNullOrWhiteSpace(this.PullTransactionPartnerCode))
            {
                internationalRemittance = new PeraLinkCoreWcf.InternationalRemittance();
                internationalRemittance.ControlNumber = this.ControlNumber;

                #region Find sending partner based on control number format
                Partner matchedPartner = SystemUtility.FindPartnerWithControlNumberFormat(internationalRemittance.ControlNumber);
                #endregion

                #region Find transaction from PeraLink database
                if (matchedPartner == null)
                {
                    internationalRemittance = serviceClient.FindInternationalRemittance(internationalRemittance);
                }
                #endregion

                #region Find transaction using partner API
                else
                {
                    // No partner API to search yet
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(20);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);

                    #region Get sending partner information

                    #endregion
                }
                #endregion
            }
            else if (partnerType == PartnerType.Push)
            {

                #region Cebuana Lhuillier Tie-up Transaction
                using (CebuanaLhuillierWSClient cebuanaLhuillierWSClient = new CebuanaLhuillierWSClient())
                {
                    internationalRemittance = cebuanaLhuillierWSClient.FindInternationalRemittance(this.ControlNumber);
                }

                if (internationalRemittance == null)
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(14);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }

                PeraLinkCoreWcf.Partner transactionPartner = new PeraLinkCoreWcf.Partner();
                transactionPartner.PartnerCode = internationalRemittance.SentByAgent.Partner.PartnerCode;
                transactionPartner = serviceClient.FindPartnerCode(transactionPartner);

                PeraLinkCoreWcf.PartnerServicePair[] partnerServicePairCollection = serviceClient.GetPartnerServicePairCollection(partnerServiceMapping);

                PeraLinkCoreWcf.PartnerServicePair pairing = Array.Find<PeraLinkCoreWcf.PartnerServicePair>(partnerServicePairCollection
                    , delegate(PeraLinkCoreWcf.PartnerServicePair eachPartnerServicePair)
                    {
                        if (eachPartnerServicePair.PairedPartner == null)
                        {
                            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(2);
                            throw new RDFramework.ClientException(message.Content, message.MessageID);
                        }
                        else
                        {
                            return eachPartnerServicePair.PairedPartner.PartnerID == transactionPartner.PartnerID;
                        }
                    });

                if (pairing == null)
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(14);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
                else
                {
                    // Proceed
                }

                if (pairing.PairedPartner.Activated) { }
                else
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }


                // Important: Keep the original sender and beneficiary number from the external partner database
                //if (internationalRemittance.Beneficiary.Client.ClientNumber != null)
                //{
                //    _externalPartnerBeneficiaryNumber = internationalRemittance.Beneficiary.Client.ClientNumber;
                //}
                //if (internationalRemittance.SenderClient.ClientNumber != null)
                //{
                //    _externalPartnerClientNumber = internationalRemittance.SenderClient.ClientNumber;
                //}
                _externalPartnerRemittanceID = internationalRemittance.InternationalRemittanceID;

                internationalRemittance.SendCurrency = serviceClient.FindCurrency(internationalRemittance.SendCurrency);

                PeraLinkCoreWcf.Agent[] agentCollection = serviceClient.GetPartnerAgentCollection(transactionPartner);
                if (agentCollection == null)
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);

                }
                else if (agentCollection.Length < 1)
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);

                }
                else
                {
                    internationalRemittance.SentByAgent.AgentCode = agentCollection[0].AgentCode;
                    internationalRemittance.SentByAgent.Partner.PartnerID = internationalRemittance.SentByAgent.Partner.PartnerID;
                    internationalRemittance.SentByAgent = serviceClient.FindAgentCode(internationalRemittance.SentByAgent);
                    internationalRemittance.SentByUserID = internationalRemittance.SentByAgent.AgentCode;
                    internationalRemittance.SentByAgent.Partner = new PeraLinkCoreWcf.Partner();
                    internationalRemittance.SentByAgent.Partner = transactionPartner;
                }
                #endregion
            }
            #endregion

            #region For pull partner
            else
            {
                #region Get sending partner information
                PeraLinkCoreWcf.Partner sendingPartner = new PeraLinkCoreWcf.Partner();
                sendingPartner.PartnerCode = this.PullTransactionPartnerCode;
                sendingPartner = serviceClient.FindPartnerCode(sendingPartner);

                if (sendingPartner.PartnerID == 0)
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
                else { }

                if (sendingPartner.Activated)
                { }
                else
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
                #endregion

                #region Find transaction

                
                internationalRemittance = new PeraLinkCoreWcf.InternationalRemittance();

                #region NYBP
                if (sendingPartner.PartnerCode == SystemSetting.NYBPPartnerCode)
                {
                    using (NybpWcfClient nybpWcfClient = new NybpWcfClient())
                    {
                        NybpWcf.LookupTransactionRequest lookupTransactionRequest = new NybpWcf.LookupTransactionRequest();
                        lookupTransactionRequest.BranchCode = this.AgentCode;
                        lookupTransactionRequest.TransactionNumber = this.ControlNumber;
                        NybpWcf.LookupTransactionResult lookupTransactionResult = nybpWcfClient.LookupTransaction(lookupTransactionRequest);

                        internationalRemittance = new PeraLinkCoreWcf.InternationalRemittance();
                        internationalRemittance.InternationalRemittanceID = 1;
                        internationalRemittance.SenderClient = new PeraLinkCoreWcf.Client();
                        internationalRemittance.SenderClient.FirstName = lookupTransactionResult.SenderFirstName;
                        internationalRemittance.SenderClient.LastName = lookupTransactionResult.SenderLastName;
                        internationalRemittance.SenderClient.BirthDate = RDFramework.Utility.Validation.MinimumSqlDateTime;
                        internationalRemittance.Beneficiary = new PeraLinkCoreWcf.Beneficiary();
                        internationalRemittance.Beneficiary.FirstName = lookupTransactionResult.BeneficiaryFirstName;
                        internationalRemittance.Beneficiary.LastName = lookupTransactionResult.BeneficiaryLastName;
                        internationalRemittance.Beneficiary.BirthDate = RDFramework.Utility.Validation.MinimumSqlDateTime;
                        internationalRemittance.ControlNumber = lookupTransactionResult.TransactionNumber;
                        internationalRemittance.DateTimeSent = lookupTransactionResult.TransactionDate;
                        internationalRemittance.PrincipalAmount = lookupTransactionResult.PayoutAmount;
                        internationalRemittance.RemittanceStatus = NybpWcfClient.MapStatus(lookupTransactionResult.TransactionStatus);
                        internationalRemittance.SendCurrency = new PeraLinkCoreWcf.Currency();
                        internationalRemittance.SendCurrency.Code = lookupTransactionResult.PayoutCurrency;
                        internationalRemittance.SendCurrency.CurrencyID = 6;

                        internationalRemittance.SentByAgent = new PeraLinkCoreWcf.Agent();
                        internationalRemittance.SentByAgent.Partner = sendingPartner;
                        //internationalRemittance.SentByAgent.AgentCode = ;
                        PeraLinkCoreWcf.Agent[] agentCollection = serviceClient.GetPartnerAgentCollection(sendingPartner);
                        if (agentCollection == null)
                        {
                            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                            throw new RDFramework.ClientException(message.Content, message.MessageID);

                        }
                        else if (agentCollection.Length < 1)
                        {
                            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                            throw new RDFramework.ClientException(message.Content, message.MessageID);

                        }
                        else
                        {
                            internationalRemittance.SentByAgent.AgentCode = agentCollection[0].AgentCode;
                            internationalRemittance.SentByAgent.Partner.PartnerID = sendingPartner.PartnerID;
                            internationalRemittance.SentByAgent = serviceClient.FindAgentCode(internationalRemittance.SentByAgent);
                            internationalRemittance.SentByUserID = internationalRemittance.SentByAgent.AgentCode;
                        }

                        decimal minSendAmount = SystemSetting.GetMinSendAmount(internationalRemittance.SendCurrency.Code);
                        decimal maxSendAmountNYBPRetailAgents = SystemSetting.MaxSendAmountNYBPRetailAgents(internationalRemittance.SendCurrency.Code);

                        Partner matchedPartnerCode = SystemUtility.FindPartnerWithPartnerCodeFormat(this.PartnerCode);

                        if (matchedPartnerCode == null)
                        {
                            // proceed
                        }
                        else
                        {
                            if (minSendAmount <= internationalRemittance.PrincipalAmount && maxSendAmountNYBPRetailAgents >= internationalRemittance.PrincipalAmount)
                            {
                                // proceed
                            }
                            else
                            {
                                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(126);
                                throw new RDFramework.ClientException(string.Format(message.Description, maxSendAmountNYBPRetailAgents), message.SystemMessageID);
                            }
                        }
                        returnValue.OtherDetails = lookupTransactionResult.SessionID;

                        PeraLinkCoreWcf.Country payoutCountry = new PeraLinkCoreWcf.Country();
                        payoutCountry.CountryID = partner.Country.CountryID;
                        payoutCountry = serviceClient.FindCountry(payoutCountry);
                        returnValue.PayoutCountry = payoutCountry.CountryCodeAlpha3;
                    }
                }
                #endregion

                #region Placid SpotCash
                else if (sendingPartner.PartnerCode == SystemSetting.PlacidSpotCashPartnerCode)
                {
                    using (PlacidWcfClient placidWcfClient = new PlacidWcfClient())
                    {
                        PeraLinkPlacidSpotCashWcf.LookupTransactionRequest lookupTransactionRequest = new PeraLinkPlacidSpotCashWcf.LookupTransactionRequest();
                        lookupTransactionRequest.TransactionNumber = this.ControlNumber;
                        PeraLinkPlacidSpotCashWcf.LookupTransactionResult lookupTransactionResult = placidWcfClient.LookupTransaction(lookupTransactionRequest);

                        internationalRemittance = new PeraLinkCoreWcf.InternationalRemittance();
                        internationalRemittance.InternationalRemittanceID = 1;
                        internationalRemittance.SenderClient = new PeraLinkCoreWcf.Client();
                        internationalRemittance.SenderClient.FirstName = lookupTransactionResult.SenderFirstName;
                        internationalRemittance.SenderClient.LastName = lookupTransactionResult.SenderLastName;
                        internationalRemittance.SenderClient.BirthDate = RDFramework.Utility.Validation.MinimumSqlDateTime;
                        internationalRemittance.Beneficiary = new PeraLinkCoreWcf.Beneficiary();
                        internationalRemittance.Beneficiary.FirstName = lookupTransactionResult.BeneficiaryFirstName;
                        internationalRemittance.Beneficiary.LastName = lookupTransactionResult.BeneficiaryLastName;
                        internationalRemittance.Beneficiary.BirthDate = RDFramework.Utility.Validation.MinimumSqlDateTime;
                        internationalRemittance.ControlNumber = lookupTransactionResult.TransactionNumber;
                        internationalRemittance.DateTimeSent = lookupTransactionResult.TransactionDate;
                        internationalRemittance.PrincipalAmount = lookupTransactionResult.PayoutAmount;
                        internationalRemittance.RemittanceStatus = PlacidWcfClient.MapStatus(lookupTransactionResult.TransactionStatus);
                        internationalRemittance.SendCurrency = new PeraLinkCoreWcf.Currency();
                        internationalRemittance.SendCurrency.Code = lookupTransactionResult.PayoutCurrency;
                        internationalRemittance.SendCurrency.CurrencyID = 6;

                        internationalRemittance.SentByAgent = new PeraLinkCoreWcf.Agent();
                        internationalRemittance.SentByAgent.Partner = sendingPartner;
                        //internationalRemittance.SentByAgent.AgentCode = ;
                        PeraLinkCoreWcf.Agent[] agentCollection = serviceClient.GetPartnerAgentCollection(sendingPartner);
                        if (agentCollection == null)
                        {
                            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                            throw new RDFramework.ClientException(message.Content, message.MessageID);

                        }
                        else if (agentCollection.Length < 1)
                        {
                            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                            throw new RDFramework.ClientException(message.Content, message.MessageID);

                        }
                        else
                        {
                            internationalRemittance.SentByAgent.AgentCode = agentCollection[0].AgentCode;
                            internationalRemittance.SentByAgent.Partner.PartnerID = sendingPartner.PartnerID;
                            internationalRemittance.SentByAgent = serviceClient.FindAgentCode(internationalRemittance.SentByAgent);
                            internationalRemittance.SentByUserID = internationalRemittance.SentByAgent.AgentCode;
                        }
                        returnValue.OtherDetails = string.Format("{0}|{1}|{2}", lookupTransactionResult.AccessCode, lookupTransactionResult.ExtraValue, lookupTransactionResult.Token);
                    }
                }
                #endregion

                #region Transfer Money Link
                else if (sendingPartner.PartnerCode == SystemSetting.TransferMoneyLinkPartnerCode)
                {
                    // Check if Request Detail is existing
                    using (PeraLinkCoreWcfClient coreWcfClient = new PeraLinkCoreWcfClient())
                    {
                        using (TmlWcfClient tmlWcfClient = new TmlWcfClient())
                        {
                            // Transaction Lookup
                            #region Transaction Look Up
                            PeraLinkTmlWcf.LookupTransactionRequest lookupTransactionRequest = new PeraLinkTmlWcf.LookupTransactionRequest();
                            lookupTransactionRequest.AgentSessionID = this._agentCode;
                            lookupTransactionRequest.ReferenceNumber = this._controlNumber;
                            PeraLinkTmlWcf.LookupTransactionResult lookupTransactionResult = tmlWcfClient.LookupTransaction(lookupTransactionRequest);

                            internationalRemittance = new PeraLinkCoreWcf.InternationalRemittance();
                            internationalRemittance.InternationalRemittanceID = 1;
                            internationalRemittance.SenderClient = new PeraLinkCoreWcf.Client();
                            internationalRemittance.SenderClient.FirstName = lookupTransactionResult.SenderFirstName;
                            internationalRemittance.SenderClient.LastName = lookupTransactionResult.SenderLastName;
                            internationalRemittance.SenderClient.BirthDate = RDFramework.Utility.Validation.MinimumSqlDateTime;
                            internationalRemittance.Beneficiary = new PeraLinkCoreWcf.Beneficiary();
                            internationalRemittance.Beneficiary.FirstName = lookupTransactionResult.BeneficiaryFirstName;
                            internationalRemittance.Beneficiary.LastName = lookupTransactionResult.BeneficiaryLastName;
                            internationalRemittance.Beneficiary.BirthDate = RDFramework.Utility.Validation.MinimumSqlDateTime;
                            internationalRemittance.ControlNumber = lookupTransactionResult.ReferenceNumber;
                            internationalRemittance.DateTimeSent = lookupTransactionResult.TransactionDate;
                            internationalRemittance.PrincipalAmount = lookupTransactionResult.PayoutAmount;
                            internationalRemittance.RemittanceStatus = TmlWcfClient.MapStatus(lookupTransactionResult.TransactionStatus);
                            internationalRemittance.SendCurrency = new PeraLinkCoreWcf.Currency();
                            internationalRemittance.SendCurrency.Code = lookupTransactionResult.PayoutCurrency;
                            internationalRemittance.SendCurrency.CurrencyID = 6;

                            internationalRemittance.SentByAgent = new PeraLinkCoreWcf.Agent();
                            internationalRemittance.SentByAgent.Partner = sendingPartner;
                            //internationalRemittance.SentByAgent.AgentCode = ;
                            PeraLinkCoreWcf.Agent[] agentCollection = serviceClient.GetPartnerAgentCollection(sendingPartner);
                            if (agentCollection == null)
                            {
                                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                                throw new RDFramework.ClientException(message.Content, message.MessageID);

                            }
                            else if (agentCollection.Length < 1)
                            {
                                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                                throw new RDFramework.ClientException(message.Content, message.MessageID);

                            }
                            else
                            {
                                internationalRemittance.SentByAgent.AgentCode = agentCollection[0].AgentCode;
                                internationalRemittance.SentByAgent.Partner.PartnerID = sendingPartner.PartnerID;
                                internationalRemittance.SentByAgent = serviceClient.FindAgentCode(internationalRemittance.SentByAgent);
                                internationalRemittance.SentByUserID = internationalRemittance.SentByAgent.AgentCode;
                            }
                            #endregion

                            PeraLinkCoreWcf.PartnerRequestDetails partnerRequestDetails = new PeraLinkCoreWcf.PartnerRequestDetails();
                            partnerRequestDetails.AgentID = agent.AgentID;
                            partnerRequestDetails.PartnerCode = this._partnerCode;
                            partnerRequestDetails.ControlNumber = this._controlNumber;
                            partnerRequestDetails = coreWcfClient.FindPartnerRequestDetails(partnerRequestDetails);
                            // Save the new Request Details for the transaction
                            // Exists? Yes - Update to tbl_PartnerRequestDetails
                            if (partnerRequestDetails.RequestDetail != null)
                            {
                                PeraLinkCoreWcf.UpdatePartnerRequestDetailsRequest updatePartnerRequestDetailsRequest = new PeraLinkCoreWcf.UpdatePartnerRequestDetailsRequest();
                                updatePartnerRequestDetailsRequest.AgentID = agent.AgentID;
                                updatePartnerRequestDetailsRequest.PartnerCode = this._partnerCode;
                                updatePartnerRequestDetailsRequest.ControlNumber = this._controlNumber;
                                updatePartnerRequestDetailsRequest.RequestDetails = lookupTransactionResult.TokenID;
                                PeraLinkCoreWcf.UpdatePartnerRequestDetailsResult updatePartnerRequestDetailsResult = coreWcfClient.UpdatePartnerRequestDetails(updatePartnerRequestDetailsRequest);
                            }
                            // Exists? No - Insert to tbl_PartnerRequestDetails
                            else
                            {
                                PeraLinkCoreWcf.InsertPartnerRequestDetailsRequest insertPartnerRequestDetailsRequest = new PeraLinkCoreWcf.InsertPartnerRequestDetailsRequest();
                                insertPartnerRequestDetailsRequest.AgentID = agent.AgentID;
                                insertPartnerRequestDetailsRequest.PartnerCode = this._partnerCode;
                                insertPartnerRequestDetailsRequest.ControlNumber = this._controlNumber;
                                insertPartnerRequestDetailsRequest.RequestDetails = lookupTransactionResult.TokenID;
                                PeraLinkCoreWcf.InsertPartnerRequestDetailsResult insertPartnerRequestDetailsResult = coreWcfClient.InsertPartnerRequestDetails(insertPartnerRequestDetailsRequest);
                            }

                            string tokenID = partnerRequestDetails.RequestDetail;

                            PeraLinkTmlWcf.UnlockTransactionRequest unlockTransactionRequest = new PeraLinkTmlWcf.UnlockTransactionRequest();
                            unlockTransactionRequest.AgentSessionID = agent.AgentCode;
                            unlockTransactionRequest.ReferenceNumber = this._controlNumber;
                            unlockTransactionRequest.PayTokenID = lookupTransactionResult.TokenID;//tokenID;
                            try
                            {
                                tmlWcfClient.UnlockTransaction(unlockTransactionRequest);
                            }
                            catch (Exception ex)
                            {
                                if (ex.Message.Contains("1007"))
                                {
                                    // Do nothing
                                }
                                else
                                {
                                    throw;
                                }
                            }

                        }
                    }
                }
                #endregion

                #region CIMB
                else if (sendingPartner.PartnerCode == SystemSetting.CimbPartnerCode)
                {
                    using (CimbWcfClient cimbWcfClient = new CimbWcfClient())
                    {
                        PeraLinkCimbWcf.LookupTransactionRequest lookupTransactionRequest = new PeraLinkCimbWcf.LookupTransactionRequest();
                        lookupTransactionRequest.Transaction = new PeraLinkCimbWcf.Transaction();
                        lookupTransactionRequest.Transaction.TransactionNumber = this.ControlNumber;
                        PeraLinkCimbWcf.LookupTransactionResult lookupTransactionResult = cimbWcfClient.LookupTransaction(lookupTransactionRequest);

                        internationalRemittance = new PeraLinkCoreWcf.InternationalRemittance();
                        internationalRemittance.InternationalRemittanceID = 1;
                        internationalRemittance.SenderClient = new PeraLinkCoreWcf.Client();
                        internationalRemittance.SenderClient.FirstName = lookupTransactionResult.Transaction.SenderFirstName;
                        internationalRemittance.SenderClient.LastName = lookupTransactionResult.Transaction.SenderLastName;
                        internationalRemittance.SenderClient.BirthDate = RDFramework.Utility.Validation.MinimumSqlDateTime;
                        internationalRemittance.Beneficiary = new PeraLinkCoreWcf.Beneficiary();
                        internationalRemittance.Beneficiary.FirstName = lookupTransactionResult.Transaction.ReceiverFirstName;
                        internationalRemittance.Beneficiary.LastName = lookupTransactionResult.Transaction.ReceiverLastName;
                        internationalRemittance.Beneficiary.BirthDate = RDFramework.Utility.Validation.MinimumSqlDateTime;
                        internationalRemittance.ControlNumber = lookupTransactionResult.Transaction.TransactionNumber;
                        internationalRemittance.DateTimeSent = lookupTransactionResult.Transaction.TransactionDate;
                        internationalRemittance.PrincipalAmount = lookupTransactionResult.Transaction.PayoutAmount;
                        internationalRemittance.RemittanceStatus = CimbWcfClient.MapStatus(lookupTransactionResult.TransactionStatus);
                        internationalRemittance.SendCurrency = new PeraLinkCoreWcf.Currency();
                        internationalRemittance.SendCurrency.Code = lookupTransactionResult.Transaction.PayoutCurrency;
                        internationalRemittance.SendCurrency.CurrencyID = CurrencyID.PHP;

                        internationalRemittance.SentByAgent = new PeraLinkCoreWcf.Agent();
                        internationalRemittance.SentByAgent.Partner = sendingPartner;

                        PeraLinkCoreWcf.Agent[] agentCollection = serviceClient.GetPartnerAgentCollection(sendingPartner);
                        if (agentCollection == null)
                        {
                            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                            throw new RDFramework.ClientException(message.Content, message.MessageID);

                        }
                        else if (agentCollection.Length < 1)
                        {
                            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                            throw new RDFramework.ClientException(message.Content, message.MessageID);

                        }
                        else
                        {
                            internationalRemittance.SentByAgent.AgentCode = agentCollection[0].AgentCode;
                            internationalRemittance.SentByAgent.Partner.PartnerID = sendingPartner.PartnerID;
                            internationalRemittance.SentByAgent = serviceClient.FindAgentCode(internationalRemittance.SentByAgent);
                            internationalRemittance.SentByUserID = internationalRemittance.SentByAgent.AgentCode;
                        }
                        returnValue.OtherDetails = String.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}"
                                                                , lookupTransactionResult.Transaction.SessionID
                                                                , lookupTransactionResult.Transaction.PayoutCurrency
                                                                , lookupTransactionResult.Transaction.ReceiverCountry
                                                                , lookupTransactionResult.Transaction.SenderIDType
                                                                , lookupTransactionResult.Transaction.SenderIDNumber
                                                                , lookupTransactionResult.Transaction.SenderCountry
                                                                , lookupTransactionResult.Transaction.ReceiverAddress
                                                                , lookupTransactionResult.Transaction.ReceiverMobileNumber
                                                                , lookupTransactionResult.Transaction.ReceiverPhoneNumber
                                                                , lookupTransactionResult.Transaction.ReceiverOccupation
                                                                , lookupTransactionResult.Transaction.ReceiverIDDetails
                                                                , lookupTransactionResult.Transaction.ReceiverIDExpiryDate.HasValue ? lookupTransactionResult.Transaction.ReceiverIDExpiryDate.Value.ToString("mm/dd/yyyy") : string.Empty);
                    }

                }
                #endregion

                #region MoneyGram
                else if (sendingPartner.PartnerCode == SystemSetting.MoneyGramPartnerCode)
                {
                    #region Pull MG Transaction
                    using (MoneyGramWcfClient moneyGramWcfClient = new MoneyGramWcfClient())
                    {
                        DateTime timeStamp = DateTime.Now;

                        PeraLinkCoreWcf.AgentAccessDetail agentAccessDetail = new PeraLinkCoreWcf.AgentAccessDetail();
                        agentAccessDetail.AgentID = agent.AgentID;
                        agentAccessDetail.PartnerID = sendingPartner.PartnerID;
                        PeraLinkCoreWcf.GetAgentAccessDetailResult getAgentAccessDetailResult = serviceClient.GetAgentAccessDetail(agentAccessDetail);
                        if (string.IsNullOrEmpty(getAgentAccessDetailResult.AgentAccessDetails.AccessDetail))
                        {
                            PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(96);
                            throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                        }
                        else
                        {
                            MoneyGramAgentDetail moneyGramAgentDetail = JsonConvert.DeserializeObject<MoneyGramAgentDetail>(getAgentAccessDetailResult.AgentAccessDetails.AccessDetail);

                            MoneyGramWcf.SearchTransactionRequest searchTransactionRequest = new MoneyGramWcf.SearchTransactionRequest();

                            searchTransactionRequest.AgentID = moneyGramAgentDetail.AgentID;
                            searchTransactionRequest.AgentSequence = moneyGramAgentDetail.AgentSequence;
                            searchTransactionRequest.ApiVersion = moneyGramAgentDetail.ApiVersion;
                            searchTransactionRequest.ClientSoftwareVersion = moneyGramAgentDetail.ClientSoftwareVersion;
                            searchTransactionRequest.ReferenceNumber = this.ControlNumber;
                            searchTransactionRequest.TimeStamp = timeStamp;
                            searchTransactionRequest.Token = moneyGramAgentDetail.Token;
                            searchTransactionRequest.PeraLinkAgentCode = agent.AgentCode;
                            searchTransactionRequest.PeraLinkUserID = this.UserID;

                            MoneyGramWcf.SearchTransactionResult searchTransactionResult = moneyGramWcfClient.SearchTransaction(searchTransactionRequest);

                            returnValue.AuthorizationCode = searchTransactionResult.AuthorizationCode;
                            returnValue.DeliveryOption = searchTransactionResult.DeliveryOption;
                            internationalRemittance = new PeraLinkCoreWcf.InternationalRemittance();
                            internationalRemittance.InternationalRemittanceID = 1;

                            internationalRemittance.SenderClient = new PeraLinkCoreWcf.Client();
                            internationalRemittance.SenderClient.FirstName = searchTransactionResult.SenderFirstName;
                            internationalRemittance.SenderClient.MiddleName = searchTransactionResult.SenderMiddleName;
                            internationalRemittance.SenderClient.LastName = searchTransactionResult.SenderLastName.Trim();
                            internationalRemittance.SenderClient.BirthDate = RDFramework.Utility.Validation.MinimumSqlDateTime;
                            internationalRemittance.SenderClient.Address = searchTransactionResult.SenderAddress;
                            internationalRemittance.SenderClient.CountryAddress = new PeraLinkCoreWcf.Country();
                            internationalRemittance.SenderClient.CountryAddress.CountryCodeAlpha3 = searchTransactionResult.SendingCountry;
                            internationalRemittance.SenderClient.CountryAddress = serviceClient.FindCountry(internationalRemittance.SenderClient.CountryAddress);
                            originatingCountry = internationalRemittance.SenderClient.CountryAddress.CountryName;

                            internationalRemittance.Beneficiary = new PeraLinkCoreWcf.Beneficiary();
                            internationalRemittance.Beneficiary.FirstName = searchTransactionResult.BeneficiaryFirstName;
                            internationalRemittance.Beneficiary.MiddleName = searchTransactionResult.BeneficiaryMiddleName;
                            internationalRemittance.Beneficiary.LastName = searchTransactionResult.BeneficiaryLastName;
                            internationalRemittance.Beneficiary.BirthDate = RDFramework.Utility.Validation.MinimumSqlDateTime;

                            internationalRemittance.ControlNumber = searchTransactionResult.TransactionNumber;
                            internationalRemittance.DateTimeSent = searchTransactionResult.TransactionDate;
                            internationalRemittance.PrincipalAmount = searchTransactionResult.PayoutAmount;
                            internationalRemittance.RemittanceStatus = MoneyGramWcfClient.MapStatus(searchTransactionResult.TransactionStatus);
                            internationalRemittance.SendCurrency = new PeraLinkCoreWcf.Currency();
                            internationalRemittance.SendCurrency.Code = searchTransactionResult.PayoutCurrency;
                            internationalRemittance.SendCurrency.CurrencyID = 6;



                            internationalRemittance.SentByAgent = new PeraLinkCoreWcf.Agent();
                            internationalRemittance.SentByAgent.Partner = sendingPartner;

                            PeraLinkCoreWcf.Agent[] agentCollection = serviceClient.GetPartnerAgentCollection(sendingPartner);
                            if (agentCollection.Length == null)
                            {
                                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                                throw new RDFramework.ClientException(message.Content, message.MessageID);

                            }
                            else if (agentCollection.Length < 1)
                            {
                                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                                throw new RDFramework.ClientException(message.Content, message.MessageID);
                            }
                            else
                            {
                                internationalRemittance.SentByAgent.AgentCode = agentCollection[0].AgentCode;
                                internationalRemittance.SentByAgent = serviceClient.FindAgentCode(internationalRemittance.SentByAgent);
                                internationalRemittance.SentByUserID = internationalRemittance.SentByAgent.AgentCode;
                            }
                            //PeraLinkCoreWcf.Country payoutCountry = new PeraLinkCoreWcf.Country();
                            //payoutCountry.CountryID = partner.Country.CountryID;
                            //payoutCountry = serviceClient.FindCountry(payoutCountry);
                            //returnValue.PayoutCountry = payoutCountry.CountryCodeAlpha3;
                            returnValue.OkForAgent = searchTransactionResult.OkForAgent;
                            returnValue.OriginalSendAmount = searchTransactionResult.OriginalSendAmount;
                            returnValue.OriginalSendCurrency = searchTransactionResult.OriginalSendCurrency;
                            returnValue.ExchangeRate = searchTransactionResult.ExchangeRate;
                        }
                    }
                    #endregion
                }
                #endregion

                #region Xoom
                else if (sendingPartner.PartnerCode == SystemSetting.XoomPartnerCode)
                {
                    using (XoomWSClient xoomWSClient = new XoomWSClient())
                    {
                        PeraLinkXoomWS.LookupTransactionRequest lookupTransactionRequest = new PeraLinkXoomWS.LookupTransactionRequest();
                        lookupTransactionRequest.TransactionNumber = this.ControlNumber;
                        PeraLinkXoomWS.CebuanaBranchInformation cebuanaBranchInformation = new PeraLinkXoomWS.CebuanaBranchInformation();
                        cebuanaBranchInformation.BranchCode = this.AgentCode;
                        cebuanaBranchInformation.BranchUserID = this.UserID;
                        cebuanaBranchInformation.ClientApplicationVersion = string.Empty;
                        lookupTransactionRequest.CebuanaBranchInformation = cebuanaBranchInformation;
                        PeraLinkXoomWS.LookupTransactionResult lookupTransactionResult = xoomWSClient.LookupTransaction(lookupTransactionRequest);

                        internationalRemittance = new PeraLinkCoreWcf.InternationalRemittance();
                        internationalRemittance.InternationalRemittanceID = 1;
                        internationalRemittance.SenderClient = new PeraLinkCoreWcf.Client();
                        internationalRemittance.SenderClient.FirstName = lookupTransactionResult.SenderFirstName;
                        internationalRemittance.SenderClient.LastName = lookupTransactionResult.SenderLastName;
                        internationalRemittance.SenderClient.BirthDate = RDFramework.Utility.Validation.MinimumSqlDateTime;
                        internationalRemittance.Beneficiary = new PeraLinkCoreWcf.Beneficiary();
                        internationalRemittance.Beneficiary.FirstName = lookupTransactionResult.BeneficiaryFirstName;
                        internationalRemittance.Beneficiary.LastName = lookupTransactionResult.BeneficiaryLastName;
                        internationalRemittance.Beneficiary.BirthDate = RDFramework.Utility.Validation.MinimumSqlDateTime;
                        internationalRemittance.ControlNumber = lookupTransactionResult.TransactionNumber;
                        internationalRemittance.DateTimeSent = lookupTransactionResult.TransactionDate;
                        internationalRemittance.PrincipalAmount = lookupTransactionResult.PayoutAmount;
                        internationalRemittance.RemittanceStatus = XoomWSClient.MapStatus(lookupTransactionResult.TransactionStatus);
                        internationalRemittance.SendCurrency = new PeraLinkCoreWcf.Currency();
                        internationalRemittance.SendCurrency.Code = lookupTransactionResult.PayoutCurrency;
                        internationalRemittance.SendCurrency.CurrencyID = 6;

                        internationalRemittance.SentByAgent = new PeraLinkCoreWcf.Agent();
                        internationalRemittance.SentByAgent.Partner = sendingPartner;
                        //internationalRemittance.SentByAgent.AgentCode = ;
                        PeraLinkCoreWcf.Agent[] agentCollection = serviceClient.GetPartnerAgentCollection(sendingPartner);
                        if (agentCollection == null)
                        {
                            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                            throw new RDFramework.ClientException(message.Content, message.MessageID);

                        }
                        else if (agentCollection.Length < 1)
                        {
                            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                            throw new RDFramework.ClientException(message.Content, message.MessageID);

                        }
                        else
                        {
                            internationalRemittance.SentByAgent.AgentCode = agentCollection[0].AgentCode;
                            internationalRemittance.SentByAgent.Partner.PartnerID = sendingPartner.PartnerID;
                            internationalRemittance.SentByAgent = serviceClient.FindAgentCode(internationalRemittance.SentByAgent);
                            internationalRemittance.SentByUserID = internationalRemittance.SentByAgent.AgentCode;
                        }
                        returnValue.OtherDetails = string.Format("{0}|{1}|{2}", lookupTransactionResult.MultiCurrencyPayoutCode, lookupTransactionResult.SecretWord, lookupTransactionResult.SenderMessageToBeneficiary);
                    }
                }
                #endregion

                #region RAS International
                else if (sendingPartner.PartnerCode.Length == 5
                    && sendingPartner.PartnerCode.StartsWith("P"))
                {
                    using (PeraLinkRASInternationalWcfClient peraLinkRASInternationalWcfClient = new PeraLinkRASInternationalWcfClient())
                    {
                        PeraLinkRASInternationalWcf.LookUpTieUpTransactionResult lookUpTieUpTransactionResult = new PeraLinkRASInternationalWcf.LookUpTieUpTransactionResult();
                        PeraLinkRASInternationalWcf.LookUpTieUpTransactionRequest lookUpTieUpTransactionRequest = new PeraLinkRASInternationalWcf.LookUpTieUpTransactionRequest();
                        lookUpTieUpTransactionRequest.ControlNumber = this.ControlNumber;
                        lookUpTieUpTransactionRequest.SendingPartnerCode = sendingPartner.PartnerCode;

                        lookUpTieUpTransactionResult = peraLinkRASInternationalWcfClient.LookUpTieUpTransaction(lookUpTieUpTransactionRequest);

                        internationalRemittance = new PeraLinkCoreWcf.InternationalRemittance();
                        internationalRemittance.InternationalRemittanceID = lookUpTieUpTransactionResult.RemittanceTransaction.TransactionID;
                        internationalRemittance.Beneficiary = new PeraLinkCoreWcf.Beneficiary();
                        internationalRemittance.Beneficiary.FirstName = lookUpTieUpTransactionResult.RemittanceTransaction.Beneficiary.FirstName;
                        internationalRemittance.Beneficiary.LastName = lookUpTieUpTransactionResult.RemittanceTransaction.Beneficiary.LastName;
                        internationalRemittance.Beneficiary.BirthDate = RDFramework.Utility.Validation.MinimumSqlDateTime;

                        internationalRemittance.ControlNumber = lookUpTieUpTransactionResult.RemittanceTransaction.ControlNumber;
                        internationalRemittance.DateTimeSent = lookUpTieUpTransactionResult.RemittanceTransaction.DateTimeCreated;
                        internationalRemittance.PrincipalAmount = lookUpTieUpTransactionResult.RemittanceTransaction.Amount;
                        internationalRemittance.ServiceFee = lookUpTieUpTransactionResult.RemittanceTransaction.ServiceCharge;

                        internationalRemittance.RemittanceStatus = PeraLinkRASInternationalWcfClient.MapStatus(lookUpTieUpTransactionResult.RemittanceTransaction.Status);

                        internationalRemittance.SendCurrency = new PeraLinkCoreWcf.Currency();
                        internationalRemittance.SendCurrency.Code = lookUpTieUpTransactionResult.RemittanceTransaction.Currency;
                        internationalRemittance.SendCurrency.CurrencyID = 6;

                        internationalRemittance.SenderClient = new PeraLinkCoreWcf.Client();
                        internationalRemittance.SenderClient.FirstName = lookUpTieUpTransactionResult.RemittanceTransaction.Sender.FirstName;
                        internationalRemittance.SenderClient.LastName = lookUpTieUpTransactionResult.RemittanceTransaction.Sender.LastName;
                        internationalRemittance.SenderClient.BirthDate = RDFramework.Utility.Validation.MinimumSqlDateTime;

                        internationalRemittance.SentByAgent = new PeraLinkCoreWcf.Agent();
                        internationalRemittance.SentByAgent.Partner = sendingPartner;
                        internationalRemittance.SentByUserID = lookUpTieUpTransactionResult.RemittanceTransaction.CreatedBy;

                        PeraLinkCoreWcf.Agent[] agentCollection = serviceClient.GetPartnerAgentCollection(sendingPartner);
                        if (agentCollection.Length == null)
                        {
                            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                            throw new RDFramework.ClientException(message.Content, message.MessageID);

                        }
                        else if (agentCollection.Length < 1)
                        {
                            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                            throw new RDFramework.ClientException(message.Content, message.MessageID);
                        }
                        else
                        {
                            internationalRemittance.SentByAgent.AgentCode = agentCollection[0].AgentCode;
                            internationalRemittance.SentByAgent = serviceClient.FindAgentCode(internationalRemittance.SentByAgent);
                        }


                    }
                }
                #endregion

                #region Tranglo
                else if (sendingPartner.PartnerCode == SystemSetting.TrangloPartnerCode)
                {
                    using (TrangloWcfClient trangloWcfClient = new TrangloWcfClient())
                    {
                        PeraLinkTrangloWcf.FindTransactionRequest findTransactionRequest = new PeraLinkTrangloWcf.FindTransactionRequest();
                        findTransactionRequest.ControlNumber = this.ControlNumber;
                        PeraLinkTrangloWcf.FindTransactionResult findTransactionResult = trangloWcfClient.FindTransaction(findTransactionRequest);
                        
                        internationalRemittance = new PeraLinkCoreWcf.InternationalRemittance();
                        internationalRemittance.InternationalRemittanceID = 1;
                        internationalRemittance.SenderClient = new PeraLinkCoreWcf.Client();
                        internationalRemittance.SenderClient.FirstName = findTransactionResult.SenderFirstName;
                        internationalRemittance.SenderClient.LastName = findTransactionResult.SenderLastName;
                        internationalRemittance.SenderClient.BirthDate = RDFramework.Utility.Validation.MinimumSqlDateTime;
                        internationalRemittance.Beneficiary = new PeraLinkCoreWcf.Beneficiary();
                        internationalRemittance.Beneficiary.FirstName = findTransactionResult.BeneficiaryFirstName;
                        internationalRemittance.Beneficiary.LastName = findTransactionResult.BeneficiaryLastName;
                        internationalRemittance.Beneficiary.BirthDate = RDFramework.Utility.Validation.MinimumSqlDateTime;
                        internationalRemittance.ControlNumber = findTransactionResult.ReferenceNumber;
                        internationalRemittance.DateTimeSent = findTransactionResult.TransactionDate;
                        internationalRemittance.PrincipalAmount = findTransactionResult.PayoutAmount;
                        internationalRemittance.RemittanceStatus = TrangloWcfClient.MapStatus(findTransactionResult.TransactionStatus);
                        internationalRemittance.SendCurrency = new PeraLinkCoreWcf.Currency();
                        internationalRemittance.SendCurrency.Code = findTransactionResult.PayoutCurrency;
                        internationalRemittance.SendCurrency.CurrencyID = 6;

                        internationalRemittance.SentByAgent = new PeraLinkCoreWcf.Agent();
                        internationalRemittance.SentByAgent.Partner = sendingPartner;
                        //internationalRemittance.SentByAgent.AgentCode = ;
                        PeraLinkCoreWcf.Agent[] agentCollection = serviceClient.GetPartnerAgentCollection(sendingPartner);
                        if (agentCollection == null)
                        {
                            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                            throw new RDFramework.ClientException(message.Content, message.MessageID);

                        }
                        else if (agentCollection.Length < 1)
                        {
                            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                            throw new RDFramework.ClientException(message.Content, message.MessageID);

                        }
                        else
                        {
                            internationalRemittance.SentByAgent.AgentCode = agentCollection[0].AgentCode;
                            internationalRemittance.SentByAgent.Partner.PartnerID = sendingPartner.PartnerID;
                            internationalRemittance.SentByAgent = serviceClient.FindAgentCode(internationalRemittance.SentByAgent);
                            internationalRemittance.SentByUserID = internationalRemittance.SentByAgent.AgentCode;
                        }

                        PeraLinkCoreWcf.Country payoutCountry = new PeraLinkCoreWcf.Country();
                        payoutCountry.CountryID = partner.Country.CountryID;
                        payoutCountry = serviceClient.FindCountry(payoutCountry);
                        returnValue.PayoutCountry = payoutCountry.CountryCodeAlpha3;
                    }
                }
                #endregion
                #region iRemit
                else if (sendingPartner.PartnerCode == SystemSetting.IRemitPartnerCode)
                {
                    IRemitWcfClient iRemitWcfClient = new IRemitWcfClient();
                    internationalRemittance = iRemitWcfClient.InquireTransaction(this.ControlNumber);
                    internationalRemittance.SentByAgent = new PeraLinkCoreWcf.Agent();
                    internationalRemittance.SentByAgent.Partner = sendingPartner;
                    //internationalRemittance.SentByAgent.AgentCode = internationalRemittance.SentByAgent.AgentCode;
                    //internationalRemittance.SentByAgent = serviceClient.FindAgentCode(internationalRemittance.SentByAgent);
                    //internationalRemittance.SentByUserID = internationalRemittance.SentByAgent.AgentCode;
                    PeraLinkCoreWcf.Agent[] agentCollection = serviceClient.GetPartnerAgentCollection(sendingPartner);
                    if (agentCollection == null)
                    {
                        PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                        throw new RDFramework.ClientException(message.Content, message.MessageID);

                    }
                    else if (agentCollection.Length < 1)
                    {
                        PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                        throw new RDFramework.ClientException(message.Content, message.MessageID);

                    }
                    else
                    {
                        internationalRemittance.SentByAgent.AgentCode = agentCollection[0].AgentCode;
                        internationalRemittance.SentByAgent.Partner.PartnerID = sendingPartner.PartnerID;
                        internationalRemittance.SentByAgent = serviceClient.FindAgentCode(internationalRemittance.SentByAgent);
                        internationalRemittance.SentByUserID = internationalRemittance.SentByAgent.AgentCode;
                    }
                    if (internationalRemittance == null)
                    {
                        PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(14);
                        throw new RDFramework.ClientException(message.Content, message.MessageID);
                    }
                    else
                    {
                        if (internationalRemittance.RemittanceStatus.RemittanceStatusID == RemittanceStatusID.Paid)
                        {
                            internationalRemittance = serviceClient.FindInternationalRemittance(internationalRemittance);
                        }
                    }
                }
                #endregion

                #region Unknown partner
                else
                {
                    PeraLinkCoreWcf.Message searchMessage = SystemResource.GetMessageWithListID(20);
                    throw new RDFramework.ClientException(searchMessage.Content, searchMessage.MessageID);
                }
                #endregion

                #endregion
            }
            #endregion

            #region Validate if transaction exists
            if (internationalRemittance.InternationalRemittanceID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(14);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }
            #endregion

            #region Validate Transaction Amount
            PeraLinkCoreWcf.PartnerServiceCurrency[] partnerServiceCurrencyCollection = serviceClient.GetPartnerServiceCurrencyCollection(partnerServiceMapping);

            PeraLinkCoreWcf.PartnerServiceCurrency transactionCurrency = Array.Find<PeraLinkCoreWcf.PartnerServiceCurrency>(partnerServiceCurrencyCollection
                , delegate(PeraLinkCoreWcf.PartnerServiceCurrency eachPartnerServiceCurrency)
                {
                    return eachPartnerServiceCurrency.Currency.CurrencyID == internationalRemittance.SendCurrency.CurrencyID;
                });

            if (transactionCurrency == null)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(4);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }

            decimal minGeneralSendAmount = SystemSetting.GetMinSendAmount(transactionCurrency.Currency.Code);
            decimal maxGeneralSendAmount = SystemSetting.GetMaxSendAmount(transactionCurrency.Currency.Code);

            if (minGeneralSendAmount <= internationalRemittance.PrincipalAmount && maxGeneralSendAmount >= internationalRemittance.PrincipalAmount)
            {
                // Proceed
            }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(6);
                throw new RDFramework.ClientException(
                    string.Format(message.Content, transactionCurrency.Currency.Code, minGeneralSendAmount, maxGeneralSendAmount)
                    , message.MessageID);
            }

            #endregion

            #region Validate requesting partner's access to transaction
            if (internationalRemittance.SentByAgent.Partner.PartnerID == agent.Partner.PartnerID)
            {
                // Proceed
            }
            else
            {
                PeraLinkCoreWcf.PartnerServicePair[] partnerServicePairCollection = serviceClient.GetPartnerServicePairCollection(partnerServiceMapping);

                PeraLinkCoreWcf.PartnerServicePair pairing = Array.Find<PeraLinkCoreWcf.PartnerServicePair>(partnerServicePairCollection
                    , delegate(PeraLinkCoreWcf.PartnerServicePair eachPartnerServicePair)
                    {
                        if (eachPartnerServicePair.PairedPartner == null)
                        {
                            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(2);
                            throw new RDFramework.ClientException(message.Content, message.MessageID);
                        }
                        else
                        {
                            return eachPartnerServicePair.PairedPartner.PartnerID == internationalRemittance.SentByAgent.Partner.PartnerID;
                        }
                    });

                if (pairing == null)
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(14);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
                else
                {
                    // Proceed
                }
            }
            #endregion

            #region Register sender in PeraLink
            PeraLinkCoreWcf.Client senderCopy = new PeraLinkCoreWcf.Client();
            senderCopy.FirstName = internationalRemittance.SenderClient.FirstName;
            senderCopy.LastName = internationalRemittance.SenderClient.LastName;
            senderCopy.MiddleName = internationalRemittance.SenderClient.MiddleName;
            senderCopy.BirthDate = internationalRemittance.SenderClient.BirthDate;
            if (internationalRemittance.SenderClient.CountryAddress != null)
            {
                senderCopy.CountryAddress = internationalRemittance.SenderClient.CountryAddress;
            }

            PeraLinkCoreWcf.Client[] registeredSenderCollection = serviceClient.FindClient(senderCopy, partner.PartnerID);

            if (registeredSenderCollection.Length > 0)
            {
                internationalRemittance.SenderClient = registeredSenderCollection[0];
            }
            else
            {
                internationalRemittance.SenderClient.CreatedByAgent = agent;
                internationalRemittance.SenderClient.CreatedByUserID = this.UserID;
                internationalRemittance.SenderClient.DateTimeCreated = DateTime.Now;
                internationalRemittance.SenderClient.CreatedByUserID = this.UserID;
                internationalRemittance.SenderClient = serviceClient.AddClient(internationalRemittance.SenderClient);
            }
            #endregion

            #region Register beneficiary in PeraLink
            internationalRemittance.Beneficiary.SenderClient = internationalRemittance.SenderClient;
            PeraLinkCoreWcf.Beneficiary beneficiaryCopy = new PeraLinkCoreWcf.Beneficiary();
            beneficiaryCopy.BeneficiaryID = this.BeneficiaryID;
            beneficiaryCopy.FirstName = internationalRemittance.Beneficiary.FirstName;
            beneficiaryCopy.LastName = internationalRemittance.Beneficiary.LastName;
            beneficiaryCopy.MiddleName = internationalRemittance.Beneficiary.MiddleName;
            beneficiaryCopy.BirthDate = internationalRemittance.Beneficiary.BirthDate;
            beneficiaryCopy.SenderClient = internationalRemittance.SenderClient;

            if (internationalRemittance.Beneficiary.CountryAddress != null)
            {
                beneficiaryCopy.CountryAddress = internationalRemittance.Beneficiary.CountryAddress;
            }

            PeraLinkCoreWcf.Beneficiary[] registeredBeneficiaryCollection = serviceClient.FindBeneficiary(beneficiaryCopy);

            if (registeredBeneficiaryCollection.Length > 0)
            {
                beneficiaryCopy = serviceClient.GetBeneficiary(registeredBeneficiaryCollection[0]);
                internationalRemittance.Beneficiary = beneficiaryCopy;
            }
            else
            {
                internationalRemittance.Beneficiary.CreatedBy = this.UserID;
                internationalRemittance.Beneficiary.DateTimeCreated = DateTime.Now;
                internationalRemittance.Beneficiary = serviceClient.AddBeneficiary(internationalRemittance.Beneficiary);
            }
            #endregion

            #region Map transaction details

            if (internationalRemittance == null)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(14);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                returnValue.BeneficiaryFirstName = internationalRemittance.Beneficiary.FirstName;
                returnValue.BeneficiaryID = internationalRemittance.Beneficiary.BeneficiaryID;
                returnValue.BeneficiaryLastName = internationalRemittance.Beneficiary.LastName;
                returnValue.BeneficiaryMiddleName = internationalRemittance.Beneficiary.MiddleName;
                returnValue.BeneficiaryBirthDate = internationalRemittance.Beneficiary.BirthDate;
                returnValue.Beneficiary = new Beneficiary();
                returnValue.Beneficiary.Load(internationalRemittance.Beneficiary);
                returnValue.ControlNumber = internationalRemittance.ControlNumber;
                returnValue.DateTimeSent = internationalRemittance.DateTimeSent;
                returnValue.InternationalRemittanceID = internationalRemittance.InternationalRemittanceID;
                returnValue.PrincipalAmount = internationalRemittance.PrincipalAmount;
                returnValue.RemittanceStatusDescription = internationalRemittance.RemittanceStatus.Description;
                returnValue.RemittanceStatusID = internationalRemittance.RemittanceStatus.RemittanceStatusID;
                returnValue.SendCurrencyCode = internationalRemittance.SendCurrency.Code;
                returnValue.SendCurrencyID = internationalRemittance.SendCurrency.CurrencyID;
                returnValue.SenderClientID = internationalRemittance.SenderClient.ClientID;
                returnValue.SenderClientNumber = internationalRemittance.SenderClient.ClientNumber;
                returnValue.SenderFirstName = internationalRemittance.SenderClient.FirstName;
                returnValue.SenderLastName = internationalRemittance.SenderClient.LastName;
                returnValue.SenderMiddleName = internationalRemittance.SenderClient.MiddleName;
                returnValue.SenderBirthDate = internationalRemittance.SenderClient.BirthDate;
                returnValue.ServiceFee = internationalRemittance.ServiceFee;
                returnValue.SentByPartnerCode = internationalRemittance.SentByAgent.Partner.PartnerCode;
                returnValue.SentByPartnerID = internationalRemittance.SentByAgent.Partner.PartnerID;
                returnValue.SentByPartnerName = internationalRemittance.SentByAgent.Partner.Name;
                returnValue.SentByAgentCode = internationalRemittance.SentByAgent.AgentCode;
                returnValue.SentByAgentID = internationalRemittance.SentByAgent.AgentID;
                returnValue.SentByUserID = internationalRemittance.SentByUserID;
                returnValue.SenderAddress = internationalRemittance.SenderClient.Address;
                returnValue.OriginatingCountry = originatingCountry;

            }
            #endregion

            #region Response Message for MG use
            if (this.PullTransactionPartnerCode == SystemSetting.MoneyGramPartnerCode)
            {

                if (String.IsNullOrWhiteSpace(internationalRemittance.Beneficiary.Address))
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(49);
                    returnValue.Message = message.Content;
                    returnValue.MessageID = message.MessageID;
                }
                else if (String.IsNullOrWhiteSpace(internationalRemittance.Beneficiary.ZipCode))
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(50);
                    returnValue.Message = message.Content;
                    returnValue.MessageID = message.MessageID;
                }
                else if (String.IsNullOrWhiteSpace(internationalRemittance.Beneficiary.Occupation))
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(51);
                    returnValue.Message = message.Content;
                    returnValue.MessageID = message.MessageID;
                }
                else if (String.IsNullOrWhiteSpace(internationalRemittance.Beneficiary.CountryAddress.CountryCodeAlpha3))
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(83);
                    returnValue.Message = message.Description;
                    returnValue.MessageID = message.SystemMessageID;
                }
                else if (String.IsNullOrWhiteSpace(internationalRemittance.Beneficiary.BirthCountry.CountryCodeAlpha3))
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(84);
                    returnValue.Message = message.Description;
                    returnValue.MessageID = message.SystemMessageID;
                }
            }
            #endregion

            returnValue.ResultStatus = ResultStatus.Successful;
            return returnValue;
        }
    }
    #endregion
}