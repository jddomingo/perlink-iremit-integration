using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetTerminalReadingReportResult: ServiceResult
{
	#region Constructor
	public GetTerminalReadingReportResult()
	{ }
	#endregion

	#region Fields/Properties
	private TerminalReadingReportCollection _terminalReadingReportCollection;
	[DataMember]
	public TerminalReadingReportCollection TerminalReadingReportCollection
	{
		get { return _terminalReadingReportCollection; }
		set { _terminalReadingReportCollection = value; }
	}

	private int _totalPageCount;
	[DataMember]
	public int TotalPageCount
	{
		get { return _totalPageCount; }
		set { _totalPageCount = value; }
	}
	#endregion
}