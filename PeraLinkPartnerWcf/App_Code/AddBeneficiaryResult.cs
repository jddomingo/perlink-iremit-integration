using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class AddBeneficiaryResult: BaseResult
{
    #region Constructor
    public AddBeneficiaryResult() { }
    #endregion

    #region Fields/Properties
    private Int64 _beneficiaryID;

    [DataMember]
    public Int64 BeneficiaryID
    {
        get { return _beneficiaryID; }
        set { _beneficiaryID = value; }
    }
    #endregion
}