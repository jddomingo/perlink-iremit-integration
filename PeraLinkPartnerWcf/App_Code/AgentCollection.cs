using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AgentCollection
/// </summary>
public class AgentCollection:List<Agent>
{
    #region Constructor
    public AgentCollection()
	{
		//

		//
    }
    #endregion

    #region Public Methods
    public void AddList(PeraLinkCoreWcf.Agent[] agentCollection)
    {
        Array.ForEach<PeraLinkCoreWcf.Agent>(agentCollection
            , delegate(PeraLinkCoreWcf.Agent eachAgent)
            {
                Agent mappedAgent = new Agent();
                mappedAgent.Load(eachAgent);
                this.Add(mappedAgent);
            });
    }
    #endregion
}