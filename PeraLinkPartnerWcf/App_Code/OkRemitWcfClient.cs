using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for OkRemitWcfClient
/// </summary>
public class OkRemitWcfClient //: IDisposable
{
    #region Constructor
    public OkRemitWcfClient()
    {
        _serviceClient = new PeraLinkOKRemitWcf.ServiceClient();
    }
    #endregion

    #region Fields/Properties
    private PeraLinkOKRemitWcf.ServiceClient _serviceClient;
    #endregion

    #region Public Methods
    public PeraLinkCoreWcf.DomesticRemittance FindRemittance(string controlNumber)
    {
        PeraLinkCoreWcf.DomesticRemittance returnValue = new PeraLinkCoreWcf.DomesticRemittance();

        PeraLinkOKRemitWcf.FindRemittanceRequest findRemittanceRequest = new PeraLinkOKRemitWcf.FindRemittanceRequest();
        findRemittanceRequest.ReferenceNumber = controlNumber;

        PeraLinkOKRemitWcf.FindRemittanceResult findRemittanceResult = _serviceClient.FindRemittance(findRemittanceRequest);

        switch (findRemittanceResult.ResultStatus)
        {
            case ResultStatus.Successful:
                returnValue.Beneficiary = new PeraLinkCoreWcf.Beneficiary();
                returnValue.Beneficiary.FirstName = findRemittanceResult.ReceiverFirstName;
                returnValue.Beneficiary.LastName = findRemittanceResult.ReceiverLastName;
                returnValue.Beneficiary.MiddleName = findRemittanceResult.ReceiverMiddleName;
                returnValue.Beneficiary.BirthDate = RDFramework.Utility.Validation.MinimumSqlDateTime;
                returnValue.DateTimeSent = DateTime.Now;
                returnValue.ControlNumber = controlNumber;
                returnValue.PrincipalAmount = findRemittanceResult.Amount;
                returnValue.ServiceFee = findRemittanceResult.ServiceFee;
                returnValue.RemittanceStatus = MapStatus(findRemittanceResult.StatusCode);
                returnValue.SendCurrency = new PeraLinkCoreWcf.Currency();
                returnValue.SendCurrency.CurrencyID = 6;
                returnValue.SendCurrency.Code = "PHP";
                returnValue.SenderClient = new PeraLinkCoreWcf.Client();
                returnValue.SenderClient.FirstName = findRemittanceResult.SenderFirstName;
                returnValue.SenderClient.LastName = findRemittanceResult.SenderLastName;
                returnValue.SenderClient.MiddleName = findRemittanceResult.SenderMiddleName;
                returnValue.SenderClient.BirthDate = RDFramework.Utility.Validation.MinimumSqlDateTime;
                returnValue.RemittanceStatus = MapStatus(findRemittanceResult.StatusCode);
                returnValue.SentByUserID = string.Format("{0}-{1}", findRemittanceResult.SendingPartnerAgent, findRemittanceResult.SendingPartnerAgentCode);
                return returnValue;
            case ResultStatus.Error:
            case ResultStatus.Failed:
                throw new RDFramework.ClientException(findRemittanceResult.Message, (int)findRemittanceResult.MessageID);
            default:
                throw new ArgumentOutOfRangeException(findRemittanceResult.Message);

        }



    }

    public void PayoutRemittance(string controlNumber)
    {
        PeraLinkOKRemitWcf.PayoutRemittanceRequest payoutRemittanceRequest = new PeraLinkOKRemitWcf.PayoutRemittanceRequest();
        payoutRemittanceRequest.ReferenceNumber = controlNumber;

        PeraLinkOKRemitWcf.PayoutRemittanceResult payoutRemittanceResult = new PeraLinkOKRemitWcf.PayoutRemittanceResult();
        payoutRemittanceResult = _serviceClient.PayoutRemittance(payoutRemittanceRequest);

        switch (payoutRemittanceResult.ResultStatus)
        {
            case ResultStatus.Successful:
                return;
            case ResultStatus.Error:
            case ResultStatus.Failed:
                throw new RDFramework.ClientException(payoutRemittanceResult.Message, (int)payoutRemittanceResult.MessageID);
            default:
                throw new ArgumentOutOfRangeException(payoutRemittanceResult.Message);
        }
    }

    public static PeraLinkCoreWcf.RemittanceStatus MapStatus(PeraLinkOKRemitWcf.OkRemitStatus okRemitStatus)
    {
        switch (okRemitStatus)
        {
            case PeraLinkOKRemitWcf.OkRemitStatus.Outstanding:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 1;
                    returnValue.Description = "Outstanding";

                    return returnValue;
                }
            case PeraLinkOKRemitWcf.OkRemitStatus.Cancelled:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 4;
                    returnValue.Description = "Cancelled";

                    return returnValue;
                }
            case PeraLinkOKRemitWcf.OkRemitStatus.Paid:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 2;
                    returnValue.Description = "Paid";

                    return returnValue;
                }
            case PeraLinkOKRemitWcf.OkRemitStatus.Inactive:
            case PeraLinkOKRemitWcf.OkRemitStatus.Unavailable:
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(97);
                    throw new RDFramework.ClientException(string.Format(message.Description, okRemitStatus.ToString()), message.SystemMessageID);
                }
            default:
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(86);
                    throw new RDFramework.ClientException(string.Format(message.Description, okRemitStatus.ToString()), message.SystemMessageID);
                }
        }
    }
    #endregion

    //void IDisposable.Dispose()
    //{
    //    bool isClosed = false;

    //    try
    //    {
    //        if (this._serviceClient.State != System.ServiceModel.CommunicationState.Faulted)
    //        {
    //            this._serviceClient.Close();
    //            isClosed = true;
    //        }
    //        else
    //        {
    //            // Proceed with Abort in finally
    //        }
    //    }
    //    finally
    //    {
    //        if (!isClosed)
    //        {
    //            this._serviceClient.Abort();
    //        }
    //        else
    //        {
    //            // Do nothing since state is already closed
    //        }
    //    }
    //}
}