using System;
using System.Collections.Generic;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetBeneficiaryBySenderRequest:BaseRequest
{
    #region Constructor
    public GetBeneficiaryBySenderRequest()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private Int64 _senderClientID;
    [DataMember]
    public Int64 SenderClientID
    {
        get { return _senderClientID; }
        set { _senderClientID = value; }
    }
    #endregion

    #region Internal Methods
    internal GetBeneficiaryBySenderResult Process()
    {
        GetBeneficiaryBySenderResult returnValue = new GetBeneficiaryBySenderResult();

        // Important: This function validates the request credential
        this.AuthenticateRequest();

        PeraLinkCoreWcf.Client senderClient = new PeraLinkCoreWcf.Client();
        senderClient.ClientID = this.SenderClientID;

        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
            returnValue.BeneficiaryCollection = new BeneficiaryCollection();
            returnValue.BeneficiaryCollection.AddList(serviceClient.GetBeneficiaryBySender(senderClient));
        }
        
        returnValue.ResultStatus = ResultStatus.Successful;

        return returnValue;
    }
    #endregion
}