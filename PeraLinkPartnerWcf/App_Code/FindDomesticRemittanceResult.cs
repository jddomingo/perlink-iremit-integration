
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class FindDomesticRemittanceResult : BaseResult
{
	#region Constructor
	public FindDomesticRemittanceResult()
	{
		//

		//
	}
	#endregion

	#region Fields/Properties
	private Int64 _domesticRemittanceID;
	[DataMember]
	public Int64 DomesticRemittanceID
	{
		get { return _domesticRemittanceID; }
		set { _domesticRemittanceID = value; }
	}

	private string _controlNumber;
	[DataMember]
	public string ControlNumber
	{
		get { return _controlNumber; }
		set { _controlNumber = value; }
	}

	private Int64 _senderClientID;
	[DataMember]
	public Int64 SenderClientID
	{
		get { return _senderClientID; }
		set { _senderClientID = value; }
	}

	private string _senderClientNumber;
	[DataMember]
	public string SenderClientNumber
	{
		get { return _senderClientNumber; }
		set { _senderClientNumber = value; }
	}

	private string _senderFirstName;
	[DataMember]
	public string SenderFirstName
	{
		get { return _senderFirstName; }
		set { _senderFirstName = value; }
	}

	private string _senderMiddleName;
	[DataMember]
	public string SenderMiddleName
	{
		get { return _senderMiddleName; }
		set { _senderMiddleName = value; }
	}

	private string _senderLastName;
	[DataMember]
	public string SenderLastName
	{
		get { return _senderLastName; }
		set { _senderLastName = value; }
	}

	private DateTime? _senderBirthDate;
	[DataMember]
	public DateTime? SenderBirthDate
	{
		get { return _senderBirthDate; }
		set { _senderBirthDate = value; }
	}

	private Int64 _beneficiaryID;
	[DataMember]
	public Int64 BeneficiaryID
	{
		get { return _beneficiaryID; }
		set { _beneficiaryID = value; }
	}

	private string _beneficiaryFirstName;
	[DataMember]
	public string BeneficiaryFirstName
	{
		get { return _beneficiaryFirstName; }
		set { _beneficiaryFirstName = value; }
	}

	private string _beneficiaryMiddleName;
	[DataMember]
	public string BeneficiaryMiddleName
	{
		get { return _beneficiaryMiddleName; }
		set { _beneficiaryMiddleName = value; }
	}

	private DateTime? _beneficiaryBirthDate;
	[DataMember]
	public DateTime? BeneficiaryBirthDate
	{
		get { return _beneficiaryBirthDate; }
		set { _beneficiaryBirthDate = value; }
	}

	private string _beneficiaryLastName;
	[DataMember]
	public string BeneficiaryLastName
	{
		get { return _beneficiaryLastName; }
		set { _beneficiaryLastName = value; }
	}

	private decimal _principalAmount;
	[DataMember]
	public decimal PrincipalAmount
	{
		get { return _principalAmount; }
		set { _principalAmount = value; }
	}

	private decimal _serviceFee;
	[DataMember]
	public decimal ServiceFee
	{
		get { return _serviceFee; }
		set { _serviceFee = value; }
	}

	private int _remittanceStatusID;
	[DataMember]
	public int RemittanceStatusID
	{
		get { return _remittanceStatusID; }
		set { _remittanceStatusID = value; }
	}

	private string _remittanceStatusDescription;
	[DataMember]
	public string RemittanceStatusDescription
	{
		get { return _remittanceStatusDescription; }
		set { _remittanceStatusDescription = value; }
	}

	private int _sendCurrencyID;
	[DataMember]
	public int SendCurrencyID
	{
		get { return _sendCurrencyID; }
		set { _sendCurrencyID = value; }
	}

	private string _sendCurrencyCode;
	[DataMember]
	public string SendCurrencyCode
	{
		get { return _sendCurrencyCode; }
		set { _sendCurrencyCode = value; }
	}

	private DateTime _dateTimeSent;
	[DataMember]
	public DateTime DateTimeSent
	{
		get { return _dateTimeSent; }
		set { _dateTimeSent = value; }
	}

	private Int64 _sentByAgentID;
	[DataMember]
	public Int64 SentByAgentID
	{
		get { return _sentByAgentID; }
		set { _sentByAgentID = value; }
	}

	private string _sentByAgentCode;
	[DataMember]
	public string SentByAgentCode
	{
		get { return _sentByAgentCode; }
		set { _sentByAgentCode = value; }
	}

	private Int64 _sentByPartnerID;
	[DataMember]
	public Int64 SentByPartnerID
	{
		get { return _sentByPartnerID; }
		set { _sentByPartnerID = value; }
	}

	private string _sentByPartnerCode;
	[DataMember]
	public string SentByPartnerCode
	{
		get { return _sentByPartnerCode; }
		set { _sentByPartnerCode = value; }
	}

	private string _sentByPartnerName;
	[DataMember]
	public string SentByPartnerName
	{
		get { return _sentByPartnerName; }
		set { _sentByPartnerName = value; }
	}

	private string _sentByUserID;
	[DataMember]
	public string SentByUserID
	{
		get { return _sentByUserID; }
		set { _sentByUserID = value; }
	}

	private string _payoutPartnerCode;
	[DataMember]
	public string PayoutPartnerCode
	{
		get { return _payoutPartnerCode; }
		set { _payoutPartnerCode = value; }
	}

	private string _payoutAgent;
	[DataMember]
	public string PayoutAgent
	{
		get { return _payoutAgent; }
		set { _payoutAgent = value; }
	}

	private DateTime? _payoutDateTime;
	[DataMember]
	public DateTime? PayoutDateTime
	{
		get { return _payoutDateTime; }
		set { _payoutDateTime = value; }
	}

	private Int64 _sentByTerminalID;
	[DataMember]
	public Int64 SentByTerminalID
	{
		get { return _sentByTerminalID; }
		set { _sentByTerminalID = value; }
	}

	private string _sentByTerminalCode;
	[DataMember]
	public string SentByTerminalCode
	{
		get { return _sentByTerminalCode; }
		set { _sentByTerminalCode = value; }
	}

	private string _payoutTerminalCode;
	[DataMember]
	public string PayoutTerminalCode
	{
		get { return _payoutTerminalCode; }
		set { _payoutTerminalCode = value; }
    }

    #endregion
}