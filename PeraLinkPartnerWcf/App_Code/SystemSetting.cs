#define ENCASHMENT_CL
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;

public class SystemSetting
{
    #region Constructor
    public SystemSetting()
    {
        //

        //
    }
    #endregion

    #region Fields/Properties
    public static string CebuanaLhuillierPartnerCode
    {
        get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("CebuanaLhuillierPartnerCode"); }
    }

    public static string NYBPPartnerCode
    {
        get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("NYBPPartnerCode"); }
    }

    public static string MetrobankPartnerCode
    {
        get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("MetrobankPartnerCode"); }
    }

    public static string SecurityBankPartnerCode
    {
        get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("SecurityBankPartnerCode"); }
    }

    public static string PlacidSpotCashPartnerCode
    {
        get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("PlacidSpotCashPartnerCode"); }
    }

    public static string TransferMoneyLinkPartnerCode
    {
        get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("TransferMoneyLinkPartnerCode"); }
    }

    public static string CimbPartnerCode
    {
        get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("CimbPartnerCode"); }
    }

    public static string LuckyMoneyIncPartnerCode
    {
        get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("LMIPartnerCode"); }
    }

    public static string ImePartnerCode
    {
        get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("ImePartnerCode"); }
    }

    public static string EMoneyBranchCode
    {
        get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("EMoneyBranchCode"); }
    }

    public static int EMoneySourceOfFundID
    {
        get { return Convert.ToInt32(RDFramework.Utility.ConfigurationManager.GetAppSetting("EMoneySourceOfFundID")); }
    }

    public static int AccountingSourceOfFundID
    {
        get { return Convert.ToInt32(RDFramework.Utility.ConfigurationManager.GetAppSetting("AccountingSourceOfFundID")); }
    }

    public static string MoneyGramPartnerCode
    {
        get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("MoneyGramPartnerCode"); }
    }

	public static string OkRemitPartnerCode
	{
		get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("OkRemitPartnerCode"); }
	}

    public static string XoomPartnerCode
    {
        get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("XoomPartnerCode"); }
    }

    public static string PeraLinkBDSBranchCode
    {
        get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("PeraLinkBDSBranchCode"); }
    }

    public static string TrangloPartnerCode
    {
        get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("TrangloPartnerCode"); }
    }
    public static string IRemitPartnerCode
    {
        get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("IRemitPartnerCode"); }
    }
#if ENCASHMENT_CL
    public static string EMoneyEncashmentPartnerCode
    {
        get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("EMoneyEncashmentPartnerCode"); }
    }
#endif

    public static string CryptoPrivateKey
    {
        get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("CryptoPrivateKey"); }
    }

    public static string LbcPartnerCode
    {
        get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("LbcPartnerCode"); }
    }

    public static string UsscPartnerCode
    {
        get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("UsscPartnerCode"); }
    }
    public static string ServiceCenterCode
    {
        get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("ServiceCenterCode"); }
    }

    public static ControlNumberFormatCollection ControlNumberFormatCollection
    {
        get
        {
            ControlNumberFormatCollection returnValue = new ControlNumberFormatCollection();
            string[] formatCollection = RDFramework.Utility.ConfigurationManager.GetAppSetting("ControlNumberFormatCollection").Split('|');

            if (formatCollection.Length > 0)
            {
                Array.ForEach<string>(formatCollection
                    , delegate(string format)
                    {
                        string[] detailCollection = format.Split('.');
                        ControlNumberFormat number = new ControlNumberFormat();
                        number.Length = int.Parse(detailCollection[2]);
                        number.PartnerCode = detailCollection[0];
                        number.Prefix = detailCollection[1];
                        returnValue.Add(number);
                    });
            }
            else { }

            return returnValue;
        }
    }

    public static PartnerCollection PartnerCollection
    {
        get
        {
            PartnerCollection returnValue = new PartnerCollection();
            string[] formatCollection = RDFramework.Utility.ConfigurationManager.GetAppSetting("RetailAgentsFormatCollection").Split('|');

            if (formatCollection.Length > 0)
            {
                Array.ForEach<string>(formatCollection
                    , delegate(string format)
                    {
                        string[] detailCollection = format.Split('.');
                        Partner number = new Partner();
                        number.Prefix = detailCollection[0];
                        returnValue.Add(number);
                    });
            }
            else { }

            return returnValue;
        }
    }

    public static decimal GetMaxSendAmount(string currencyCode)
    {
        return decimal.Parse(RDFramework.Utility.ConfigurationManager.GetAppSetting(string.Concat("MaxSendAmount_", currencyCode)));
    }

    public static decimal GetMinSendAmount(string currencyCode)
    {
        return decimal.Parse(RDFramework.Utility.ConfigurationManager.GetAppSetting(string.Concat("MinSendAmount_", currencyCode)));
    }

    public static decimal MaxSendAmountNYBPRetailAgents(string currencyCode)
    {
        return decimal.Parse(RDFramework.Utility.ConfigurationManager.GetAppSetting(string.Concat("MaxSendAmountNYBPRetailAgents_", currencyCode)));
    }

    public static decimal GetMaxRemitToAccountAmount(string currencyCode)
    {
        return decimal.Parse(RDFramework.Utility.ConfigurationManager.GetAppSetting(string.Concat("MaxRemitToAccountAmount_", currencyCode)));
    }

    public static decimal GetMinRemitToAccountAmount(string currencyCode)
    {
        return decimal.Parse(RDFramework.Utility.ConfigurationManager.GetAppSetting(string.Concat("MinRemitToAccountAmount_", currencyCode)));
    }

    public static string ApplicationCodeForEums
    {
        get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("ApplicationCodeForEums"); }
    }

    public static string EventSource
    {
        get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("EventSource"); }
    }

    public static string ServerErrorMessage
    {
        get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("ServerErrorMessage"); }
    }

    public static string ApplicationDirectory
    {
        get { return HostingEnvironment.ApplicationPhysicalPath; }
    }

    public static string PeraLinkSiteImages
    {
        get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("PeraLinkSiteImages"); }
    }

    public static string EmailSubject
    {
        get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("EmailSubject"); }
    }

    public static string EmailRecipientTO
    {
        get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("EmailRecipientTO"); }
    }

    public static string EmailRecipientCC
    {
        get { return RDFramework.Utility.ConfigurationManager.GetAppSetting("EmailRecipientCC"); }
    }

#endregion

}