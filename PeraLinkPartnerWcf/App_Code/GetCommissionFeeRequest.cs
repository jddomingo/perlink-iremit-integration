using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetCommissionFeeRequest: BaseRequest
{
	#region Constructor
	public GetCommissionFeeRequest()
	{ }
	#endregion

	#region Fields/Properties
	private string _partnerCode;
	[DataMember]
	public string PartnerCode
	{
		get { return _partnerCode; }
		set { _partnerCode = value; }
	}

	private Int64 _currencyID;
	[DataMember]
	public Int64 CurrencyID
	{
		get { return _currencyID; }
		set { _currencyID = value; }
	}

	private ServiceType _serviceTypeID;
	[DataMember]
	public ServiceType ServiceTypeID
	{
		get { return _serviceTypeID; }
		set { _serviceTypeID = value; }
	}


	private decimal _principalAmount;
	[DataMember]
	public decimal PrincipalAmount
	{
		get { return _principalAmount; }
		set { _principalAmount = value; }
	}

	private decimal _serviceFee;
	[DataMember]
	public decimal ServiceFee
	{
		get { return _serviceFee; }
		set { _serviceFee = value; }
	}

	#endregion

	#region Internal Methods
	internal GetCommissionFeeResult Process()
	{
		GetCommissionFeeResult returnValue = new GetCommissionFeeResult();
        int serviceTypeID = (int)this.ServiceTypeID;

		using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
		{
			#region Partner Record
			PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
			partner.PartnerCode = this.PartnerCode;
			partner = serviceClient.FindPartnerCode(partner);

            if (partner.PartnerID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else { }

            if (partner.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
			#endregion

			#region Service Type
			PeraLinkCoreWcf.PartnerServiceMapping[] partnerServiceMappingCollection = serviceClient.GetPartnerServiceMappingCollection(partner);

			PeraLinkCoreWcf.PartnerServiceMapping partnerServiceMapping = new PeraLinkCoreWcf.PartnerServiceMapping();
			partnerServiceMapping = Array.Find<PeraLinkCoreWcf.PartnerServiceMapping>(partnerServiceMappingCollection
				 , delegate(PeraLinkCoreWcf.PartnerServiceMapping eachPartnerServiceMapping)
				 {
                     return eachPartnerServiceMapping.ServiceType.ServiceTypeID == (int)this.ServiceTypeID;
				 });

            if (partnerServiceMapping == null)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(18);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else { }
			#endregion

			#region Partner Currency
			PeraLinkCoreWcf.PartnerServiceCurrency[] partnerServiceCurrencyCollection = serviceClient.GetPartnerServiceCurrencyCollection(partnerServiceMapping);

			PeraLinkCoreWcf.PartnerServiceCurrency partnerServiceCurrency = Array.Find<PeraLinkCoreWcf.PartnerServiceCurrency>(partnerServiceCurrencyCollection
				, delegate(PeraLinkCoreWcf.PartnerServiceCurrency eachPartnerServiceCurrency)
				{
					return eachPartnerServiceCurrency.Currency.CurrencyID == this.CurrencyID;
				});

            if (partnerServiceCurrency == null)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(26);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else { }

			#endregion

			#region Get Commission Fee
			PeraLinkCoreWcf.GetPartnerCommissionFeeRequest getPartnerCommissionFeeRequest = new PeraLinkCoreWcf.GetPartnerCommissionFeeRequest();

			getPartnerCommissionFeeRequest.PartnerCommissionFee = new PeraLinkCoreWcf.PartnerCommissionFee();
            getPartnerCommissionFeeRequest.PartnerCommissionFee.Amount = this.PrincipalAmount;            
            getPartnerCommissionFeeRequest.PartnerCommissionFee.PartnerServiceCurrencyID = partnerServiceCurrency.PartnerServiceCurrencyID;

			PeraLinkCoreWcf.PartnerCommissionFee matchedCommissionFee = serviceClient.GetCommissionFee(getPartnerCommissionFeeRequest);

            if (matchedCommissionFee.PartnerCommissionFeeID == 0)
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(136);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
            else
            {
                if (matchedCommissionFee.FeeTypeID == (int)FeeType.Fixed)
                {
                    returnValue.CommissionFee = matchedCommissionFee.Fee;
                }
                else
                {
                    if (serviceTypeID == 8)
                    {
                        returnValue.CommissionFee = this.PrincipalAmount * (matchedCommissionFee.Fee / 100);
                    }
                    else
                    {
                        returnValue.CommissionFee = this.ServiceFee * (matchedCommissionFee.Fee / 100);
                    }
                }
            }
			#endregion

		}

		returnValue.ResultStatus = ResultStatus.Successful;

		return returnValue;
	}
	#endregion
}