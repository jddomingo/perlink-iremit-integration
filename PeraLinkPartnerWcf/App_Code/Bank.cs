using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Bank
/// </summary>
public class Bank
{
	#region Constructor
    public Bank() { }
    #endregion

    #region Fields/Properties
    private Int64 _bankID;
    public Int64 BankID { get { return _bankID; } set { _bankID = value; } }

    private string _bankCode;
    public string BankCode { get { return _bankCode; } set { _bankCode = value; } }

	private string _bankName;
	public string BankName { get { return _bankName; } set { _bankName = value; } }

    private string _regExValidator;
    public string RegExValidator { get { return _regExValidator; } set { _regExValidator = value; } }

    private decimal? _minAmount;
    public decimal? MinAmount { get { return _minAmount; } set { _minAmount = value; } }

    private decimal? _maxAmount;
    public decimal? MaxAmount { get { return _maxAmount; } set { _maxAmount = value; } }

    private int? _accountNumberMinLength;
    public int? AccountNumberMinLength { get { return _accountNumberMinLength; } set { _accountNumberMinLength = value; } }

    private int? _accountNumberMaxLength;
    public int? AccountNumberMaxLength { get { return _accountNumberMaxLength; } set { _accountNumberMaxLength = value; } }
    #endregion

	#region Public Methods
	public void Load(PeraLinkCoreWcf.Bank bank)
	{
		if (bank == null)
		{ }
		else
		{
			this.AccountNumberMaxLength = bank.AccountNumberMaxLength;
			this.AccountNumberMinLength = bank.AccountNumberMinLength;
			this.BankCode = bank.BankCode;
			this.BankName = bank.BankName;
			this.BankID = bank.BankID;
			this.MaxAmount = bank.MaxAmount;
			this.MinAmount = bank.MinAmount;
			this.RegExValidator = bank.RegExValidator;
		}
	}
	#endregion
}