
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class TmlWcfClient: IDisposable
{
    #region Constructors
    public TmlWcfClient()
	{
        _serviceClient = new PeraLinkTmlWcf.ServiceClient();
    }
    #endregion

    #region Fields
    private PeraLinkTmlWcf.ServiceClient _serviceClient;
    #endregion

    #region Methods
    public PeraLinkTmlWcf.LookupTransactionResult LookupTransaction(PeraLinkTmlWcf.LookupTransactionRequest lookupTransactionRequest)
    {
        lookupTransactionRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkTmlWcf.LookupTransactionResult lookupTransactionResult = _serviceClient.LookupTransaction(lookupTransactionRequest);

        switch (lookupTransactionResult.ResultCode)
        {
            case PeraLinkTmlWcf.LookupTransactionResultCode.Successful:
            {
                return lookupTransactionResult;
            }
            case PeraLinkTmlWcf.LookupTransactionResultCode.PartnerError:
            case PeraLinkTmlWcf.LookupTransactionResultCode.ServerError:
            case PeraLinkTmlWcf.LookupTransactionResultCode.UnrecognizedResponse:
            case PeraLinkTmlWcf.LookupTransactionResultCode.Unsuccessful:
            {
                throw new RDFramework.ClientException(lookupTransactionResult.MessageToClient, (int)lookupTransactionResult.ResultCode);
            }
            default:
            {
                throw new ArgumentOutOfRangeException(lookupTransactionResult.ResultCode.ToString());
            }
        }
    }

    public PeraLinkTmlWcf.PayoutTransactionResult PayoutTransaction(PeraLinkTmlWcf.PayoutTransactionRequest payoutTransactionRequest)
    {
        payoutTransactionRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkTmlWcf.PayoutTransactionResult payoutTransactionResult = _serviceClient.PayoutTransaction(payoutTransactionRequest);

        switch (payoutTransactionResult.ResultCode)
        {
            case PeraLinkTmlWcf.PayoutTransactionResultCode.Successful:
            {
                return payoutTransactionResult;
            }
            case PeraLinkTmlWcf.PayoutTransactionResultCode.PartnerError:
            case PeraLinkTmlWcf.PayoutTransactionResultCode.ServerError:
            case PeraLinkTmlWcf.PayoutTransactionResultCode.UnrecognizedResponse:
            case PeraLinkTmlWcf.PayoutTransactionResultCode.Unsuccessful:
            {
                throw new RDFramework.ClientException(payoutTransactionResult.MessageToClient, (int)payoutTransactionResult.ResultCode);
            }
            default:
            {
                throw new ArgumentOutOfRangeException(payoutTransactionResult.ResultCode.ToString());
            }
        }
    }

    public PeraLinkTmlWcf.UnlockTransactionResult UnlockTransaction(PeraLinkTmlWcf.UnlockTransactionRequest unlockTransactionRequest)
    {
        unlockTransactionRequest.PassKey = Cryptor.GeneratePassKey();

        PeraLinkTmlWcf.UnlockTransactionResult unlockTransactionResult = _serviceClient.UnlockTransaction(unlockTransactionRequest);

        switch (unlockTransactionResult.ResultCode)
        {
            case PeraLinkTmlWcf.UnlockTransactionResultCode.Successful:
                {
                    return unlockTransactionResult;
                }
            case PeraLinkTmlWcf.UnlockTransactionResultCode.PartnerError:
            case PeraLinkTmlWcf.UnlockTransactionResultCode.ServerError:
            case PeraLinkTmlWcf.UnlockTransactionResultCode.UnrecognizedResponse:
            case PeraLinkTmlWcf.UnlockTransactionResultCode.Unsuccessful:
                {
                    throw new RDFramework.ClientException(unlockTransactionResult.MessageToClient, (int)unlockTransactionResult.ResultCode);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(unlockTransactionResult.ResultCode.ToString());
                }
        }
    }

    public PeraLinkTmlWcf.GetTransactionStatusResult GetTransactionStatus(PeraLinkTmlWcf.GetTransactionStatusRequest getTransactionStatusRequest)
    {
        getTransactionStatusRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkTmlWcf.GetTransactionStatusResult getTransactionStatusResult = _serviceClient.GetTransactionStatus(getTransactionStatusRequest);

        switch (getTransactionStatusResult.ResultCode)
        {
            case PeraLinkTmlWcf.GetTransactionStatusResultCode.Successful:
                {
                    return getTransactionStatusResult;
                }
            case PeraLinkTmlWcf.GetTransactionStatusResultCode.PartnerError:
            case PeraLinkTmlWcf.GetTransactionStatusResultCode.ServerError:
            case PeraLinkTmlWcf.GetTransactionStatusResultCode.UnrecognizedResponse:
            case PeraLinkTmlWcf.GetTransactionStatusResultCode.Unsuccessful:
                {
                    throw new RDFramework.ClientException(getTransactionStatusResult.MessageToClient, (int)getTransactionStatusResult.ResultCode);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(getTransactionStatusResult.ResultCode.ToString());
                }
        }
    }

    public static PeraLinkCoreWcf.RemittanceStatus MapStatus(PeraLinkTmlWcf.TransactionStatus transactionStatus)
    {
        switch (transactionStatus)
        {
            case PeraLinkTmlWcf.TransactionStatus.ForPayout:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 1;
                    returnValue.Description = "Outstanding";

                    return returnValue;
                }
            case PeraLinkTmlWcf.TransactionStatus.PaidOut:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 2;
                    returnValue.Description = "Paid";

                    return returnValue;
                }
            case PeraLinkTmlWcf.TransactionStatus.Cancelled:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 4;
                    returnValue.Description = "Cancelled";

                    return returnValue;
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(transactionStatus.ToString());
                }
        }
    }
    #endregion

    void IDisposable.Dispose()
    {
        bool isClosed = false;

        try
        {
            if (this._serviceClient.State != System.ServiceModel.CommunicationState.Faulted)
            {
                this._serviceClient.Close();
                isClosed = true;
            }
            else
            {
                // Proceed with Abort in finally
            }
        }
        finally
        {
            if (!isClosed)
            {
                this._serviceClient.Abort();
            }
            else
            {
                // Do nothing since state is already closed
            }
        }
    }
}