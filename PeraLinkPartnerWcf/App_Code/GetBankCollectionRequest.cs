using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for GetBankCollectionRequest
/// </summary>
/// 
[DataContract]
public class GetBankCollectionRequest : BaseRequest
{
	#region Constructor
	public GetBankCollectionRequest()
	{
		//

		//
	}
	#endregion

	#region Fields/Properties
	private string _partnerCode;
	[DataMember]
	public string PartnerCode
	{
		get { return _partnerCode; }
		set { _partnerCode = value; }
	}

	private string _token;

	public string Token
	{
		get { return _token; }
		set { _token = value; }
	}

	#region Internal Methods
	internal GetBankCollectionResult Process()
	{
		GetBankCollectionResult returnValue = new GetBankCollectionResult();
		this.AuthenticateRequest();

		using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
		{
			#region Validate Partner Record
			PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
			partner.PartnerCode = this.PartnerCode;
			partner = serviceClient.FindPartnerCode(partner);

			if (partner.PartnerID == 0)
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			else { }

			if (partner.Token == this.Token) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}

			if (partner.Activated) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			#endregion

			#region Get PartnerServiceMapping for the RTA service of the current partner
			PartnerCollection pairedPartners = new PartnerCollection();
			
			PeraLinkCoreWcf.PartnerServiceMapping mapping = new PeraLinkCoreWcf.PartnerServiceMapping();
			mapping.Partner = partner;
			mapping.ServiceType = new PeraLinkCoreWcf.ServiceType();
			mapping.ServiceType.ServiceTypeID = ServiceTypeID.RemitToAccount;

			PeraLinkCoreWcf.GetPartnerServiceMappingRequest getPartnerServiceMappingRequest = new PeraLinkCoreWcf.GetPartnerServiceMappingRequest();
			getPartnerServiceMappingRequest.PartnerServiceMapping = mapping;
			mapping = serviceClient.GetPartnerServiceMapping(getPartnerServiceMappingRequest).PartnerServiceMapping;
			#endregion

			#region Get the paired partners
			PeraLinkCoreWcf.PartnerServicePair[] partnerServicePairCollection = serviceClient.GetPartnerServicePairCollection(mapping);
			Array.ForEach(partnerServicePairCollection,
				delegate(PeraLinkCoreWcf.PartnerServicePair item)
				{
					if (item.PairedPartner.PartnerType.PartnerTypeID == PartnerTypeID.Domestic && item.PairedPartner.Activated)
					{
						Partner convertedPartner = new Partner();
						convertedPartner.Load(item.PairedPartner);
						pairedPartners.Add(convertedPartner);
					}
					else { /* Do nothing */ }
				});
			#endregion

			#region Get the list of paired banks
			PeraLinkCoreWcf.Bank[] rawBankCollection = serviceClient.GetBankCollection(new PeraLinkCoreWcf.GetBankCollectionRequest());

			SortedSet<string> mainBankCodes = new SortedSet<string>(pairedPartners.Select(m => m.PartnerCode));
			
			var results = rawBankCollection.Where(b => mainBankCodes.Contains(b.BankCode.Substring(0, (rawBankCollection[0].BankCode.IndexOf('-')))));
			#endregion

			returnValue.BankCollection = new BankCollection();
			returnValue.BankCollection.AddList(results.ToArray());
			
			returnValue.ResultStatus = ResultStatus.Successful;

			return returnValue;
		}
	}
	#endregion

	#endregion
}