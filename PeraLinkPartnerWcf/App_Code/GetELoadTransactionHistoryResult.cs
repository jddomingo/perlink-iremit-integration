using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetELoadTransactionHistoryResult: ServiceResult
{
    #region Constructor
    public GetELoadTransactionHistoryResult()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion

    #region Fields/Properties
    private ELoadTransactionCollection _eLoadTransactionCollection;

    public ELoadTransactionCollection ELoadTransactionCollection
    {
        get { return _eLoadTransactionCollection; }
        set { _eLoadTransactionCollection = value; }
    }

    private int _totalPageCount;
    [DataMember]
    public int TotalPageCount
    {
        get { return _totalPageCount; }
        set { _totalPageCount = value; }
    }
    #endregion
}