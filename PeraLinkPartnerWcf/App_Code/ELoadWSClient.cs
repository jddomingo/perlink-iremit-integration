using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ELoadWSClient
/// </summary>
public class ELoadWSClient : IDisposable
{
    #region Constructor
    public ELoadWSClient()
	{
        _serviceClient = new PeraLinkELoadGatewayWcf.ServiceClient();
    }
    #endregion

    #region Fields/Properties
    private PeraLinkELoadGatewayWcf.ServiceClient _serviceClient;
    #endregion

    #region Public Methods
    public PeraLinkELoadGatewayWcf.GetTelcoCollectionResult GetTelcoCollection(PeraLinkELoadGatewayWcf.GetTelcoCollectionRequest getTelcoCollectionRequest)
    {
        getTelcoCollectionRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkELoadGatewayWcf.GetTelcoCollectionResult returnValue = _serviceClient.GetTelcoCollection(getTelcoCollectionRequest);
        switch (returnValue.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return returnValue;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(returnValue.Message, returnValue.MessageID, returnValue.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(returnValue.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkELoadGatewayWcf.GetTelcoProductCollectionResult GetTelcoProductCollection(PeraLinkELoadGatewayWcf.GetTelcoProductCollectionRequest getTelcoProductCollectionRequest)
    {
        getTelcoProductCollectionRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkELoadGatewayWcf.GetTelcoProductCollectionResult returnValue = _serviceClient.GetTelcoProductCollection(getTelcoProductCollectionRequest);
        switch (returnValue.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return returnValue;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(returnValue.Message, returnValue.MessageID, returnValue.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(returnValue.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkELoadGatewayWcf.ProcessLoadTransactionResult ProcessLoadTransaction(PeraLinkELoadGatewayWcf.ProcessLoadTransactionRequest processLoadTransactionRequest)
    {
        processLoadTransactionRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkELoadGatewayWcf.ProcessLoadTransactionResult returnValue = _serviceClient.ProcessLoadTransaction(processLoadTransactionRequest);
        switch (returnValue.ResultStatus)
        {
            case ResultStatus.Successful:
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    return returnValue;
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(returnValue.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkELoadGatewayWcf.CheckELoadTransactionResult CheckEloadTransaction(PeraLinkELoadGatewayWcf.CheckELoadTransactionRequest checkEloadTransactionRequest)
    {
        checkEloadTransactionRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkELoadGatewayWcf.CheckELoadTransactionResult returnValue = _serviceClient.CheckELoadTransaction(checkEloadTransactionRequest);
        switch (returnValue.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return returnValue;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(returnValue.Message, returnValue.MessageID, returnValue.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(returnValue.ResultStatus.ToString());
                }
        }    
    }

    public PeraLinkELoadGatewayWcf.GetELoadTransactionResult GetELoadTransaction(PeraLinkELoadGatewayWcf.GetELoadTransactionRequest getELoadTransactionRequest)
    {
        getELoadTransactionRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkELoadGatewayWcf.GetELoadTransactionResult returnValue = _serviceClient.GetELoadTransaction(getELoadTransactionRequest);
        switch (returnValue.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return returnValue;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(returnValue.Message, returnValue.MessageID, returnValue.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(returnValue.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkELoadGatewayWcf.GetELoadStatusResult GetELoadStatus(PeraLinkELoadGatewayWcf.GetELoadStatusRequest getELoadStatusRequest)
    {
        getELoadStatusRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkELoadGatewayWcf.GetELoadStatusResult returnValue = _serviceClient.GetELoadStatus(getELoadStatusRequest);
        switch (returnValue.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return returnValue;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(returnValue.Message, returnValue.MessageID, returnValue.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(returnValue.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkELoadGatewayWcf.GetMobilePrefixCollectionResult GetMobilePrefixCollection(PeraLinkELoadGatewayWcf.GetMobilePrefixCollectionRequest getMobilePrefixCollectionRequest)
    {
        getMobilePrefixCollectionRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkELoadGatewayWcf.GetMobilePrefixCollectionResult returnValue = _serviceClient.GetMobilePrefixCollection(getMobilePrefixCollectionRequest);
        switch (returnValue.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return returnValue;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(returnValue.Message, returnValue.MessageID, returnValue.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(returnValue.ResultStatus.ToString());
                }
        }
    }

    public PeraLinkELoadGatewayWcf.GenerateELoadReferenceNumberResult GenerateELoadReferenceNumber(PeraLinkELoadGatewayWcf.GenerateELoadReferenceNumberRequest generateELoadReferenceNumberRequest)
    {
        generateELoadReferenceNumberRequest.PassKey = Cryptor.GeneratePassKey();
        PeraLinkELoadGatewayWcf.GenerateELoadReferenceNumberResult returnValue = _serviceClient.GenerateELoadReferenceNumber(generateELoadReferenceNumberRequest);
        switch (returnValue.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return returnValue;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(returnValue.Message, returnValue.MessageID, returnValue.LogID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(returnValue.ResultStatus.ToString());
                }
        }
    }

    #endregion

    void IDisposable.Dispose()
    {
        bool isClosed = false;

        try
        {
            if (this._serviceClient.State != System.ServiceModel.CommunicationState.Faulted)
            {
                this._serviceClient.Close();
                isClosed = true;
            }
            else
            {
                // Proceed with Abort in finally
            }
        }
        finally
        {
            if (!isClosed)
            {
                this._serviceClient.Abort();
            }
            else
            {
                // Do nothing since state is already closed
            }
        }
    }
}