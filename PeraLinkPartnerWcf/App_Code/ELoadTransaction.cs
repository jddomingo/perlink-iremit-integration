using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for ELoadTransaction
/// </summary>
public class ELoadTransaction
{
    #region Constructor
    public ELoadTransaction()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion

    #region Fields/Properties
    private string _destination;
    [DataMember]
    public string Destination
    {
        get { return _destination; }
        set { _destination = value; }
    }
    private decimal _amount;
    [DataMember]
    public decimal Amount
    {
        get { return _amount; }
        set { _amount = value; }
    }

    private DateTime _dateCreated;
    [DataMember]
    public DateTime DateCreated
    {
        get { return _dateCreated; }
        set { _dateCreated = value; }
    }

    private Int64 _loadedByAgentID;
    [DataMember]
    public Int64 LoadedByAgentID
    {
        get { return _loadedByAgentID; }
        set { _loadedByAgentID = value; }
    }

    private string _loadedByUserID;
    [DataMember]
    public string LoadedByUserID
    {
        get { return _loadedByUserID; }
        set { _loadedByUserID = value; }
    }
    private string _sessionID;
    [DataMember]
    public string SessionID
    {
        get { return _sessionID; }
        set { _sessionID = value; }
    }

    private string _transactionNumber = string.Empty;
    [DataMember]
    public string TransactionNumber
    {
        get { return _transactionNumber; }
        set { _transactionNumber = value; }
    }

    private string _referenceNumber;
    [DataMember]
    public string ReferenceNumber
    {
        get { return _referenceNumber; }
        set { _referenceNumber = value; }
    }

    private string _productDescription;
    [DataMember]
    public string ProductDescription
    {
        get { return _productDescription; }
        set { _productDescription = value; }
    }

    private string _statusDescription;
    [DataMember]
    public string StatusDescription
    {
        get { return _statusDescription; }
        set { _statusDescription = value; }
    }

    private string _loadedByAgent;
    [DataMember]
    public string LoadedByAgent
    {
        get { return _loadedByAgent; }
        set { _loadedByAgent = value; }
    }

    private string _loadedByTerminal;
    [DataMember]
    public string LoadedByTerminal
    {
        get { return _loadedByTerminal; }
        set { _loadedByTerminal = value; }
    }
    #endregion


    #region Public Methods
    public void Load(PeraLinkELoadGatewayWcf.ELoadTransaction eLoadTransaction)
    {
        this.Amount = eLoadTransaction.Amount;
        this.Destination = eLoadTransaction.Destination;
        this.LoadedByAgentID = eLoadTransaction.LoadedByAgentID;
        this.LoadedByUserID = eLoadTransaction.LoadedByUserID;
        this.ReferenceNumber = eLoadTransaction.ReferenceNumber;
        this.SessionID = eLoadTransaction.SessionID;
        this.TransactionNumber = eLoadTransaction.TransactionNumber;
        this.DateCreated = eLoadTransaction.DateCreated;
    }

    public void Load(PeraLinkCoreWcf.ELoadTransaction eLoadTransaction)
    {
        this.Amount = eLoadTransaction.Amount;
        this.Destination = eLoadTransaction.Destination;
        this.LoadedByAgentID = eLoadTransaction.LoadedByAgentID;
        this.LoadedByUserID = eLoadTransaction.LoadedByUserID;
        this.ReferenceNumber = eLoadTransaction.ReferenceNumber;
        this.SessionID = eLoadTransaction.SessionID;
        this.TransactionNumber = eLoadTransaction.TransactionNumber;
        this.DateCreated = eLoadTransaction.DateCreated;
      
        this.ProductDescription = eLoadTransaction.TelcoProduct.ProductDescription;
        this.StatusDescription = eLoadTransaction.ELoadStatus.InternalStatus;
        this.LoadedByAgent = eLoadTransaction.LoadedByAgent.AgentCode;
        this.LoadedByTerminal = eLoadTransaction.LoadedByTerminal.TerminalCode;
    }
    #endregion
}