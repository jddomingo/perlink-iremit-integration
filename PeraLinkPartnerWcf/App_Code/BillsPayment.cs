﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

/// <summary>
/// Summary description for BillsPayment
/// </summary>
public class BillsPayment
{
    #region Constructor
    public BillsPayment()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion

    #region Fields/Properties
    private Int64 _TUID;
    [DataMember]
    public Int64 TUID
    {
        get { return _TUID; }
        set { _TUID = value; }
    }

    private string _requestParam;
    [DataMember]
    public string RequestParam
    {
        get { return _requestParam; }
        set { _requestParam = value; }
    }

    private string _clientMobileNumber;

    public string ClientMobileNumber
    {
        get { return _clientMobileNumber; }
        set { _clientMobileNumber = value; }
    }
    private Int64 _processedByTerminalID;
    [DataMember]
    public Int64 ProcessedByTerminalID
    {
        get { return _processedByTerminalID; }
        set { _processedByTerminalID = value; }
    }

    private Int64 _processedByAgentID;
    [DataMember]
    public Int64 ProcessedByAgentID
    {
        get { return _processedByAgentID; }
        set { _processedByAgentID = value; }
    }

    private string _processedByUserID;
    [DataMember]
    public string ProcessedByUserID
    {
        get { return _processedByUserID; }
        set { _processedByUserID = value; }
    }

    private string _ReferenceNumber;
    [DataMember]
    public string ReferenceNumber
    {
        get { return _ReferenceNumber; }
        set { _ReferenceNumber = value; }
    }

    private DateTime _DatetimeCreated;
    [DataMember]
    public DateTime DatetimeCreated
    {
        get { return _DatetimeCreated; }
        set { _DatetimeCreated = value; }
    }

    private string _processedByTerminalCode;
    [DataMember]
    public string ProcessedByTerminalCode
    {
        get { return _processedByTerminalCode; }
        set { _processedByTerminalCode = value; }
    }

    private string _processedByAgentCode;
    [DataMember]
    public string ProcessedByAgentCode
    {
        get { return _processedByAgentCode; }
        set { _processedByAgentCode = value; }
    }

    private string _apiTransactionNumber;
    [DataMember]
    public string ApiTransactionNumber
    {
        get { return _apiTransactionNumber; }
        set { _apiTransactionNumber = value; }
    }

    private int _billsPaymentStatusID;
    [DataMember]
    public int BillsPaymentStatusID
    {
        get { return _billsPaymentStatusID; }
        set { _billsPaymentStatusID = value; }
    }

    private string _billsPaymentStatusDescription;
    [DataMember]
    public string BillsPaymentStatusDescription
    {
        get { return _billsPaymentStatusDescription; }
        set { _billsPaymentStatusDescription = value; }
    }

    private string _billerCode;
    [DataMember]
    public string BillerCode
    {
        get { return _billerCode; }
        set { _billerCode = value; }
    }

    private string _billerName;
    [DataMember]
    public string BillerName
    {
        get { return _billerName; }
        set { _billerName = value; }
    }

    private decimal _amount;
    [DataMember]
    public decimal Amount
    {
        get { return _amount; }
        set { _amount = value; }
    }

    private string _clientFirstName;
    [DataMember]
    public string ClientFirstName
    {
        get { return _clientFirstName; }
        set { _clientFirstName = value; }
    }

    private string _clientLastName;
    [DataMember]
    public string ClientLastName
    {
        get { return _clientLastName; }
        set { _clientLastName = value; }
    }

    private string _clientID;
    [DataMember]
    public string ClientID
    {
        get { return _clientID; }
        set { _clientID = value; }
    }

    private decimal _convFee;
    [DataMember]
    public decimal ConvenienceFee
    {
        get { return _convFee; }
        set { _convFee = value; }
    }

    private string _accountNo;
    [DataMember]
    public string AccountNo
    {
        get { return _accountNo; }
        set { _accountNo = value; }
    }
    
    #endregion

    #region Public Methods
    public void Load(PeraLinkCoreWcf.BillsPayment billsPayment)
    {
        this.TUID = billsPayment.TUID;
        this.DatetimeCreated = billsPayment.DateTimeCreated;
        this.ProcessedByAgentID = billsPayment.ProcessedByAgent.AgentID;
        this.ProcessedByTerminalID = billsPayment.ProcessedByTerminal.TerminalID;
        this.ProcessedByUserID = billsPayment.ProcessedByUserID;
        this.ProcessedByTerminalCode = billsPayment.ProcessedByTerminal.TerminalCode;
        this.ProcessedByAgentCode = billsPayment.ProcessedByAgent.AgentCode;
        this.ReferenceNumber = billsPayment.ReferenceNumber;
        this.RequestParam = billsPayment.RequestParam;

        this.ApiTransactionNumber = billsPayment.ApiTransactionNumber;
        this.BillsPaymentStatusID = billsPayment.Status.Status;
        this.BillsPaymentStatusDescription = billsPayment.Status.Description;
        this.ClientMobileNumber = billsPayment.ClientMobileNumber;


        var jsonObject = JObject.Parse(this.RequestParam);

        this.BillerCode = jsonObject.Value<string>("billercode");
        this.Amount = jsonObject.Value<decimal>("amount");
        this.BillerName = jsonObject.Value<string>("billername");
        this.AccountNo = jsonObject.Value<string>("acctno");
        this.ConvenienceFee = jsonObject.Value<decimal>("convfee");
        this.ClientLastName = (string)jsonObject["client"]["lname"];
        this.ClientFirstName = (string)jsonObject["client"]["fname"];
        this.ClientID = (string)jsonObject["client"]["id"];
    }
    #endregion
}