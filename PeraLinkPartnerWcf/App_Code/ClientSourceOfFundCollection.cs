using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ClientSourceOfFundCollection
/// </summary>
public class ClientSourceOfFundCollection: List<ClientSourceOfFund>
{
	#region Constructor
	public ClientSourceOfFundCollection()
	{
		//

		//
	}
	#endregion

	#region Public Methods
	public void AddList(PeraLinkCoreWcf.ClientSourceOfFund[] clientSourceOfFundCollection)
	{
		Array.ForEach<PeraLinkCoreWcf.ClientSourceOfFund>(clientSourceOfFundCollection
			, delegate(PeraLinkCoreWcf.ClientSourceOfFund eachClientSourceOfFund)
			{
				ClientSourceOfFund mappedClientSourceOfFund = new ClientSourceOfFund();
				mappedClientSourceOfFund.Load(eachClientSourceOfFund);
				this.Add(mappedClientSourceOfFund);
			});
	}
	#endregion
}