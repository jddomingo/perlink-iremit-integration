﻿#define PeraLink_Agent_Insurance_Renewal

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

/// <summary>
/// Summary description for PeraLinkInsuranceWcfClient
/// </summary>
public class PeraLinkInsuranceWcfClient : IDisposable
{
    #region Constructor
    public PeraLinkInsuranceWcfClient()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion

    #region Fields/Properties
    private PeraLinkInsuranceWcf.ServiceClient _serviceClient = new PeraLinkInsuranceWcf.ServiceClient();
    #endregion

    #region Public Methods
    public PeraLinkInsuranceWcf.ProcessMiCareResult ProcessMiCare(PeraLinkInsuranceWcf.ProcessMiCareRequest processMiCareRequest)
    {
        processMiCareRequest.PassKey = TokenAuth4.TokenAuth.Generate(ConfigurationManager.AppSettings["CryptoPrivateKey"].ToString());
        PeraLinkInsuranceWcf.ProcessMiCareResult processMiCareResult = _serviceClient.ProcessMiCare(processMiCareRequest);

        switch (processMiCareResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return processMiCareResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(processMiCareResult.Message, processMiCareResult.MessageID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(processMiCareResult.ResultStatus.ToString());
                }
        }
    }

#if PeraLink_Agent_Insurance_Renewal
    public PeraLinkInsuranceWcf.ProcessRenewResult ProcessRenew(PeraLinkInsuranceWcf.ProcessRenewRequest processRenewRequest)
    {
        processRenewRequest.PassKey = TokenAuth4.TokenAuth.Generate(ConfigurationManager.AppSettings["CryptoPrivateKey"].ToString());
        PeraLinkInsuranceWcf.ProcessRenewResult processRenewResult = _serviceClient.ProcessRenew(processRenewRequest);

        switch (processRenewResult.ResultStatus)
        {
            case ResultStatus.Successful:
                {
                    return processRenewResult;
                }
            case ResultStatus.Failed:
            case ResultStatus.Error:
                {
                    throw new RDFramework.ClientException(processRenewResult.Message, processRenewResult.MessageID);
                }
            default:
                {
                    throw new ArgumentOutOfRangeException(processRenewResult.ResultStatus.ToString());
                }
        }
    }
#endif
    void IDisposable.Dispose()
    {
        bool isClosed = false;

        try
        {
            if (this._serviceClient.State != System.ServiceModel.CommunicationState.Faulted)
            {
                this._serviceClient.Close();
                isClosed = true;
            }
            else
            {
                // Proceed with Abort in finally
            }
        }
        finally
        {
            if (!isClosed)
            {
                this._serviceClient.Abort();
            }
            else
            {
                // Do nothing since state is already closed
            }
        }
    }
    #endregion

}
