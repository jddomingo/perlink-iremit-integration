using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MobilePrefix
/// </summary>
public class MobilePrefix
{
    #region Constructor
    public MobilePrefix()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    #endregion

    #region Fields/Properties
    private string _prefix;
    public string Prefix { get { return _prefix; } set { _prefix = value; } }

    private string _refTelcoCode;
    public string RefTelcoCode { get { return _refTelcoCode; } set { _refTelcoCode = value; } }

    private string _underTelco;
    public string UnderTelco { get { return _underTelco; } set { _underTelco = value; } }

    private string _telcoName;

    public string TelcoName
    {
        get { return _telcoName; }
        set { _telcoName = value; }
    }
    #endregion

    #region Public Methods
    public void Load(PeraLinkELoadGatewayWcf.MobilePrefix mobilePrefix)
    {
        this.Prefix = mobilePrefix.Prefix;
        this.RefTelcoCode = mobilePrefix.RefTelcoCode;
        this.UnderTelco = mobilePrefix.UnderTelco;
        this.TelcoName = mobilePrefix.Telco.TelcoName;

    }
    #endregion
}