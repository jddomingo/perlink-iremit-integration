﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TokenAuth4;

public class UsscWcfClient
{
    #region Constructor
    public UsscWcfClient()
	{
        _serviceClient = new PeraLinkUsscWcf.ServiceClient();
    }
    #endregion

    #region Fields/Properties
    private PeraLinkUsscWcf.ServiceClient _serviceClient;
    #endregion

    #region Public Methods
    public PeraLinkCoreWcf.DomesticRemittance FindRemittance(string controlNumber, string branchCode, string usscReferenceNumber)
    {
        PeraLinkCoreWcf.DomesticRemittance returnValue = new PeraLinkCoreWcf.DomesticRemittance();

        PeraLinkUsscWcf.FindTransactionRequest findTransactionRequest = new PeraLinkUsscWcf.FindTransactionRequest();
        findTransactionRequest.BranchCode = branchCode;
        findTransactionRequest.ControlNumber = controlNumber;
        findTransactionRequest.UsscReferenceNumber = usscReferenceNumber;
        findTransactionRequest.RequestToken = TokenAuth.Generate(SystemSetting.CryptoPrivateKey);

        PeraLinkUsscWcf.FindTransactionResult findTransactionResult = _serviceClient.Find(findTransactionRequest);

        switch (findTransactionResult.ResultStatus)
        {
             case ResultStatus.Successful:
                returnValue.Beneficiary = new PeraLinkCoreWcf.Beneficiary();
                returnValue.Beneficiary.FirstName = findTransactionResult.BeneficiaryFirstName;
                returnValue.Beneficiary.LastName = findTransactionResult.BeneficiaryLastName;
                returnValue.Beneficiary.BirthDate = RDFramework.Utility.Validation.MinimumSqlDateTime;
                returnValue.DateTimeSent = findTransactionResult.TransactionDate;
                returnValue.DateTimeUpdated = findTransactionResult.PayoutDate;
                returnValue.ControlNumber = controlNumber;
                returnValue.PrincipalAmount = findTransactionResult.Amount;
                returnValue.ServiceFee = findTransactionResult.ServiceCharge;
                returnValue.RemittanceStatus = MapStatus(findTransactionResult.TransactionStatus.Value);
                returnValue.SendCurrency = new PeraLinkCoreWcf.Currency();
                returnValue.SendCurrency.CurrencyID = 6;
                returnValue.SendCurrency.Code = "PHP";
                returnValue.SenderClient = new PeraLinkCoreWcf.Client();
                returnValue.SenderClient.FirstName = findTransactionResult.SenderFirstName;
                returnValue.SenderClient.LastName = findTransactionResult.SenderLastName;
                returnValue.SenderClient.BirthDate = RDFramework.Utility.Validation.MinimumSqlDateTime;
                returnValue.SentByUserID =findTransactionResult.SendingAgent;
                returnValue.SentByAgent = new PeraLinkCoreWcf.Agent();
                returnValue.SentByAgent.AgentCode = findTransactionResult.SendingAgent;
                returnValue.DateTimeSent = findTransactionResult.TransactionDate;
                return returnValue;
            case ResultStatus.Error:
            case ResultStatus.Failed:
                throw new RDFramework.ClientException(findTransactionResult.Message, (int)findTransactionResult.MessageID);
            default:
                throw new ArgumentOutOfRangeException(findTransactionResult.Message);
        }
    }

    public PeraLinkUsscWcf.PayoutRemittanceResult PayoutRemittance(string controlNumber, int payoutID, string idNumber, string usscReferenceNumber)
    {
        PeraLinkUsscWcf.PayoutRemittanceResult returnValue = new PeraLinkUsscWcf.PayoutRemittanceResult();
        PeraLinkUsscWcf.PayoutRemittanceRequest payoutRemittanceRequest = new PeraLinkUsscWcf.PayoutRemittanceRequest();
        payoutRemittanceRequest.ControlNumber = controlNumber;
        payoutRemittanceRequest.UsscReferenceNumber = usscReferenceNumber;
        payoutRemittanceRequest.PayoutOption = new PeraLinkUsscWcf.PayoutOption();
        payoutRemittanceRequest.PayoutOption.IdNumber = idNumber;
        payoutRemittanceRequest.PayoutOption.IdType = payoutID;

        string defaultString = "..";
        string defaultCode = "0000";

        payoutRemittanceRequest.PayoutOption.DateOfBirth = defaultString;
        payoutRemittanceRequest.PayoutOption.Nationality = defaultString;
        payoutRemittanceRequest.PayoutOption.Occupation = defaultString;
        payoutRemittanceRequest.PayoutOption.PermanentAddress = defaultString;
        payoutRemittanceRequest.PayoutOption.PermanentCity = defaultString;
        payoutRemittanceRequest.PayoutOption.PermanentProvince = defaultString;
        payoutRemittanceRequest.PayoutOption.PermanentZipCode = defaultCode;
        payoutRemittanceRequest.PayoutOption.PresentAddress = defaultString;
        payoutRemittanceRequest.PayoutOption.PresentCity = defaultString;
        payoutRemittanceRequest.PayoutOption.PresentProvince = defaultString;
        payoutRemittanceRequest.PayoutOption.PresentZipCode = defaultCode;
        payoutRemittanceRequest.PayoutOption.SourceOfFunds = defaultString;
        
        payoutRemittanceRequest.RequestToken = TokenAuth.Generate(SystemSetting.CryptoPrivateKey);

        returnValue = _serviceClient.Payout(payoutRemittanceRequest);


        switch (returnValue.ResultStatus)
        {
            case ResultStatus.Successful:
                return returnValue;
            case ResultStatus.Error:
            case ResultStatus.Failed:
                RDFramework.Utility.EventLog.SaveError(string.Format("LBC: {0}|{1}", controlNumber, returnValue.Key));
                throw new RDFramework.ClientException(returnValue.Message, (int)returnValue.MessageID);
            default:
                RDFramework.Utility.EventLog.SaveError(string.Format("LBC: {0}|{1}", controlNumber, returnValue.Key));
                throw new ArgumentOutOfRangeException(returnValue.Message);
        }
    }

    public static PeraLinkCoreWcf.RemittanceStatus MapStatus(PeraLinkUsscWcf.TransactionStatus partnerStatus)
    {
        switch (partnerStatus)
        {
            case PeraLinkUsscWcf.TransactionStatus.Available:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 1;
                    returnValue.Description = "Outstanding";

                    return returnValue;
                }
            case PeraLinkUsscWcf.TransactionStatus.Paid:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 2;
                    returnValue.Description = "Paid";

                    return returnValue;
                }
            case PeraLinkUsscWcf.TransactionStatus.Cancelled:
                {
                    PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
                    returnValue.RemittanceStatusID = 4;
                    returnValue.Description = "Cancelled";

                    return returnValue;
                }
            case PeraLinkUsscWcf.TransactionStatus.Expired:
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(97);
                    throw new RDFramework.ClientException(string.Format(message.Description, partnerStatus.ToString()), message.SystemMessageID);
                }
            default:
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(86);
                    throw new RDFramework.ClientException(string.Format(message.Description, partnerStatus.ToString()), message.SystemMessageID);
                }
        }
    }
    #endregion
}