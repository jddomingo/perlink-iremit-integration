using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ClientSourceOfFundCollection
/// </summary>
public class ClientSourceOfFund
{
	#region Constructor
	public ClientSourceOfFund()
	{
		//

		//
	}
	#endregion

	#region Fields/Properties
	private Int64 _sourceOfFundID;

	public Int64 SourceOfFundID
	{
		get { return _sourceOfFundID; }
		set { _sourceOfFundID = value; }
	}

	private string _sourceOfFund;

	public string SourceOfFund
	{
		get { return _sourceOfFund; }
		set { _sourceOfFund = value; }
	}
	#endregion

	#region Public Methods
	public void Load(PeraLinkCoreWcf.ClientSourceOfFund clientSourceOfFund)
	{
		if (clientSourceOfFund == null)
		{ }
		else
		{
			this.SourceOfFund = clientSourceOfFund.SourceOfFund;
			this.SourceOfFundID = clientSourceOfFund.SourceOfFundID;
		}
	}
	#endregion
}