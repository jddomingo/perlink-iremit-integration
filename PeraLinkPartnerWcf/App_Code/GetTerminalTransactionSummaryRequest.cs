using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetTerminalTransactionSummaryRequest: BaseRequest
{
	#region Constructor
	public GetTerminalTransactionSummaryRequest()
	{ }
	#endregion

	#region Fields/Properties
	private const int _pageSize = 10;

	private int _pageNumber;
	[DataMember]
	public int PageNumber
	{
		get { return _pageNumber; }
		set { _pageNumber = value; }
	}

	private string _partnerCode;
	[DataMember]
	public string PartnerCode
	{
		get { return _partnerCode; }
		set { _partnerCode = value; }
	}

	private string _token;
	[DataMember]
	public string Token
	{
		get { return _token; }
		set { _token = value; }
	}

	private string _agentCode;
	[DataMember]
	public string AgentCode
	{
		get { return _agentCode; }
		set { _agentCode = value; }
	}

	private DateTime _startDate;
	[DataMember]
	public DateTime StartDate
	{
		get { return _startDate; }
		set { _startDate = value; }
	}

	private DateTime _endDate;
	[DataMember]
	public DateTime EndDate
	{
		get { return _endDate; }
		set { _endDate = value; }
	}
	#endregion

	#region Internal Methods
	internal GetTerminalTransactionSummaryResult Process()
	{
		GetTerminalTransactionSummaryResult returnValue = new GetTerminalTransactionSummaryResult();

		// Important: This function validates the request credential
		this.AuthenticateRequest();

		using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
		{
			#region Validate Partner Record
			PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
			partner.PartnerCode = this.PartnerCode;
			partner = serviceClient.FindPartnerCode(partner);

			if (partner.PartnerID == 0)
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			else { }

			if (partner.Token == this.Token) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}

			if (partner.Activated) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			#endregion

			#region Validate Agent Record
			PeraLinkCoreWcf.Agent agent = new PeraLinkCoreWcf.Agent();
			if (this.AgentCode != null)
			{
				agent.AgentCode = this.AgentCode;
				agent.Partner = partner;
				agent = serviceClient.FindAgentCodeOfPartner(agent);

				if (agent.AgentID == 0)
				{
					PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
					throw new RDFramework.ClientException(message.Content, message.MessageID);
				}
				else
				{
					// Proceed
				}

				if (agent.Partner.PartnerID == partner.PartnerID)
				{ }
				else
				{
					PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(43);
					throw new RDFramework.ClientException(message.Content, message.MessageID);
				}

				if (agent.Activated) { }
				else
				{
					PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(46);
					throw new RDFramework.ClientException(message.Content, message.MessageID);
				}
			}
			else
			{
				agent.Partner = partner;
			}
			#endregion

			PeraLinkCoreWcf.GetTerminalTransactionSummaryRequest ttsRequest = new PeraLinkCoreWcf.GetTerminalTransactionSummaryRequest();
			ttsRequest.Agent = agent;
			ttsRequest.EndDate = this.EndDate;
			ttsRequest.EndRowNumber = (this.PageNumber * _pageSize);
			ttsRequest.StartDate = this.StartDate;
			ttsRequest.StartRowNumber = (ttsRequest.EndRowNumber - _pageSize) + 1;

			PeraLinkCoreWcf.GetTerminalTransactionSummaryResult ttsResult = serviceClient.GetTerminalTransactionSummary(ttsRequest);
			returnValue.TerminalTransactionSummaryCollection = new TerminalTransactionSummaryCollection();
			returnValue.TerminalTransactionSummaryCollection.AddList(ttsResult.TerminalTransactionSummaryCollection);
			returnValue.TotalPageCount = ((ttsResult.TotalRecordCount % _pageSize) == 0) ? (ttsResult.TotalRecordCount / _pageSize) : (ttsResult.TotalRecordCount / _pageSize) + 1;
		}

		returnValue.ResultStatus = ResultStatus.Successful;

		return returnValue;
	}
	#endregion
}