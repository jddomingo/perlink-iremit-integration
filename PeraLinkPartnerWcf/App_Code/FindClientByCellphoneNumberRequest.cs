﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for FindClientByCellphoneNumber
/// </summary>
public class FindClientByCellphoneNumberRequest:BaseRequest
{
    #region Consructor
    public FindClientByCellphoneNumberRequest()
	{

    }
    #endregion

    #region Fields/Properties

    private string _cellphoneNumber;
    [DataMember]
    public string CellphoneNumber
    { 
        get { return _cellphoneNumber; }
        set { _cellphoneNumber = value; }
    }

    private string _partnerCode;
    [DataMember]
    public string PartnerCode
    {
        get { return _partnerCode; }
        set { _partnerCode = value; }
    }
     #endregion

    #region Internal Methods
    internal FindClientByCellphoneNumberResult Process()
    {
        FindClientByCellphoneNumberResult returnValue = new FindClientByCellphoneNumberResult();

        this.AuthenticateRequest();

        PeraLinkCoreWcf.Client searchClient = new PeraLinkCoreWcf.Client();
        searchClient.CellphoneNumber = this.CellphoneNumber;



        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
            #region Validate Partner Record
            PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
            partner.PartnerCode = this.PartnerCode;
            partner = serviceClient.FindPartnerCode(partner);

            if (partner.PartnerID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else { }

            if (partner.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            #endregion

            returnValue.ClientCollection  = new ClientCollection();
            returnValue.ClientCollection.AddList(serviceClient.FindClientMobile(searchClient));
        }


        returnValue.ResultStatus = ResultStatus.Successful;
        return returnValue;
    }
    #endregion


   
}