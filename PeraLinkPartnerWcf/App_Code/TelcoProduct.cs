using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TelcoProduct
/// </summary>
public class TelcoProduct
{
    #region Constructor
    public TelcoProduct()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion

    #region Fields/Properties
    private string _productCode;
    public string ProductCode
    {
        get { return _productCode; }
        set { _productCode = value; }
    }

    private string _productDescription;
    public string ProductDescription
    {
        get { return _productDescription; }
        set { _productDescription = value; }
    }

    private decimal _minAmount;
    public decimal MinAmount
    {
        get { return _minAmount; }
        set { _minAmount = value; }
    }

    private decimal _maxAmount;
    public decimal MaxAmount
    {
        get { return _maxAmount; }
        set { _maxAmount = value; }
    }

    private decimal _reqAmount;
    public decimal ReqAmount
    {
        get { return _reqAmount; }
        set { _reqAmount = value; }
    }
    #endregion


    #region Public Methods
    public void Load(PeraLinkELoadGatewayWcf.TelcoProduct telcoProduct)
    {
        this.MaxAmount = telcoProduct.MaxAmount;
        this.MinAmount = telcoProduct.MinAmount;
        this.ProductCode = telcoProduct.ProductCode;
        this.ReqAmount = telcoProduct.ReqAmount;
        this.ProductDescription = telcoProduct.ProductDescription;
    }
    #endregion
}