﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for GetBillsPaymentTransactionHistoryResult
/// </summary>
public class GetBillsPaymentTransactionHistoryResult:ServiceResult
{
    #region Constructor
    public GetBillsPaymentTransactionHistoryResult()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion

    #region Fields/Properties
    private BillsPaymentCollection _billsPaymentCollection;
    [DataMember]
    public BillsPaymentCollection BillsPaymentCollection
    {
        get { return _billsPaymentCollection; }
        set { _billsPaymentCollection = value; }
    }

    private int _totalPageCount;
    [DataMember]
    public int TotalPageCount
    {
        get { return _totalPageCount; }
        set { _totalPageCount = value; }
    }
    #endregion

}
