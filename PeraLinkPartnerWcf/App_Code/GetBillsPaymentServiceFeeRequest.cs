﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for GetBillsPaymentServiceFee
/// </summary>
public class GetBillsPaymentServiceFeeRequest:BaseRequest
{
    #region Constructor
    public GetBillsPaymentServiceFeeRequest()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion

    #region Fields/Properties
    private string _partnerCode;
    [DataMember]
    public string PartnerCode
    {
        get { return _partnerCode; }
        set { _partnerCode = value; }
    }

    private string _billerCode;
    [DataMember]
    public string BillerCode
    {
        get { return _billerCode; }
        set { _billerCode = value; }
    }

    private string _token;
    [DataMember]
    public string Token
    {
        get { return _token; }
        set { _token = value; }
    }

    private string _agentCode;
    [DataMember]
    public string AgentCode
    {
        get { return _agentCode; }
        set { _agentCode = value; }
    }

    private decimal _principalAmount;
    [DataMember]
    public decimal PrincipalAmount
    {
        get { return _principalAmount; }
        set { _principalAmount = value; }
    }

    private Int64 _currencyID;
    [DataMember]
    public Int64 CurrencyID
    {
        get { return _currencyID; }
        set { _currencyID = value; }
    }
    #endregion

    #region Internal Method
    internal GetBillsPaymentServiceFeeResult Process()
    {
        GetBillsPaymentServiceFeeResult returnValue = new GetBillsPaymentServiceFeeResult();

        // Important: This function validates the request credential
        this.AuthenticateRequest();
        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
            #region Validate Partner Record
            PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
            partner.PartnerCode = this.PartnerCode;
            partner = serviceClient.FindPartnerCode(partner);

            if (partner.PartnerID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else { }

            if (partner.Token == this.Token) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (partner.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            #endregion

            #region Validate Agent Record
            PeraLinkCoreWcf.Agent agent = new PeraLinkCoreWcf.Agent();
            agent.AgentCode = this.AgentCode;
            agent.Partner = partner;
            agent = serviceClient.FindAgentCodeOfPartner(agent);

            if (agent.AgentID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }

            if (agent.Partner.PartnerID == partner.PartnerID)
            { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(43);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (agent.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(46);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            #endregion

            #region Validate Partner's Access To BillsPayment Service
            PeraLinkCoreWcf.PartnerServiceMapping[] partnerServiceMappingCollection = serviceClient.GetPartnerServiceMappingCollection(partner);

            PeraLinkCoreWcf.PartnerServiceMapping partnerServiceMapping = Array.Find<PeraLinkCoreWcf.PartnerServiceMapping>(partnerServiceMappingCollection
                , delegate(PeraLinkCoreWcf.PartnerServiceMapping eachPartnerServiceMapping)
                {
                    return eachPartnerServiceMapping.ServiceType.ServiceTypeID == ServiceTypeID.BillsPayment;
                });


            if (partnerServiceMapping == null)
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(128);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
            else
            {
                // Proceed
            }
            #endregion

            #region Validate Partner's Access to Biller
            PeraLinkCoreWcf.PartnerServicePair[] partnerServicePairCollection = serviceClient.GetPartnerServicePairCollection(partnerServiceMapping);

            PeraLinkCoreWcf.PartnerServicePair pairing = Array.Find<PeraLinkCoreWcf.PartnerServicePair>(partnerServicePairCollection
                    , delegate(PeraLinkCoreWcf.PartnerServicePair eachPartnerServicePair)
                    {
                        if (eachPartnerServicePair.PairedPartner == null)
                        {
                            PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(131);
                            throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                        }
                        else
                        {
                            return eachPartnerServicePair.PairedPartner.PartnerCode == this.BillerCode;
                        }
                    });

            if (pairing == null)
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(131);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
            else
            {
                //Proceed
            }

            if (pairing.PairedPartner.Activated)
            { }
            else
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(132);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
            #endregion

            #region Validate Partner Bills Pay Currency
            PeraLinkCoreWcf.PartnerServiceCurrency[] partnerServiceCurrencyCollection = serviceClient.GetPartnerServiceCurrencyCollection(partnerServiceMapping);

            PeraLinkCoreWcf.PartnerServiceCurrency sendCurrency = Array.Find<PeraLinkCoreWcf.PartnerServiceCurrency>(partnerServiceCurrencyCollection
                , delegate(PeraLinkCoreWcf.PartnerServiceCurrency eachPartnerServiceCurrency)
                {
                    return eachPartnerServiceCurrency.Currency.CurrencyID == this.CurrencyID;
                });

            if (sendCurrency == null)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(4);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }
            #endregion

            #region Get Service Fee
            PeraLinkCoreWcf.GetPartnerPairingServiceFeeRequest feeRequest = new PeraLinkCoreWcf.GetPartnerPairingServiceFeeRequest();
            feeRequest.PartnerPairingServiceFee = new PeraLinkCoreWcf.PartnerPairingServiceFee();
            feeRequest.PartnerPairingServiceFee.Amount = this.PrincipalAmount;
            feeRequest.PartnerPairingServiceFee.PartnerServicePair = pairing;

            PeraLinkCoreWcf.GetPartnerPairingServiceFeeResult feeResult = new PeraLinkCoreWcf.GetPartnerPairingServiceFeeResult();
            feeResult = serviceClient.GetPartnerPairingServiceFee(feeRequest);

            if (feeResult.PartnerPairingServiceFee.FeeType.FeeTypeID == FeeTypeID.Fixed)
            {
                returnValue.ServiceFee = feeResult.PartnerPairingServiceFee.Fee;
            }
            else
            {
                returnValue.ServiceFee = this.PrincipalAmount * (feeResult.PartnerPairingServiceFee.Fee / 100);
            }

            #endregion
        }

        returnValue.ResultStatus = ResultStatus.Successful;

        return returnValue;
    }
    #endregion
}
