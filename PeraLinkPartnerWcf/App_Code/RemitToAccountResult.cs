using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for RemitToAccountResult
/// </summary>
[DataContract]
public class RemitToAccountResult : ServiceResult
{
	#region Constructor
	public RemitToAccountResult()
	{
		//

		//
	}
	#endregion

	#region Fields/Properties
	private string _referenceNumber;

	[DataMember]
	public string ReferenceNumber
	{
		get { return _referenceNumber; }
		set { _referenceNumber = value; }
	}
	private decimal _serviceFee;

	[DataMember]
	public decimal ServiceFee
	{
		get { return _serviceFee; }
		set { _serviceFee = value; }
	}
	#endregion
}