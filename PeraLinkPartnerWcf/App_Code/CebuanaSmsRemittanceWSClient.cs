using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CebuanaSmsRemittanceWSClient
/// </summary>
public class CebuanaSmsRemittanceWSClient : IDisposable
{
	#region Constructor
	public CebuanaSmsRemittanceWSClient()
	{
		_serviceSoapClient = new CebuanaSmsRemittanceWS.CebuanaSmsRemittanceWSSoapClient();
	}
	#endregion

	#region Fields/Properties
	CebuanaSmsRemittanceWS.CebuanaSmsRemittanceWSSoapClient _serviceSoapClient;
	#endregion

	#region Public Methods
	public CebuanaSmsRemittanceWS.ConfirmEMoneySendResponse ConfirmEMoneySend(CebuanaSmsRemittanceWS.ConfirmEMoneySendRequest requestValue)
	{
		CebuanaSmsRemittanceWS.ConfirmEMoneySendResponse responseValue = new CebuanaSmsRemittanceWS.ConfirmEMoneySendResponse();
		return responseValue = _serviceSoapClient.ConfirmEMoneySend(requestValue);
	}
	#endregion

	void IDisposable.Dispose()
	{
		bool isClosed = false;

		try
		{
			if (this._serviceSoapClient.State != System.ServiceModel.CommunicationState.Faulted)
			{
				this._serviceSoapClient.Close();
				isClosed = true;
			}
			else
			{
				// Proceed with Abort in finally
			}
		}
		finally
		{
			if (!isClosed)
			{
				this._serviceSoapClient.Abort();
			}
			else
			{
				// Do nothing since state is already closed
			}
		}
	}
}