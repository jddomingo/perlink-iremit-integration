using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MoneyGramWcfClient
/// </summary>
public class MoneyGramWcfClient: IDisposable
{
	#region Constructor
	public MoneyGramWcfClient()
	{
		_serviceClient = new MoneyGramWcf.ServiceClient();
	}
	#endregion

	#region Fields/Properties
	private MoneyGramWcf.ServiceClient _serviceClient;
	#endregion

	#region Public Methods
	public MoneyGramWcf.SearchTransactionResult SearchTransaction(MoneyGramWcf.SearchTransactionRequest searchTransactionRequest)
	{
		MoneyGramWcf.SearchTransactionResult searchTransactionResult = _serviceClient.SearchTransaction(searchTransactionRequest);

		switch (searchTransactionResult.ResultCode)
		{
			case MoneyGramWcf.LookupTransactionResultCode.Successful:
				{
					return searchTransactionResult;
				}
			case MoneyGramWcf.LookupTransactionResultCode.PartnerError:
			case MoneyGramWcf.LookupTransactionResultCode.ServerError:
			case MoneyGramWcf.LookupTransactionResultCode.UnrecognizedResponse:
			case MoneyGramWcf.LookupTransactionResultCode.Unsuccessful:
				{
					throw new RDFramework.ClientException(searchTransactionResult.MessageToClient, (int)searchTransactionResult.ResultCode);
				}
			default:
				{
					throw new ArgumentOutOfRangeException(searchTransactionResult.ResultCode.ToString());
				}
		}
	}

	public MoneyGramWcf.PayoutTransactionResult PayoutTransaction(MoneyGramWcf.PayoutTransactionRequest payoutTransactionRequest)
	{
		MoneyGramWcf.PayoutTransactionResult payoutTransactionResult = _serviceClient.PayoutTransaction(payoutTransactionRequest);

		switch (payoutTransactionResult.ResultCode)
		{
			case MoneyGramWcf.PayoutTransactionResultCode.Successful:
				{
					return payoutTransactionResult;
				}
			case MoneyGramWcf.PayoutTransactionResultCode.PartnerError:
			case MoneyGramWcf.PayoutTransactionResultCode.ServerError:
			case MoneyGramWcf.PayoutTransactionResultCode.UnrecognizedResponse:
			case MoneyGramWcf.PayoutTransactionResultCode.Unsuccessful:
				{
					throw new RDFramework.ClientException(payoutTransactionResult.Message, (int)payoutTransactionResult.ResultCode);
				}
			default:
				{
					throw new ArgumentOutOfRangeException(payoutTransactionResult.ResultCode.ToString());
				}
		}
	}

	public static PeraLinkCoreWcf.RemittanceStatus MapStatus(MoneyGramWcf.TransactionStatus transactionStatus)
	{
		switch (transactionStatus)
		{
			case MoneyGramWcf.TransactionStatus.ForPayout:
				{
					PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
					returnValue.RemittanceStatusID = 1;
					returnValue.Description = "Outstanding";

					return returnValue;
				}
			case MoneyGramWcf.TransactionStatus.PaidOut:
				{
					PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
					returnValue.RemittanceStatusID = 2;
					returnValue.Description = "Paid";

					return returnValue;
				}
			case MoneyGramWcf.TransactionStatus.Cancelled:
				{
					PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
					returnValue.RemittanceStatusID = 4;
					returnValue.Description = "Cancelled";

					return returnValue;
				}
			case MoneyGramWcf.TransactionStatus.Refunded:
				{
					PeraLinkCoreWcf.RemittanceStatus returnValue = new PeraLinkCoreWcf.RemittanceStatus();
					returnValue.RemittanceStatusID = 3;
					returnValue.Description = "Refunded";

					return returnValue;
				}
			default:
				{
					PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(86);
					throw new RDFramework.ClientException(string.Format(message.Description, transactionStatus.ToString()), message.SystemMessageID);
				}
		}
	}
	#endregion

	void IDisposable.Dispose()
	{
		bool isClosed = false;

		try
		{
			if (this._serviceClient.State != System.ServiceModel.CommunicationState.Faulted)
			{
				this._serviceClient.Close();
				isClosed = true;
			}
			else
			{
				// Proceed with Abort in finally
			}
		}
		finally
		{
			if (!isClosed)
			{
				this._serviceClient.Abort();
			}
			else
			{
				// Do nothing since state is already closed
			}
		}
	}
}