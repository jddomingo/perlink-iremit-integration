using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for Agent
/// </summary>
/// 

public class Agent
{
    #region Constructor
    public Agent()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private Int64 _agentID;

    public Int64 AgentID
    {
        get { return _agentID; }
        set { _agentID = value; }
    }
    private string _agentCode;

    public string AgentCode
    {
        get { return _agentCode; }
        set { _agentCode = value; }
    }

    private string _name;

    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }

	private bool _activated;

	public bool Activated
	{
		get { return _activated; }
		set { _activated = value; }
	}
    #endregion

    #region Public Methods
    public void Load(PeraLinkCoreWcf.Agent agent)
    {
        this.AgentCode = agent.AgentCode;
        this.AgentID = agent.AgentID;
        this.Name = agent.Name;
		this.Activated = agent.Activated;
    }
    #endregion
}