#define CONVERTION_TO_BUSINESS_ACCOUNT
#define ELOAD_COMMISSION_FEE

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using AppCryptor;

/// <summary>
/// Summary description for ProcessLoadTransactionRequest
/// </summary>
public class ProcessLoadTransactionRequest : BaseRequest
{

    #region Constructor
    public ProcessLoadTransactionRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion

    #region Fields/Properties
    private ELoadTransaction _eLoadTransaction;
    [DataMember]
    public ELoadTransaction ELoadTransaction
    {
        get { return _eLoadTransaction; }
        set { _eLoadTransaction = value; }
    }

    private ELoadStatus _eLoadStatus;
    [DataMember]
    public ELoadStatus ELoadStatus
    {
        get { return _eLoadStatus; }
        set { _eLoadStatus = value; }
    }

    private TelcoProduct _telcoProduct;
    [DataMember]
    public TelcoProduct TelcoProduct
    {
        get { return _telcoProduct; }
        set { _telcoProduct = value; }
    }

    private Telco _telco;
    [DataMember]
    public Telco Telco
    {
        get { return _telco; }
        set { _telco = value; }
    }

    private string _partnerCode;
    [DataMember]
    public string PartnerCode
    {
        get { return _partnerCode; }
        set { _partnerCode = value; }
    }

    private string _token;
    [DataMember]
    public string Token
    {
        get { return _token; }
        set { _token = value; }
    }

    private string _agentCode;
    [DataMember]
    public string AgentCode
    {
        get { return _agentCode; }
        set { _agentCode = value; }
    }

    private string _terminalCode;
    [DataMember]
    public string TerminalCode
    {
        get { return _terminalCode; }
        set { _terminalCode = value; }
    }

    private string _userID;
    [DataMember]
    public string UserID
    {
        get { return _userID; }
        set { _userID = value; }
    }

    private string _pin;
    [DataMember]
    public string Pin
    {
        get { return _pin; }
        set { _pin = value; }
    }
    #endregion

    #region Internal Methods
    public ProcessLoadTransactionResult Process()
    {
        ProcessLoadTransactionResult returnValue = new ProcessLoadTransactionResult();
        ELoadWSClient eLoadServiceClient = new ELoadWSClient();
        bool isEmoney = false;
        decimal commissionFee = 0;
        // Important: This function validates the request credential
        this.AuthenticateRequest();

        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
            #region Validate Partner Record
            PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
            partner.PartnerCode = this.PartnerCode;
            partner = serviceClient.FindPartnerCode(partner);

            if (partner.PartnerID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else { }

            if (partner.Token == this.Token) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (partner.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (partner.SourceOfFundID == SystemSetting.EMoneySourceOfFundID)
            {
                isEmoney = true;
            }
            #endregion

            #region Validate Agent Record
            PeraLinkCoreWcf.Agent agent = new PeraLinkCoreWcf.Agent();
            agent.AgentCode = this.AgentCode;
            agent.Partner = partner;
            agent = serviceClient.FindAgentCodeOfPartner(agent);

            if (agent.AgentID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }

            if (agent.Partner.PartnerID == partner.PartnerID)
            { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(43);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (agent.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(46);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            #endregion

            #region Validate Terminal Record
            PeraLinkCoreWcf.Terminal terminal = new PeraLinkCoreWcf.Terminal();

            if (serviceClient.GetAgentTerminalCollection(agent).Length > 0)
            {
                terminal.TerminalCode = this.TerminalCode;
                terminal.Agent = agent;
                terminal.Agent.Partner = partner;
                terminal = serviceClient.FindTerminal(terminal);

                if (terminal.TerminalID == 0)
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(104);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
                else
                {
                    // Proceed
                }

                if (terminal.Agent.AgentID == agent.AgentID)
                { }
                else
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(105);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }

                if (terminal.Activated) { }
                else
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(106);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
            }
            else
            {
                //do nothing
            }
            #endregion

            #region Validate Partner's Access To ELoad Service
            PeraLinkCoreWcf.PartnerServiceMapping[] partnerServiceMappingCollection = serviceClient.GetPartnerServiceMappingCollection(partner);

            PeraLinkCoreWcf.PartnerServiceMapping partnerServiceMapping = Array.Find<PeraLinkCoreWcf.PartnerServiceMapping>(partnerServiceMappingCollection
                , delegate(PeraLinkCoreWcf.PartnerServiceMapping eachPartnerServiceMapping)
                {
                    return eachPartnerServiceMapping.ServiceType.ServiceTypeID == ServiceTypeID.ELoad;
                });


            if (partnerServiceMapping == null)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(1);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }

            if (partner.PartnerType.PartnerTypeID == PartnerTypeID.Domestic)
            {
                // Proceed
            }
            else
            {
                PeraLinkCoreWcf.PartnerServicePair[] partnerServicePairCollection = serviceClient.GetPartnerServicePairCollection(partnerServiceMapping);

                PeraLinkCoreWcf.PartnerServicePair domesticPartner = Array.Find<PeraLinkCoreWcf.PartnerServicePair>(partnerServicePairCollection
                    , delegate(PeraLinkCoreWcf.PartnerServicePair eachPartnerServicePair)
                    {
                        if (eachPartnerServicePair.PairedPartner == null)
                        {
                            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(2);
                            throw new RDFramework.ClientException(message.Content, message.MessageID);
                        }
                        else
                        {
                            return eachPartnerServicePair.PairedPartner.PartnerType.PartnerTypeID == PartnerTypeID.Domestic;
                        }
                    });

                if (domesticPartner == null)
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(3);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
                else
                {
                    // Proceed
                }
            }
            #endregion

            #region Validate Partner's Access to Telco
            PeraLinkCoreWcf.PartnerServicePair[] telcoPartnerServicePairCollection = serviceClient.GetPartnerServicePairCollection(partnerServiceMapping);

            PeraLinkCoreWcf.PartnerServicePair pairing = Array.Find<PeraLinkCoreWcf.PartnerServicePair>(telcoPartnerServicePairCollection
                    , delegate(PeraLinkCoreWcf.PartnerServicePair eachPartnerServicePair)
                    {
                        if (eachPartnerServicePair.PairedPartner == null)
                        {
                            PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(131);
                            throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                        }
                        else
                        {
                            return eachPartnerServicePair.PairedPartner.PartnerCode == this.Telco.TelcoCode;
                        }
                    });

            if (pairing == null)
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(131);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
            else
            {
                //Proceed
            }

            if (pairing.PairedPartner.Activated)
            { }
            else
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(132);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
            #endregion


            PeraLinkCoreWcf.EMoneyProfile eMoneyProfile = new PeraLinkCoreWcf.EMoneyProfile();
            if (partner.SourceOfFundID == SourceOfFundID.EMoney)
            {
                #region E-Money
                #region Get E-Money Profile
                PeraLinkCoreWcf.GetEMoneyProfileRequest request = new PeraLinkCoreWcf.GetEMoneyProfileRequest();
                request.Partner = partner;

                eMoneyProfile = serviceClient.GetEMoneyProfile(request).EMoneyProfile;

                if (string.IsNullOrWhiteSpace(eMoneyProfile.AccountNumber))
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(2);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }

                if (string.IsNullOrWhiteSpace(eMoneyProfile.ClientNumber))
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(3);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }

                #endregion

                #region Validate Balance
                EMoneyWSClient eMoneyServiceClient = new EMoneyWSClient();
#if CONVERTION_TO_BUSINESS_ACCOUNT
                        decimal balance = eMoneyServiceClient.EMoneyInquiry(eMoneyProfile.AccountNumber).AvailableBalance;
#else
                decimal balance = eMoneyServiceClient.RetrieveAccountDetails(eMoneyProfile.ClientNumber).DcAvailableBalance;
#endif
               
                if ((balance - Convert.ToDecimal(this.ELoadTransaction.Amount)) < 0)
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(6);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
                #endregion

                #endregion

            }
#if ELOAD_COMMISSION_FEE
#else
            #region Commission Fee
            GetCommissionFeeRequest getCommissionFeeRequest = new GetCommissionFeeRequest();
            getCommissionFeeRequest.PartnerCode = this.PartnerCode;
            getCommissionFeeRequest.CurrencyID = 6; //PHP
            getCommissionFeeRequest.ServiceTypeID = ServiceType.ELoad;
            getCommissionFeeRequest.PrincipalAmount = this.ELoadTransaction.Amount;
            getCommissionFeeRequest.ServiceFee = 0;

            GetCommissionFeeResult getCommissionFeeResult = new GetCommissionFeeResult();

            getCommissionFeeResult = getCommissionFeeRequest.Process();
            commissionFee = getCommissionFeeResult.CommissionFee;
            #endregion
#endif



            //Generate E-Load Number
            PeraLinkELoadGatewayWcf.GenerateELoadReferenceNumberRequest generateELoadReferenceNumberRequest = new PeraLinkELoadGatewayWcf.GenerateELoadReferenceNumberRequest();
            PeraLinkELoadGatewayWcf.GenerateELoadReferenceNumberResult generateELoadReferenceNumberResult = new PeraLinkELoadGatewayWcf.GenerateELoadReferenceNumberResult();
            generateELoadReferenceNumberRequest.AgentID = agent.AgentID;
            generateELoadReferenceNumberRequest.TelcoCode = this.Telco.TelcoCode;
            generateELoadReferenceNumberResult = eLoadServiceClient.GenerateELoadReferenceNumber(generateELoadReferenceNumberRequest);
            this.ELoadTransaction.ReferenceNumber = generateELoadReferenceNumberResult.ReferenceNumber;
      

            #region Process E-Load
            PeraLinkELoadGatewayWcf.ProcessLoadTransactionResult processLoadTransactionResult = new PeraLinkELoadGatewayWcf.ProcessLoadTransactionResult();
            PeraLinkELoadGatewayWcf.ProcessLoadTransactionRequest processLoadTransactionRequest = new PeraLinkELoadGatewayWcf.ProcessLoadTransactionRequest();
            try
            {
                processLoadTransactionRequest.ELoadTransaction = new PeraLinkELoadGatewayWcf.ELoadTransaction();
                processLoadTransactionRequest.ELoadTransaction.ReferenceNumber = this.ELoadTransaction.ReferenceNumber;
                processLoadTransactionRequest.ELoadTransaction.Amount = this.ELoadTransaction.Amount;
                processLoadTransactionRequest.ELoadTransaction.BranchCode = this.PartnerCode;
                processLoadTransactionRequest.ELoadTransaction.Destination = this.ELoadTransaction.Destination;
                processLoadTransactionRequest.ELoadTransaction.LoadedByAgentID = agent.AgentID;
                processLoadTransactionRequest.ELoadTransaction.LoadedByUserID = this.UserID;
                processLoadTransactionRequest.ELoadTransaction.LoadedByTerminalID = terminal.TerminalID;
                processLoadTransactionRequest.ELoadTransaction.SessionID = this.ELoadTransaction.SessionID;

                processLoadTransactionRequest.ELoadTransaction.TelcoProduct = new PeraLinkELoadGatewayWcf.TelcoProduct();
                processLoadTransactionRequest.ELoadTransaction.TelcoProduct.MaxAmount = this.TelcoProduct.MaxAmount;
                processLoadTransactionRequest.ELoadTransaction.TelcoProduct.MinAmount = this.TelcoProduct.MinAmount;
                processLoadTransactionRequest.ELoadTransaction.TelcoProduct.ProductCode = this.TelcoProduct.ProductCode;
                processLoadTransactionRequest.ELoadTransaction.TelcoProduct.ProductDescription = this.TelcoProduct.ProductDescription;

                processLoadTransactionRequest.ELoadTransaction.TelcoProduct.Telco = new PeraLinkELoadGatewayWcf.Telco();
                processLoadTransactionRequest.ELoadTransaction.TelcoProduct.Telco.IsActivated = this.Telco.IsActivated;
                processLoadTransactionRequest.ELoadTransaction.TelcoProduct.Telco.TelcoCode = this.Telco.TelcoCode;
                processLoadTransactionRequest.ELoadTransaction.TelcoProduct.Telco.TelcoName = this.Telco.TelcoName;

                processLoadTransactionResult = eLoadServiceClient.ProcessLoadTransaction(processLoadTransactionRequest);
                switch (processLoadTransactionResult.ResultStatus)
                {

                    case ResultStatus.Successful:
                        //do nothing
                        break;
                    case ResultStatus.Failed:
                    case ResultStatus.Error:
                        {
                            throw new RDFramework.ClientException(processLoadTransactionResult.Message, processLoadTransactionResult.MessageID, processLoadTransactionResult.LogID);
                        }
                }

                returnValue.ELoadTransaction = new ELoadTransaction();
                returnValue.ELoadTransaction.Load(processLoadTransactionResult.ELoadTransaction);
                returnValue.Telco = new Telco();
                returnValue.Telco.Load(processLoadTransactionResult.ELoadTransaction.TelcoProduct.Telco);
                returnValue.TelcoProduct = new TelcoProduct();
                returnValue.TelcoProduct.Load(processLoadTransactionResult.ELoadTransaction.TelcoProduct);
            }
            catch (RDFramework.ClientException error)
            {
                if (isEmoney && processLoadTransactionResult != null && processLoadTransactionResult.ForReversal)
                {                    
                    this.ELoadTransaction.LoadedByAgent = this.AgentCode;
                    EMoneyWSClient eMoneyServiceClient = new EMoneyWSClient();
                    EMoneyWS.UpdateEMoneyRequest request = new EMoneyWS.UpdateEMoneyRequest();

                    List<EMoneyWS.UpdateEMoneyRequest> requestList = new List<EMoneyWS.UpdateEMoneyRequest>();

                    EMoney eMoneyLoadRequest = new EMoney();
                    eMoneyLoadRequest.AccountNumber = eMoneyProfile.AccountNumber; //EMONEY ACCOUNT FROM PARTNER's SETTINGS
                    eMoneyLoadRequest.ControlNumber = this.ELoadTransaction.ReferenceNumber;
                    eMoneyLoadRequest.Remarks = "ELoadReversal";
                    eMoneyLoadRequest.PeraLinkAgentCode = this.AgentCode;

                    eMoneyLoadRequest.CashInAmount = this.ELoadTransaction.Amount;
                    eMoneyLoadRequest.CashOutAmount = 0;
                    requestList.Add(eMoneyLoadRequest.Principal());
                    eMoneyLoadRequest.CashInAmount = 0;
                    eMoneyLoadRequest.CashOutAmount = commissionFee;
                    requestList.Add(eMoneyLoadRequest.CommissionFee());

                    EMoneyWS.UpdateEMoneyResult result = eMoneyServiceClient.UpdateEMoney(requestList);

                    switch (result.ResultCode)
                    {
                        case EMoneyWS.ResultCodes.Successful:
                            // do nothing
                            break;
                        case EMoneyWS.ResultCodes.Failed:
                        case EMoneyWS.ResultCodes.Error:
                            throw new RDFramework.ClientException(result.ResultMessage);
                    }
                }

                throw new RDFramework.ClientException(error.Message, error.MessageID);
            }

            #endregion
            

            
            if (isEmoney)
            {
#if ELOAD_COMMISSION_FEE
                #region Commission Fee
                PeraLinkCoreWcf.GetPartnerPairingCommissionFeeRequest pairingfeeRequest = new PeraLinkCoreWcf.GetPartnerPairingCommissionFeeRequest();
                pairingfeeRequest.PartnerPairingCommissionFee = new PeraLinkCoreWcf.PartnerPairingCommissionFee();
                pairingfeeRequest.PartnerPairingCommissionFee.Amount = processLoadTransactionRequest.ELoadTransaction.Amount;
                pairingfeeRequest.PartnerPairingCommissionFee.PartnerServicePair = pairing;

                PeraLinkCoreWcf.GetPartnerPairingCommissionFeeResult pairingfeeResult = new PeraLinkCoreWcf.GetPartnerPairingCommissionFeeResult();
                pairingfeeResult = serviceClient.GetPartnerPairingCommissionFee(pairingfeeRequest);

                if (pairingfeeResult.PartnerPairingCommissionFee.PartnerPairingCommissionFeeID == 0)
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(137);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
                else
                {

                    if (pairingfeeResult.PartnerPairingCommissionFee.FeeType.FeeTypeID == FeeTypeID.Fixed)
                    {
                       commissionFee = pairingfeeResult.PartnerPairingCommissionFee.Fee;
                    }
                    else
                    {
                        commissionFee = processLoadTransactionRequest.ELoadTransaction.Amount * (pairingfeeResult.PartnerPairingCommissionFee.Fee / 100);                
                    }

                    returnValue.CommissionFee = commissionFee;
                }
                #endregion
#else
#endif

                #region Debit E-Money
                EMoneyWSClient eMoneyServiceClient = new EMoneyWSClient();
                EMoneyWS.UpdateEMoneyRequest request = new EMoneyWS.UpdateEMoneyRequest();

                List<EMoneyWS.UpdateEMoneyRequest> requestList = new List<EMoneyWS.UpdateEMoneyRequest>();
                EMoney eMoneyRequest = new EMoney();
                eMoneyRequest.AccountNumber = eMoneyProfile.AccountNumber; //EMONEY ACCOUNT FROM PARTNER's SETTINGS
                eMoneyRequest.ControlNumber = this.ELoadTransaction.ReferenceNumber;
                eMoneyRequest.Remarks = "E-Load";
                eMoneyRequest.PeraLinkAgentCode = this.AgentCode;

                eMoneyRequest.CashInAmount = 0;
                eMoneyRequest.CashOutAmount = this.ELoadTransaction.Amount;

                requestList.Add(eMoneyRequest.Principal());
                eMoneyRequest.CashInAmount = commissionFee;
                eMoneyRequest.CashOutAmount = 0;
                requestList.Add(eMoneyRequest.CommissionFee());
                EMoneyWS.UpdateEMoneyResult result = eMoneyServiceClient.UpdateEMoney(requestList);

                switch (result.ResultCode)
                {
                    case EMoneyWS.ResultCodes.Successful:
                        // do nothing
                        break;
                    case EMoneyWS.ResultCodes.Failed:
                    case EMoneyWS.ResultCodes.Error:
                        {
                            throw new RDFramework.ClientException("Unsuccessful E-Money debiting");
                        }
                }
                #endregion

                #region Insert Total Commission
                AddCommissionTransactionRequest addCommissionTransactionRequest = new AddCommissionTransactionRequest();
                addCommissionTransactionRequest.ISControlNo = this.ELoadTransaction.ReferenceNumber;
                addCommissionTransactionRequest.PartnerID = partner.PartnerID;

                AddCommissionTransactionResult addCommissionTransactionResult = new AddCommissionTransactionResult();
                addCommissionTransactionResult = addCommissionTransactionRequest.Process();
                #endregion
            }
            

            #region Audit Trail
            PeraLinkCoreWcf.AuditTrail auditTrail = new PeraLinkCoreWcf.AuditTrail();
            auditTrail.CreatedByAgent = agent;
            auditTrail.CreatedByUserID = this.UserID;
            auditTrail.DateTimeCreated = DateTime.Now;
            auditTrail.IPAddress = SystemUtility.GetClientIPAddress();

            PeraLinkCoreWcf.ProcessLoadTransactionResult LoadTransactionResult = new PeraLinkCoreWcf.ProcessLoadTransactionResult();
            PeraLinkCoreWcf.ProcessLoadTransactionRequest LoadTransactionRequest = new PeraLinkCoreWcf.ProcessLoadTransactionRequest();
            LoadTransactionRequest.ELoadTransaction = new PeraLinkCoreWcf.ELoadTransaction();
            LoadTransactionRequest.ELoadTransaction.Amount = processLoadTransactionResult.ELoadTransaction.Amount;
            LoadTransactionRequest.ELoadTransaction.BranchCode = SystemSetting.EMoneyBranchCode;
            LoadTransactionRequest.ELoadTransaction.Destination = processLoadTransactionResult.ELoadTransaction.Destination;
            LoadTransactionRequest.ELoadTransaction.LoadedByAgentID = agent.AgentID;
            LoadTransactionRequest.ELoadTransaction.LoadedByUserID = this.UserID;
            LoadTransactionRequest.ELoadTransaction.LoadedByTerminalID = terminal.TerminalID;
            LoadTransactionRequest.ELoadTransaction.SessionID = processLoadTransactionResult.ELoadTransaction.SessionID;
            LoadTransactionRequest.ELoadTransaction.ReferenceNumber = processLoadTransactionResult.ELoadTransaction.ReferenceNumber;
            LoadTransactionRequest.ELoadTransaction.TransactionNumber = processLoadTransactionResult.ELoadTransaction.TransactionNumber;

            PeraLinkELoadGatewayWcf.ELoadTransaction eLoadTransaction = new PeraLinkELoadGatewayWcf.ELoadTransaction();
            eLoadTransaction.ReferenceNumber = processLoadTransactionResult.ELoadTransaction.ReferenceNumber;
            PeraLinkELoadGatewayWcf.ELoadStatus eLoadStatus = new PeraLinkELoadGatewayWcf.ELoadStatus();
            PeraLinkELoadGatewayWcf.GetELoadStatusRequest getELoadStatusRequest = new PeraLinkELoadGatewayWcf.GetELoadStatusRequest();
            PeraLinkELoadGatewayWcf.GetELoadStatusResult getELoadStatusResult = new PeraLinkELoadGatewayWcf.GetELoadStatusResult();
            getELoadStatusRequest.ReferenceNumber = processLoadTransactionResult.ELoadTransaction.ReferenceNumber;
            getELoadStatusResult = eLoadServiceClient.GetELoadStatus(getELoadStatusRequest);
            eLoadStatus = getELoadStatusResult.ELoadStatus;

            LoadTransactionRequest.ELoadTransaction.ELoadStatus = new PeraLinkCoreWcf.ELoadStatus();
            LoadTransactionRequest.ELoadTransaction.ELoadStatus.InternalStatus = eLoadStatus.InternalStatus;
            LoadTransactionRequest.ELoadTransaction.ELoadStatus.InternalStatusCode = eLoadStatus.InternalStatusCode;
            LoadTransactionRequest.ELoadTransaction.ELoadStatus.PartnerResponseCode = eLoadStatus.PartnerResponseCode;
            LoadTransactionRequest.ELoadTransaction.ELoadStatus.PartnerStatus = eLoadStatus.PartnerStatus;
            LoadTransactionRequest.ELoadTransaction.ELoadStatus.PartnerStatusCode = eLoadStatus.PartnerStatusCode;

            LoadTransactionRequest.ELoadTransaction.TelcoProduct = new PeraLinkCoreWcf.TelcoProduct();
            LoadTransactionRequest.ELoadTransaction.TelcoProduct.MaxAmount = processLoadTransactionResult.ELoadTransaction.TelcoProduct.MaxAmount;
            LoadTransactionRequest.ELoadTransaction.TelcoProduct.MinAmount = processLoadTransactionResult.ELoadTransaction.TelcoProduct.MinAmount;
            LoadTransactionRequest.ELoadTransaction.TelcoProduct.ProductCode = processLoadTransactionResult.ELoadTransaction.TelcoProduct.ProductCode;
            LoadTransactionRequest.ELoadTransaction.TelcoProduct.ProductDescription = processLoadTransactionResult.ELoadTransaction.TelcoProduct.ProductDescription;
            LoadTransactionRequest.ELoadTransaction.TelcoProduct.ReqAmount = processLoadTransactionResult.ELoadTransaction.TelcoProduct.ReqAmount;

            LoadTransactionRequest.ELoadTransaction.TelcoProduct.Telco = new PeraLinkCoreWcf.Telco();
            LoadTransactionRequest.ELoadTransaction.TelcoProduct.Telco.IsActivated = processLoadTransactionResult.ELoadTransaction.TelcoProduct.Telco.IsActivated;
            LoadTransactionRequest.ELoadTransaction.TelcoProduct.Telco.TelcoCode = processLoadTransactionResult.ELoadTransaction.TelcoProduct.Telco.TelcoCode;
            LoadTransactionRequest.ELoadTransaction.TelcoProduct.Telco.TelcoName = processLoadTransactionResult.ELoadTransaction.TelcoProduct.Telco.TelcoName;
            LoadTransactionRequest.AuditTrail = auditTrail;

            LoadTransactionResult = serviceClient.ProcessLoadTransaction(LoadTransactionRequest);
            #endregion
        }

        returnValue.ResultStatus = ResultStatus.Successful;

        return returnValue;
    }
    #endregion
}