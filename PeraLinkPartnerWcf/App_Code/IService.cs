#define INSURANCE_MICARE
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService" in both code and config file together.
[ServiceContract]
[XmlSerializerFormat]
public interface IService
{
    [OperationContract]
    GetEMoneyCollectionResult GetEMoneyCollection(GetEMoneyCollectionRequest getEMoneyCollectionRequest);

    [OperationContract]
    GetAmendmentFeeResult GetAmendmentFee(GetAmendmentFeeRequest getAmendmentFeeRequest);

    [OperationContract]
    GetBeneficiaryResult GetBeneficiary(GetBeneficiaryRequest getBeneficiaryRequest);

    [OperationContract]
    GetClientResult GetClient(GetClientRequest getClientRequest);

    [OperationContract]
	ValidateBeneficiaryDetailsResult ValidateBeneficiaryDetails(ValidateBeneficiaryDetailsRequest validateBeneficiaryRequest);

    [OperationContract]
    UpdateBeneficiaryResult UpdateBeneficiary(UpdateBeneficiaryRequest updateBeneficiaryRequest);

    [OperationContract]
    UpdateClientResult UpdateClient(UpdateClientRequest updateClientRequest);

    [OperationContract]
    GetCountryCollectionResult GetCountryCollection(GetCountryCollectionRequest getCountryCollectionRequest);

    [OperationContract]
    GetStateCollectionByCountryResult GetStateCollectionByCountry(GetStateCollectionByCountryRequest getStateCollectionByCountryrRequest);

    [OperationContract]
    GetAgentCollectionResult GetAgentCollection(GetAgentCollectionRequest getAgentCollectionRequest);

	[OperationContract]
    GetInternationalPartnerCollectionResult GetInternationalPartnerCollection(GetInternationalPartnerCollectionRequest getInternationalPartnerCollectionRequest);

    [OperationContract]
    GetDomesticPullPartnerResult GetDomesticPullPartner(GetDomesticPullPartnerRequest getDomesticPullPartnerRequest);

    [OperationContract]
    GetDomesticRemittanceHistoryResult GetDomesticRemittanceHistory(GetDomesticRemittanceHistoryRequest getDomesticRemittanceHistoryRequest);

    [OperationContract]
    GetInternationalRemittanceHistoryResult GetInternationalRemittanceHistory(GetInternationalRemittanceHistoryRequest getInternationalRemittanceHistoryRequest);

    [OperationContract]
    GetSendServiceFeeResult GetSendServiceFee(GetSendServiceFeeRequest getSendServiceFeeRequest);

    [OperationContract]
    GetSendCurrencyCollectionResult GetSendCurrencyCollection(GetSendCurrencyCollectionRequest getSendCurrencyCollectionRequest);

    [OperationContract]
    FindClientResult FindClient(FindClientRequest findClientRequest);

    [OperationContract]
    RefundDomesticRemittanceResult RefundDomesticRemittance(RefundDomesticRemittanceRequest refundDomesticRemittance);

    [OperationContract]
    CancelDomesticRemittanceResult CancelDomesticRemittance(CancelDomesticRemittanceRequest cancelDomesticRemittance);

    [OperationContract]
    AmendDomesticRemittanceResult AmendDomesticRemittance(AmendDomesticRemittanceRequest amendDomesticRemittanceRequest);

    [OperationContract]
    GetBeneficiaryBySenderResult GetBeneficiaryBySender(GetBeneficiaryBySenderRequest getBeneficiaryBySenderRequest);

    [OperationContract]
    FindBeneficiaryResult FindBeneficiary(FindBeneficiaryRequest findBeneficiaryRequest);

    [OperationContract]
    GetIdentificationTypeCollectionResult GetIdentificationTypeCollection(GetIdentificationTypeCollectionRequest getIdentificationTypeCollectionRequest);

    [OperationContract]
    FindDomesticRemittanceResult FindDomesticRemittance(FindDomesticRemittanceRequest findDomesticRemittanceRequest);

    [OperationContract]
    FindInternationalRemittanceResult FindInternationalRemittance(FindInternationalRemittanceRequest findInternationalRemittanceRequest);

    [OperationContract]
    AddBeneficiaryResult AddBeneficiary(AddBeneficiaryRequest addBeneficiaryRequest);

    [OperationContract]
    AddClientResult AddClient(AddClientRequest addClientRequest);

    [OperationContract]
    SendDomesticRemittanceResult SendDomesticRemittance(SendDomesticRemittanceRequest sendDomesticRemittanceRequest);

    [OperationContract]
    PayoutDomesticRemittanceResult PayoutDomesticRemittance(PayoutDomesticRemittanceRequest payoutDomesticRemittanceRequest);

    [OperationContract]
    PayoutInternationalRemittanceResult PayoutInternationalRemittance(PayoutInternationalRemittanceRequest payoutInternationalRemittanceRequest);

	[OperationContract]
	GetAgentResult GetAgent(GetAgentRequest getAgentequest);

	[OperationContract]
	GetEMoneyAccountDetailsResult GetEMoneyAccountDetail(GetEMoneyAccountDetailsRequest getEMoneyAccountDetailsRequest);

	[OperationContract]
	GetClientSourceOfFundCollectionResult GetClientSourceOfFundCollection(GetClientSourceOfFundCollectionRequest getClientSourceOfFundRequest);

	[OperationContract]
	RemitToAccountResult RemitToAccount(RemitToAccountRequest remitToAccountRequest);

	[OperationContract]
	GetBankCollectionResult GetBankCollection(GetBankCollectionRequest getBankCollectionRequest);

	[OperationContract]
	GetRemitToAccountServiceFeeResult GetRemitToAccountServiceFee(GetRemitToAccountServiceFeeRequest getRemitToAccountServiceFeeRequest);

	[OperationContract]
	GetRemitToAccountTransactionHistoryResult GetRemitToAccountTransactionHistory(GetRemitToAccountTransactionHistoryRequest getRemitToAccountTransactionHistoryRequest);

	[OperationContract]
	VerifyRemitToAccountTransactionResult VerifyRemitToAccountTransaction(VerifyRemitToAccountTransactionRequest verifyRemitToAccountTransactionRequest);

	[OperationContract]
	GetTerminalTransactionSummaryResult GetTerminalTransactionSummary(GetTerminalTransactionSummaryRequest getTerminalTransactionSummaryRequest);

	[OperationContract]
	GetTerminalReadingReportResult GetTerminalReadingReport(GetTerminalReadingReportRequest getTerminalReadingRequest);

	[OperationContract]
	GetSummaryReportResult GetSummaryReport(GetSummaryReportRequest getSummartReportRequest);

    [OperationContract]
    GetLastDomesticRemittanceControlNumberResult GetLastDomesticRemittanceControlNumber(GetLastDomesticRemittanceControlNumberRequest getLastDomesticRemittanceControlNumberRequest);

    [OperationContract]
    GetLastProcessedTransactionResult GetLastProcessedTransaction(GetLastProcessedTransactionRequest getLastProcessedTransactionRequest);

    [OperationContract]
    GetSendingTransactionReportResult GetSendingTransactionReport(GetSendingTransactionReportRequest getSendingTransactionReportRequest);

    [OperationContract]
    GetPayoutTransactionReportResult GetPayoutTransactionReport(GetPayoutTransactionReportRequest getPayoutTransactionReportRequest);

    [OperationContract]
    GetCancelTransactionReportResult GetCancelTransactionReport(GetCancelTransactionReportRequest getCancelTransactionReportRequest);

    [OperationContract]
    GetRefundTransactionReportResult GetRefundTransactionReport(GetRefundTransactionReportRequest getRefundTransactionReportRequest);

    [OperationContract]
    GetAmendTransactionReportResult GetAmendTransactionReport(GetAmendTransactionReportRequest getAmendTransactionReportRequest);
   
	[OperationContract]
    GetTelcoCollectionResult GetTelcoCollection(GetTelcoCollectionRequest getTelcoCollectionRequest);

    [OperationContract]
    GetTelcoProductCollectionResult GetTelcoProductCollection(GetTelcoProductCollectionRequest getTelcoProductCollectionRequest);

    [OperationContract]
    ProcessLoadTransactionResult ProcessLoadTransaction(ProcessLoadTransactionRequest processLoadTransactionRequest);

    [OperationContract]
    CheckELoadTransactionResult CheckELoadTransaction(CheckELoadTransactionRequest checkELoadTransactionRequest);

    [OperationContract]
    GetELoadTransactionHistoryResult GetELoadTransactionHistory(GetELoadTransactionHistoryRequest getELoadTransactionHistoryRequest);

    [OperationContract]
    GetMobilePrefixCollectionResult GetMobilePrefixCollection(GetMobilePrefixCollectionRequest getMobilePrefixCollectionRequest);

    [OperationContract]
    PayBillResult PayBill(PayBillRequest payBillRequest);

    [OperationContract]
    FindClientByCellphoneNumberResult FindClientByCellphoneNumber(FindClientByCellphoneNumberRequest findClientByCellphoneNumberRequest);

    [OperationContract]
    UpdateBeneficiaryMobileResult UpdateBeneficiaryMobile(UpdateBeneficiaryMobileRequest updateBeneficiaryMobileRequest);

    [OperationContract]
    PayoutDomesticRemittanceMobileResult PayoutDomesticRemittanceMobile(PayoutDomesticRemittanceMobileRequest payoutDomesticRemittanceMobileRequest);

    [OperationContract]
    FindClientByNameResult FindClientByName(FindClientByNameRequest findClientByNameRequest);

    [OperationContract]
    FindClientByClientNumberResult FindClientByClientNumber(FindClientByClientNumberRequest findClientByNameRequest);

    [OperationContract]
    GetCommissionFeeResult GetCommissionFee(GetCommissionFeeRequest getCommissionFeeRequest);

    [OperationContract]
    GetBillsPaymentServiceFeeResult GetBillsPaymentServiceFee(GetBillsPaymentServiceFeeRequest getBillsPaymentFeeRequest);

    [OperationContract]
    GetBillsPaymentTransactionHistoryResult GetBillsPaymentTransactionHistory(GetBillsPaymentTransactionHistoryRequest getBillsPaymentTransactionHistoryRequest);

#if INSURANCE_MICARE
    [OperationContract]
    ProcessInsuranceResult ProcessInsurance(ProcessInsuranceRequest processInsuranceRequest);

    [OperationContract]
    GetInsuranceTransactionHistoryResult GetInsuranceTransactionHistory(GetInsuranceTransactionHistoryRequest getInsuranceTransactionHistoryRequest);
#else
#endif
}