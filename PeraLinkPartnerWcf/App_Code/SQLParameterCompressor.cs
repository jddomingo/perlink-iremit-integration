using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;


public class SQLParameterCompressor
{
    string[] Parm;
    string[] ParmValue;
    int[] ParmDBType;

    public string[] ParameterName
    {
        get { return Parm; }
    }

    public string[] ParameterValue
    {
        get { return ParmValue; }
    }

    public int[] ParameterDataType
    {
        get { return ParmDBType; }
    }

    public void CompressParameter(SqlCommand sqlcomm)
    {
        Parm = new string[sqlcomm.Parameters.Count];
        ParmValue = new string[sqlcomm.Parameters.Count];
        ParmDBType = new int[sqlcomm.Parameters.Count];

        for (int xcount = 0; xcount <= (sqlcomm.Parameters.Count - 1); xcount++)
        {

            Parm[xcount] = sqlcomm.Parameters[xcount].ParameterName.ToString();

            if (sqlcomm.Parameters[xcount].SqlDbType == SqlDbType.VarChar)
            {
                ParmDBType[xcount] = 1;
                ParmValue[xcount] = sqlcomm.Parameters[xcount].Value.ToString();
            }
            else
            {
                if (sqlcomm.Parameters[xcount].SqlDbType == SqlDbType.Bit)
                {
                    ParmDBType[xcount] = 2;
                    ParmValue[xcount] = sqlcomm.Parameters[xcount].Value.ToString();
                }
                else
                {
                    if (sqlcomm.Parameters[xcount].SqlDbType == SqlDbType.DateTime)
                    {
                        ParmDBType[xcount] = 3;
                        ParmValue[xcount] = sqlcomm.Parameters[xcount].Value.ToString();
                    }
                    else
                    {
                        if (sqlcomm.Parameters[xcount].SqlDbType == SqlDbType.Decimal)
                        {
                            ParmDBType[xcount] = 5;
                            ParmValue[xcount] = sqlcomm.Parameters[xcount].Value.ToString();
                        }
                        else
                        {
                            if (sqlcomm.Parameters[xcount].SqlDbType == SqlDbType.Int)
                            {
                                ParmDBType[xcount] = 6;
                                ParmValue[xcount] = sqlcomm.Parameters[xcount].Value.ToString();
                            }
                            else
                            {
                                if (sqlcomm.Parameters[xcount].SqlDbType == SqlDbType.BigInt)
                                {
                                    ParmDBType[xcount] = 7;
                                    ParmValue[xcount] = sqlcomm.Parameters[xcount].Value.ToString();
                                }
                                else
                                {
                                    if (sqlcomm.Parameters[xcount].SqlDbType == SqlDbType.Xml)
                                    {
                                        ParmDBType[xcount] = 4;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}