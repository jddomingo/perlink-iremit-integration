using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for GetPartnerComputedCommissionFeeRequest
/// </summary>
public class GetPartnerComputedCommissionFeeRequest:ServiceResult
{
    #region Constructor
    public GetPartnerComputedCommissionFeeRequest()
	{
		//

		//
	}
    #endregion

    #region Fields/Properties
    private string _computedCommissionFee;

    [DataMember]
    public string ComputedCommissionFee
    {
        get { return _computedCommissionFee; }
        set { _computedCommissionFee = value; }
    }
    #endregion


    #region Internal Methods
    //internal  Process()
    //{
    //    GetDomesticRemittanceHistoryResult returnValue = new GetDomesticRemittanceHistoryResult();

    //    // Important: This function validates the request credential
    //    this.AuthenticateRequest();

    //    using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
    //    {
    //        #region Validate Partner Record
    //        PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
    //        partner.PartnerCode = this.PartnerCode;
    //        partner = serviceClient.FindPartnerCode(partner);

    //        if (partner.PartnerID == 0)
    //        {
    //            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
    //            throw new RDFramework.ClientException(message.Content, message.MessageID);
    //        }
    //        else { }

    //        if (partner.Token == this.Token) { }
    //        else
    //        {
    //            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
    //            throw new RDFramework.ClientException(message.Content, message.MessageID);
    //        }

    //        if (partner.Activated) { }
    //        else
    //        {
    //            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
    //            throw new RDFramework.ClientException(message.Content, message.MessageID);
    //        }
    //        #endregion

    //        #region Validate Agent Record
    //        PeraLinkCoreWcf.Agent agent = new PeraLinkCoreWcf.Agent();
    //        if (this.AgentCode != null)
    //        {
    //            agent.AgentCode = this.AgentCode;
    //            agent.Partner = partner;
    //            agent = serviceClient.FindAgentCodeOfPartner(agent);

    //            if (agent.AgentID == 0)
    //            {
    //                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
    //                throw new RDFramework.ClientException(message.Content, message.MessageID);
    //            }
    //            else
    //            {
    //                // Proceed
    //            }

    //            if (agent.Partner.PartnerID == partner.PartnerID)
    //            { }
    //            else
    //            {
    //                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(43);
    //                throw new RDFramework.ClientException(message.Content, message.MessageID);
    //            }

    //            if (agent.Activated) { }
    //            else
    //            {
    //                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(46);
    //                throw new RDFramework.ClientException(message.Content, message.MessageID);
    //            }
    //        }
    //        else
    //        {
    //            agent.Partner = partner;
    //        }
    //        #endregion

    //        PeraLinkCoreWcf.GetDomesticRemittanceHistoryRequest historyRequest = new PeraLinkCoreWcf.GetDomesticRemittanceHistoryRequest();
    //        historyRequest.Agent = agent;
    //        historyRequest.UserID = this.UserID;
    //        historyRequest.EndDate = DateTime.Today.AddDays(1);
    //        historyRequest.EndRowNumber = (this.PageNumber * _pageSize);
    //        historyRequest.StartRowNumber = (historyRequest.EndRowNumber - _pageSize) + 1;
    //        historyRequest.StartDate = DateTime.Today;

    //        PeraLinkCoreWcf.GetDomesticRemittanceHistoryResult historyResult = serviceClient.GetDomesticRemittanceHistory(historyRequest);
    //        returnValue.DomesticRemittanceCollection = new DomesticRemittanceCollection();
    //        returnValue.DomesticRemittanceCollection.AddList(historyResult.DomesticRemittanceCollection);
    //        returnValue.TotalPageCount = ((historyResult.TotalRecordCount % _pageSize) == 0) ? (historyResult.TotalRecordCount / _pageSize) : (historyResult.TotalRecordCount / _pageSize) + 1;
    //    }

    //    returnValue.ResultStatus = ResultStatus.Successful;

    //    return returnValue;
    //}
    #endregion

}