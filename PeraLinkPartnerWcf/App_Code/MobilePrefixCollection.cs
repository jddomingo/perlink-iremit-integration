using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MobilePrefixCollection
/// </summary>
public class MobilePrefixCollection: List<MobilePrefix>
{
    #region Constructor
    public MobilePrefixCollection()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion

    #region Public Methods
    public void AddList(PeraLinkELoadGatewayWcf.MobilePrefix[] mobilePrefixCollection)
    {
        Array.ForEach<PeraLinkELoadGatewayWcf.MobilePrefix>(mobilePrefixCollection
            , delegate(PeraLinkELoadGatewayWcf.MobilePrefix eachMobilePrefix)
            {
                MobilePrefix mappedMobilePrefix = new MobilePrefix();
                mappedMobilePrefix.Load(eachMobilePrefix);
                this.Add(mappedMobilePrefix);
            });
    }
    #endregion
}