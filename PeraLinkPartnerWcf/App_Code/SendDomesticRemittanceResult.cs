using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
/// <summary>
/// Summary description for SendDomesticRemittanceResult
/// </summary>
[DataContract]
public class SendDomesticRemittanceResult: BaseResult
{
    #region Constructor
    public SendDomesticRemittanceResult()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private string _controlNumber;

    [DataMember]
    public string ControlNumber
    {
        get { return _controlNumber; }
        set { _controlNumber = value; }
    }
    private decimal _serviceFee;

    [DataMember]
    public decimal ServiceFee
    {
        get { return _serviceFee; }
        set { _serviceFee = value; }
    }
    #endregion
}