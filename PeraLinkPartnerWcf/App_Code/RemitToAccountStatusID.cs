using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for RemitToAccountStatusID
/// </summary>
public class RemitToAccountStatusID
{
    #region Constructor
	public RemitToAccountStatusID()
	{ }
    #endregion

    #region Fields/Properties
    public const int InProcess = 1;
    public const int Successful = 2;
	public const int SuccessfulWithIssue = 3;
    public const int Failed = 4;
    public const int MultipleTransactionFound = 5;
	public const int Error = 6;
	public const int SuccessfulDueToTimeout = 7;
    #endregion
}