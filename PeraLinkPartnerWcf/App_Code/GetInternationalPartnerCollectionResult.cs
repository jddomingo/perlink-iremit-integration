using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;


[DataContract]
public class GetInternationalPartnerCollectionResult: BaseResult
{
	#region Constructor
    public GetInternationalPartnerCollectionResult()
	{
		//

		//
	}
	#endregion

	#region Fields/Properties
	private PartnerCollection _partnerCollection;
	[DataMember]
	public PartnerCollection PartnerCollection
	{
		get { return _partnerCollection; }
		set { _partnerCollection = value; }
	}
	#endregion
}