using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for CheckELoadTransactionResult
/// </summary>
public class CheckELoadTransactionResult: BaseResult
{
    #region Constructor
    public CheckELoadTransactionResult()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion

    #region Fields/Properties
    private ELoadTransaction _eLoadTransaction;
    [DataMember]
    public ELoadTransaction ELoadTransaction
    {
        get { return _eLoadTransaction; }
        set { _eLoadTransaction = value; }
    }

    private ELoadStatus _eLoadStatus;
    [DataMember]
    public ELoadStatus ELoadStatus
    {
        get { return _eLoadStatus; }
        set { _eLoadStatus = value; }
    }

    private Telco _telco;
    [DataMember]
    public Telco Telco
    {
        get { return _telco; }
        set { _telco = value; }
    }

    private TelcoProduct _telcoProduct;
    [DataMember]
    public TelcoProduct TelcoProduct
    {
        get { return _telcoProduct; }
        set { _telcoProduct = value; }
    }

    private string _otherDetail;
    [DataMember]
    public string OtherDetail
    {
        get { return _otherDetail; }
        set { _otherDetail = value; }
    }
    private Agent _agent;
    [DataMember]
    public Agent Agent
    {
        get { return _agent; }
        set { _agent = value; }
    }
    private Terminal _terminal;
    [DataMember]
    public Terminal Terminal
    {
        get { return _terminal; }
        set { _terminal = value; }
    }
    #endregion
}