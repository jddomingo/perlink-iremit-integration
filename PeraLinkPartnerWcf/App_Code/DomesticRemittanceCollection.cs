using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class DomesticRemittanceCollection: List<DomesticRemittance>
{
    #region Constructor
    public DomesticRemittanceCollection()
	{
		//

		//
    }
    #endregion

    #region Public Methods
    public void AddList(PeraLinkCoreWcf.DomesticRemittance[] domesticRemittanceCollection)
    {
        Array.ForEach<PeraLinkCoreWcf.DomesticRemittance>(domesticRemittanceCollection
            , delegate(PeraLinkCoreWcf.DomesticRemittance eachDomesticRemittance)
            {
                DomesticRemittance mappedRemittance = new DomesticRemittance();
                mappedRemittance.Load(eachDomesticRemittance);
                this.Add(mappedRemittance);
            });
    }
    #endregion
}