using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class RemitToAccountTransactionCollection: List<RemitToAccountTransaction>
{
	#region Constructor
	public RemitToAccountTransactionCollection()
	{ }
	#endregion

	#region Public Methods
	public void AddList(PeraLinkCoreWcf.RemitToAccountTransaction[] remitToAccountTransactionCollection)
	{
		Array.ForEach<PeraLinkCoreWcf.RemitToAccountTransaction>(remitToAccountTransactionCollection
			, delegate(PeraLinkCoreWcf.RemitToAccountTransaction eachRemitToAccountTransaction)
			{
				RemitToAccountTransaction mappedRemitToAccountTransaction = new RemitToAccountTransaction();
				mappedRemitToAccountTransaction.Load(eachRemitToAccountTransaction);
				this.Add(mappedRemitToAccountTransaction);
			});
	}
	#endregion
}