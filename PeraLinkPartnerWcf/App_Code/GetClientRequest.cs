using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetClientRequest: BaseRequest
{
    #region Constructor
    public GetClientRequest()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private Int64 _clientID;

    public Int64 ClientID
    {
        get { return _clientID; }
        set { _clientID = value; }
    }

    private string _clientNumber;

    public string ClientNumber
    {
        get { return _clientNumber; }
        set { _clientNumber = value; }
    }
    #endregion

    #region Internal Methods
    internal GetClientResult Process()
    {
        GetClientResult returnValue = new GetClientResult();

        // Important: This function validates the request credential
        this.AuthenticateRequest();

        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
            PeraLinkCoreWcf.Client clientRecord = new PeraLinkCoreWcf.Client();
            clientRecord.ClientID = this.ClientID;
            clientRecord = serviceClient.GetClient(clientRecord);

            if (clientRecord.ClientID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(24);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                if (clientRecord.ClientNumber == this.ClientNumber)
                {
                    returnValue.Client = new Client();
                    returnValue.Client.Load(clientRecord);
                }
                else
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(37);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
            }
        }

        returnValue.ResultStatus = ResultStatus.Successful;

        return returnValue;
    }
    #endregion
}