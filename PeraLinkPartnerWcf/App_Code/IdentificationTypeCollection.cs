using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for IdentificationTypeCollection
/// </summary>
public class IdentificationTypeCollection: List<IdentificationType>
{
    #region Constructor
    public IdentificationTypeCollection()
	{
		//

		//
    }
    #endregion

    #region Public Methods
    public void AddList(PeraLinkCoreWcf.IdentificationType[] identificationTypeCollection)
    {
        Array.ForEach<PeraLinkCoreWcf.IdentificationType>(identificationTypeCollection
            , delegate(PeraLinkCoreWcf.IdentificationType eachIdentificationType)
            {
                IdentificationType mappedIdentificationType = new IdentificationType();
                mappedIdentificationType.Load(eachIdentificationType);
                this.Add(mappedIdentificationType);
            });
    }
    #endregion
}