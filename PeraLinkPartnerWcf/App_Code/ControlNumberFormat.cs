using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class ControlNumberFormat
{
    #region Constructor
    public ControlNumberFormat()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private string _partnerCode;

    public string PartnerCode
    {
        get { return _partnerCode; }
        set { _partnerCode = value; }
    }

    private string _prefix;

    public string Prefix
    {
        get { return _prefix; }
        set { _prefix = value; }
    }

    private int _length;

    public int Length
    {
        get { return _length; }
        set { _length = value; }
    }
    #endregion
}