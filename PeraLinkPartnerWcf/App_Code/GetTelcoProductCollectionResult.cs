using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for GetTelcoProductCollectionResult
/// </summary>
/// 
[DataContract]
public class GetTelcoProductCollectionResult: ServiceResult
{
    #region Constructor
    public GetTelcoProductCollectionResult()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion

    #region Fields/Properties
    private TelcoProductCollection _telcoProductCollection;
    [DataMember]
    public TelcoProductCollection TelcoProductCollection
    {
        get { return _telcoProductCollection; }
        set { _telcoProductCollection = value; }
    }
    #endregion
}