using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetCancelTransactionReportResult : ServiceResult
{
    #region Constructor
    public GetCancelTransactionReportResult()
    { }
    #endregion

    #region Fields/Properties
    private TransactionReportCollection _transactionReportCollection;
    [DataMember]
    public TransactionReportCollection TransactionReportCollection
    {
        get { return _transactionReportCollection; }
        set { _transactionReportCollection = value; }
    }

    private int _totalPageCount;
    [DataMember]
    public int TotalPageCount
    {
        get { return _totalPageCount; }
        set { _totalPageCount = value; }
    }
    #endregion
}