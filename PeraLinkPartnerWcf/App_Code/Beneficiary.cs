using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Beneficiary
/// </summary>
public class Beneficiary
{
	#region Constructor
	public Beneficiary()
	{
		//

		//
	}
	#endregion

	#region Fields/Properties
	private Int64 _beneficiaryID;

	public Int64 BeneficiaryID
	{
		get { return _beneficiaryID; }
		set { _beneficiaryID = value; }
	}

	private string _firstName;

	public string FirstName
	{
		get { return _firstName; }
		set { _firstName = value; }
	}

	private string _middleName;

	public string MiddleName
	{
		get { return _middleName; }
		set { _middleName = value; }
	}

	private string _lastName;

	public string LastName
	{
		get
		{
			return RDFramework.Utility.Conversion.Trim(_lastName);
		}
		set { _lastName = value; }
	}

	private DateTime? _birthDate;

	public DateTime? BirthDate
	{
		get { return _birthDate; }
		set { _birthDate = value; }
	}

	private Country _cellphoneCountry;

	public Country CellphoneCountry
	{
		get { return _cellphoneCountry; }
		set { _cellphoneCountry = value; }
	}

	private string _cellphoneNumber;

	public string CellphoneNumber
	{
		get { return _cellphoneNumber; }
		set { _cellphoneNumber = value; }
	}

	private Country _telephoneCountry;

	public Country TelephoneCountry
	{
		get { return _telephoneCountry; }
		set { _telephoneCountry = value; }
	}

	private string _telephoneAreaCode;

	public string TelephoneAreaCode
	{
		get { return _telephoneAreaCode; }
		set { _telephoneAreaCode = value; }
	}

	private string _telephoneNumber;

	public string TelephoneNumber
	{
		get { return _telephoneNumber; }
		set { _telephoneNumber = value; }
	}

	private Country _countryAddress;

	public Country CountryAddress
	{
		get { return _countryAddress; }
		set { _countryAddress = value; }
	}

	public Country BirthCountry { get; set; }

	private string _provinceAddress;

	public string ProvinceAddress
	{
		get { return _provinceAddress; }
		set { _provinceAddress = value; }
	}

	private string _address;

	public string Address
	{
		get { return _address; }
		set { _address = value; }
	}

	private string _zipCode;

	public string ZipCode
	{
		get { return _zipCode; }
		set { _zipCode = value; }
	}

	private string _occupation;

	public string Occupation
	{
		get { return _occupation; }
		set { _occupation = value; }
	}

	private int? _stateIDAddress;

	public int? StateIDAddress
	{
		get { return _stateIDAddress; }
		set { _stateIDAddress = value; }
	}

	private string _tin;

	public string TIN
	{
		get { return _tin; }
		set { _tin = value; }
	}
	#endregion

	#region Public Methods
	public void Load(PeraLinkCoreWcf.Beneficiary beneficiary)
	{
		if (beneficiary == null)
		{ }
		else
		{
			this.BeneficiaryID = beneficiary.BeneficiaryID;
			this.FirstName = beneficiary.FirstName;
			this.LastName = beneficiary.LastName;
			this.MiddleName = beneficiary.MiddleName;
			this.BirthDate = beneficiary.BirthDate;
			this.CellphoneCountry = new Country();
			this.CellphoneCountry.Load(beneficiary.CellphoneCountry);
			this.CellphoneNumber = beneficiary.CellphoneNumber;
			this.TelephoneCountry = new Country();
			this.TelephoneCountry.Load(beneficiary.TelephoneCountry);
			this.TelephoneAreaCode = beneficiary.TelephoneAreaCode;
			this.TelephoneNumber = beneficiary.TelephoneNumber;
			this.CountryAddress = new Country();
			this.CountryAddress.Load(beneficiary.CountryAddress);
			this.BirthCountry = new Country();
			this.BirthCountry.Load(beneficiary.BirthCountry);
			this.ProvinceAddress = beneficiary.ProvinceAddress;
			this.Address = beneficiary.Address;
			this.ZipCode = beneficiary.ZipCode;
			this.Occupation = beneficiary.Occupation;
			this.StateIDAddress = beneficiary.StateIDAddress;
			this.TIN = beneficiary.TIN;
		}
	}
	#endregion
}