#define CONVERTION_TO_BUSINESS_ACCOUNT
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using AppCryptor;
/// <summary>
/// Summary description for PayoutDomesticRemittanceRequest
/// </summary>
[DataContract]
public class PayoutDomesticRemittanceRequest : BaseRequest
{
    #region Constructor
    public PayoutDomesticRemittanceRequest()
    {
        //

        //
    }
    #endregion

    #region Fields/Properties
    private string _controlNumber;
    [DataMember]
    public string ControlNumber
    {
        get { return _controlNumber; }
        set { _controlNumber = value; }
    }

    private int _identificationTypeID;
    [DataMember]
    public int IdentificationTypeID
    {
        get { return _identificationTypeID; }
        set { _identificationTypeID = value; }
    }

    private string _identificationNumber;
    [DataMember]
    public string IdentificationNumber
    {
        get { return _identificationNumber; }
        set { _identificationNumber = value; }
    }

    private string _partnerCode;
    [DataMember]
    public string PartnerCode
    {
        get { return _partnerCode; }
        set { _partnerCode = value; }
    }

    private string _token;
    [DataMember]
    public string Token
    {
        get { return _token; }
        set { _token = value; }
    }

    private string _agentCode;
    [DataMember]
    public string AgentCode
    {
        get { return _agentCode; }
        set { _agentCode = value; }
    }

    private string _userID;
    [DataMember]
    public string UserID
    {
        get { return _userID; }
        set { _userID = value; }
    }

    private string _pullTransactionPartnerCode;
    [DataMember]
    public string PullTransactionPartnerCode
    {
        get { return _pullTransactionPartnerCode; }
        set { _pullTransactionPartnerCode = value; }
    }

    private string _pin;
    [DataMember]
    public string Pin
    {
        get { return _pin; }
        set { _pin = value; }
    }

    private string _terminalCode;

    public string TerminalCode
    {
        get { return _terminalCode; }
        set { _terminalCode = value; }
    }

    private long _beneficiaryID;

    public long BeneficiaryID
    {
        get { return _beneficiaryID; }
        set { _beneficiaryID = value; }
    }
    #endregion



    #region Internal Methods
    internal PayoutDomesticRemittanceResult Process()
    {
        PayoutDomesticRemittanceResult returnValue = new PayoutDomesticRemittanceResult();
        decimal principalAmount = 0;
        decimal commissionFee = 0;
        bool isEMoney = false;

        // Important: This function validates the request credential
        this.AuthenticateRequest();

        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
            #region Validate Partner Record
            PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
            partner.PartnerCode = this.PartnerCode;
            partner = serviceClient.FindPartnerCode(partner);

            if (partner.PartnerID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else { }

            if (partner.Token == this.Token) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(41);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (partner.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (partner.SourceOfFundID == SystemSetting.EMoneySourceOfFundID)
            {
                isEMoney = true;
            }
            #endregion

            #region Validate Agent Record
            PeraLinkCoreWcf.Agent agent = new PeraLinkCoreWcf.Agent();
            agent.AgentCode = this.AgentCode;
            agent.Partner = partner;
            agent = serviceClient.FindAgentCodeOfPartner(agent);

            if (agent.AgentID == 0)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(8);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }

            if (agent.Partner.PartnerID == partner.PartnerID)
            { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(43);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            if (agent.Activated) { }
            else
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(46);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }

            #endregion

            #region Validate Terminal Record
            PeraLinkCoreWcf.Terminal terminal = new PeraLinkCoreWcf.Terminal();
            int terminalCount = serviceClient.GetAgentTerminalCollection(agent).Length;

            if (terminalCount > 0)
            {
                terminal.TerminalCode = this.TerminalCode;
                terminal.Agent = agent;
                terminal.Agent.Partner = partner;
                terminal = serviceClient.FindTerminal(terminal);

                if (terminal.TerminalID == 0)
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(104);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
                else
                {
                    // Proceed
                }

                if (terminal.Agent.AgentID == agent.AgentID)
                { }
                else
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(105);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }

                if (terminal.Activated) { }
                else
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(106);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
            }
            else
            {
                //do nothing
            }
            #endregion

            #region Validate Partner's Access To Payout Domestic Remittance Service
            PeraLinkCoreWcf.PartnerServiceMapping[] partnerServiceMappingCollection = serviceClient.GetPartnerServiceMappingCollection(partner);

            PeraLinkCoreWcf.PartnerServiceMapping partnerServiceMapping = Array.Find<PeraLinkCoreWcf.PartnerServiceMapping>(partnerServiceMappingCollection
                , delegate(PeraLinkCoreWcf.PartnerServiceMapping eachPartnerServiceMapping)
                {
                    return eachPartnerServiceMapping.ServiceType.ServiceTypeID == ServiceTypeID.PayoutRemittance;
                });


            if (partnerServiceMapping == null)
            {
                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(18);
                throw new RDFramework.ClientException(message.Content, message.MessageID);
            }
            else
            {
                // Proceed
            }
            #endregion

            #region Validate Payout Details
            if (RDFramework.Utility.Validation.IsEmptyString(this.IdentificationNumber))
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(98);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
            else { }

            if (this.IdentificationTypeID <= 0)
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(99);
                throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
            }
            else { }

            if (this.IdentificationNumber.Length > 50)
            {
                PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(100);
                throw new RDFramework.ClientException(string.Format(message.Description, "50"), message.SystemMessageID);
            }
            #endregion

            #region Get E-Money Profile
            PeraLinkCoreWcf.EMoneyProfile eMoneyProfile = new PeraLinkCoreWcf.EMoneyProfile();

            if (isEMoney)
            {
                PeraLinkCoreWcf.GetEMoneyProfileRequest profileRequest = new PeraLinkCoreWcf.GetEMoneyProfileRequest();
                profileRequest.Partner = partner;

                eMoneyProfile = serviceClient.GetEMoneyProfile(profileRequest).EMoneyProfile;

                if (string.IsNullOrWhiteSpace(eMoneyProfile.AccountNumber))
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(2);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }

                if (string.IsNullOrWhiteSpace(eMoneyProfile.ClientNumber))
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(3);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
            }
            #endregion

            #region Validate PIN for E-Money
            //do nothing
            #endregion

            if (string.IsNullOrWhiteSpace(this.PullTransactionPartnerCode))
            {

                #region Find Domestic Remittance
                FindDomesticRemittanceRequest findDomesticRemittanceRequest = new FindDomesticRemittanceRequest();
                findDomesticRemittanceRequest.AgentCode = this.AgentCode;
                findDomesticRemittanceRequest.ControlNumber = this.ControlNumber;
                findDomesticRemittanceRequest.PartnerCode = this.PartnerCode;
                findDomesticRemittanceRequest.TerminalCode = this.TerminalCode;
                findDomesticRemittanceRequest.PullTransactionPartnerCode = this.PullTransactionPartnerCode;
                findDomesticRemittanceRequest.ServiceCredential = this.ServiceCredential;
                findDomesticRemittanceRequest.Token = this.Token;
                findDomesticRemittanceRequest.UserID = this.UserID;
                FindDomesticRemittanceResult findDomesticRemittanceResult = findDomesticRemittanceRequest.Process();

                PeraLinkCoreWcf.DomesticRemittance domesticRemittance = new PeraLinkCoreWcf.DomesticRemittance();
                domesticRemittance.ControlNumber = findDomesticRemittanceResult.ControlNumber;
                domesticRemittance.DomesticRemittanceID = findDomesticRemittanceResult.DomesticRemittanceID;
                domesticRemittance.SentByAgent = new PeraLinkCoreWcf.Agent();
                domesticRemittance.SentByAgent.AgentID = findDomesticRemittanceResult.SentByAgentID;
                domesticRemittance.SentByAgent.AgentCode = findDomesticRemittanceResult.SentByAgentCode;
                domesticRemittance.SentByAgent.Partner = new PeraLinkCoreWcf.Partner();
                domesticRemittance.SentByAgent.Partner.PartnerID = findDomesticRemittanceResult.SentByPartnerID;
                domesticRemittance.SentByAgent.Partner.PartnerCode = findDomesticRemittanceResult.SentByPartnerCode;
                domesticRemittance.SendCurrency = new PeraLinkCoreWcf.Currency();
                domesticRemittance.SendCurrency.CurrencyID = findDomesticRemittanceResult.SendCurrencyID;
                domesticRemittance.SendCurrency.Code = findDomesticRemittanceResult.SendCurrencyCode;
                domesticRemittance.Beneficiary = new PeraLinkCoreWcf.Beneficiary();

                domesticRemittance.Beneficiary.BeneficiaryID = (findDomesticRemittanceResult.BeneficiaryID == 0 ? this.BeneficiaryID : findDomesticRemittanceResult.BeneficiaryID);
                domesticRemittance.PrincipalAmount = findDomesticRemittanceResult.PrincipalAmount;
                domesticRemittance.ServiceFee = findDomesticRemittanceResult.ServiceFee;
                domesticRemittance.DateTimeSent = findDomesticRemittanceResult.DateTimeSent;
                domesticRemittance.SenderClient = new PeraLinkCoreWcf.Client();
                domesticRemittance.SenderClient.ClientID = findDomesticRemittanceResult.SenderClientID;
                domesticRemittance.SentByUserID = findDomesticRemittanceResult.SentByUserID;

                principalAmount = domesticRemittance.PrincipalAmount;
                #endregion

                #region Validate Partner's access To Transaction
                if (domesticRemittance.SentByAgent.Partner.PartnerID == agent.Partner.PartnerID)
                {
                    if (domesticRemittance.SentByUserID == this.UserID)
                    {
                        PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(44);
                        throw new RDFramework.ClientException(message.Content, message.MessageID);
                    }
                    else { }

                    if (domesticRemittance.SentByAgent.AgentID == agent.AgentID)
                    {
                        PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(16);
                        throw new RDFramework.ClientException(message.Content, message.MessageID);
                    }
                    else
                    {
                        // Proceed
                    }
                }
                else
                {
                    PeraLinkCoreWcf.PartnerServicePair[] partnerServicePairCollection = serviceClient.GetPartnerServicePairCollection(partnerServiceMapping);

                    PeraLinkCoreWcf.PartnerServicePair pairing = Array.Find<PeraLinkCoreWcf.PartnerServicePair>(partnerServicePairCollection
                        , delegate(PeraLinkCoreWcf.PartnerServicePair eachPartnerServicePair)
                        {
                            if (eachPartnerServicePair.PairedPartner == null)
                            {
                                PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(2);
                                throw new RDFramework.ClientException(message.Content, message.MessageID);
                            }
                            else
                            {
                                return eachPartnerServicePair.PairedPartner.PartnerID == domesticRemittance.SentByAgent.Partner.PartnerID;
                            }
                        });

                    if (pairing == null)
                    {
                        PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(14);
                        throw new RDFramework.ClientException(message.Content, message.MessageID);
                    }
                    else
                    {
                        // Proceed
                    }
                }
                #endregion

                #region Validate transaction status
                if (findDomesticRemittanceResult.RemittanceStatusID == RemittanceStatusID.Outstanding
                    || findDomesticRemittanceResult.RemittanceStatusID == RemittanceStatusID.Amended)
                {
                    //Proceed
                }
                else
                {
                    PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(82);
                    throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                }
                #endregion

                #region ValidateCapping
                if (agent.CapAmount > 0)
                {
                    PeraLinkCoreWcf.GetAgentTotalTransactionAmountRequest getAgentTotalTransactionAmountRequest = new PeraLinkCoreWcf.GetAgentTotalTransactionAmountRequest();
                    getAgentTotalTransactionAmountRequest.Agent = agent;
                    getAgentTotalTransactionAmountRequest.StartDate = DateTime.Today;
                    getAgentTotalTransactionAmountRequest.EndDate = DateTime.Today.AddDays(1);

                    PeraLinkCoreWcf.GetAgentTotalTransactionAmountResult getAgentTotalTransactionAmountResult = new PeraLinkCoreWcf.GetAgentTotalTransactionAmountResult();
                    getAgentTotalTransactionAmountResult = serviceClient.GetAgentTotalTransactionAmount(getAgentTotalTransactionAmountRequest);

                    decimal totalAmount = domesticRemittance.PrincipalAmount + getAgentTotalTransactionAmountResult.TotalTransactionAmount;

                    if (agent.CapAmount < totalAmount)
                    {
                        decimal remainingAmount = agent.CapAmount - getAgentTotalTransactionAmountResult.TotalTransactionAmount;
                        remainingAmount = (remainingAmount < 0) ? 0 : remainingAmount;
                        PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(102);
                        throw new RDFramework.ClientException(string.Format(message.Description, remainingAmount), message.SystemMessageID);
                    }
                }
                #endregion

                #region Validate Transaction Currency
                PeraLinkCoreWcf.PartnerServiceCurrency[] partnerServiceCurrencyCollection = serviceClient.GetPartnerServiceCurrencyCollection(partnerServiceMapping);

                PeraLinkCoreWcf.PartnerServiceCurrency transactionCurrency = Array.Find<PeraLinkCoreWcf.PartnerServiceCurrency>(partnerServiceCurrencyCollection
                    , delegate(PeraLinkCoreWcf.PartnerServiceCurrency eachPartnerServiceCurrency)
                    {
                        return eachPartnerServiceCurrency.Currency.CurrencyID == domesticRemittance.SendCurrency.CurrencyID;
                    });

                if (transactionCurrency == null)
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(26);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
                else
                {
                    // Proceed
                }
                #endregion

                #region Payout

                PeraLinkCoreWcf.PayoutDomesticRemittanceRequest request = new PeraLinkCoreWcf.PayoutDomesticRemittanceRequest();

                request.DomesticBeneficiaryIdentification = new PeraLinkCoreWcf.DomesticBeneficiaryIdentification();
                request.DomesticBeneficiaryIdentification.IdentificationNumber = this.IdentificationNumber;
                request.DomesticBeneficiaryIdentification.IdentificationType = new PeraLinkCoreWcf.IdentificationType();
                request.DomesticBeneficiaryIdentification.IdentificationType.IdentificationTypeID = this.IdentificationTypeID;
                request.DomesticBeneficiaryIdentification.CreatedBy = this.UserID;
                request.DomesticBeneficiaryIdentification.DateTimeCreated = DateTime.Now;

                domesticRemittance.DomesticPayoutDetail = new PeraLinkCoreWcf.DomesticPayoutDetail();
                domesticRemittance.DomesticPayoutDetail.PayoutAmount = domesticRemittance.PrincipalAmount;
                domesticRemittance.DomesticPayoutDetail.PayoutCurrency = domesticRemittance.SendCurrency;

                domesticRemittance.UpdatedByAgent = agent;
                domesticRemittance.UpdatedByTerminal = terminal;
                domesticRemittance.UpdatedByUserID = this.UserID;
                domesticRemittance.DateTimeUpdated = DateTime.Now;
                request.DomesticRemittance = domesticRemittance;

                PeraLinkCoreWcf.AuditTrail auditTrail = new PeraLinkCoreWcf.AuditTrail();
                auditTrail.CreatedByAgent = agent;
                auditTrail.CreatedByUserID = this.UserID;
                auditTrail.DateTimeCreated = DateTime.Now;
                auditTrail.IPAddress = SystemUtility.GetClientIPAddress();
                request.AuditTrail = auditTrail;

                Partner matchedPartner = SystemUtility.FindPartnerWithControlNumberFormat(domesticRemittance.ControlNumber);

                if (matchedPartner == null)
                {
                    #region eMoney Encashment
                    if (domesticRemittance.SentByAgent.Partner.PartnerCode == SystemSetting.EMoneyEncashmentPartnerCode)
                    {
                        #region Get E-Money Profile
                        PeraLinkCoreWcf.Partner encashingPartner = new PeraLinkCoreWcf.Partner();
                        encashingPartner.PartnerCode = domesticRemittance.SentByAgent.AgentCode;
                        encashingPartner = serviceClient.FindPartnerCode(encashingPartner);

                        if (encashingPartner.PartnerID == 0)
                        {
                            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
                            throw new RDFramework.ClientException(message.Content, message.MessageID);
                        }
                        else { }

                        PeraLinkCoreWcf.GetEMoneyProfileRequest profileRequest = new PeraLinkCoreWcf.GetEMoneyProfileRequest();
                        profileRequest.Partner = encashingPartner;

                        eMoneyProfile = serviceClient.GetEMoneyProfile(profileRequest).EMoneyProfile;

                        if (string.IsNullOrWhiteSpace(eMoneyProfile.AccountNumber))
                        {
                            PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(2);
                            throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                        }

                        if (string.IsNullOrWhiteSpace(eMoneyProfile.ClientNumber))
                        {
                            PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(3);
                            throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                        }
                        #endregion

                        #region Validate Balance
                        EMoneyWSClient eMoneyServiceClient = new EMoneyWSClient();
#if CONVERTION_TO_BUSINESS_ACCOUNT
                        decimal balance = eMoneyServiceClient.EMoneyInquiry(eMoneyProfile.AccountNumber).AvailableBalance;
#else
                        decimal balance = eMoneyServiceClient.RetrieveAccountDetails(eMoneyProfile.ClientNumber).DcAvailableBalance;
#endif

                        if ((balance - domesticRemittance.PrincipalAmount) < 0)
                        {
                            PeraLinkCoreWcf.SystemMessage message = SystemResource.GetSystemMessage(133);
                            throw new RDFramework.ClientException(message.Description, message.SystemMessageID);
                        }
                        #endregion

                        #region Credit to E-Money
                        List<EMoneyWS.UpdateEMoneyRequest> requestList = new List<EMoneyWS.UpdateEMoneyRequest>();

                        EMoney eMoneyRequest = new EMoney();
                        eMoneyRequest.AccountNumber = eMoneyProfile.AccountNumber; //EMONEY ACCOUNT FROM PARTNER's SETTINGS
                        eMoneyRequest.ControlNumber = domesticRemittance.ControlNumber;
                        eMoneyRequest.Remarks = "Encashment";
                        eMoneyRequest.PeraLinkAgentCode = this.AgentCode;

                        eMoneyRequest.CashInAmount = 0;
                        eMoneyRequest.CashOutAmount = domesticRemittance.PrincipalAmount;
                        requestList.Add(eMoneyRequest.Principal());

                        EMoneyWS.UpdateEMoneyResult result = eMoneyServiceClient.UpdateEMoney(requestList);

                        switch (result.ResultCode)
                        {
                            case EMoneyWS.ResultCodes.Successful:
                                // do nothing
                                break;
                            case EMoneyWS.ResultCodes.Failed:
                            case EMoneyWS.ResultCodes.Error:
                                throw new RDFramework.ClientException(result.ResultMessage);
                        }
                        #endregion
                    }
                    #endregion

                    serviceClient.PayoutDomesticRemittance(request);
                }
                else
                {
                    if (matchedPartner.PartnerCode == SystemSetting.CebuanaLhuillierPartnerCode)
                    {
                        #region Payout in Cebuana Lhuillier
                        CebuanaLhuillierWS.PayoutDomesticRemittanceRequest payoutDomesticRemittanceRequest = new CebuanaLhuillierWS.PayoutDomesticRemittanceRequest();
                        payoutDomesticRemittanceRequest.BeneficiaryNumber = findDomesticRemittanceRequest.ExternalPartnerBeneficiaryNumber; // Provide the Cebuana Lhuillier Bene Number
                        payoutDomesticRemittanceRequest.ControlNumber = findDomesticRemittanceRequest.ExternalPartnerRemittanceID.ToString(); // Provide the Cebuana Lhuillier domestic remittance ID
                        payoutDomesticRemittanceRequest.ID1Details = this.IdentificationNumber;

                        PeraLinkCoreWcf.IdentificationType[] identificationTypeCollection = serviceClient.GetIdentificationTypeCollection();
                        PeraLinkCoreWcf.IdentificationType submittedID = Array.Find<PeraLinkCoreWcf.IdentificationType>(identificationTypeCollection
                            , delegate(PeraLinkCoreWcf.IdentificationType eachIdentificationType)
                            {
                                return eachIdentificationType.IdentificationTypeID == this.IdentificationTypeID;
                            });
                        payoutDomesticRemittanceRequest.ID1Presented = submittedID.Description;
                        payoutDomesticRemittanceRequest.PayoutBranchCode = this.AgentCode;
                        payoutDomesticRemittanceRequest.SenderCustomerNumber = findDomesticRemittanceRequest.ExternalPartnerClientNumber; // Provide the Cebuana Lhuillier Sender Number
                        payoutDomesticRemittanceRequest.UserID = this.UserID;

                        using (CebuanaLhuillierWSClient cebuanaLhuillierWSClient = new CebuanaLhuillierWSClient())
                        {
                            cebuanaLhuillierWSClient.PayoutDomesticRemittance(payoutDomesticRemittanceRequest);
                        }
                        #endregion

                        #region Create a local copy of remittance
                        PeraLinkCoreWcf.SendDomesticRemittanceRequest sendDomesticRemittanceRequest = new PeraLinkCoreWcf.SendDomesticRemittanceRequest();
                        sendDomesticRemittanceRequest.DomesticRemittance = domesticRemittance;
                        sendDomesticRemittanceRequest.UseAssignedControlNumber = true;
                        domesticRemittance = serviceClient.SendDomesticRemittance(sendDomesticRemittanceRequest);
                        #endregion

                        #region Payout in PeraLink
                        if (this.IdentificationTypeID == 23)
                            request.DomesticBeneficiaryIdentification.IdentificationType.IdentificationTypeID = 22;

                        request.DomesticRemittance = domesticRemittance;
                        serviceClient.PayoutDomesticRemittance(request);
                        #endregion
                    }
                    else if (matchedPartner.PartnerCode == SystemSetting.OkRemitPartnerCode)
                    {
                        #region Payout in OK Remit
                        OkRemitWcfClient okRemitWcfClient = new OkRemitWcfClient();
                        okRemitWcfClient.PayoutRemittance(this.ControlNumber);

                        #endregion

                        #region Create a local copy of remittance
                        PeraLinkCoreWcf.SendDomesticRemittanceRequest sendDomesticRemittanceRequest = new PeraLinkCoreWcf.SendDomesticRemittanceRequest();
                        sendDomesticRemittanceRequest.DomesticRemittance = domesticRemittance;
                        sendDomesticRemittanceRequest.UseAssignedControlNumber = true;
                        domesticRemittance = serviceClient.SendDomesticRemittance(sendDomesticRemittanceRequest);
                        #endregion

                        #region Payout in PeraLink
                        request.DomesticRemittance = domesticRemittance;
                        serviceClient.PayoutDomesticRemittance(request);
                        #endregion
                    }

                    else if (matchedPartner.PartnerCode == SystemSetting.LbcPartnerCode)
                    {
                        #region Payout in LBC
                        LbcWcfClient lbcWcfClient = new LbcWcfClient();

                        PeraLinkLbcWcf.PayoutRemittanceResult payoutRemittanceResult = new PeraLinkLbcWcf.PayoutRemittanceResult();
                        payoutRemittanceResult = lbcWcfClient.PayoutRemittance(this.ControlNumber, this.IdentificationTypeID, this.IdentificationNumber);
                        #endregion

                        #region Create a local copy of remittance
                        PeraLinkCoreWcf.SendDomesticRemittanceRequest sendDomesticRemittanceRequest = new PeraLinkCoreWcf.SendDomesticRemittanceRequest();
                        sendDomesticRemittanceRequest.DomesticRemittance = domesticRemittance;
                        sendDomesticRemittanceRequest.UseAssignedControlNumber = true;

                        try
                        {
                            domesticRemittance = serviceClient.SendDomesticRemittance(sendDomesticRemittanceRequest);
                        }
                        catch
                        {
                            RDFramework.Utility.EventLog.SaveError(string.Format("LBC: {0}|{1}", this.ControlNumber, payoutRemittanceResult.Key));
                            throw;
                        }
                        #endregion

                        #region Payout in PeraLink
                        request.DomesticRemittance = domesticRemittance;

                        try
                        {
                            serviceClient.PayoutDomesticRemittance(request);
                        }
                        catch
                        {
                            RDFramework.Utility.EventLog.SaveError(string.Format("LBC: {0}|{1}", this.ControlNumber, payoutRemittanceResult.Key));
                            throw;
                        }
                        #endregion
                    }
                    else if (matchedPartner.PartnerCode == SystemSetting.UsscPartnerCode)
                    {
                        #region Log USSC Reference Number in Audit Trail
                        string refNum = string.Format("{0}CEB{1}", DateTime.Now.ToString("yyyyMMdd"), DateTime.Now.Ticks.ToString().PadLeft(20, '0'));

                        PeraLinkCoreWcf.AuditTrail refNumAuditTrail = new PeraLinkCoreWcf.AuditTrail();
                        refNumAuditTrail.CreatedByAgent = agent;
                        refNumAuditTrail.CreatedByUserID = this.UserID;
                        refNumAuditTrail.DateTimeCreated = DateTime.Now;
                        refNumAuditTrail.IPAddress = SystemUtility.GetClientIPAddress();
                        refNumAuditTrail.AuditAction = new PeraLinkCoreWcf.AuditAction();
                        refNumAuditTrail.AuditAction.AuditActionID = AuditActionID.PayoutDomesticRemittance;
                        refNumAuditTrail.AuditDetails = string.Format("{0}|{1}", this.ControlNumber, refNum);
                        serviceClient.AddAuditTrail(refNumAuditTrail);
                        #endregion

                        #region Payout in USSC
                        UsscWcfClient usscWcfClient = new UsscWcfClient();
                        PeraLinkUsscWcf.PayoutRemittanceResult payoutRemittanceResult = new PeraLinkUsscWcf.PayoutRemittanceResult();
                        payoutRemittanceResult = usscWcfClient.PayoutRemittance(this.ControlNumber, this.IdentificationTypeID, this.IdentificationNumber, refNum);
                        #endregion

                        #region Create a local copy of remittance
                        PeraLinkCoreWcf.SendDomesticRemittanceRequest sendDomesticRemittanceRequest = new PeraLinkCoreWcf.SendDomesticRemittanceRequest();
                        sendDomesticRemittanceRequest.DomesticRemittance = domesticRemittance;
                        sendDomesticRemittanceRequest.UseAssignedControlNumber = true;
                        domesticRemittance = serviceClient.SendDomesticRemittance(sendDomesticRemittanceRequest);
                        #endregion

                        #region Payout in PeraLink
                        request.DomesticRemittance = domesticRemittance;
                        serviceClient.PayoutDomesticRemittance(request);
                        #endregion
                    }
                    else
                    {
                        PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(39);

                        throw new RDFramework.ClientException(message.Content, message.MessageID);
                    }
                }
                #endregion




                if (isEMoney)
                {
                    #region Commission Fee
                    GetCommissionFeeRequest getCommissionFeeRequest = new GetCommissionFeeRequest();
                    getCommissionFeeRequest.PartnerCode = this.PartnerCode;
                    getCommissionFeeRequest.CurrencyID = domesticRemittance.SendCurrency.CurrencyID;
                    getCommissionFeeRequest.ServiceTypeID = ServiceType.PayoutRemittance;
                    getCommissionFeeRequest.PrincipalAmount = principalAmount;
                    getCommissionFeeRequest.ServiceFee = domesticRemittance.ServiceFee;


                    GetCommissionFeeResult getCommissionFeeResult = new GetCommissionFeeResult();

                    getCommissionFeeResult = getCommissionFeeRequest.Process();
                    commissionFee = getCommissionFeeResult.CommissionFee;
                    #endregion

                    #region Credit to E-Money
                    EMoneyWSClient eMoneyServiceClient = new EMoneyWSClient();

                    List<EMoneyWS.UpdateEMoneyRequest> requestList = new List<EMoneyWS.UpdateEMoneyRequest>();

                    EMoney eMoneyRequest = new EMoney();
                    eMoneyRequest.AccountNumber = eMoneyProfile.AccountNumber; //EMONEY ACCOUNT FROM PARTNER's SETTINGS
                    eMoneyRequest.ControlNumber = domesticRemittance.ControlNumber;
                    eMoneyRequest.Remarks = "Payout";
                    eMoneyRequest.PeraLinkAgentCode = this.AgentCode;

                    eMoneyRequest.CashInAmount = domesticRemittance.PrincipalAmount;
                    eMoneyRequest.CashOutAmount = 0;
                    requestList.Add(eMoneyRequest.Principal());
                    eMoneyRequest.CashInAmount = commissionFee;
                    eMoneyRequest.CashOutAmount = 0;
                    requestList.Add(eMoneyRequest.CommissionFee());
                    EMoneyWS.UpdateEMoneyResult result = eMoneyServiceClient.UpdateEMoney(requestList);

                    switch (result.ResultCode)
                    {
                        case EMoneyWS.ResultCodes.Successful:
                            // do nothing
                            break;
                        case EMoneyWS.ResultCodes.Failed:
                        case EMoneyWS.ResultCodes.Error:
                            throw new RDFramework.ClientException(result.ResultMessage);
                    }

                    #endregion

                    #region Insert Total Commission
                    AddCommissionTransactionRequest addCommissionTransactionRequest = new AddCommissionTransactionRequest();
                    addCommissionTransactionRequest.ISControlNo = domesticRemittance.ControlNumber;
                    addCommissionTransactionRequest.PartnerID = partner.PartnerID;

                    AddCommissionTransactionResult addCommissionTransactionResult = new AddCommissionTransactionResult();
                    addCommissionTransactionResult = addCommissionTransactionRequest.Process();
                    #endregion
                }



            }
            else
            {
                #region Validate Pull Partner

                PeraLinkCoreWcf.Partner pullPartner = new PeraLinkCoreWcf.Partner();
                pullPartner.PartnerCode = this.PullTransactionPartnerCode;
                pullPartner = serviceClient.FindPartnerCode(partner);

                if (pullPartner.PartnerID == 0)
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
                else
                {
                    // Proceed
                }
                #endregion

                #region Validate Partner's access To Transaction
                PeraLinkCoreWcf.PartnerServicePair[] partnerServicePairCollection = serviceClient.GetPartnerServicePairCollection(partnerServiceMapping);

                PeraLinkCoreWcf.PartnerServicePair pairing = Array.Find<PeraLinkCoreWcf.PartnerServicePair>(partnerServicePairCollection
                    , delegate(PeraLinkCoreWcf.PartnerServicePair eachPartnerServicePair)
                    {
                        if (eachPartnerServicePair.PairedPartner == null)
                        {
                            PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(2);
                            throw new RDFramework.ClientException(message.Content, message.MessageID);
                        }
                        else
                        {
                            return eachPartnerServicePair.PairedPartner.PartnerID == pullPartner.PartnerID;
                        }
                    });

                if (pairing == null)
                {
                    PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(14);
                    throw new RDFramework.ClientException(message.Content, message.MessageID);
                }
                else
                {
                    // Proceed
                }
                #endregion

                #region Direct transaction search
                PeraLinkCoreWcf.Message searchMessage = SystemResource.GetMessageWithListID(20);
                throw new RDFramework.ClientException(searchMessage.Content, searchMessage.MessageID);
                #endregion
            }
        }



        returnValue.ResultStatus = ResultStatus.Successful;

        return returnValue;
    }
    #endregion

}