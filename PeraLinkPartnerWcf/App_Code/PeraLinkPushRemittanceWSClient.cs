using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PeraLinkPushRemittanceWSClient
/// </summary>
public class PeraLinkPushRemittanceWSClient: IDisposable
{
    #region Constructor
    public PeraLinkPushRemittanceWSClient()
	{
        _serviceSoapClient = new PeraLinkPushRemittanceWS.PushRemittanceServiceSoapClient();
	}
    #endregion

    #region Fields/Properties
    PeraLinkPushRemittanceWS.PushRemittanceServiceSoapClient _serviceSoapClient;
    #endregion

    #region Public Methods
    public PeraLinkPushRemittanceWS.TransResult NewPayoutTransaction(PeraLinkPushRemittanceWS.PushRemittanceData PushRemData)
    {
        return _serviceSoapClient.NewPayoutTransaction(PushRemData);
    }
    #endregion

    void IDisposable.Dispose()
    {
        bool isClosed = false;

        try
        {
            if (this._serviceSoapClient.State != System.ServiceModel.CommunicationState.Faulted)
            {
                this._serviceSoapClient.Close();
                isClosed = true;
            }
            else
            {
                // Proceed with Abort in finally
            }
        }
        finally
        {
            if (!isClosed)
            {
                this._serviceSoapClient.Abort();
            }
            else
            {
                // Do nothing since state is already closed
            }
        }
    }
}