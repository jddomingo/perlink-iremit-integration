using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetSendCurrencyCollectionResult: BaseResult
{
    #region Constructor
    public GetSendCurrencyCollectionResult()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private CurrencyCollection _currencyCollection;
    [DataMember]
    public CurrencyCollection CurrencyCollection
    {
        get { return _currencyCollection; }
        set { _currencyCollection = value; }
    }
    #endregion
}