using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
/// <summary>
/// Summary description for MoneyGramAgentDetail
/// </summary>
public class MoneyGramAgentDetail
{
	#region Constructor
    public MoneyGramAgentDetail()
	{ }
	#endregion

	#region Fields/Properties
	private string _agentID;
	public string AgentID
	{
		get { return _agentID; }
		set { _agentID = value; }
	}

	private string _agentSequence;
	public string AgentSequence
	{
		get { return _agentSequence; }
		set { _agentSequence = value; }
	}

	private string _apiVersion;
	public string ApiVersion
	{
		get { return _apiVersion; }
		set { _apiVersion = value; }
	}

	private string _clientSoftwareVersion;
	public string ClientSoftwareVersion
	{
		get { return _clientSoftwareVersion; }
		set { _clientSoftwareVersion = value; }
	}

	private string _token;
	public string Token
	{
		get { return _token; }
		set { _token = value; }
	}
	#endregion

	#region Methods
	
	#endregion
}