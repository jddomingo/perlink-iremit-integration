
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class GetCommissionFeeResult: ServiceResult
{
	#region Constructor
	public GetCommissionFeeResult()
	{ }
	#endregion

	#region Fields/Properties
	private decimal _commissionFee;
	[DataMember]
	public decimal CommissionFee
	{
		get { return _commissionFee; }
		set { _commissionFee = value; }
	}
	#endregion
}