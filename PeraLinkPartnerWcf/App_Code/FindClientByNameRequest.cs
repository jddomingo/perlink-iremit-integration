using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class FindClientByNameRequest: BaseRequest
{
    #region Constructor
    public FindClientByNameRequest()
	{
		//

		//
    }
    #endregion

    #region Fields/Properties
    private string _firstName;
    [DataMember]
    public string FirstName
    {
        get { return _firstName; }
        set { _firstName = value; }
    }

    private string _lastName;
    [DataMember]
    public string LastName
    {
        get { return _lastName; }
        set { _lastName = value; }
    }

    private DateTime? _birthDate;
    [DataMember]
    public DateTime? BirthDate
    {
        get { return _birthDate; }
        set { _birthDate = value; }
    }

    private string _clientNumber;
    [DataMember]
    public string ClientNumber
    {
        get { return _clientNumber; }
        set { _clientNumber = value; }
    }

	private string _partnerCode;
	[DataMember]
	public string PartnerCode
	{
		get { return _partnerCode; }
		set { _partnerCode = value; }
	}

    private string _senderCellphoneNumber;
    [DataMember]
    public string SenderCellphoneNumber
    {
        get { return _senderCellphoneNumber; }
        set { _senderCellphoneNumber = value; }
    }
    #endregion

    #region Internal Methods
    internal FindClientByNameResult Process()
    {
        FindClientByNameResult returnValue = new FindClientByNameResult();

        // Important: This function validates the request credential
        this.AuthenticateRequest();

        PeraLinkCoreWcf.Client searchClient = new PeraLinkCoreWcf.Client();
        searchClient.BirthDate = this.BirthDate;
        searchClient.ClientNumber = this.ClientNumber;
        searchClient.FirstName = this.FirstName;
        searchClient.LastName = this.LastName;
        searchClient.CellphoneNumber = this.SenderCellphoneNumber;

        using (PeraLinkCoreWcfClient serviceClient = new PeraLinkCoreWcfClient())
        {
			#region Validate Partner Record
			PeraLinkCoreWcf.Partner partner = new PeraLinkCoreWcf.Partner();
			partner.PartnerCode = this.PartnerCode;
			partner = serviceClient.FindPartnerCode(partner);

			if (partner.PartnerID == 0)
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(30);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			else { }

			if (partner.Activated) { }
			else
			{
				PeraLinkCoreWcf.Message message = SystemResource.GetMessageWithListID(45);
				throw new RDFramework.ClientException(message.Content, message.MessageID);
			}
			#endregion

            returnValue.ClientCollection = new ClientCollection();
            returnValue.ClientCollection.AddList(serviceClient.FindClient(searchClient, partner.PartnerID));
        }

        returnValue.ResultStatus = ResultStatus.Successful;

        return returnValue;
    }
    #endregion
}