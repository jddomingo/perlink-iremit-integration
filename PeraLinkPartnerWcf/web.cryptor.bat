@ECHO OFF
:Start
cd C:\Windows\Microsoft.NET\Framework64\v4.0.30319
set dir=%~dp0%
set dir=%dir:~0,-1%

set /p action=Type encrypt or decrypt: 

IF "%action%"=="encrypt" (GOTO Encrypt)
IF "%action%"=="decrypt" (GOTO Decrypt)
GOTO Start

:Encrypt
aspnet_regiis.exe -pef "appSettings" "%dir%" -prov "DataProtectionConfigurationProvider"
aspnet_regiis.exe -pef "connectionStrings" "%dir%" -prov "DataProtectionConfigurationProvider"
GOTO Start

:Decrypt
aspnet_regiis.exe -pdf "appSettings" "%dir%"
aspnet_regiis.exe -pdf "connectionStrings" "%dir%"
GOTO Start